import Dashboard from './modules/home/Dashboard';
//import GoogleMaps from 'views/maps/GoogleMaps.jsx';
//import Grid from 'views/components/Grid.jsx';
import Location from './modules/devices/Location';
import Login from './modules/login/Login';
import ManagementTable from './modules/devices/ManagementTable';
import AdminInventory from './modules/devices/AdminInventory'
//import Notifications from 'views/components/Notifications.jsx';
//import Panels from 'views/components/Panels.jsx';
//import Pricing from 'views/pages/Pricing.jsx';
import AddMember from 'views/pages/AddMember.jsx';
import AdminTeam from './modules/team/AdminTeam'
import UserDetail from './modules/users/UserDetail';
import UsersTable from './modules/users/UsersTable';
import PaymentTable from './modules/payment/PaymentTable';
import RideTable from './modules/rides/RideTable';
import AdminOrders from './modules/orders/ordersAdmin'
import EditOrder from './modules/orders/EditOrder'
//import ValidationForms from 'views/forms/ValidationForms.jsx';
import Wizard from 'views/forms/Wizard.jsx';
import VerificationCode from './modules/auth/VerificationCode';
import ResetPassword from './modules/auth/ResetPassword';
import ChangePassword from './modules/auth/ChangePassword';
import AddInventory from './modules/devices/AddInventory'

import CouponsAdmin from './modules/coupons/CouponsAdmin'
import CouponsCreate from './modules/coupons/CouponsCreate'
import Clean from './modules/Clean'
import PaymentDetail from './modules/payment/PaymentDetail';
import EditInventory from './modules/devices/EditInventory';

import SubOrgAdmin from './modules/suborganization/SubOrgAdmin'
import SubOrgCreate from './modules/suborganization/SubOrgCreate'
import AdminOwner from './modules/owner/AdminOwner';
import EditOwner from './modules/owner/EditOwner';
import AddOwner from './modules/owner/AddOwner';
import SeeTeam from './modules/team/SeeTeam'

const routes = [
  {
    id_menu:1,
    collapse: false,
    name: 'Dashboard',    
    icon: 'tim-icons icon-chart-pie-36',
    path: '/Dashboard',    
    rtlName: 'لوحة القيادة',        
    mini: 'UD',
    component: Dashboard,
    layout: '/admin',
    link:true
  },//HOME  

  {
    id_menu:2,
    collapse: false,
    name: 'MY TEAM',
    layout: '/admin',
    path: '/admin-team',
    rtlName: 'صفحات',
    icon: 'tim-icons icon-molecule-40',        
    children:true,
    link:true,
    views: [      
      {
        id_submenu: 2,
        path: '/admin-team',
        name: 'Admin Team',
        parent: 'TEAMS',
        rtlName: 'عالتسعير',
        mini: 'AT',
        rtlMini: 'ع',
        component: AdminTeam,
        layout: '/admin',
        link:true
      },
      {
        id_submenu: 3,
        path: '/add-member',
        name: 'Add Member',
        parent: 'TEAMS',
        rtlName: 'صودعم رتل',
        mini: 'AM',
        rtlMini: 'صو',
        component: AddMember,
        layout: '/admin'
      }
    ]
  },

  {
    id_menu:3,
    collapse: false,
    name: 'Operators',
    rtlName: 'صفحات',
    icon: 'tim-icons icon_suborg',
    state: 'suborgCollapse',
    layout: '/admin',
    path: '/admin-operator',
    children:true,
    link:true,
    views: [      
      {
        id_submenu: 4,
        path: '/admin-operator',
        parent: 'Operators',
        name: 'Admin Operators',
        rtlName: 'عالتسعير',
        mini: 'AS',
        rtlMini: 'ع',
        component: SubOrgAdmin,
        layout: '/admin',
        link:true
        
        
      },
      {
        id_submenu: 5,
        path: '/add-suborg',
        parent: 'Operators',
        name: 'Add Operators',
        rtlName: 'صودعم رتل',
        mini: 'SN',
        rtlMini: 'صو',
        component: SubOrgCreate,
        layout: '/admin'
      }
    ]
  },//SUBORG

  {
    id_menu:4,
    collapse: false,
    name: 'OWNERS',
    rtlName: 'صفحات',
    icon: 'tim-icons icon_owner',
    state: 'ownersCollapse',
    path: '/admin-owner',
    layout: '/admin',
    children:true,
    link:true,
    views: [      
      {
        id_submenu: 6,
        path: '/admin-owner',
        parent: 'OWNERS',
        name: 'Admin Owners',
        mini: 'AO',
        component: AdminOwner,
        layout: '/admin',
        link:true
      },
      {
        id_submenu: 7,
        path: '/add-owner',
        parent: 'OWNERS',
        name: 'Add Owner',
        mini: 'CO',
        component: AddOwner,
        layout: '/admin'
      }
    ]
  },//OWNERS

  {
    id_menu:5,
    collapse: false,    
    path: '/users-table',
    layout: '/admin',
    children:false,
    name: 'RIDERS/RENTERS',
    icon: 'tim-icons icon_user_rounded',
    component: UsersTable,
    link:true
  },//USERS

  {
    id_menu:6,
    collapse: true,
    name: 'MY FLEET',
    rtlName: 'المكونات',
    icon: 'tim-icons icon_scooter',
    state: 'scooterCollapse',
    link:true,
    views: [
      {
        id_submenu: 8,
        path: '/management-table',
        name: 'Control Device',
        rtlName: 'وصفت',
        parent: 'MY FLEET',
        mini: 'MT',
        rtlMini: 'ب',
        component: ManagementTable,
        layout: '/admin',
        link:true
      },
      {
        id_submenu: 9,
        path: '/admin-inventory',
        name: 'Inventory',
        rtlName: 'نظام الشبكة',
        parent: 'MY FLEET',
        mini: 'AI',
        rtlMini: 'زو',
        component: AdminInventory,
        layout: '/admin',
        link:true
      },
      {
        path: '/add-inventory',
        id_submenu: 10,
        name: 'Add Inventory',
        rtlName: 'لوحات',
        parent: 'MY FLEET',
        mini: 'I',
        rtlMini: 'ع',
        component: AddInventory,
        layout: '/admin',        
      },
      {
        id_submenu: 11,
        path: '/location',
        name: 'Operations',
        parent: 'MY FLEET',
        rtlName: 'الحلو تنبيه',
        mini: 'LT',
        rtlMini: 'ومن',
        component: Location,
        layout: '/admin',
        link:true
      },
      {
        id_submenu: 12,
        path: '/settings',
        name: 'Settings',
        parent: 'SCOOTERS',
        rtlName: 'إخطارات',
        mini: 'ST',
        rtlMini: 'ن',
        component: Clean,
        layot: '/admin',
        link:true
      }
    ]
  },//SCOOTERS

  {
    id_menu:8,
    collapse: false,    
    path: '/rides',
    layout: '/admin',
    children:false,
    name: 'RIDES & USAGE',
    rtlName: 'الجداول',
    icon: 'tim-icons icon_worldwide',
    state: 'ridesCollapse',
    id_submenu: 13,
    component: RideTable,
    link:true
  },//RIDES

  {
    id_menu:9,    
    collapse: false,    
    path: '/payment',
    layout: '/admin',
    children:false,
    name: 'PAYMENTS',
    rtlName: 'خرائط',
    icon: 'tim-icons icon_wallet',
    id_submenu: 14,
    component: PaymentTable,
    link:true
  },//BILLING

  {
    id_menu:10,
    collapse: true,
    name: 'SETTINGS',
    rtlName: 'خرائط',
    icon: 'tim-icons icon_feature',
    state: 'settingsCollapse',
    views: [
      {
        id_submenu: 15,
        path: '/google-maps',
        name: 'Customize',
        parent: 'SETTINGS',
        rtlName: 'خرائط جوجل',
        mini: 'GM',
        rtlMini: 'زم',
        component: Clean,
        layout: '/admin'
      },
      {
        id_submenu: 16,
        path: '/api-keys',
        name: 'API Keys',
        parent: 'SETTINGS',
        rtlName: '',
        mini: 'AK',
        rtlMini: '',
        component: Clean,
        layout: '/admin'
      }
    ]
  },//SETTINGS

  {
    id_menu:11,
    collapse: false,
    name: 'COUPONS',
    rtlName: '',
    icon: 'tim-icons icon_coupon',
    state: 'couponsCollapse',    
    layout: '/admin',
    path: '/admin-coupons',
    children:true,
    link:true,
    views: [
      {
        id_submenu: 17,
        path: '/admin-coupons',
        name: 'Admin Coupons',
        parent: 'COUPONS',
        rtlName: 'أ',
        mini: 'AC',
        rtlMini: '',
        component: CouponsAdmin,
        layout: '/admin',
        link:true
      },
      {
        id_submenu: 18,
        path: '/create-coupons',
        name: 'Create Coupons',
        parent: 'COUPONS',
        rtlName: '',
        mini: 'CC',
        rtlMini: '',
        component: CouponsCreate,
        layout: '/admin'
      }
    ]
  },//COUPONS

  {
    id_menu:12,
    collapse: false,    
    path: '/orders',
    layout: '/admin',
    children:false,
    name: 'MY ORDERS',
    rtlName: 'الجداول',
    icon: 'tim-icons icon_worldwide',
    state: 'ridesCollapse',
    id_submenu: 19,
    component: AdminOrders,
    link:true
  },//ORDERS

  // { 
  //   id_menu:1,
  //   collapse: false,
  //   path: '/logout',
  //   name: 'LOGOUT',
  //   rtlName: 'خرائط',
  //   icon: 'tim-icons icon_exit',
  //   layout: '/auth',
  //   link:true
  // },//LOG OUT*/

  {
    id_menu:21,
    path: '/login',
    name: 'Login',
    mini: 'L',
    component: Login,
    layout: '/auth',
    redirect: true
  },//LOGIN

  {
    id_menu:22,
    path: '/user-detail',
    param: '/:cuid',
    name: 'User Detail',
    mini: 'L',
    component: UserDetail,
    layout: '/admin',
    redirect: true,
    required: true,
  },//USER DETAIL

  {
    id_menu:23,
    path: '/verification-code',
    name: 'verifiocation code',
    mini: 'VC',
    component: VerificationCode,
    layout: '/auth',
    redirect: true
  },//VERIFICATION CODE

  {
    id_menu:24,
    path: '/reset-password',
    name: 'reset password',
    mini: 'RP',
    component: ResetPassword,
    layout: '/auth',
    redirect: true
  },//RESET PASSWORD

  {
    id_menu:25,
    path: '/recovery_password',
    param: '/:token',
    name: 'change password',
    mini: 'CP',
    component: ChangePassword,
    layout: '/auth',
    redirect: true
  },//CHANGE PASSWORD

  {
    id_menu:26,
    path: '/edit-inventory',
    name: 'Edit Inventory',
    mini: 'EI',
    component: EditInventory,
    layout: '/admin',
    redirect: true,
    required: true,
  },//EDIT INVENTORY

  {
    id_menu:27,
    path: '/payment',
    param: '/:id_payment',
    name: 'Payment Detail',
    mini: 'PD',
    component: PaymentDetail,
    layout: '/admin',
    redirect: true,
    required: true,
  },//PAYMENT DETAIL

  {
    id_menu:28,
    path: '/add-member',
    param: '/:uuid',
    name: 'EDIT MEMBER',
    mini: 'AD',
    component: AddMember,
    layout: '/admin',
    redirect: true,
    required: true,
  },//EDIT MEMBER

  {
    id_menu:29,
    path: '/edit-owner',
    param: '/:uuid',
    name: 'Edit Owner',
    mini: 'EO',
    component: EditOwner,
    layout: '/admin',
    redirect: true,
    required: true,
  },//EDIT OWNER

  {
    id_menu:30,
    path: '/see-team',
    param: '/:id_operator',
    name: 'See Team',
    mini: 'ST',
    component: SeeTeam,
    layout: '/admin',
    redirect: true,
    required: true,
  },//SEE TEAM

  {
    id_menu:31,
    path: '/edit-order',
    param: '/:id_service',
    name: 'Edit Order',
    mini: 'EO',
    component: EditOrder,
    layout: '/admin',
    redirect: true,
    required: true,
  },//EDIT ORDER

  {
    id_menu:32,
    path: '/add-operator',
    param: '/:id_operator',
    name: 'ADD MEMBER',
    mini: 'AM',
    component: AddMember,
    layout: '/admin',
    redirect: true,
    required: true,
  },//ADD OPERATOR

];

export default routes;