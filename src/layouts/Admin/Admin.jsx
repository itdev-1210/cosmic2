import auth from './../../api/auth';
import {apiKey,gateway} from '../../environment';
import {emitter} from '../../utils/EventComponents'
import {logoutUser} from './../../api/LoginApi'
import AdminNavbar from 'components/Navbars/AdminNavbar.jsx';
import Sidebar from 'components/Sidebar/Sidebar.jsx';
import routes from 'routes.js';


import { Route, Switch, Redirect } from 'react-router-dom';
import NotificationAlert from 'react-notification-alert';
import PerfectScrollbar from 'perfect-scrollbar';
import React from 'react';
import socketIOClient from 'socket.io-client'

let ps,socket;

class Admin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeColor: 'blue',
      sidebarMini: true,
      opacity: 0,
      sidebarOpened: false,
      sidebarType:1,
      menu:[],
      submenu: []
    };

    this.setType = this.setType.bind(this)
    socket = socketIOClient(gateway, {
      secure: window.location.protocol.includes('https'),
      transports: ['websocket'],
      upgrade: true
    })

    this._connect = this._connect.bind(this)
    this._authenticated = this._authenticated.bind(this)
    this._unauthorized = this._unauthorized.bind(this)
    this._updateData = this._updateData.bind(this)

    socket.on('authenticated', this._authenticated);
    socket.on('unauthorized', this._unauthorized);
    socket.on('scooter_update', this._updateData);
  }

  _connect() {
    socket.emit('authenticate', {
      api_key: apiKey
    })
  }

  _authenticated() {
    console.log('authenticated')
  }

  _unauthorized() {
    console.log('unauthorized')
    this._connect()
  }

  _updateData(data) {
    emitter('socket',data)
  }  
  
  setType = () =>{
    this.changeSideBar(localStorage.getItem('role_type'))
  }

  componentDidMount() {
    if(auth.hasLogin()){
      if (navigator.platform.indexOf('Win') > -1) {
        document.documentElement.className += ' perfect-scrollbar-on';
        document.documentElement.classList.remove('perfect-scrollbar-off');
        ps = new PerfectScrollbar(this.refs.mainPanel);
        let tables = document.querySelectorAll('.table-responsive');
        for (let i = 0; i < tables.length; i++) {
          ps = new PerfectScrollbar(tables[i]);
        }
      }
      window.addEventListener('scroll', this.showNavbarButton);
      this.setType()  
      this._connect()    
    }    
  }

  componentWillUnmount() {
    if(auth.hasLogin()){
      if (navigator.platform.indexOf('Win') > -1) {
        ps.destroy();
        document.documentElement.className += ' perfect-scrollbar-off';
        document.documentElement.classList.remove('perfect-scrollbar-on');
      }
      window.removeEventListener('scroll', this.showNavbarButton);      
    }
  }

  componentDidUpdate(e) {
    if (e.location.pathname !== e.history.location.pathname) {
      if (navigator.platform.indexOf('Win') > -1) {
        let tables = document.querySelectorAll('.table-responsive');
        for (let i = 0; i < tables.length; i++) {
          ps = new PerfectScrollbar(tables[i]);
        }
      }
      document.documentElement.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
      this.refs.mainPanel.scrollTop = 0;
    }
  }

  showNavbarButton = () => {
    if (
      document.documentElement.scrollTop > 50 ||
      document.scrollingElement.scrollTop > 50 ||
      this.refs.mainPanel.scrollTop > 50
    ) {
      this.setState({ opacity: 1 });
    } else if (
      document.documentElement.scrollTop <= 50 ||
      document.scrollingElement.scrollTop <= 50 ||
      this.refs.mainPanel.scrollTop <= 50
    ) {
      this.setState({ opacity: 0 });
    }
  };

  getCollapseRoutes = (routes,childrens) => {
    return routes.map((prop, key) => {      
      if (prop.layout === '/admin' && this.state.submenu.indexOf(prop.id_submenu.toString())>-1){
        return <Route exact 
        path={prop.layout + prop.path} 
        render={(props) => <prop.component {...props} childrens={childrens}/>}
        key={key} />;
      } else {
        return null;
      }
    });
  };

  getObjectChildrens = (children)  =>{
    return children.map((prop, key) => {      
      if (prop.layout === '/admin' && this.state.submenu.indexOf(prop.id_submenu.toString())>-1){
        return prop.id_submenu
      } else {
        return null;
      }
    });
  }

  getRoutes = (routes) => {
    return routes.map((prop, key) => { 
      if(this.state.menu.indexOf(prop.id_menu.toString())>-1 || prop.required){
        if (prop.collapse || prop.children) {
          const childrens = this.getObjectChildrens(prop.views)
          return this.getCollapseRoutes(prop.views,childrens);
        }
        if (prop.layout === '/admin') {
          return <Route exact 
          path={prop.param? prop.layout + prop.path + prop.param : prop.layout + prop.path}   
          render={(props) => <prop.component {...props} />}
          key={key} />;
        } else {
          return null;
        }
      }      
    });
  };

  getActiveRoute = (routes) => {
    let activeRoute = 'Default Brand Text';
    for (let i = 0; i < routes.length; i++) {
      if (routes[i].collapse) {
        let collapseActiveRoute = this.getActiveRoute(routes[i].views);
        if (collapseActiveRoute !== activeRoute) {
          return collapseActiveRoute;
        }
      } else {
        if (window.location.pathname.indexOf(routes[i].layout + routes[i].path) !== -1) {
          return routes[i].name;
        }
      }
    }
    return activeRoute;
  };

  handleActiveClick = (color) => {
    this.setState({ activeColor: color });
  };

  toggleSidebar = () => {
    this.setState({
      sidebarOpened: !this.state.sidebarOpened
    });
    document.documentElement.classList.toggle('nav-open');
  };

  closeSidebar = () => {
    this.setState({
      sidebarOpened: false
    });
    document.documentElement.classList.remove('nav-open');
  };

  changeSideBar = async (type) => {
    let menu = null 
    let submenu = null
    if(type>0 && type<4){
      type = parseInt(type)
      auth.setUserType(type)      
      menu = type === 1 ? localStorage.getItem('menu') : type === 2 ? localStorage.getItem('op_menu') : localStorage.getItem('ow_menu');
      submenu = type === 1 ? localStorage.getItem('submenu') : type === 2 ? localStorage.getItem('op_submenu') : localStorage.getItem('ow_submenu');
    }else{      
      await logoutUser()
      window.location.href='/auth/login'
    }
    if(menu && submenu){
      menu = menu ? menu.split(',') : []
      submenu = submenu ? submenu.split(',') : []
      this.setState({sidebarType: type, menu: menu, submenu: submenu})
    }
    else{
      await logoutUser()
      window.location.href='/auth/login'
    }
  }

  render() {
    const { menu, submenu } = this.state
    if(!auth.hasLogin()){      
      logoutUser()
      return <Redirect from="/" to="/auth/login" />
    }
    else{
      return (
        <React.Fragment>
          <div className="wrapper">
            <div className="rna-container">
              <NotificationAlert ref="notificationAlert" />
            </div>
            <Sidebar
              {...this.props}              
              routes={routes}
              menu = {menu}
              submenu = {submenu}
              activeColor={this.state.activeColor}
              closeSidebar={this.closeSidebar}              
            />
            <div id='admin-container' className="main-panel" ref="mainPanel" data={this.state.activeColor}>
              <AdminNavbar
                {...this.props}
                sidebarOpened={this.state.sidebarOpened}
                toggleSidebar={this.toggleSidebar}
                changeSideBar={this.changeSideBar}
              />
                <Switch>
                  {this.getRoutes(routes)}
                </Switch>
            </div>
          </div>
        </React.Fragment>
      )
    }
  }
}

export default Admin;
