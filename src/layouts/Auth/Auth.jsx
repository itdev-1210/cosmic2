import { Route, Switch, Redirect } from 'react-router-dom';

import React from 'react';
import routes from 'routes.js';
import { IntlProvider } from 'react-intl';
// import esLocaleData from 'react-intl/locale-data/es';
import translations from '../../lang/auth/locales';

// addLocaleData(esLocaleData);
const localeProp = 'en';

class Pages extends React.Component {

  getRoutes = (routes) => {
    return routes.map((prop, key) => {
      if (prop.collapse) {
        return this.getRoutes(prop.views);
      }
      if (prop.layout === '/auth' && prop.path !== '/logout') {
        return <Route path={prop.param? prop.layout + prop.path + prop.param : prop.layout + prop.path} component={prop.component} key={key} />;
      } else {
        return null;
      }
    });
  };

  getActiveRoute = (routes) => {
    let activeRoute = 'Default Brand Text';
    for (let i = 0; i < routes.length; i++) {
      if (routes[i].collapse) {
        let collapseActiveRoute = this.getActiveRoute(routes[i].views);
        if (collapseActiveRoute !== activeRoute) {
          return collapseActiveRoute;
        }
      } else {
        if (window.location.pathname.indexOf(routes[i].layout + routes[i].path) !== -1) {
          return routes[i].name;
        }
      }
    }
    return activeRoute;
  };

  getFullPageName = (routes) => {
    let pageName = this.getActiveRoute(routes);
    switch (pageName) {
      case 'Pricing':
        return 'pricing-page';
      case 'Login':
        return 'login-page';
      case 'Register':
        return 'register-page';
      case 'Lock Screen':
        return 'lock-page';
      case 'verifiocation code':
        return 'verfication-code';
      case 'reset password':
        return 'reset-password';
      case 'change password':
        return 'change_password';
      default:
        return 'Default Brand Text';
    }
  };

  componentDidMount() {
    document.documentElement.classList.remove('nav-open');
  }

  render() {
    return (
      <>
        {/* <AuthNavbar brandText={this.getActiveRoute(routes) + " Page"} /> */}
        <div className="wrapper wrapper-full-page" ref="fullPages">
          <div className={'full-page ' + this.getFullPageName(routes)}>
            <IntlProvider
              locale={localeProp}
              defaultLocale="en"
              messages={translations[localeProp]}
            >
              <Switch>{this.getRoutes(routes)}
                <Redirect from="/" to="/auth/login"/>              
              </Switch>
            </IntlProvider>
          </div>
        </div>
      </>
    );
  }
}

export default Pages;
