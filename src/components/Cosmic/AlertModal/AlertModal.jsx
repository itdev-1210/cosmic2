import React, { Component } from 'react';
import './AlertModal.scss'

class AlertModal extends Component {
  render() {
    const { status, msj } = this.props
    return (
      <div className="alert-body">
        <div className="close-button">
          <div onClick={() => this.props.onClose()}>&#10005;</div>
        </div>
        <div className="status-icon">
          <img alt="" src={status===200 ? require("assets/img/icon_check.svg") : require("assets/img/icon-fail.svg")}/>
        </div>
        <div className="bottom-border"><div></div></div>
        <div className="alert-description">
          {msj}
        </div>
      </div>
    )
  }
}

export default AlertModal