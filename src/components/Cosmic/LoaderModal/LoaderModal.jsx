import React, { Component } from 'react';
import ReactLoading from 'react-loading'
import './LoaderModal.scss'

class LoaderModal extends Component {
  render() {
    return (
      <div className="log-out">
        <div className="change-title">
          <span className="title">{this.props.title}</span>
        </div>
        <div className="home_loader">
          <ReactLoading type={'spinningBubbles'} color="#000" height={'96px'} width={'96px'}/>
        </div>
      </div>
    )
  }
}

export default LoaderModal