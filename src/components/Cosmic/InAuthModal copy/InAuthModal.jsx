import React, { Component } from 'react';
import './InAuthModal.scss'
import {
  Button,
  Label,
  FormGroup,
  Input,
  Row,
  Form,
  Col,
} from 'reactstrap';

import { loginUniverseUser, AuthTokenLogin } from '../../../api/LoginApi'
class ConfirmModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      emailState: "",
      passwordState: "",
      email: "",
      password: ""
    };
    this.authenticate = this.authenticate.bind(this)
    this.change = this.change.bind(this)
  }

  change(e) {
    var obj = {}
    obj[e.target.name] = e.target.value
    if (e.target.value === '')
      obj[e.target.name+'State'] = 'has-danger'

    this.setState(obj)
  }

  async authenticate(event) {
    event.preventDefault();
    if (this.state.email === '')
      this.setState({emailState: 'has-danger'}, ()=>{return})
    if (this.state.password === '')
      this.setState({passwordState: 'has-danger'}, ()=>{return})

    await AuthTokenLogin();
    const res = await loginUniverseUser({email: this.state.email, password: this.state.password})
    res.oldpass = this.state.password
    this.props.onAuthenticate(res)
  }

  render() {
    const { onClose, type } = this.props
    const { passwordState, emailState } = this.state
    return (
      <div className="change-password">
        <Form className="form" onSubmit={this.authenticate}>
          <div className="change-title">
            <span className="title">{'To continue, please enter your password'}</span>
          </div>
          {type === 'isRide' ? (
            <div className="change-description">
              {'You need additional privileges to execute this action'}
            </div>
          ) : null}
          <div className="password-group">
            <Row>
              <Label sm="3" className="password-label">Email</Label>
              <Col sm="8">
                <FormGroup className={emailState}>
                  <Input
                    name="email"
                    type="email"
                    placeholder="admin@mail.com"
                    onChange={this.change}
                  />
                  {this.state.emailState === "has-danger" ? (
                    <label className="error">
                      This field is required.
                    </label>
                  ) : null}
                </FormGroup>
              </Col>
            </Row>

            <Row>
              <Label sm="3" className="password-label">Password</Label>
              <Col sm="8">
                <FormGroup className={passwordState}>
                  <Input
                    name="password"
                    type="password"
                    placeholder="Password"
                    onChange={this.change}
                  />
                  {this.state.passwordState === "has-danger" ? (
                    <label className="error">
                      This field is required.
                    </label>
                  ) : null}
                </FormGroup>
              </Col>
            </Row>
          </div>
          <div className="btn-change-group">
            <Button type="submit" color="success" className="change-btn primary-btn" onClick={this.authenticate}>Authenticate</Button>
            <Button className="change-btn" onClick={onClose}>Cancel</Button>
          </div>
        </Form>
      </div>
    )
  }
}

export default ConfirmModal