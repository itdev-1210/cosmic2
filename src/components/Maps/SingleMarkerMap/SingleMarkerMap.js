import React from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow ,Polyline } from 'react-google-maps';
import ResponsiveModal from 'react-responsive-modal';

import './SingleMarkerMap.scss'

const defaultMapOptions = {
    disableDefaultUI: true
  }

const RegularMap = withScriptjs(    
    withGoogleMap((props) => {        
        return (
            <GoogleMap
                defaultZoom={16}
                defaultCenter={{lat: parseFloat(props.center.lat), lng: parseFloat(props.center.lng)}}
                defaultOptions={defaultMapOptions}
            >
                {props.geofence.map((geo,i)=>{
                    return <Polyline key={i} path={geo} options={{strokeColor:'#71B8C2'}} />
                })}

                {props.markers.map((marker, index) => { return (

                <div key={index}>
                    <Marker
                        onClick={()=>props.onMarkerClick(index)}
                        position={{lat : marker.lat, lng : marker.lng}}
                        defaultAnimation={2}                        
                        icon = {{url:marker.urlIcon,scaledSize: new window.google.maps.Size(36, 36)}}
                    >
                       {props.showMarkId === index ? (
                            <InfoWindow 
                                onCloseClick={()=>props.onMarkerClose()}
                                >
                                <div className="info-window-ride">
                                    <div className="info-one">{marker.address}</div>
                                    <div className="info-two">{marker.msj}</div>
                                </div>
                            </InfoWindow>
                        ):null}
                    </Marker>
                </div>)})}
            </GoogleMap>
        );
    })
);

class SingleMarkerMap extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showMarkId:-1
        }
    }

    onMarkerClick = (index) =>{
        if(this.state.showMarkId===index)this.setState({showMarkId:-1})
        else this.setState({showMarkId:index})
    }
    onMarkerClose = () =>{
        this.setState({showMarkId:-1})
    }

    render() {      
        return (
            <ResponsiveModal 
                open={this.props.statusModal} 
                onClose={this.props.toggleModal}
                center
                classNames={{'modal':'dark-modal-map'}}
                // showCloseIcon={false}
                // closeIconId='exitIcon'
            >
                <div className="modal-header-map">
                    <h6 className="title">{this.props.title}</h6>
                </div>   
                <RegularMap
                    markers={this.props.markers}
                    center={this.props.center}
                    geofence={this.props.geofence}
                    showMarkId={this.state.showMarkId}
                    onMarkerClose={this.onMarkerClose}
                    onMarkerClick={this.onMarkerClick}
                    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDASQ1gDtuxVXsoeJZHXqsNRASQ17yVoEI"                    
                    loadingElement={<div id='loadingMap' style={{ height: `100%` }}/>}
                    containerElement={<div id='contMap'/>}
                    mapElement={<div id='map'/>}
                />
            </ResponsiveModal>
        );
    }
}
  
export default SingleMarkerMap;