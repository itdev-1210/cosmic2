import React from "react";
import { ToastContainer, toast } from 'react-toastify'
// nodejs library that concatenates classes
import classNames from "classnames";
import './styles.scss'
import auth from './../../api/auth';
import { logoutUser } from './../../api/LoginApi'
import {SearchBar} from '../SearchBar/SearchBar'

// reactstrap components
import {
  Collapse,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Input,
  Navbar,
  NavLink,
  Nav,
  Container,
  Modal,
} from "reactstrap";
import ResponsiveModal from 'react-responsive-modal';
import LogoutModal from './LogoutModal'
class AdminNavbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapseOpen: false,
      modalSearch: false,
      color: "navbar-transparent",
      uuid: null,
      showModal: false,
      type: auth.getUserType(),
      menus: [],
      roles: []
    };
  }
  componentDidMount() {
    let menus = localStorage.getItem('user_menu').split(',')
    let roles = localStorage.getItem('role_type').split(',')
    window.addEventListener("resize", this.updateColor);
    this.setState({uuid:auth.getUUID(), menus: menus, roles: roles})  
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateColor);
  }
  editMember = () => {
    this.props.history.push('/admin/add-member/' + this.state.uuid)
  }
  singOut = async () => {
    this.setState({showModal: true})
    const res = await logoutUser()
    if (res.response.status !== 200) {
      this.setState({showModal: false}, ()=>{toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})})
      window.location.href='/auth/login'
    } else {
      window.location.href='/auth/login'
    }
  }
  // function that adds color white/transparent to the navbar on resize (this is for the collapse)
  updateColor = () => {
    if (window.innerWidth < 993 && this.state.collapseOpen) {
      this.setState({
        color: "bg-white"
      });
    } else {
      this.setState({
        color: "navbar-transparent"
      });
    }
  };
  // this function opens and closes the collapse on small devices
  toggleCollapse = () => {
    if (this.state.collapseOpen) {
      this.setState({
        color: "navbar-transparent"
      });
    } else {
      this.setState({
        color: "bg-white"
      });
    }
    this.setState({
      collapseOpen: !this.state.collapseOpen
    });
  };
  // this function is to open the Search modal
  toggleModalSearch = () => {
    this.setState({
      modalSearch: !this.state.modalSearch
    });
  };
  handleChange = (type) => {
    this.setState({type: type}, ()=>{
      this.props.changeSideBar(type)
    })
  }
  render() {
    const {type} = this.state
    return (
      <>
        <Navbar
          className={classNames("navbar-absolute", {
            [this.state.color]:
              this.props.location.pathname.indexOf("full-screen-map") === -1
          })}
          expand="lg"
        >
          <Container fluid>
            <ToastContainer />
            <div>
            <div className="navbar-wrapper">
              <div
                className={classNames("navbar-toggle d-inline", {
                  toggled: this.props.sidebarOpened
                })}
              >
                <button
                  className="navbar-toggler"
                  type="button"
                  onClick={this.props.toggleSidebar}
                >
                  <span className="navbar-toggler-bar bar1" />
                  <span className="navbar-toggler-bar bar2" />
                  <span className="navbar-toggler-bar bar3" />
                </button>
              </div>
              <a className="logo-mini" href="#">
                <img alt="" src={auth.getBrandLogo() ? auth.getBrandLogo() : require("assets/img/Cosmic Logo.svg")}/>
                {auth.getBrandName() ? auth.getBrandName() : "COSMICO GO"}
              </a>
              {this.state.menus.length > 1 &&
              <UncontrolledDropdown>
                <DropdownToggle
                    caret
                    className="user-type"
                    data-toggle="dropdown"
                    nav
                  >
                    <img alt="..." src={require("assets/img/icon_features_white.svg")} />
                </DropdownToggle>
                <DropdownMenu className="dropdown-navbar">
                  <div className="type-header">User Type</div>
                  <div className="type-group">
                    {this.state.menus.map((menu, index) => 
                      <div key={index} className={type===this.state.roles[index] ? 'active-div' : ''} onClick={()=>{this.handleChange(this.state.roles[index])}}>{menu}</div>
                    )}
                  </div>
                  </DropdownMenu>
                  </UncontrolledDropdown> }
                  </div>
                  <div className="operator-name">{localStorage.getItem('operator')}</div>
                </div>
                <SearchBar/>
                <Collapse navbar isOpen={this.state.collapseOpen}>
                  <Nav className="ml-auto" navbar>
                <UncontrolledDropdown nav>
                  <DropdownToggle
                    caret
                    color="default"
                    data-toggle="dropdown"
                    nav
                    onClick={e => e.preventDefault()}
                  >
                    <div className="photo">
                      <img alt="..." src={"https://csmc-asset.s3-us-west-2.amazonaws.com/Web+Assets/Dashboard/icon_guy1.png"} />
                    </div>
                    <b className="caret d-none d-lg-block d-xl-block" />
                    <p className="d-lg-none">Log out</p>
                  </DropdownToggle>
                  <DropdownMenu className="dropdown-navbar" right tag="ul">
                    <NavLink tag="li">
                      <DropdownItem className="nav-item profile-name">
                        <span>{auth.getUserName()}</span>
                        <span className="operator">{auth.getUserRole()}</span>
                      </DropdownItem>
                    </NavLink>
                    <DropdownItem divider tag="li" />
                    <NavLink tag="li">
                      <DropdownItem className="nav-item" onClick={this.editMember}>Profile</DropdownItem>
                    </NavLink>
                    <NavLink tag="li">
                      <DropdownItem className="nav-item" onClick={this.singOut}>Sign out</DropdownItem>
                    </NavLink>
                  </DropdownMenu>
                </UncontrolledDropdown>
                <li className="separator d-lg-none" />
              </Nav>
            </Collapse>
          </Container>
        </Navbar>
        <Modal
          modalClassName="modal-search"
          isOpen={this.state.modalSearch}
          toggle={this.toggleModalSearch}
        >
          <div className="modal-header">
            <Input id="inlineFormInputGroup" placeholder="SEARCH" type="text" />
            <button
              aria-label="Close"
              className="close"
              data-dismiss="modal"
              type="button"
              onClick={this.toggleModalSearch}
            >
              <i className="tim-icons icon-simple-remove" />
            </button>
          </div>
        </Modal>
        <ResponsiveModal open={this.state.showModal} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LogoutModal title="Logging Out"/>
        </ResponsiveModal>
      </>
    );
  }
}

export default AdminNavbar;
