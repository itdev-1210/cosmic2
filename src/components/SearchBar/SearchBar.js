import './SearchBar.scss'
import React from "react";
import {emitter, subscribe,unSubscribe } from '../../utils/EventComponents';

export class SearchBar extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            hideSearch:false,
            valSearch:''
        }
        this.timer={}
    }
    componentWillMount() {
        subscribe('searchBar',(value)=> this.stateSearch(value))
    }
    componentWillUnmount() {
        unSubscribe('searchBar')
    }
    stateSearch = (value)=>{
        this.setState({hideSearch:value,valSearch:''})
    }
    startSearch = (e) =>{
        clearTimeout(this.timer)
        const value = e.target.value
        this.timer=setTimeout(()=>emitter('search',value), 700);
        this.setState({
            valSearch:value
        })
    }
    render() {
      return <input type='text' id='searchBar' className='searchBar' placeholder='Search' onChange={this.startSearch} hidden={!this.state.hideSearch} value={this.state.valSearch}/>
    }
}