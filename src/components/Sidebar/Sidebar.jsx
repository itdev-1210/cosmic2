/*eslint-disable*/
import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from 'perfect-scrollbar';

// reactstrap components
import { Nav, Collapse } from 'reactstrap';
import {logoutUser } from './../../api/LoginApi'
import './logout.scss'

var ps;

class Sidebar extends React.Component {
  constructor(props) {
    super(props); 
    this.state = this.getCollapseStates(props.routes);
  }
  
  getCollapseStates = (routes) => {
    let initialState = {};
    routes.map((prop, key) => {
      if (prop.collapse) {
        initialState = {
          [prop.state]: this.getCollapseInitialState(prop.views),
          ...this.getCollapseStates(prop.views),
          ...initialState
        };
      }
      return null;
    });
    return initialState;
  };

  getCollapseInitialState(routes) {
    for (let i = 0; i < routes.length; i++) {
      if (routes[i].collapse && this.getCollapseInitialState(routes[i].views)) {
        return true;
      } else if (window.location.href.indexOf(routes[i].path) !== -1) {
        return true;
      }
    }
    return false;
  }
  
  createLinks = (routes,array) => {
    const { rtlActive } = this.props;
    return routes.map((prop, key) => {
      if (prop.redirect) {
        return null;
      }      
      if((array=='id_menu'?this.props.menu:this.props.submenu).indexOf(array=='id_menu'?prop.id_menu.toString():prop.id_submenu.toString())>-1){
        if(prop.name==='LOGOUT'){
          var st = {};
          st[prop['state']] = !this.state[prop.state];
          return (
            <li key={key}>
              <a
                href="#pablo"
                data-toggle="collapse"
                aria-expanded={this.state[prop.state]}
                onClick={(e) => {
                  e.preventDefault();
                  this.setState(st);
                  this.logout();
                }}
              >
                {prop.icon !== undefined ? (
                  <>
                    <i className={prop.icon} />
                    <p>
                      {rtlActive ? prop.rtlName : prop.name}
                    </p>
                  </>
                ) : (
                  <>
                    <span className="sidebar-mini-icon">{rtlActive ? prop.rtlMini : prop.mini}</span>
                    <span className="sidebar-normal">
                      {rtlActive ? prop.rtlName : prop.name}
                      <b className="caret" />
                    </span>
                  </>
                )}
              </a>
            </li>
          );
        }
        else if (prop.collapse ){
          var st = {};
          st[prop['state']] = !this.state[prop.state];
          return (
            <li className={this.getCollapseInitialState(prop.views) ? 'active' : ''} key={key}>
              <a
                href="#pablo"
                data-toggle="collapse"
                aria-expanded={this.state[prop.state]}
                onClick={(e) => {
                  e.preventDefault();
                  this.setState(st);
                }}
              >
                {prop.icon !== undefined ? (
                  <>
                    <i className={prop.icon} />
                    <p>
                      {rtlActive ? prop.rtlName : prop.name}
                      <b className="caret" />
                    </p>
                  </>
                ) : (
                  <>
                    <span className="sidebar-mini-icon">{rtlActive ? prop.rtlMini : prop.mini}</span>
                    <span className="sidebar-normal">
                      {rtlActive ? prop.rtlName : prop.name}
                      <b className="caret" />
                    </span>
                  </>
                )}
              </a>
              <Collapse isOpen={this.state[prop.state]}>
                <ul className="nav">{this.createLinks(prop.views,'id_submenu')}</ul>
              </Collapse>
            </li>
          );
        }        
        else if (prop.link){
          return (            
            <li className={this.activeRoute(prop.layout + prop.path)} key={key}>
                <NavLink to={prop.layout + prop.path} activeClassName="" onClick={()=>{this.setText(prop.name)}}>
                  {prop.icon !== undefined ? (
                    <>
                      <i className={prop.icon} />
                      <p>{rtlActive ? prop.rtlName : prop.name}</p>
                    </>
                  ) : (
                    <>
                      <span className="sidebar-mini-icon">{rtlActive ? prop.rtlMini : prop.mini}</span>
                      <span className="sidebar-normal">{rtlActive ? prop.rtlName : prop.name}</span>
                    </>
                  )}
                </NavLink>
            </li>
          );
        }
        else return null
      }
    });
  };

  logout = async () =>{
    await logoutUser()
    window.location.href = '/auth/login'
  }

  // verifies if routeName is the one active (in browser input)
  activeRoute = (routeName) => {
    return this.props.location.pathname.indexOf(routeName) > -1 ? 'active' : '';
  };

  setText = (name) => {
    this.setState({text:name}, ()=>{this.props.closeSidebar()})
  }

  getActiveText = () => {
    let text = ''
    this.props.routes.map((prop, key) => {
      if (prop.redirect) {
        return null;
      }      
      if (prop.collapse || prop.children){
        if (this.getCollapseInitialState(prop.views))
          text = prop.name
        return null
      }
      if(prop.name==='LOGOUT'){
        return null
      }
      else{
        return null
      }
    })
    this.setState({text: text})
  }

  componentDidMount() {
    // if you are using a Windows Machine, the scrollbars will have a Mac look
    if (navigator.platform.indexOf('Win') > -1) {
      ps = new PerfectScrollbar(this.refs.sidebar);
    }
    this.getActiveText()
  }

  componentWillUnmount() {
    // we need to destroy the false scrollbar when we navigate
    // to a page that doesn't have this component rendered
    if (navigator.platform.indexOf('Win') > -1) {
      ps.destroy();
    }
  }

  render() {
    const { activeColor } = this.props;
    return (
      <div className="sidebar" data={activeColor}>
        <div className="sidebar-wrapper" ref="sidebar">
          <div className="logo">
            {this.state.text}
          </div>
          <Nav>{this.createLinks(this.props.routes,'id_menu')}</Nav>
        </div>
      </div>
    );
  }
}

Sidebar.propTypes = {
  activeColor: PropTypes.oneOf(['primary', 'blue', 'green', 'orange', 'red']),
  rtlActive: PropTypes.bool,
  routes: PropTypes.array.isRequired,
    logo: PropTypes.oneOfType([
    PropTypes.shape({
      innerLink: PropTypes.string.isRequired,
      imgSrc: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired
    }),
    PropTypes.shape({
      outterLink: PropTypes.string.isRequired,
      imgSrc: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired
    })
  ]),
  // this is used on responsive to close the sidebar on route navigation
  closeSidebar: PropTypes.func
};

export default Sidebar;
