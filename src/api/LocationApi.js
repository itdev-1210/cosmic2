import { appFetch, dashFetch } from './fetch';
//import host from './host';
import auth from './auth';

import {envEndpoint} from './../environment';
const host = envEndpoint

export const getGeneralView = async () => {
  const url = `${host}devices/location/general`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const getNav = async(params) => {
  var url = `${host}devices/location/detail${params}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}

export const getDetailInfo = async(id_device) => {
  var url = `${host}/devices/location/detail/info/${id_device}`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}

export const transGeofence = async(back_geo) => {
  let geofence = []
  await back_geo.map(async (geo,i) =>{
    let points = []
    await geo.geofence[0][1].map((point,i) => {
      return points.push({lat:parseFloat(point.lat),lng:parseFloat(point.lon)})
    })      
    points.push(points[0])
    geofence.push(points)
  })  
  return geofence
}