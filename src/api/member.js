import { appFetch ,dashFetch} from './fetch';
//import host from './host';
import auth from './auth';

import {envEndpoint} from './../environment';
const host = envEndpoint

export const createUser = async (user) => {
  const url = `${host}universe_user/create`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(user), method: 'POST' });
};

export const createOperator = async (user) => {
  const url = `${host}operators/create_user`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(user), method: 'POST' });
}

export const editUser = async (user) => {
  const url = `${host}universe_user/update`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(user), method: 'POST' });
};

export const getUser = async (uuid) => {
  const url = `${host}universe_user/detail_user/${uuid}`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}

export const getAllCountries = async () => {
  const url = `${host}ubication/get_all_countries`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}

export const getAllStates = async (params) => {
  const url = `${host}ubication/get_state_by_country`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(params), method: 'POST' });
}

export const getAllCities = async (params) => {
  const url = `${host}ubication/get_cities_by_state`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(params), method: 'POST' });
}

export const changePass = async (params) => {
  const url = `${host}universe_user/admin_change_password`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(params), method: 'POST' });
}

export const changeOwnPass = async (params) => {
  const url = `${host}universe_user/update_password_profile`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(params), method: 'POST' });
}

export const getInfo = async (uuid) => {
  const url = `${host}universe_user/get_user_info`
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}

export const getInfoToOp = async (uuid) => {
  const url = `${host}universe_user/get_operator_user_info`
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}