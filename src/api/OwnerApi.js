import { appFetch,dashFetch } from './fetch';
//import host from './host';
import auth from './auth';

import {envEndpoint} from './../environment';
const host = envEndpoint

export const getOwnersAdmin = async () => {
  const url = `${host}owners/get_owners`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const getOwner = async (uuid) => {
  const url = `${host}owner/detail_owner/${uuid}`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const createOwnerUser = async (owner) => {
  const url = `${host}owner/create`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(owner), method: 'POST' });
}

export const updateOwner = async (owner) => {
  const url = `${host}owner/update`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(owner), method: 'POST' });
}

export const deleteOwnerAdmin = async (ownerId) => {
  const url = `${host}owner/delete`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(ownerId), method: 'POST' });
};

export const getFilterAdmin = async (filter) => {
  var url = `${host}/owners/get_owners?${filter}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const getNavAdmin = async(params) => {
  var url = `${host}/owners/get_owners${params}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}
