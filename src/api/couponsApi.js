import { appFetch, dashFetch } from './fetch';
import auth from './auth';

import {envEndpoint} from './../environment';

const host = envEndpoint

export const getInfoCoupons = async () => {    
  const url = `${host}coupon/info`
  const response = await dashFetch({ url, method: 'GET', auth: auth.getAuthTokenUniverseUser()});
  return response
};

export const getCoupons = async (info) => {
  const url = `${host}coupon/get_coupons`    
  const response = await appFetch({ url, method: 'GET', auth: auth.getAuthTokenUniverseUser()});
  return response
};

export const getNavCoupon = async (params) => {
  const url = `${host}coupon/get_coupons${params}`    
  return await appFetch({ url, method: 'GET', auth: auth.getAuthTokenUniverseUser()});
}

export const getFilter = async (filter) => {
  const url = `${host}coupon/get_coupons?${filter}`    
  return await appFetch({ url, method: 'GET', auth: auth.getAuthTokenUniverseUser()});
}

export const createCoupons = async (info) => {
  const url = `${host}coupon/create_coupon`
  const response = await appFetch({ url:url, body:JSON.stringify(info), method: 'POST', auth: auth.getAuthTokenUniverseUser()});
  return response
};

export const editCoupons = async (info) => {
  const url = `${host}coupon/edit_coupon`
  const response = await appFetch({ url:url, body:JSON.stringify(info), method: 'POST', auth: auth.getAuthTokenUniverseUser()});
  return response
};

export const deleteCoupon = async (info) => {
  const url = `${host}coupon/delete_coupon`
  const response = await appFetch({ url:url, body:JSON.stringify(info), method: 'POST', auth: auth.getAuthTokenUniverseUser()});
  return response
};