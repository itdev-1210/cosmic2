import { appFetch } from './fetch';
//import host from './host';
import auth from './auth';
// const uuid = auth.getUUID()

import {envEndpoint} from './../environment';
const host = envEndpoint

export const getAllPayments = async () => {
  var url = `${host}payments`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const getDetail = async (id_payment) => {
  const url = `${host}payment/${id_payment}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const getFilter = async (filter) => {
  var url = `${host}payments/?${filter}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const getNav = async(params) => {
  var url = `${host}payments/${params}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}

export const refundPayment = async (params) => {
  var url = `${host}user_payments/refund_payment_admin`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(params), method: 'POST' });
}