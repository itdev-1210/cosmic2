import docCookies from './docCookies';

var auth = (function() {

  const headerLogin = 'login-authorization'
  const headerDash  = 'dash-authorization'
  
  function setAuthToken(authToken) {
    this.authToken = authToken;
    docCookies.setItem(headerLogin, authToken ,Infinity,'/');
  }function removeAuthToken() {;
    docCookies.removeItem(headerLogin,'/');    
  }

  function setUUID(uuid) {
    docCookies.setItem('uuid', uuid,Infinity,'/');
  }function getUUID() {
    return docCookies.getItem('uuid');
  }function removeUUID() {
    docCookies.removeItem('uuid','/');
  }

  function setUserType(type) {
    docCookies.setItem('user_type', type,Infinity,'/')
  }  function removeUserType() {
    docCookies.removeItem('user_type','/')
  }  function getUserType() {
    return docCookies.getItem('user_type')
  }

  function setOrganization(organization) {
    docCookies.setItem('organization', organization,Infinity,'/');
  }  function getOrganization() {
    return docCookies.getItem('organization');
  }  function removeOrganization() {
    return docCookies.removeItem('organization','/');
  }

  function setUserInfo(user) {
    docCookies.setItem('user_name', user.first_name + ' ' + user.last_name,Infinity,'/')
  }function getUserName() {
    return docCookies.getItem('user_name')
  }function removeUserName() {
    return docCookies.removeItem('user_name','/');
  }

  function setUserRole(role) {
    docCookies.setItem('user_role', role,Infinity,'/')
  }function getUserRole() {
    return docCookies.getItem('user_role')
  }

  function getAuthToken() {
    return docCookies.getItem(headerLogin);
  }
  
  function setApiKey(apiKey) {
    localStorage.setItem('apiKey', apiKey);
  }  
  function getApiKey() {
    return localStorage.getItem('apiKey');
  }
  function removeApiKey() {
    localStorage.removeItem('apiKey');    
  }
  
  function setLastDigits(lastDigits) {
    this.lastDigits = lastDigits;
  }
  function getLastDigits() {
    return this.lastDigits;    
  }  
  function removeLastDigits() {
    this.lastDigits = null;
  }
  function hasLogin() {
    return this.getAuthTokenUniverseUser() ? true : false;
  }
  function hasSMSBeenSent() {
    return this.getUUID() ? true : false;
  }
  function setAuthTokenUniverseUser(authTokenUniverseUser) {
    docCookies.setItem(headerDash, authTokenUniverseUser,Infinity,'/');
  }
  function getAuthTokenUniverseUser() {
    return docCookies.getItem(headerDash);
  }
  function removeAuthTokenUniverseUser() {
    docCookies.removeItem(headerDash,'/');
  }
  
  function removeRole() {    
    docCookies.removeItem('role');
  }  
  
  function removeUserInfo() {
    docCookies.removeItem('user_name','/')
    docCookies.removeItem('user_role','/')
    localStorage.removeItem('menu')
    localStorage.removeItem('submenu')
    localStorage.removeItem('op_menu')
    localStorage.removeItem('op_submenu')
    localStorage.removeItem('ow_menu')
    localStorage.removeItem('ow_submenu')
    localStorage.removeItem('admin_roles')
    localStorage.removeItem('operator_roles')
    localStorage.removeItem('owner_roles')
    localStorage.removeItem('user_menu')
    localStorage.removeItem('role_type')
  }
  
  function setRoles(roles) {
    roles.forEach(function(role) {
      if(role[0][0].id === 1)
        docCookies.setItem('admin_roles', JSON.stringify(role[1]))
      if(role[0][0].id === 2)
        docCookies.setItem('operator_roles', JSON.stringify(role[1]))
      if(role[0][0].id === 3)
        docCookies.setItem('owner_roles', JSON.stringify(role[1]))
    })
  }
  function checkRole(menu, submenu) {
    let user_type = docCookies.getItem('user_type')
    let roles = []
    if (user_type.toString() === '1') {
      roles = JSON.parse(docCookies.getItem('admin_roles'))
    } else if (user_type.toString() === '2') {
      roles = JSON.parse(docCookies.getItem('operator_roles'))
    } else {
      roles = JSON.parse(docCookies.getItem('owner_roles'))
    }

    let index = roles.findIndex(role=>role.menu === menu)
    if (index === -1)
      return false
    roles = roles[index]
    index = roles['submenu'].findIndex(role=>role.name===submenu)
    if (index === -1)
      return false

    return roles['submenu'][index]['permission'];
  }
  function setBrand(brand) {
    if (brand) {
      localStorage.setItem('brand_logo', brand.logo);
      localStorage.setItem('brand_name', brand.name);
    } else {
      localStorage.setItem('brand_logo', null);
      localStorage.setItem('brand_name', null);
    }
  }
  function getBrandLogo() {
    return localStorage.getItem('brand_logo');
  }
  function getBrandName() {
    return localStorage.getItem('brand_name');
  }
  function removeBrand() {
    localStorage.removeItem('brand_logo');
    localStorage.removeItem('brand_name');
  }
  function setOperator(operator) {
    docCookies.setItem('operator_name', operator ? operator.name : null);
  }
  function getOperator() {
    docCookies.getItem('operator_name');
  }
  function removeOperator() {
    docCookies.removeItem('operator_name');
  }
  return {
    setUserType: setUserType,
    getUserType: getUserType,
    removeUserType:removeUserType,
    setRoles: setRoles,
    checkRole: checkRole,
    setAuthToken: setAuthToken,
    getAuthToken: getAuthToken,
    removeAuthToken: removeAuthToken,  
    setAuthTokenUniverseUser: setAuthTokenUniverseUser,
    getAuthTokenUniverseUser: getAuthTokenUniverseUser,
    setUUID: setUUID,
    getUUID: getUUID,
    hasLogin: hasLogin,
    hasSMSBeenSent: hasSMSBeenSent,
    getLastDigits: getLastDigits,
    setLastDigits: setLastDigits,
    removeAuthTokenUniverseUser: removeAuthTokenUniverseUser,
    removeUUID: removeUUID,
    removeLastDigits: removeLastDigits,
    removeRole: removeRole,
    setOrganization: setOrganization,
    getOrganization: getOrganization,
    setApiKey:setApiKey,
    getApiKey:getApiKey,
    removeApiKey:removeApiKey,
    setUserInfo: setUserInfo,
    setUserRole: setUserRole,
    getUserName: getUserName,
    removeUserName:removeUserName,
    getUserRole: getUserRole,
    removeOrganization:removeOrganization,
    removeUserInfo:removeUserInfo,
    setBrand: setBrand,
    getBrandLogo: getBrandLogo,
    getBrandName: getBrandName,
    removeBrand: removeBrand,
    setOperator: setOperator,
    getOperator: getOperator,
    removeOperator: removeOperator,
    headerDash:headerDash,
    headerLogin:headerLogin,
  };
})();

export default auth;
