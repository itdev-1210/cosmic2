import { appFetch, dashFetch } from './fetch';
//import host from './host';
import auth from './auth';

import {envEndpoint} from './../environment';
const host = envEndpoint

export const createUser = async (user) => {
  const url = `${host}User`;
  return await appFetch({ url, body: JSON.stringify(user), method: 'POST' });
};

export const getUsers = async () => {
  const url = `${host}users/get_all_users`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const getUser = async (userId) => {
  const url = `${host}users/detail_user/${userId}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const deleteUser = async (userId) => {
  const url = `${host}User/${userId}`;
  return await appFetch({ url, method: 'DELETE' });
};

export const getNav = async(params) => {
  var url = `${host}users/get_all_users${params}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}

export const getFilter = async(params) => {
  var url = `${host}users/get_all_users?${params}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}

export const changeAccount = async(params) => {
  var url = `${host}users/change_status`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(params), method: 'POST' });
}