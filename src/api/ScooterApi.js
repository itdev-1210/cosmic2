import { appFetch, appFetchCos, appFetchCsv, dashFetch ,dashFetchGate} from './fetch';
//import host from './host';
import auth from './auth';
import {envEndpoint,gateway} from './../environment';

const host = envEndpoint
const other_url = gateway+'/api/v1/scooter/'

export const createScooter = async (device) => {
  const url = `${host}devices/create_device`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(device), method: 'POST' });
};

export const editScooter = async (device) => {
  const url = `${host}devices/edit_serial_devices_admin`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(device), method: 'POST' });
};

export const createScooterCsv = async (device) => {
  const url = `${host}devices/create_devices_csv`;
  return await appFetchCsv({ url,  auth: auth.getAuthTokenUniverseUser(), data: device, method: 'POST' });
}
export const getControlDevices = async () => {
  const url = `${host}devices/get_control_devices`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const getInventoriDevices = async () => {
  const url = `${host}devices/get_inventory_devices`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const getScooter = async (deviceId) => {
  const url = `${host}devices/${deviceId}`;
  return await appFetch({ url });
};

export const deleteScooter = async (deviceId) => {
  const url = `${host}devices/${deviceId}`;
  return await appFetch({ url, method: 'DELETE' });
};

export const deleteDeviceAdmin = async (deviceId) => {
  const url = `${host}devices/delete_devices_admin`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(deviceId), method: 'POST' });
};

export const getControlDevicesFil = async (filter) => {
  var url = `${host}devices/get_control_devices?${filter}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const getInventoriDevicesFil = async (filter) => {
  var url = `${host}devices/get_inventory_devices?${filter}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const getNav = async(params) => {
  var url = `${host}devices/get_control_devices${params}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}

export const getNavAdmin = async(params) => {
  var url = `${host}devices/get_inventory_devices${params}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}

export const changeStatus = async(params) => {
  const url = `${host}devices/change_status`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(params), method: 'POST' });
}
export const changeBulkStatus = async(params) => {
  const url = `${host}devices/change_status`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(params), method: 'POST' });
}

export const scooterLock = async(params) => {
  const url = `${other_url}lock`;
  return await appFetchCos({ url, body: JSON.stringify(params), method: 'POST' });
}

export const endRideLock = async(params) => {
  const url = `${other_url}dashboard/lock`;
  return await dashFetchGate({ url, body: JSON.stringify(params), method: 'POST' });
}


export const scooterUnlock = async(params) => {
  const url = `${other_url}unlock`;
  return await appFetchCos({ url, body: JSON.stringify(params), method: 'POST' });
}

export const deviceTracking = async(params) => {
  const url = `${other_url}tracking`;
  return await appFetchCos({ url, body: JSON.stringify(params), method: 'POST' });
}

// export const scooterSetting = async(params) => {
//   const url = `${gateway}settings`;
//   console.log(url)
//   return await appFetchCos({ url, body: JSON.stringify(params), method: 'POST' });
// }

// export const scooterInstructions = async (params) => {
//   const url = `${gateway}instructions`;
//   return await appFetchCos({ url, body: JSON.stringify(params), method: 'POST' });
// }

export const getDeviceInfo = async () => {
  const url = `${host}devices/info`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser()});
}

export const getOperators = async () => {
  var url = `${host}operators/get_operators`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}
export const getOwnersAdmin = async () => {
  const url = `${host}owners/get_owners`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const ownerAssignDevices = async (params) => {
  const url = `${host}owner/assign_devices`;
  return await appFetch({ url, body: JSON.stringify(params), method: 'POST' , auth: auth.getAuthTokenUniverseUser()});
}

export const operatorAssignDevices = async (params) => {
  const url = `${host}operators/assign_devices`;
  return await appFetch({ url, body: JSON.stringify(params), method: 'POST' , auth: auth.getAuthTokenUniverseUser()});
}

export const scooterSetting = async(params) => {
  const url = `${other_url}settings`;
  return await dashFetchGate({ url, body: JSON.stringify(params), method: 'POST' });
}

export const scooterInstructions = async (params) => {
  const url = `${other_url}instructions`;
  return await dashFetchGate({ url, body: JSON.stringify(params), method: 'POST' })
}