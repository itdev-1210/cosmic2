import { appFetch,dashFetch } from './fetch';
//import host from './host';
import auth from './auth';

import {envEndpoint} from './../environment';
const host = envEndpoint

export const getAllRides = async () => {
  var url = `${host}ride/get_rides`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const getFilter = async (filter) => {
  var url = `${host}ride/get_rides?${filter}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const getNav = async(params) => {
  var url = `${host}ride/get_rides/${params}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}

export const endRide = async(id) => {
  const url = `${host}ride/end_ride_admin`;
  return await dashFetch({ url, body: JSON.stringify({id_service: id}), method: 'POST', auth: auth.getAuthTokenUniverseUser() });
}