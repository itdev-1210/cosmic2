import { dashFetch } from './fetch';
import auth from './auth';
import {envEndpoint} from './../environment';

export const getOrder = async (id_service) => {
  var url = `${envEndpoint}orders/detail_order/${id_service}`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}

export const getFailedOrders = async () => {
  var url = `${envEndpoint}orders/failed_orders`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}
export const getAllOrders = async () => {
  var url = `${envEndpoint}orders/get_orders`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const getNav = async(params) => {
  var url = `${envEndpoint}orders/get_orders/${params}`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}

export const getFilter = async (filter) => {
  var url = `${envEndpoint}orders/get_orders?${filter}`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const changeStatus = async(params) => {  
  const url = `${envEndpoint}orders/change_order`;
  return await dashFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(params), method: 'POST' });
}