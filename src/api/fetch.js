
import {envDash, envLogin} from './../environment';
import {logoutUser,forceLogoutUser} from './LoginApi';
import auth from './auth';

const headerLogin = envLogin
const headerDash  = envDash

export const parseResponse = (mode) => async (response) => {
  if (response.status === 204 || response.status === 401) {
    if(response.message==='Unauthorized'){
      await forceLogoutUser()
      window.location.href = '/auth/login'
    }
    return response
  }
  if (response.status >= 200 && response.status <= 500) {
    return mode === 'blob' ? response.blob() : response.json();
  }
  const error = new Error(response.statusText);
  //if (response.messages && response.messages.length > 0) error.message = response.messages[0];
  throw error;
};

export const parseResponseCos = (mode) => (response) => {
  return mode === 'blob' ? response.blob() : response.json();
};

export const appFetchLogin = (options) =>
  fetch(options.url, {
    headers: {
      Accept: 'application/json',
      [headerLogin]: options.auth,
      'Content-Type': 'application/json'
    },
    body: options.body,
    method: options.method
  })
    .then(parseResponse('json'))
    .then((response) => ({ response }))
    .catch((error) => {
      return error;
    });

export const appFetch = (options) =>
  fetch(options.url, {
    headers: {
      [headerDash]: options.auth,
      'user-type': auth.getUserType(),
      'Content-Type': 'application/json'
    },
    body: options.body,
    method: options.method
  })
    .then(parseResponse('json'))
    .then((response) => ({ response }))
    .catch((err) => {      
      return err;
    });

export const dashFetch = (options) =>
  fetch(options.url, {
    headers: {
      [headerDash]: options.auth,
      'user-type': auth.getUserType(),
      'Content-Type': 'application/json'
    },
    body: options.body,
    method: options.method
  })  
  .then((response) => { return response.json() })
  .then(async (response) => {
    if(response.status===401 && response.message==='Unauthorized'){
      await logoutUser()
      window.location.href = '/auth/login'
    }
    else{
      response.st=true
      return response
    }
  })
  .catch((err) => {
    return {st:false,err:err};
});
  
export const appFetchCsv = (options) =>
  fetch(options.url, {
    headers: {
      [headerDash]: options.auth,
      'user-type': auth.getUserType(),
      // 'Content-Type': 'multipart/form-data'
    },
    body: options.data,
    method: options.method
  })
  .then(parseResponse('json'))
  .then((response) => ({ response }))
  .catch((error) => {
    return error;
  });

export const appFetchCos = (options) =>
  fetch(options.url, {
      headers: {
        Accept: 'application/json',
        'Authorization': 'Api-Key ' + auth.getApiKey(),
        'Content-Type': 'application/json'
      },
      body: options.body,
      method: options.method
    })
    .then(parseResponseCos('json'))
    .then((response) => ({ response }))
    .catch((error) => {
      return error;
    });

export const dashFetchGate = (options) =>
  fetch(options.url, {
    headers: {
      Accept: 'application/json',
      'Authorization': 'Api-Key '+ auth.getApiKey(),
      'Content-Type': 'application/json'
    },
    body: options.body,
    method: options.method
  })  
  .then((response) => { return response.json() })
  .then(async (response) => {
    if(response.status===401 && response.message==='Unauthorized'){
      await logoutUser()
      window.location.href = '/auth/login'
    }
    else{
      response.st=true
      return response
    }
  })
  .catch((err) => {
    return {st:false,err:err};
});
  