import { DH_CHECK_P_NOT_PRIME } from "constants"

const getListSubRoutes = (roleObject) => {
    const numbers = roleObject.map((prop) => {
        if(prop.permission){
            return prop.id_submenu
        }
    });
    return numbers;
};

export const getListRoutes = (roleObject) => {
    let listSubRoutes = [], listSubRoute = [], types = [];
    const numbers = roleObject.map((role) => {
        const type = role[0].map((prop) => {
            return prop.id
        })
        types.push(type[0])
        const roles = role[1].map((prop) => {
            if(prop.submenu){
                listSubRoute.push(getListSubRoutes(prop.submenu))
            }        
            return prop.id_menu
        })
        listSubRoutes.push(listSubRoute)
        return roles
    });

    return {
        menu:numbers,
        submenu:listSubRoutes,
        types: types,
    }
};

export const getListRoles = (roleObject) => {
    const roles = roleObject.map((role) => {
        const type = role[0].map((prop) => {
            return prop.user_type
        })
        return type
    })

    return roles
}