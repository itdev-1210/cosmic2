import { appFetch, dashFetch } from './fetch';
import auth from './auth';
import moment from 'moment'

import {envEndpoint} from './../environment';
const host = envEndpoint

let chartInfo = {
    labelsChart : [],      
    dataChart :   [],
    maxValue : 0
}

export const canvasInfo = canvas => {
  let ctx = canvas.getContext("2d");

  let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

  gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
  gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
  gradientStroke.addColorStop(0, "rgba(29,140,248,0)");


  return {
    labels: chartInfo.labelsChart,
    datasets: [
      {
        label: "Total Revenue",
        fill: true,
        backgroundColor: gradientStroke,
        borderColor: "#1f8ef1",
        borderWidth: 2,
        borderDash: [],
        borderDashOffset: 0.0,
        pointBackgroundColor: "#1f8ef1",
        pointBorderColor: "rgba(255,255,255,0)",
        pointHoverBackgroundColor: "#1f8ef1",
        pointBorderWidth: 20,
        pointHoverRadius: 4,
        pointHoverBorderWidth: 15,
        pointRadius: 4,
        data: chartInfo.dataChart
      }
    ]
  };
}

export const getAnalyticsBy = async (filters) => {
  let url = `${host}analytics/home`;
  if(filters.filter!=='all' && filters.filter!=='date'){
    url += `?filter_date=${filters.filter}`;      
  }
  else if(filters.filter==='date'){
    if(filters.start!=='' && filters.end!=='')
      url+=`?filter_start_date=${filters.start}&filter_end_date=${filters.end}`
  }
  if(filters.country!=='0'){
    url+= `${filters.filter==='all'?'?':'&'}country=${filters.country}`
  }
      
  const response = await dashFetch({ url, method: 'GET', auth: auth.getAuthTokenUniverseUser()});
  if(response.st && response.status === 200){
    if(response['Time_revenue']){
      const labels =  await response['Time_revenue'].map(object =>{
        switch (filters.filter){
          case 'daily':
            return moment(object.date).format('Do H:mm')
          break
          case 'montly':
            return moment(object.date).format('MMM DD')
          break
          case 'annual':
            return moment(object.date).format('MMM YYYY')
          break
          case 'mtd':
            return moment(object.date).format('MMM DD')
          break
          case 'qtd':
            return moment(object.date).format('MMM DD')
          break
          case 'ytd':
            return moment(object.date).format('MMM DD')
          break
          default:
            return moment(object.date).format('DD-MM-YY')
          break
        }
      })
      const data = await response['Time_revenue'].map(object => object.revenue)
      chartInfo.labelsChart = labels.reverse()    
      chartInfo.dataChart = data.reverse()
      chartInfo.maxValue = await Math.max.apply(null,chartInfo.dataChart)
      return {valid:true,country:response['Countries_revenue']}
    }
  }
  else return {valid:false}
}
  

export const getScootersInfoBy = async (filter) => {
  let endPoint = "";  
  switch (filter){
    case 'operation':
      endPoint = 'devices_in_operation'
      break
    case 'onRide':
      endPoint = 'devices_on_ride'
      break
    case 'available':
      endPoint = 'devices_available'
      break
    case 'unavailable':
      endPoint = 'devices_unavailable'
      break
    default:
      break;
  }
  const url = `${host}analytics/${endPoint}`;  
  const response = await dashFetch({ url, method: 'GET', auth: auth.getAuthTokenUniverseUser()});
  return response
};

export const canvasOption = {
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    tooltips: {
      backgroundColor: "#f5f5f5",
      titleFontColor: "#333",
      bodyFontColor: "#666",
      bodySpacing: 4,
      xPadding: 12,
      mode: "nearest",
      intersect: 0,
      position: "nearest"
    },
    responsive: true,
    scales: {
      yAxes: [
        {
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: "rgba(29,140,248,0.0)",
            zeroLineColor: "transparent"
          },
          ticks: {
            suggestedMin: 0,
            suggestedMax: chartInfo.maxValue+10,
            padding: 20,
            fontColor: "#9a9a9a",
            callback: function(value, index, values) {
              return value+' USD';
          }
          }
        }
      ],
      xAxes: [
        {
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: "rgba(29,140,248,0.1)",
            zeroLineColor: "transparent"
          },
          ticks: {
            padding: 20,
            fontColor: "#9a9a9a"
          }
        }
      ]
    }
  };