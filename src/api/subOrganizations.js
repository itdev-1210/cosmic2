import { appFetch ,dashFetch} from './fetch';
import auth from './auth';

import {envEndpoint} from './../environment';

const host = envEndpoint

export const getInfoSubOrg = async () => {
    const url = `${host}operators/info`
    const response = await dashFetch({ url, method: 'GET', auth: auth.getAuthTokenUniverseUser()});
    return response
};

export const getSubOrg = async (info) => {
    const url = `${host}operators/get_operators`    
    const response = await dashFetch({ url, method: 'GET', auth: auth.getAuthTokenUniverseUser()});
    return response
};

export const getNavSubOrg = async (params) => {
    const url = `${host}operators/get_operators${params}`    
    const response = await dashFetch({ url, method: 'GET', auth: auth.getAuthTokenUniverseUser()});
    return response
}

export const createSubOrg = async (info) => {
    const url = `${host}operators/create`
    const response = await dashFetch({ url:url, body:JSON.stringify(info), method: 'POST', auth: auth.getAuthTokenUniverseUser()});
    return response
};

export const editSubOrg = async (info) => {
    const url = `${host}operators/update`    
    const response = await dashFetch({ url:url, body:JSON.stringify(info), method: 'POST', auth: auth.getAuthTokenUniverseUser()});
    return response
};

export const editSchedule = async (info) => {
    const url = `${host}operators/update_schedule`
    const response = await dashFetch({ url:url, body:JSON.stringify(info), method: 'POST', auth: auth.getAuthTokenUniverseUser()});
    return response
};

export const getStates = async (info) => {
    const url = `${host}operators/states`    
    const response = await dashFetch({ url, body:JSON.stringify(info), method: 'POST', auth: auth.getAuthTokenUniverseUser()});
    return response
};

export const getCities = async (info) => {
    const url = `${host}operators/city`    
    const response = await dashFetch({ url, body:JSON.stringify(info), method: 'POST', auth: auth.getAuthTokenUniverseUser()});
    return response
};

export const deleteSubOrg = async (info) => {
    const url = `${host}operators/delete`    
    const response = await appFetch({ url, body:JSON.stringify(info), method: 'POST', auth: auth.getAuthTokenUniverseUser()});
    return response
};

export const getDetailSubOrg = async (id) => {
    const url = `${host}operators/detail/${id}`
    return await dashFetch({url, auth: auth.getAuthTokenUniverseUser()});
};

export const getTeams = async (id_operator) => {
    const url = `${host}operators/team/${id_operator}`
    return await appFetch({url, auth: auth.getAuthTokenUniverseUser()});
}

export const changeAccount = async (info) => {
    const url = `${host}operators/update_status`
    const response = await appFetch({ url, body:JSON.stringify(info), method: 'POST', auth: auth.getAuthTokenUniverseUser()});
    return response
}