import { appFetch } from './fetch';
// import host from './host';
import auth from './auth';
import {envEndpoint} from './../environment';
const host = envEndpoint

export const createTeam = async (team) => {
  const url = `${host}Team`;
  return await appFetch({ url, body: JSON.stringify(team), method: 'POST' });
};

export const getTeams = async () => {
  const url = `${host}universe_user/get_all`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
};

export const getTeam = async (teamId) => {
  const url = `${host}Team/${teamId}`;
  return await appFetch({ url });
};

export const deleteTeam = async (teamId) => {
  const url = `${host}Team/${teamId}`;
  return await appFetch({ url, method: 'DELETE' });
};

export const getNav = async(params) => {
  var url = `${host}universe_user/get_all${params}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}

export const getFilter = async(params) => {
  var url = `${host}universe_user/get_all?${params}`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser() });
}

export const deleteAccount = async(params) => {
  var url = `${host}universe_user/delete`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(params), method: 'POST' });
}

export const changeAccount = async(params) => {
  var url = `${host}universe_user/change_status`;
  return await appFetch({ url, auth: auth.getAuthTokenUniverseUser(), body: JSON.stringify(params), method: 'POST' });
}