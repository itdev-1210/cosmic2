import { appFetch, appFetchLogin } from './fetch';
import {getListRoutes, getListRoles} from './../api/adminRoutes'

import auth from './auth';
import {envDash, envLogin, envEndpoint} from './../environment';

const headerLogin = envLogin
const headerDash  = envDash
const host        = envEndpoint

export const AuthTokenLogin = async () => {
  const url = `${host}login/universe/auth_token_login`;
  const response = await appFetchLogin({ url, method: 'GET' });
  auth.setAuthToken(response.response[headerLogin]);
  return response;
};

export const loginUniverseUser = async (user) => {
  const url = `${host}login/universe_user`;
  const response = await appFetchLogin({url, body: JSON.stringify(user), method: 'POST', auth: auth.getAuthToken() });

  if(response.response.status === 201){
    auth.setUUID(response.response.uuid);
    auth.setLastDigits(response.response.last_four);
    auth.removeAuthTokenUniverseUser();
    localStorage.setItem('login',true)
  }
  else if(response.response.status === 200){
    if(response.response.operator) localStorage.setItem('operator', response.response.operator.name)
    auth.setUUID(response.response.user.uuid);
    auth.setAuthTokenUniverseUser(response.response[headerDash]);
    auth.setOrganization(response.response.user.id_organization)
    auth.setApiKey(response.response["api_key"])    
    auth.setUserInfo(response.response.user)
    auth.setUserRole(response.response.roles[0][0][0].role)
    auth.setRoles(response.response.roles)
    const objectRoutes = await getListRoutes(response.response.roles)
    const objectRoles = await getListRoles(response.response.roles)    
    localStorage.setItem('user_menu', objectRoles)
    localStorage.setItem('role_type', objectRoutes.types)
    localStorage.setItem('menu', objectRoutes.menu[objectRoutes.types.findIndex(type => type === 1)])
    localStorage.setItem('submenu', objectRoutes.submenu[objectRoutes.types.findIndex(type => type === 1)])
    localStorage.setItem('op_menu', objectRoutes.menu[objectRoutes.types.findIndex(type => type === 2)])
    localStorage.setItem('op_submenu', objectRoutes.submenu[objectRoutes.types.findIndex(type => type === 2)])
    localStorage.setItem('ow_menu', objectRoutes.menu[objectRoutes.types.findIndex(type => type === 3)])
    localStorage.setItem('ow_submenu', objectRoutes.submenu[objectRoutes.types.findIndex(type => type === 3)])    
    auth.removeAuthToken()
    auth.setUserType(objectRoutes.types[0])
    auth.setBrand(response.response.organization)
    auth.removeAuthToken()
    localStorage.removeItem('login')

  }
  return response;
};

export const verifySMSCode = async (code) => {
  const url = `${host}login/universe_user/verify_sms_code`;
  const body = { uuid: auth.getUUID(), token: code };
  const response = await appFetchLogin({ url, body: JSON.stringify(body), method: 'POST', auth: auth.getAuthToken() });
  if(response.response.status === 200){
    if(response.response.operator) localStorage.setItem('operator', response.response.operator.name)
    auth.setUUID(response.response.user.uuid);    
    auth.setAuthTokenUniverseUser(response.response[headerDash]);
    auth.setOrganization(response.response.user.id_organization)
    auth.setApiKey(response.response["api_key"])
    auth.setUserInfo(response.response.user)
    auth.setUserRole(response.response.user.role)
    auth.setRoles(response.response.roles)
    const objectRoutes = await getListRoutes(response.response.roles)
    const objectRoles = await getListRoles(response.response.roles)
    localStorage.setItem('user_menu', objectRoles)
    localStorage.setItem('role_type', objectRoutes.types)    
    localStorage.setItem('menu', objectRoutes.menu[objectRoutes.types.findIndex(type => type === 1)])
    localStorage.setItem('submenu', objectRoutes.submenu[objectRoutes.types.findIndex(type => type === 1)])
    localStorage.setItem('op_menu', objectRoutes.menu[objectRoutes.types.findIndex(type => type === 2)])
    localStorage.setItem('op_submenu', objectRoutes.submenu[objectRoutes.types.findIndex(type => type === 2)])
    localStorage.setItem('ow_menu', objectRoutes.menu[objectRoutes.types.findIndex(type => type === 3)])
    localStorage.setItem('ow_submenu', objectRoutes.submenu[objectRoutes.types.findIndex(type => type === 3)])
    auth.setUserType(objectRoutes.types[0])
    auth.setBrand(response.response.organization)
    auth.removeAuthToken()
    localStorage.removeItem('login')
  }
  return response;
};

export const requestSMSCode = async () => {
  const url = `${host}login/universe_user/request_sms_code`;
  const body = { uuid: auth.getUUID() };
  const response = await appFetchLogin({ url, body: JSON.stringify(body), method: 'POST', auth: auth.getAuthToken() });
  if(response.response.status === 201){
    auth.setUUID(response.response.uuid);
    auth.setLastDigits(response.response.last_four);
    auth.removeAuthTokenUniverseUser();
  }
  return response;
}

export const logoutUser = async () => {
  const url = `${host}logout/universe_user`;
  localStorage.clear()
  auth.removeApiKey();
  auth.removeOrganization();
  auth.removeLastDigits();
  auth.removeRole();
  auth.removeUserInfo()
  auth.removeOperator()
  auth.removeUserName()
  const response = await appFetch({ url, body: JSON.stringify({uuid: auth.getUUID()}), method: 'POST', auth: auth.getAuthTokenUniverseUser()});
  auth.removeAuthTokenUniverseUser();
  auth.removeUUID();
  auth.removeBrand();
  auth.removeUserType();
  localStorage.clear()
  return response
}
export const forceLogoutUser = async () => {  
  localStorage.clear()
  auth.removeApiKey();
  auth.removeOrganization();
  auth.removeLastDigits();
  auth.removeRole();
  auth.removeUserInfo()
  auth.removeOperator()
  auth.removeUserName()
  // const response = await appFetch({ url, body: JSON.stringify({uuid: auth.getUUID()}), method: 'POST', auth: auth.getAuthTokenUniverseUser()});
  auth.removeAuthTokenUniverseUser();
  auth.removeUUID();
  auth.removeBrand();
  auth.removeUserType();
  localStorage.clear()
  // return response
}

export const recoveryPassword = async(data) => {
  const url = `${host}universe_user/request_recovery_password`;
  const response = await appFetchLogin({ url, body: JSON.stringify(data), method: 'POST', auth: auth.getAuthToken() });
  return response
}

export const recoveryPasswordView = async (token) => {
  const url = `${host}universe_user/recovery_password/${token}`;
  const response = await appFetchLogin({ url, method: 'GET'});
  return response
}

export const recoverPasswordLogin = async(data) => {
  const url = `${host}universe_user/recovery_password`;
  const response = await appFetchLogin({ url, body: JSON.stringify(data), method: 'POST', auth: auth.getAuthToken()});
  return response
}