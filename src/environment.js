import auth from './api/auth';

export const  environment = (function() {
    function getEnvEndpoint () {
        const environment = process.env.REACT_APP_ENV
        if(environment === 'Dev'){
          return 'https://dev-api.cosmicgo.co/api/v2/'
        }else if(environment === 'QA'){
          return 'https://qa-api.cosmicgo.co/api/v2/'
        }else if(environment === 'Prod'){
          return 'https://api.cosmicgo.co/api/v2/'
        }else{
          console.log('Default endpoint');
          return 'https://dev-api.cosmicgo.co/api/v2/'
          // return 'https://qa-api.cosmicgo.co/api/v2/'
        }
      }
    
      function getLogo () {
        const environment = process.env.REACT_APP_ENV
        if(environment === 'Dev'){
          return 'https://csmc-asset.s3-us-west-2.amazonaws.com/Web+Assets/Dashboard/Cosmic-Dev.png'
        }else if(environment === 'QA'){
          return 'https://csmc-asset.s3-us-west-2.amazonaws.com/Web+Assets/Dashboard/Cosmic-QA.png'
        }else if(environment === 'Prod'){
          return 'https://csmc-asset.s3-us-west-2.amazonaws.com/Web+Assets/Dashboard/Cosmic-Prod.png'
        }else{
          console.log('Default flavors');
          return 'https://csmc-asset.s3-us-west-2.amazonaws.com/Web+Assets/Dashboard/Cosmic-Dev.png'
        }
      }
  
    function getGateway () {
        const environment = process.env.REACT_APP_ENV
        if(environment === 'Dev'){
          return 'https://dev-gateway.cosmicgo.co'
        }else if(environment === 'QA'){
          return 'https://qa-gateway.cosmicgo.co'
        }else if(environment === 'Prod'){
          return 'https://gateway.cosmicgo.co'
        }else{
          console.log('Default gateway');
          return 'https://dev-gateway.cosmicgo.co'
          // return 'https://qa-gateway.cosmicgo.co'
        }
    }
  
    return {
      getEnvEndpoint:getEnvEndpoint,
      getGateway:getGateway,
      getLogo:getLogo
    };
  })();

export const gateway       = environment.getGateway()
export const logo          = environment.getLogo()
export const envEndpoint   = environment.getEnvEndpoint()
export const captcha       = '6LdOWMAUAAAAAOeEie44VgCgyQ3j8_UsbhwOTJ7o'
export const apiKey        = auth.getApiKey()
export const envDash       = auth.headerDash
export const envLogin      = auth.headerLogin