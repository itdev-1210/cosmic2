import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from 'history';
import { Router, Route, Switch, Redirect } from 'react-router-dom';

import AuthLayout from 'layouts/Auth/Auth.jsx';
import AdminLayout from 'layouts/Admin/Admin.jsx';
import RTLLayout from 'layouts/RTL/RTL.jsx';
import auth from './api/auth';

import 'assets/css/nucleo-icons.css';
import 'assets/scss/black-dashboard-pro-react.scss?v=1.0.0';
import 'assets/scss/cosmic.scss';
import 'assets/demo/demo.css';
import 'react-notification-alert/dist/animate.css';

const hist = createBrowserHistory();
auth.hasLogin()

ReactDOM.render(
  <Router history={hist}>
    <Switch>
      <Route path="/auth" render={(props) => <AuthLayout {...props} />} />
      <Route path="/admin" render={(props) => <AdminLayout {...props} />} />
      <Route path="/rtl" render={(props) => <RTLLayout {...props} />} />
      <Redirect from="/" to="/auth/login"/>
    </Switch>
  </Router>,
  document.getElementById('root')
);

if(process.env.REACT_APP_ENV === 'Dev' || process.env.REACT_APP_ENV === 'QA' || process.env.NODE_ENV === 'development'){
  (function() { var s = document.createElement("script"); s.type = "text/javascript"; s.async = true; s.src = '//api.usersnap.com/load/0b31d4ed-820f-476b-b787-0a14f076c17d.js';
  var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); })();
}

    
