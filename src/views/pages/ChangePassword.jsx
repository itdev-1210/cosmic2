import React, { Component } from 'react';
import './modal.scss'
import {
  Button,
  Label,
  Form,
  FormGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
} from 'reactstrap';
class ChangePassword extends Component {

  constructor(props) {
    super(props);
    this.state = {
      passwordState: "",
      confirmState: "",
      password: "",
      confirm: "",
      type: 'password'
    };
  }

  changeValue = (e) => {
    const name = e.target.name, value = e.target.value.trim()
    let valid="",obj = {}
    switch(name) {
      case "password":
        if (value === '')
          valid='has-danger'
        break;
      case 'confirm':
        if (value !== this.state.password)
          valid='has-danger'
        break;
      default:
        break
    }    
    obj[name] = value
    obj[name+'State'] = valid
    this.setState(obj)
  }
  validate = () => {
    const {password,confirm} = this.state
    let obj = {}
    if (password !== '' && password === confirm && password.length>=8)
      return true;
    if (this.state.password === '') obj['passwordState'] = 'has-danger'
    else if (password.length<8) obj['passwordState'] = 'has-danger'
    if (this.state.confirm !== this.state.password) obj['confirmState'] = 'has-danger'
    this.setState(obj, ()=>{return false})
  }
  onSave = async (event) => {
    event.preventDefault();
    const res = await this.validate()
    if (res) this.props.onSave(this.state.password)    
  }
  showHide = () => {
    this.setState({
      type: this.state.type === 'input' ? 'password' : 'input'
    })
  }
  render() {
    const { onClose } = this.props
    const { password, confirm, passwordState, confirmState } = this.state
    return (
      <div className="change-password">
        <Form onSubmit={this.onSave}>
          <div className="change-title">
            <span className="title">{'Change password'}</span>
          </div>
          <div className="password-group">
            <Row>
              <Label sm="4" className="password-label">New password</Label>
              <Col sm="8">
                <FormGroup className={passwordState}>
                  <InputGroup>
                    <Input placeholder="Password" name="password" type={this.state.type} value={password} onChange={this.changeValue} onBlur={this.changeValue} maxLength={12}/>
                    <InputGroupAddon addonType="append" onClick={this.showHide}>
                      <InputGroupText>
                        {this.state.type === 'input' ? <i className="fa fa-eye-slash" /> : <i className="fa fa-eye" />}
                      </InputGroupText>
                    </InputGroupAddon>
                  </InputGroup>
                  {this.state.passwordState === "has-danger" ?
                    <label className="error">
                      Your password must have at least 8 characters
                    </label>
                  :null}
                </FormGroup>
              </Col>
            </Row>

            <Row>
              <Label sm="4" className="password-label">Confirm password</Label>
              <Col sm="8">
                <FormGroup className={confirmState}>
                  <InputGroup>
                    <Input placeholder="Re enter password" name="confirm" type={this.state.type} value={confirm} onChange={this.changeValue} onBlur={this.changeValue} maxLength={12}/>
                    <InputGroupAddon addonType="append" onClick={this.showHide}>
                      <InputGroupText>
                        {this.state.type === 'input' ? <i className="fa fa-eye-slash" /> : <i className="fa fa-eye" />}
                      </InputGroupText>
                    </InputGroupAddon>
                  </InputGroup>
                  {this.state.confirmState === "has-danger" ? (
                    <label className="error">
                      This field is required.
                    </label>
                  ) : null}
                </FormGroup>
              </Col>
            </Row>
          </div>
          <div className="change-desc">
            <span className="change-description">{'This action requires additional privileges'}</span>
          </div>
          <Row className='cosmic-butons butonsUpdateOpe'>
            <Button className="cosmic-cancel" onClick={onClose}>Cancel</Button>
            <Button className="cosmic-confirm" type="submit">Change password</Button>
          </Row>
        </Form>
      </div>
    )
  }
}

export default ChangePassword