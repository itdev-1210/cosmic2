import React from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';
import { ToastContainer, toast } from 'react-toastify'
import {
  Button,
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  CardFooter,
  Label,
  FormGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
} from 'reactstrap';
// import FileDrop from 'react-file-drop'
import Modal from 'react-responsive-modal';
import ChangePassword from './ChangePassword'
import ConfirmModal from './ConfirmModal'
import './styles.scss'
import auth from '../../api/auth';
import { createUser, editUser, getUser, getAllStates, getAllCities ,changePass,getInfo, changeOwnPass, createOperator,getInfoToOp} from '../../api/member'
import ResponsiveModal from 'react-responsive-modal';
import LoadingModal from './../../components/Navbars/LogoutModal';

class AddMember extends React.Component {
  constructor(props) {
    super(props);
    this.valid = {
      first_name_state: "",
      last_name_state: "",
      email_state: "",
      indicative_state: "",
      phone_state: "",
      street_state: "",
      zip_code_state: "",
      id_city_state: "",
      id_country_state: "",
      id_state_state: "",
      id_role_state: "",
      password_state: "",
      confirm_state: "",
    }
    this.state = {
      alert:null,
      is_loading: true,
      first_name:"",
      last_name:"",
      email:"",
      indicative:"",
      phone:"",
      street:"",
      zip_code:"",
      id_country:"",
      id_state:"",
      id_city:"",
      id_role:"",
      password:"",
      confirm: "",
      use_2fa:false,

      countries: [],
      states: [],
      cities: [],
      roles: [],
      
      new_password: '',
      digit_pass:'',
      uuid: null,

      true: false,
      showModal: false,
      confirmModal: false,      
      userRole:true,
      type: 'password'
    };
    this.changePassword = this.changePassword.bind(this)    
    this.getAllStates = this.getAllStates.bind(this)
    this.getAllCities = this.getAllCities.bind(this)
    this.changeValue = this.changeValue.bind(this)
    this.onSave = this.onSave.bind(this)
    this.authenticate = this.authenticate.bind(this)
  }
  handleDrop = (files, event) => {
    // console.log(files, event);
  }
  onCloseModal = () => {
    this.setState({ showModal: false });
  };
  changePassword() {
    this.setState({ showModal: true });
  }
  onCloseConfirmModal = () => {
    this.setState({ confirmModal: false });
  };
  changeConfirmPassword = (new_password) => {
    this.setState({ showModal: false, confirmModal: true, new_password: new_password });
  }

  showHide = () => {
    this.setState({
      type: this.state.type === 'input' ? 'password' : 'input'
    })
  }

  validForm = () =>{
    const { first_name,last_name,email,indicative,phone,street,zip_code,id_country,
      id_state,id_city,id_role,password,confirm,uuid} = this.state 
    let valid = true

    if (first_name === '') {
      this.valid.first_name_state = 'has-danger'
      valid = false
    }
    else{this.valid.first_name_state =''}
    if (last_name === '') {
      this.valid.last_name_state = 'has-danger'
      valid = false
    }
    else{this.valid.last_name_state =''}
    if (email === '') {
      this.valid.email_state = 'has-danger'
      valid = false
    }
    else{this.valid.email_state =''}
    
    if (indicative==='' || phone === '') {
      this.valid.phone_state = 'has-danger'
      valid = false
    }
    else{this.valid.phone_state =''}

    if (street === '') {
      this.valid.street_state = 'has-danger'
      valid = false
    }
    else{this.valid.street_state =''}
    if (zip_code === '') {
      this.valid.zip_code_state = 'has-danger'
      valid = false
    }
    else{this.valid.zip_code_state =''}
    if (id_city === '') {
      this.valid.id_city_state = 'has-danger'
      valid = false
    }
    else{this.valid.id_city_state =''}
    if (id_country === '') {
      this.valid.id_country_state = 'has-danger'
      valid = false
    }
    else{this.valid.id_country_state =''}
    if (id_state === '') {
      this.valid.id_state_state = 'has-danger'
      valid = false
    }
    else{this.valid.id_state_state =''}
    if (id_role === '') {
      this.valid.id_role_state = 'has-danger'
      valid = false
    }
    else{this.valid.id_role_state =''}
    if (!uuid){      
      if (password === '') {
        this.valid.password_state = 'has-danger'
        valid = false
      }
      else if(password.length<8){
        toast.error('Password length must be 8 to 12 characters', {position: toast.POSITION_TOP_RIGHT})
        valid = false
      }
      if (confirm !== password) {
        this.valid.confirm_state = 'has-danger'
        toast.error('Passwords do not match', {position: toast.POSITION_TOP_RIGHT})
        valid = false
      }
    }
    return valid
  }

  async onSave() {
    const { first_name,last_name,email,indicative,phone,street,zip_code,id_country,
      id_state,id_city,id_role,password,uuid,use_2fa} = this.state 
    let valid = false
    valid = this.validForm()
    if (valid) {
      this.setState({is_loading: true}, async ()=>{
        let userObject = {
          first_name: first_name,
          last_name: last_name,
          email: email,
          indicative: indicative,
          phone: phone,
          address: street,
          zip_code: zip_code,
          id_country: id_country,
          id_state: id_state,
          id_city: id_city,
          id_role: id_role,
          use_2fa: use_2fa
        }
        if (uuid) {
          userObject['uuid']=uuid
          const res = await editUser(userObject)
          if (res.status !== 200) {
            toast.error(res.message, {position: toast.POSITION_TOP_RIGHT})
            this.setState({
              is_loading: false
            })
          }
          else{
            this.setState({
              is_loading: false,
              alert: <SweetAlert success title="User has been Update" onConfirm={this.closeAlert}></SweetAlert>
            })
          }
          // this.setState({is_loading: false}, ()=>{
          //   if (res.status !== 200) toast.error(res.message, {position: toast.POSITION_TOP_RIGHT})
          //   else {
          //     toast.success(res.message, {position: toast.POSITION_TOP_RIGHT})
          //     if(this.props.match.params.id_operator){
          //       this.props.history.push(`/admin/see-team/${this.props.match.params.id_operator}`)
          //     }
          //     else{this.props.history.push('/admin/admin-team')}
          //   }
          // })
        } else if (this.props.match.params.id_operator) {
          userObject['id_operator'] = this.props.match.params.id_operator
          userObject['password']=password
          const res = await createOperator(userObject)
          this.setState({is_loading: false}, ()=>{
            if (res.status !== 201) toast.error(res.message, {position: toast.POSITION_TOP_RIGHT})
            else {
              toast.success(res.message, {position: toast.POSITION_TOP_RIGHT})          
              this.props.history.push('/admin/admin-operator')
            }
          })
        } else {
          userObject['password']=password
          const res = await createUser(userObject)
          if (res.status !== 201) {
            toast.error(res.message, {position: toast.POSITION_TOP_RIGHT})
            this.setState({
              is_loading: false
            })
          }
          else{
            this.setState({
              is_loading: false,
              alert: <SweetAlert success title="User has been Create" onConfirm={this.closeAlert}></SweetAlert>
            })
          }
        }
      })
    }else{
      this.setState({ state: this.state });      
    }
  }
  
  closeAlert = () =>{
    this.props.history.goBack()
  }

  authenticate = async (res) => {
    const {new_password,uuid} = this.state
    if (res.response.status !== 200)
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    else {  
      let resChange
      if(uuid === auth.getUUID()){
        resChange = await changeOwnPass({uuid:uuid,password:res.oldpass,new_password:new_password})
      }else{
        resChange = await changePass({uuid:uuid,password:new_password})
      }
      if(resChange.st && resChange.status===200){
        toast.success('Password Update', {position: toast.POSITION_TOP_RIGHT})
      }
      else{
        toast.error('Error to change password', {position: toast.POSITION_TOP_RIGHT})
      }
      this.setState({confirmModal: false})
    }
  }
  
  changeValue(e) {    
    var obj = {}
    var name = e.target.name, value = e.target.value    
    if(name==='use_2fa') obj[name] = e.target.checked
    else obj[name] = value?value.trim():value
    this.valid[name+'_state']=''
    this.setState(obj, ()=>{
      if(name==='id_country' && value !=='') {
        this.getAllStates(value)
      }
      if(name==='id_state' && value !=='') {
        this.getAllCities(value)
      }
    })
  }

  async getAllStates(id_country) {
    const res = await getAllStates({id_country: id_country})
    if (res.status === 200 && res.st) {
      var states = res.States
      // states.sort((a,b)=>(a.state > b.state) ? 1 : ((b.state > a.state) ? -1 : 0))      
      this.setState({states: states ? states : []})
    }
  }

  async getAllCities(id_state) {
    const res = await getAllCities({id_state: id_state})
    if (res.status === 200 && res.st) {
      var cities = res.Cities
      // cities.sort((a,b)=>(a.city > b.city) ? 1 : ((b.city > a.city) ? -1 : 0))      
      this.setState({cities: cities ? cities : []})
    }
  }

  componentDidMount() {
    this.getInitInfo()
    document.body.classList.toggle('add-member');
  }

  componentWillUnmount() {    
    document.body.classList.toggle('add-member');
  }

  getInitInfo = async () =>{
    let initial={}
    if (this.props.match.params.id_operator) {
      initial = await getInfoToOp()
    }else{
      initial = await getInfo()
    }
    const { match: { params } } = this.props;
    if (params.uuid) {      
      const res = await getUser(params.uuid)
      if (res.status === 200 && res.st){
        const user = res.user
        if (user.id_country) await this.getAllStates(user.id_country)
        if (user.id_state) await this.getAllCities(user.id_state)
        this.setState({
          is_loading: false,
          uuid:params.uuid,
          first_name: user.first_name ? user.first_name : '',
          last_name: user.last_name ? user.last_name : '',
          email: user.email ? user.email : '',
          street: user.address ? user.address : '',
          zip_code: user.zip_code ? user.zip_code : '',
          indicative: user.indicative ? user.indicative : '',
          phone: user.phone ? user.phone : '',
          id_country: user.id_country ? user.id_country :'',
          id_state: user.id_state ? user.id_state :'',
          id_city: user.id_city ? user.id_city :'',
          id_role: user.id_role ? user.id_role :'',
          countries:initial.Countries?initial.Countries:[],
          roles:initial.Roles?initial.Roles:[],
        })
      }
      else{
        toast.error(res.message, {position: toast.POSITION_TOP_RIGHT})
      }
    }
    else{
      if(initial.st && initial.status===200){
        this.setState({
          is_loading: false,
          countries:initial.Countries,
          roles:initial.Roles,
        })
      }
      else{
        toast.error(initial.message, {position: toast.POSITION_TOP_RIGHT})
      }
    }  
  }
 
  render() {
    const { first_name,last_name,email,indicative,phone,street,zip_code,id_country,
    id_state,id_city,id_role,password,confirm,countries,cities,states,roles,uuid,userRole,use_2fa} = this.state
    // const {first_name_state,last_name_state,email_state,phone_state,street_state,zip_code_state,
    //   id_country_state,id_state_state,id_city_state,id_role_state,password_state,confirm_state,} = this.valid.

    return (
      <React.Fragment>  
        {this.state.alert} 
        <Row className="content" id="add-member">
          <ResponsiveModal open={this.state.is_loading} 
            onClose={()=>{}} center 
            classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
            <LoadingModal title="Loading..."/>
          </ResponsiveModal>   
          <Col md='12'>
            <CardHeader className="management-header">
              <Col md='4'>
                <CardTitle tag="h4" onClick={()=>{this.props.history.push('/admin/admin-team')}}>
                  <img alt="" src={require("assets/img/icon_arrow.svg")}/>{uuid ? "Edit Member" : "Add Member"}
                </CardTitle>
              </Col>  
            </CardHeader>            
          </Col>
          <Col md="12">
            <Card>
              <Row>
                <Col md="9">
                  <CardBody>
                    <Row>
                      <Label sm="2">Profile</Label>
                    </Row>
                    <Row>
                      <Label sm="2">First Name</Label>
                      <Col sm="4">
                        <FormGroup className={this.valid.first_name_state}>
                          <Input
                            name="first_name"
                            type="text"
                            placeholder="Daniel"
                            value={first_name}
                            onChange={this.changeValue}
                            onBlur={this.changeValue}
                            disabled={ userRole ? false : true }
                          />
                          {this.valid.first_name_state === "has-danger" ? (
                            <label className="error">
                              This field is required.
                            </label>
                          ) : null}
                        </FormGroup>
                      </Col>
                      <Label sm="2">Last Name</Label>
                      <Col sm="4">
                        <FormGroup className={this.valid.last_name_state}>
                          <Input
                            name="last_name"
                            type="text"
                            placeholder="Rogers"
                            value={last_name}
                            onChange={this.changeValue}
                            onBlur={this.changeValue}
                            disabled={ userRole ? false : true }
                          />
                          {this.valid.last_name_state === "has-danger" ? (
                            <label className="error">
                              This field is required.
                            </label>
                          ) : null}
                        </FormGroup>
                      </Col>
                    </Row>

                    <Row>
                      <Label sm="2">Email</Label>
                      <Col sm="4">
                        <FormGroup className={this.valid.email_state}>
                          <Input
                            name="email"
                            type="email"
                            placeholder="danielrogers@mail.com"
                            value={email}
                            onChange={this.changeValue}
                            onBlur={this.changeValue}
                            disabled={ userRole ? false : true }
                          />
                          {this.valid.email_state === "has-danger" ? (
                            <label className="error">
                              This field is required.
                            </label>
                          ) : null}
                        </FormGroup>
                      </Col>
                      <Label sm={2}>Mobile</Label>
                      <Col sm={4}>
                        <FormGroup className={this.valid.phone_state+' phone-cosmic'}>
                          <Input 
                            className='phone-cosmic-1'
                            name='indicative'
                            type="select"
                            value={indicative}
                            onChange={this.changeValue}
                          >
                            <option value=''>-Country-</option>
                            {countries.map((country, index) => <option key={index} value={country.indicative}>{country.country}</option>)}
                          </Input >   
                          <Input
                            className='phone-cosmic-2'
                            type="text"
                            disabled 
                            placeholder="+00"
                            value={indicative} />
                          <Input                      
                            className='phone-cosmic-3'
                            name='phone'
                            type="text"
                            maxLength={15}
                            placeholder="3114511876"
                            value={phone}
                            onChange={this.changeValue}
                          />
                        </FormGroup>
                        {this.valid.phone_state === "phone-error" ? (
                          <label className="error">
                            This field is required.
                          </label>
                        ) : null}
                      </Col>
                    </Row>
                  </CardBody>

                  <CardBody>
                    <Row>
                      <Label sm="2">Address</Label>
                    </Row>

                    <Row>
                      <Label sm="2">Country</Label>
                      <Col sm="4">
                        <FormGroup className={this.valid.id_country_state}>
                          <Input
                            name="id_country"
                            type="select"
                            value={id_country}
                            onChange={this.changeValue}
                            onBlur={this.changeValue}
                            disabled={ userRole ? false : true }
                          >
                          <option key={0} value='' className="dark-back">{'--Select Country--'}</option>
                          {countries.map((country, index) => <option key={index} value={country.id_country} className="dark-back">{country.country}</option>)}
                          </Input>
                          {this.valid.id_country_state === "has-danger" ? (
                            <label className="error">
                              This field is required.
                            </label>
                          ) : null}
                        </FormGroup>
                      </Col>
                      <Label sm="2">State</Label>
                      <Col sm="4">
                        <FormGroup className={this.valid.id_state_state}>
                          <Input
                            name="id_state"
                            type="select"
                            value={id_state}
                            onChange={this.changeValue}
                            onBlur={this.changeValue}
                            disabled={ userRole ? false : true }
                          >
                            <option key={0} value='' className="dark-back">{'--Select State--'}</option>
                          {states.map((state, index) => <option key={index} value={state.id_state} className="dark-back">{state.state}</option>)}
                          </Input>
                          {this.valid.id_state_state === "has-danger" ? (
                            <label className="error">
                              This field is required.
                            </label>
                          ) : null}
                        </FormGroup>
                      </Col>
                    </Row>

                    <Row>
                      <Label sm="2">City</Label>
                      <Col sm="4">
                        <FormGroup className={this.valid.id_city_state}>
                          <Input
                            name="id_city"
                            type="select"
                            value={id_city}
                            onChange={this.changeValue}
                            onBlur={this.changeValue}
                            disabled={ userRole ? false : true }
                          >
                            <option key={0} value='' className="dark-back">{'--Select City--'}</option>
                          {cities.map((city, index) => <option key={index} value={city.id_city} className="dark-back">{city.city}</option>)}
                          </Input>
                          {this.valid.id_city_state === "has-danger" ? (
                            <label className="error">
                              This field is required.
                            </label>
                          ) : null}
                        </FormGroup>
                      </Col>
                      <Label sm="2">Street</Label>
                      <Col sm="4">
                        <FormGroup className={this.valid.street_state}>
                          <Input
                            name="street"
                            type="text"
                            placeholder="Text"
                            value={street}
                            onChange={this.changeValue}
                            onBlur={this.changeValue}
                            disabled={ userRole ? false : true }
                          />
                          {this.valid.street_state === "has-danger" ? (
                            <label className="error">
                              This field is required.
                            </label>
                          ) : null}
                        </FormGroup>
                      </Col>
                    </Row>

                    <Row>
                      <Label sm="2">Zip Code</Label>
                      <Col sm="4">
                        <FormGroup className={this.valid.zip_code_state}>
                          <Input
                            name="zip_code"
                            type="text"
                            placeholder="Text"
                            value={zip_code}
                            onChange={this.changeValue}
                            onBlur={this.changeValue}
                            disabled={ userRole ? false : true }
                          />
                          {this.valid.zip_code_state === "has-danger" ? (
                            <label className="error">
                              This field is required.
                            </label>
                          ) : null}
                        </FormGroup>
                      </Col>
                    </Row>
                  </CardBody>
                </Col>
                {/* <Col md="4">
                { uuid ? null : (
                  <div className="drag-drop">
                    <FileDrop onDrop={this.handleDrop} className="drop-class">
                      <img src={require("assets/img/drag_drop.svg")} alt=""/>
                    </FileDrop>
                    <span className="drop_desc">Drop files anywhere to upload</span>
                    <div className="select_file">SELECT FILES</div>
                    <span className="drop_max">Maximum upload file size: 2MB</span>
                    <span className="drop_dimension">Suggested image dimensions: 512*512</span>
                  </div>
                )}
                </Col> */}
              </Row>
            </Card>
          </Col>
          <Col>
            <Card>
              <CardBody>
                <Row>
                  <Col md="6">
                    <Row>
                      <Label sm="12" className={userRole?'':'disable-color'}>Choose a role</Label>
                    </Row>
                    <Row>
                      <Label sm="12" className={userRole?'sub-description':'sub-description disable-color'}>The access to information will be visible to the user, based on the role assigned.</Label>
                    </Row>
                    <Row>
                      <Label sm="4" className={userRole?'':'disable-color'}>Role</Label>
                      <Col sm="8">
                        <FormGroup className={this.valid.id_role_state} >
                          <Input
                            name="id_role"
                            type="select"
                            disabled={userRole?false:true}
                            value={id_role}
                            onChange={this.changeValue}
                            onBlur={this.changeValue}
                          >
                            <option key={0} value='' className="dark-back">--Select Role--</option>
                          {roles.map((role, index) => <option key={index} value={role.id_role} className="dark-back">{role.role}</option>)}
                          </Input>
                          {this.valid.id_role_state === "has-danger" ? (
                            <label className="error">
                              This field is required.
                            </label>
                          ) : null}
                        </FormGroup>
                      </Col>
                    </Row>
                    {uuid ? (
                      <Row>
                        <Col sm="2">
                          <div className="change_password_btn" onClick={this.changePassword}>Change Password...</div>
                        </Col>
                      </Row>
                    ) : (
                    <span>
                    <Row>
                      <Label sm="4">Password</Label>
                      <Col sm="8">
                        <FormGroup className={this.valid.password_state}>
                          <InputGroup>
                            <Input placeholder="Password" name="password" type={this.state.type} value={password} onChange={this.changeValue} onBlur={this.changeValue} maxLength={12}/>
                            <InputGroupAddon addonType="append" onClick={this.showHide}>
                              <InputGroupText>
                                {this.state.type === 'input' ? <i className="fa fa-eye-slash" /> : <i className="fa fa-eye" />}
                              </InputGroupText>
                            </InputGroupAddon>
                          </InputGroup>
                          {this.valid.password_state === "has-danger" ? (
                            <label className="error">
                              This field is required.
                            </label>
                          ) : null}
                        </FormGroup>
                      </Col>
                    </Row>

                    <Row>
                      <Label sm="4">Confirm password</Label>
                      <Col sm="8">
                        <FormGroup className={this.valid.confirm_state}>
                          <InputGroup>
                            <Input placeholder="Re enter password" name="confirm" type={this.state.type} value={confirm} onChange={this.changeValue} onBlur={this.changeValue} maxLength={12}/>
                            <InputGroupAddon addonType="append" onClick={this.showHide}>
                              <InputGroupText>
                                {this.state.type === 'input' ? <i className="fa fa-eye-slash" /> : <i className="fa fa-eye" />}
                              </InputGroupText>
                            </InputGroupAddon>
                          </InputGroup>
                          {this.valid.confirm_state === "has-danger" ? (
                            <label className="error">
                              Passwords do not match.
                            </label>
                          ) : null}
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>                      
                      <Col sm="12">
                        <FormGroup check className="mt-3">
                          <Label check>
                            <Input 
                              type="checkbox"
                              name="use_2fa"
                              checked={use_2fa}
                              onChange={this.changeValue}
                            />
                            <span className="form-check-sign" />
                            Two-step authentication          
                            <p>Keep your account extra secure with a second authentication step.</p>
                          </Label>
                        </FormGroup>
                      </Col>
                    </Row> 
                    </span>
                    )}
                  </Col>
                </Row>
              </CardBody>
              <CardFooter className="text-center cosmic-butons">
                <Button className="cosmic-cancel" onClick={()=>{this.props.history.goBack()}}>Cancel</Button>
                <Button className="cosmic-confirm" onClick={this.onSave}>Save</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
        <Modal open={this.state.showModal} onClose={this.onCloseModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <ChangePassword onSave={this.changeConfirmPassword} history={this.props.history} onClose={this.onCloseModal}/>
        </Modal>
        <Modal open={this.state.confirmModal} onClose={this.onCloseConfirmModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <ConfirmModal history={this.props.history} onClose={this.onCloseConfirmModal} onAuthenticate={this.authenticate}/>
        </Modal>
        <ToastContainer />
    </React.Fragment>
    )
  }
}

export default AddMember;


  // componentDidUpdate(prevProps) {
  //   if (prevProps !== this.props) {
  //     const { match: { params } } = this.props;
  //     this.setState({uuid: params.uuid})
  //     if (params.uuid) {
  //       this.getUser(params.uuid)
  //     }

  //     this.getRoles(auth.getUUID())
  //   }
  // }