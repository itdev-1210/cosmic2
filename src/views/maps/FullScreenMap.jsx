// react plugin used to create google maps
import { GoogleMap, Marker, withGoogleMap, withScriptjs, InfoWindow } from 'react-google-maps';

import React from 'react';

import './styles.scss'
import mapEScooter from '../../assets/img/map_e_scooter.svg'
import mapEBike from '../../assets/img/map_e_bike.svg'
import mapBike from '../../assets/img/map_bike.svg'
import mapBattery from '../../assets/img/map_battery.svg'

import iconInUse from '../../assets/img/mark_scooter_inuse.svg'
import iconAvailable from '../../assets/img/mark-scooter-available.svg'
import iconUnavailable from '../../assets/img/mark-scooter-inactive.svg'
import iconBroken from '../../assets/img/mark-scooter-broken.svg'
import iconMaintenance from '../../assets/img/mark-scooter-maintenance.svg'
import iconTransporting from '../../assets/img/mark-scooter-transport.svg'
import iconStolen from '../../assets/img/mark-scooter-stolen.svg'
import iconLowBattery from '../../assets/img/mark-scooter-lowbattery.svg'
import iconLogistic from '../../assets/img/mark-scooter-inlogistic.svg'
import iconTeam from '../../assets/img/mark-scooter-logistic.svg'

import SearchBox from "react-google-maps/lib/components/places/SearchBox";
import MarkerClusterer from "react-google-maps/lib/components/addons/MarkerClusterer";
import { createPortal } from 'react-dom'
import { MAP } from 'react-google-maps/lib/constants'
import PropTypes from 'prop-types'

const devices = [
  {
    name: 'E-Scooter',
    url: mapEScooter
  },
  {
    name: 'E-Bike',
    url: mapEBike
  },
  {
    name: 'Bike',
    url: mapBike
  },
  {
    name: 'Battery',
    url: mapBattery
  },
]

const scooter_status = [
  {
    name: 'In Use',
    url: iconInUse,
    id_status_device: 2
  },
  {
    name: 'Available',
    url: iconAvailable,
    id_status_device: 1
  },
  {
    name: 'Unavailable',
    url: iconUnavailable,
    id_status_device: 6
  },
  {
    name: 'Broken',
    url: iconBroken,
    id_status_device: 4
  },
  {
    name: 'Maintenance',
    url: iconMaintenance,
    id_status_device: 7
  },
  {
    name: 'Transporting',
    url: iconTransporting,
    id_status_device: 3
  },
  {
    name: 'Stolen',
    url: iconStolen,
    id_status_device: 5
  },
  {
    name: 'Low Battery',
    url: iconLowBattery,
    id_status_device: 9
  },
  {
    name: 'Logistic',
    url: iconLogistic,
    id_status_device: 8
  },
  {
    name: 'Team',
    url: iconTeam,
    id_status_device: 0
  },
]

class MapControl extends React.Component {

  static contextTypes = {[MAP]: PropTypes.object}

  componentWillMount() {
    this.map = this.context[MAP]
    this.controlDiv = document.createElement('div')
    this.map.controls[this.props.position].push(this.controlDiv)
  }
  componentWillUnmount() {
    this.map.controls[this.props.position].pop()
  }
  render() {
    return createPortal(this.props.children, this.controlDiv)
  }
}

class TopLeftMenu extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      scooterType: this.props.scooterType,
      device_type: 0,
      devices: [],
      scooter_status: [],
      show_info: false,
      clusterStatus: this.props.clusterStatus
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps)
      this.setState({clusterStatus: nextProps.clusterStatus, scooterType: nextProps.scooterType})
  }
  changeStatus = (e) => {
    this.props.setCluster(!this.state.clusterStatus)
  }

  render() {
    const {device_type, show_info, clusterStatus, scooterType} = this.state
    const {filterHandle} = this.props
    return (
      <div className="map-devices">
        <div className="device-types">
          <div className="d-title">DEVICES</div>
          <div className="d-flex">
            {devices.map((device, index) => {
              if(device.name==='E-Scooter'){
                return <div key={index} className={index === device_type ? "one-device active" : "one-device"} onClick={()=>{this.setState({device_type: index})}}>
                  <div className="device-img"><img alt="" src={device.url}/></div>
                  <div className="device-name">{device.name}</div>
                </div>
              }else{
                return null
              }
            })}
          </div>
        </div>
        <div className="cluster-on-off">
          <div className="">
            <img alt="" src={require("assets/img/icon_cluster.svg")}/>Cluster
          </div>
          <div className="onoffswitch">
              <input type="checkbox" name="onoffswitch" className="onoffswitch-checkbox" id="myonoffswitch" checked={clusterStatus} onChange={this.changeStatus}/>
              <label className="onoffswitch-label" htmlFor="myonoffswitch">
                  <div className="onoffswitch-inner"></div>
                  <span className="onoffswitch-switch"></span>
              </label>
          </div>
        </div>
        {show_info ? (
          <div className="d-show-info">
            {scooter_status.map((scooter, index) => {
              return <div key={index} className={scooterType[parseInt(scooter.id_status_device)].active ? "one-show-info scooter-active" : "one-show-info"} onClick={()=>{filterHandle(scooter.id_status_device)}}>
                <div className="scooter_name">
                  <img alt="" src={scooter.url}/>{scooter.name}
                </div>
                <div className="scooter_count">
                  {scooter.id_status_device === 0 ? this.props.markers.filter((obj) => !obj['id_device']).length + ' scooters' : null}
                  {scooter.id_status_device === 9 ? this.props.markers.filter((obj) => obj.level_charge <= 20).length + ' scooters' : null}
                  {scooter.id_status_device !== 0 && scooter.id_status_device !== 9 ? 
                    this.props.markers.filter((obj) => obj.id_status_device === scooter.id_status_device).length + ' scooters' : null}
                </div>
              </div>
            })}
            <div className="d-hide-info" onClick={()=>{this.setState({show_info: false})}}>
              <img alt="" src={require("assets/img/icon_arrow_up.svg")}/>
              Hide all
            </div>
          </div>
        ) : (
          <div className="d-info" onClick={()=>{this.setState({show_info: true})}}>
            <img alt="" src={require("assets/img/icon_arrow_down.svg")}/>
            Icons related information
          </div>
        )}
      </div>
    )
  }
}

const MapWrapper = withScriptjs(
  withGoogleMap((props) => (
    <GoogleMap
      defaultZoom={8}
      defaultCenter={props.center}
      center={props.center}
      defaultOptions={{
        scrollwheel: true, //we disable de scroll over the map, it is a really annoing when you scroll through page
        mapTypeControl: true,
        zoomControl: true,
        minZoom: 2,
      }}
      onClick={props.onMapClick}
    >
      <MapControl position={window.google.maps.ControlPosition.RIGHT_CENTER}>
        <div className="current_position" onClick={()=>props.onCurrentPosition()}>
          <img alt="" src={require("assets/img/icon_map_gps.svg")}/>
        </div>
      </MapControl>
      {props.view ? null : <SearchBox
        controlPosition={window.google.maps.ControlPosition.TOP_LEFT}
        >
        <TopLeftMenu 
          filterHandle={props.filterHandle}
          setCluster={props.setCluster}
          clusterStatus={props.clusterStatus}
          markers={props.markers}
          scooterType={props.scooterType}
          />
      </SearchBox>}
      {props.clusterStatus ? 
      <MarkerClusterer
        averageCenter
        enableRetinaIcons
        gridSize={60}
        maxZoom={20}
        >
        {props.markers.map((a, index) => (
          <div key={index}>
          {props.scooterType[0].active && a && !a['id_device'] ? 
            (<Marker
              onClick={()=>props.onMarkerClick(index)}
              position={{ lat : parseFloat(a.lat.replace('"','').replace('"','')), lng : parseFloat(a.lon.replace('"','').replace('"','')) }}
              defaultAnimation={2}
              icon = {{url:require("assets/img/mark-scooter-logistic.svg"),
                scaledSize: new window.google.maps.Size(36, 36)}}
            >
            {props.showMarkId === index ? (
              <InfoWindow 
                onCloseClick={()=>props.onMarkerClose()}
                >
                <div>
                  <div className="info-window">
                    <div className="info-name">{a.first_name + ' ' + a.last_name}</div>
                    <div className="info-email">{a.email}</div>
                  </div>
                </div>
              </InfoWindow>
            ) : null}
            </Marker> 
          ): a && a['id_device'] && props.scooterType[parseInt(a.id_status_device)].active ? (
            <Marker
              onClick={()=>props.onMarkerClick(index)}
              position={{ lat : parseFloat(a.lat.replace('"','').replace('"','')), lng : parseFloat(a.lon.replace('"','').replace('"','')) }}
              defaultAnimation={2}
              icon = {{url: 
                a.id_status_device === 1 ? require("assets/img/mark-scooter-available.svg") :
                a.id_status_device === 2 ? require("assets/img/mark-scooter-inuse.svg") :
                a.id_status_device === 3 ? require("assets/img/mark-scooter-transport.svg") :
                a.id_status_device === 4 ? require("assets/img/mark-scooter-broken.svg") :
                a.id_status_device === 5 ? require("assets/img/mark-scooter-stolen.svg") :
                a.id_status_device === 6 ? require("assets/img/mark-scooter-inactive.svg") :
                a.id_status_device === 7 ? require("assets/img/mark-scooter-maintenance.svg") :
                a.id_status_device === 8 ? require("assets/img/mark-scooter-inlogistic.svg") :'',
                scaledSize: new window.google.maps.Size(36, 36)}}
            >
            {props.showMarkId === index ? (
              <InfoWindow 
                onCloseClick={()=>props.onMarkerClose()}
                >
                <div>
                  <div className="info-window-scooter">
                    <div className="info-scooter">{a.id_device}</div>
                    <div className="info-scooter-detail">
                      <div className="scooter-battery">
                        <div className="info-desc">LEVEL BATTERY</div>
                        <div className="info-value">{a.level_charge}%</div>
                      </div>
                      <div className="scooter-rides">
                        <div className="info-desc">HISTORIC RIDES</div>
                        <div className="historic-rides-view" onClick={()=>props.historicView(a.id_device)}>
                          <img alt="" src={require("assets/img/icon_guy.svg")}/> View
                        </div>
                      </div>
                    </div>
                    <div className="info-scooter-detail">
                      <div className="scooter-setting">
                        <div className="info-desc">LOCK/UNLOCK</div>
                        <div className="lock-unlock">
                          <div className="scooter-lock" onClick={()=>(props.scooterLock(a.serial, a.id_device))}><img alt="" src={require("assets/img/icon_padlock_open.svg")}/></div>
                          <div className="scooter-unlock" onClick={()=>(props.scooterUnlock(a.serial, a.id_device))}><img alt="" src={require("assets/img/icon_padlock_close.svg")}/></div>
                        </div>
                      </div>
                      <div className="scooter-status">
                        <div className="info-desc">CHANGE STATUS</div>
                        <div className={
                          a.id_status_device === 1 ? 'status-value scooter-available' :
                          a.id_status_device === 2 ? 'status-value scooter-in-use' :
                          a.id_status_device === 3 ? 'status-value scooter-transport' :
                          a.id_status_device === 4 ? 'status-value scooter-broken' :
                          a.id_status_device === 5 ? 'status-value scooter-stolen' :
                          a.id_status_device === 6 ? 'status-value scooter-unavailable' :
                          a.id_status_device === 7 ? 'status-value scooter-maintenance' :
                          a.id_status_device === 8 ? 'status-value scooter-logistic' : 'status-value'}
                          onClick={()=>(props.scooterSetting(a.id_device, a.id_status_device))}>{
                            a.id_status_device === 1 ? 'Available':
                            a.id_status_device === 2 ? 'In Use':
                            a.id_status_device === 3 ? 'In Transport' :
                            a.id_status_device === 4 ? 'Broken' :
                            a.id_status_device === 5 ? 'Stolen' :
                            a.id_status_device === 6 ? 'Unavailable' :
                            a.id_status_device === 7 ? 'Maintenance' :
                            a.id_status_device === 8 ? 'Logistic User' : 'Other'}
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </InfoWindow>
            ) : null}
            </Marker>
          ) : props.scooterType[9].active && a && a['id_device'] && a.level_charge <= 20 ? (
            <Marker
              onClick={()=>props.onMarkerClick(index)}
              position={{ lat : parseFloat(a.lat.replace('"','').replace('"','')), lng : parseFloat(a.lon.replace('"','').replace('"','')) }}
              defaultAnimation={2}
              icon = {{url: require("assets/img/mark-scooter-lowbattery.svg"),
                scaledSize: new window.google.maps.Size(36, 36)}}
            >
            {props.showMarkId === index ? (
              <InfoWindow 
                onCloseClick={()=>props.onMarkerClose()}
                >
                <div>
                  <div className="info-window-scooter">
                    <div className="info-scooter">{a.id_device}</div>
                    <div className="info-scooter-detail">
                      <div className="scooter-battery">
                        <div className="info-desc">LEVEL BATTERY</div>
                        <div className="info-value">{a.level_charge}%</div>
                      </div>
                      <div className="scooter-rides">
                        <div className="info-desc">HISTORIC RIDES</div>
                        <div className="historic-rides-view" onClick={()=>props.historicView(a.id_device)}>
                          <img alt="" src={require("assets/img/icon_guy.svg")}/> View
                        </div>
                      </div>
                    </div>
                    <div className="info-scooter-detail">
                      <div className="scooter-setting">
                        <div className="info-desc">LOCK/UNLOCK</div>
                        <div className="lock-unlock">
                          <div className="scooter-lock" onClick={()=>(props.scooterLock(a.serial, a.id_device))}><img alt="" src={require("assets/img/icon_padlock_open.svg")}/></div>
                          <div className="scooter-unlock" onClick={()=>(props.scooterUnlock(a.serial, a.id_device))}><img alt="" src={require("assets/img/icon_padlock_close.svg")}/></div>
                        </div>
                      </div>
                      <div className="scooter-status">
                        <div className="info-desc">CHANGE STATUS</div>
                        <div className={
                          a.id_status_device === 1 ? 'status-value scooter-available' :
                          a.id_status_device === 2 ? 'status-value scooter-in-use' :
                          a.id_status_device === 3 ? 'status-value scooter-transport' :
                          a.id_status_device === 4 ? 'status-value scooter-broken' :
                          a.id_status_device === 5 ? 'status-value scooter-stolen' :
                          a.id_status_device === 6 ? 'status-value scooter-unavailable' :
                          a.id_status_device === 7 ? 'status-value scooter-maintenance' :
                          a.id_status_device === 8 ? 'status-value scooter-logistic' : 'status-value'}
                          onClick={()=>(props.scooterSetting(a.id_device, a.id_status_device))}>{
                            a.id_status_device === 1 ? 'Available':
                            a.id_status_device === 2 ? 'In Use':
                            a.id_status_device === 3 ? 'In Transport' :
                            a.id_status_device === 4 ? 'Broken' :
                            a.id_status_device === 5 ? 'Stolen' :
                            a.id_status_device === 6 ? 'Unavailable' :
                            a.id_status_device === 7 ? 'Maintenance' :
                            a.id_status_device === 8 ? 'Logistic User' : 'Other'}
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </InfoWindow>
            ) : null}
            </Marker>
          ) : null}
          </div>
        ))}
      </MarkerClusterer>
      : props.markers.map((a, index) => (
          <div key={index}>
          {props.scooterType[0].active && a && !a['id_device'] ? 
            (<Marker
              onClick={()=>props.onMarkerClick(index)}
              position={{ lat : parseFloat(a.lat.replace('"','').replace('"','')), lng : parseFloat(a.lon.replace('"','').replace('"','')) }}
              defaultAnimation={2}
              icon = {{url:require("assets/img/mark-scooter-logistic.svg"),
                scaledSize: new window.google.maps.Size(36, 36)}}
            >
            {props.showMarkId === index ? (
              <InfoWindow 
                onCloseClick={()=>props.onMarkerClose()}
                >
                <div>
                  <div className="info-window">
                    <div className="info-name">{a.first_name + ' ' + a.last_name}</div>
                    <div className="info-email">{a.email}</div>
                  </div>
                </div>
              </InfoWindow>
            ) : null}
            </Marker> 
          ): a && a['id_device'] && props.scooterType[parseInt(a.id_status_device)].active ? (
            <Marker
              onClick={()=>props.onMarkerClick(index)}
              position={{ lat : parseFloat(a.lat.replace('"','').replace('"','')), lng : parseFloat(a.lon.replace('"','').replace('"','')) }}
              defaultAnimation={2}
              icon = {{url: 
                a.id_status_device === 1 ? require("assets/img/mark-scooter-available.svg") :
                a.id_status_device === 2 ? require("assets/img/mark-scooter-inuse.svg") :
                a.id_status_device === 3 ? require("assets/img/mark-scooter-transport.svg") :
                a.id_status_device === 4 ? require("assets/img/mark-scooter-broken.svg") :
                a.id_status_device === 5 ? require("assets/img/mark-scooter-stolen.svg") :
                a.id_status_device === 6 ? require("assets/img/mark-scooter-inactive.svg") :
                a.id_status_device === 7 ? require("assets/img/mark-scooter-maintenance.svg") :
                a.id_status_device === 8 ? require("assets/img/mark-scooter-inlogistic.svg") :'',
                scaledSize: new window.google.maps.Size(36, 36)}}
            >
            {props.showMarkId === index ? (
              <InfoWindow 
                onCloseClick={()=>props.onMarkerClose()}
                >
                <div>
                  <div className="info-window-scooter">
                    <div className="info-scooter">{a.id_device}</div>
                    <div className="info-scooter-detail">
                      <div className="scooter-battery">
                        <div className="info-desc">LEVEL BATTERY</div>
                        <div className="info-value">{a.level_charge}%</div>
                      </div>
                      <div className="scooter-rides">
                        <div className="info-desc">HISTORIC RIDES</div>
                        <div className="historic-rides-view" onClick={()=>props.historicView(a.id_device)}>
                          <img alt="" src={require("assets/img/icon_guy.svg")}/> View
                        </div>
                      </div>
                    </div>
                    <div className="info-scooter-detail">
                      <div className="scooter-setting">
                        <div className="info-desc">LOCK/UNLOCK</div>
                        <div className="lock-unlock">
                          <div className="scooter-lock" onClick={()=>(props.scooterLock(a.serial, a.id_device))}><img alt="" src={require("assets/img/icon_padlock_open.svg")}/></div>
                          <div className="scooter-unlock" onClick={()=>(props.scooterUnlock(a.serial, a.id_device))}><img alt="" src={require("assets/img/icon_padlock_close.svg")}/></div>
                        </div>
                      </div>
                      <div className="scooter-status">
                        <div className="info-desc">CHANGE STATUS</div>
                        <div className={
                          a.id_status_device === 1 ? 'status-value scooter-available' :
                          a.id_status_device === 2 ? 'status-value scooter-in-use' :
                          a.id_status_device === 3 ? 'status-value scooter-transport' :
                          a.id_status_device === 4 ? 'status-value scooter-broken' :
                          a.id_status_device === 5 ? 'status-value scooter-stolen' :
                          a.id_status_device === 6 ? 'status-value scooter-unavailable' :
                          a.id_status_device === 7 ? 'status-value scooter-maintenance' :
                          a.id_status_device === 8 ? 'status-value scooter-logistic' : 'status-value'}
                          onClick={()=>(props.scooterSetting(a.id_device, a.id_status_device))}>{
                            a.id_status_device === 1 ? 'Available':
                            a.id_status_device === 2 ? 'In Use':
                            a.id_status_device === 3 ? 'In Transport' :
                            a.id_status_device === 4 ? 'Broken' :
                            a.id_status_device === 5 ? 'Stolen' :
                            a.id_status_device === 6 ? 'Unavailable' :
                            a.id_status_device === 7 ? 'Maintenance' :
                            a.id_status_device === 8 ? 'Logistic User' : 'Other'}
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </InfoWindow>
            ) : null}
            </Marker>
          ) : props.scooterType[9].active && a && a['id_device'] && a.level_charge <= 20 ? (
            <Marker
              onClick={()=>props.onMarkerClick(index)}
              position={{ lat : parseFloat(a.lat.replace('"','').replace('"','')), lng : parseFloat(a.lon.replace('"','').replace('"','')) }}
              defaultAnimation={2}
              icon = {{url: require("assets/img/mark-scooter-lowbattery.svg"),
                scaledSize: new window.google.maps.Size(36, 36)}}
            >
            {props.showMarkId === index ? (
              <InfoWindow 
                onCloseClick={()=>props.onMarkerClose()}
                >
                <div>
                  <div className="info-window-scooter">
                    <div className="info-scooter">{a.id_device}</div>
                    <div className="info-scooter-detail">
                      <div className="scooter-battery">
                        <div className="info-desc">LEVEL BATTERY</div>
                        <div className="info-value">{a.level_charge}%</div>
                      </div>
                      <div className="scooter-rides">
                        <div className="info-desc">HISTORIC RIDES</div>
                        <div className="historic-rides-view" onClick={()=>props.historicView(a.id_device)}>
                          <img alt="" src={require("assets/img/icon_guy.svg")}/> View
                        </div>
                      </div>
                    </div>
                    <div className="info-scooter-detail">
                      <div className="scooter-setting">
                        <div className="info-desc">LOCK/UNLOCK</div>
                        <div className="lock-unlock">
                          <div className="scooter-lock" onClick={()=>(props.scooterLock(a.serial, a.id_device))}><img alt="" src={require("assets/img/icon_padlock_open.svg")}/></div>
                          <div className="scooter-unlock" onClick={()=>(props.scooterUnlock(a.serial, a.id_device))}><img alt="" src={require("assets/img/icon_padlock_close.svg")}/></div>
                        </div>
                      </div>
                      <div className="scooter-status">
                        <div className="info-desc">CHANGE STATUS</div>
                        <div className={
                          a.id_status_device === 1 ? 'status-value scooter-available' :
                          a.id_status_device === 2 ? 'status-value scooter-in-use' :
                          a.id_status_device === 3 ? 'status-value scooter-transport' :
                          a.id_status_device === 4 ? 'status-value scooter-broken' :
                          a.id_status_device === 5 ? 'status-value scooter-stolen' :
                          a.id_status_device === 6 ? 'status-value scooter-unavailable' :
                          a.id_status_device === 7 ? 'status-value scooter-maintenance' :
                          a.id_status_device === 8 ? 'status-value scooter-logistic' : 'status-value'}
                          onClick={()=>(props.scooterSetting(a.id_device, a.id_status_device))}>{
                            a.id_status_device === 1 ? 'Available':
                            a.id_status_device === 2 ? 'In Use':
                            a.id_status_device === 3 ? 'In Transport' :
                            a.id_status_device === 4 ? 'Broken' :
                            a.id_status_device === 5 ? 'Stolen' :
                            a.id_status_device === 6 ? 'Unavailable' :
                            a.id_status_device === 7 ? 'Maintenance' :
                            a.id_status_device === 8 ? 'Logistic User' : 'Other'}
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </InfoWindow>
            ) : null}
            </Marker>
          ) : null}
          </div>
        ))}
    </GoogleMap>
  ))
);

class FullScreenMap extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      markers: this.props.scooters.concat(this.props.users),
      scooters: this.props.scooters,
      users: this.props.users,
      showMarkId: -1,
      center: this.props.center,
      cluster_status: true,
      scooterType: [
        {
          active: true,
          id_status_device: 0
        },
        {
          active: true,
          id_status_device: 1
        },
        {
          active: true,
          id_status_device: 2
        },
        {
          active: true,
          id_status_device: 3
        },
        {
          active: true,
          id_status_device: 4
        },
        {
          active: true,
          id_status_device: 5
        },
        {
          active: true,
          id_status_device: 6
        },
        {
          active: true,
          id_status_device: 7
        },
        {
          active: true,
          id_status_device: 8
        },
        {
          active: true,
          id_status_device: 9
        },
      ]
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.scooters !== nextProps.scooters || this.props.users !== nextProps.users)
      this.setState({
        markers: nextProps.scooters.concat(nextProps.users),
        scooters: nextProps.scooters,
        users: nextProps.users,
      })

    if (this.props.center !== nextProps.center) this.setState({center: nextProps.center})
    if (nextProps.view) {
      let scooterType = Object.assign([], this.state.scooterType)
      scooterType.map((type) => type.active = true)
      this.setState({scooterType: scooterType})
    }
  }

  handleMarkerClick = (markerId) =>{
    this.setState({showMarkId: markerId})
  }

  handleMarkerClose = () => {
    this.setState({showMarkId: -1})
  }

  filterHandle = (id_status_device) => {
    let scooterType = Object.assign([], this.state.scooterType)
    const index = scooterType.findIndex(type => type.id_status_device === id_status_device)
    scooterType[index].active = !scooterType[index].active
    this.setState({scooterType: scooterType})
  }

  setCluster = (status) => {
    this.setState({cluster_status: status})
  }

  onCurrentPosition = () => {
    var _this = this
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        }; 
        _this.setState({center: pos})
      })
    }
  }

  historicView = (id_device) => {
    this.props.historicView(id_device)
  }

  scooterLock = (serial, id_device) => {
    this.props.scooterLock(serial, id_device)
  }

  scooterUnlock = (serial, id_device) => {
    this.props.scooterUnlock(serial, id_device)
  }

  scooterSetting = (id_device, id_status_device) => {
    this.setState({showMarkId: -1}, ()=>{this.props.scooterSetting(id_device, id_status_device)})
  }

  render() {
    const {markers, center} = this.state
    const {view} = this.props
    return (
      <div id="map" style={{ position: 'relative', overflow: 'hidden' }}>
        <MapWrapper ref="googleMap"
          googleMapURL="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyDASQ1gDtuxVXsoeJZHXqsNRASQ17yVoEI"
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `100%` }} />}
          mapElement={<div style={{ height: `100%` }} />}
          markers={markers}
          showMarkId={this.state.showMarkId}
          onMarkerClick={this.handleMarkerClick}
          onMarkerClose={this.handleMarkerClose}
          onMapClick={this.handleMarkerClose}
          onCurrentPosition={this.onCurrentPosition}
          center={center}
          view={view}
          filterHandle={this.filterHandle}
          clusterStatus={this.state.cluster_status}
          setCluster={this.setCluster}
          scooterType={this.state.scooterType}
          historicView={this.historicView}
          scooterLock={this.scooterLock}
          scooterUnlock={this.scooterUnlock}
          scooterSetting={this.scooterSetting}
        />
      </div>
    );
  }
}
export default FullScreenMap;
