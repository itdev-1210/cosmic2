import React, { Component } from 'react';
import {
  Button,
  Input,
  Row,
} from 'reactstrap';

import './modal.scss'
class ChangeModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      ope_status: this.props.operator.id_status
    }
    this.change = this.change.bind(this)
  }

  change(e) {
    this.setState({ope_status: e.target.value})
  }

  changeStatus = () => {
    let newOpe = Object.assign([], this.props.operator);
    newOpe.id_status = this.state.ope_status
    this.props.onChange(newOpe)
  }
  render() {
    const { operator, status, onClose } = this.props
    const { ope_status } = this.state
    return (
      <div className="ope_status">
        <div className="change-title">
          <span className="title">{'Change status'}</span>
        </div>
        <div className="status-group">
          <div className="input_group">
            <div>Operator</div>
            <div className="text-center">{operator.name}</div>

            <div>Status</div>
            <div>
              <Input
                name="status"
                type="select"
                value={ope_status}
                className={ope_status.toString() === '1' ? 'available' :
                ope_status.toString() === '2' ? 'unavailable' : 
                ope_status.toString() === '3' ? 'demo' : 
                ope_status.toString() === '4' ? 'deleted' : 
                ope_status.toString() === '5' ? 'default' : ''}
                    onChange={this.change}
              >
              {status.map((status, index) => 
                <option key={index} value={status.id_status} 
                  className={status.id_status === 1 ? 'available' :
                    status.id_status === 2 ? 'unavailable' :
                    status.id_status === 3 ? 'demo' :
                    status.id_status === 4 ? 'deleted' :
                    status.id_status === 5 ? 'default' : ''}>{status.status}</option>)}
              </Input>
            </div>
          </div>
        </div>
        <Row className='cosmic-butons butonsUpdateOpe'>
          <Button className="cosmic-cancel" onClick={onClose}>Cancel</Button>
          <Button className="cosmic-confirm" onClick={this.changeStatus}>Change status</Button>
        </Row>
      </div>
    )
  }
}

export default ChangeModal