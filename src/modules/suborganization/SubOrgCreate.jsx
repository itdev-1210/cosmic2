import React from 'react';
import {
  Row,
  Col,
  CardHeader,
  CardTitle,
} from "reactstrap";
//import ReactWizard from "react-bootstrap-wizard";
import StepWizard from 'react-step-wizard';
import ReactWizard from "react-bootstrap-wizard";

import {getInfoSubOrg,createSubOrg} from '../../api/subOrganizations.js'
import SweetAlert from 'react-bootstrap-sweetalert';
import iconOKCou from '../../assets/img/icon_ok_coupons.png'
import iconFailCou from '../../assets/img/icon_fail_coupons.png' 

import FormSubOrg from './component/FormSubOrg'
import Schedule from './component/Schedule'
import './SubOrg.scss'
import Scrollbar from 'perfect-scrollbar-react';
import 'perfect-scrollbar-react/dist/style.min.css';
import ResponsiveModal from 'react-responsive-modal';
import LoadingModal from './../../components/Navbars/LogoutModal'

class SubOrgCreate extends React.Component {
  constructor(props) {
    super(props)
    this.information={
      suborg:{},
      user:{},
      schedule:{}
    }
    this.state = {
      Countries:[],   
      Plans:[],
      ListDevices:[],
      ListStatus:[],
      alert:null,
      is_loading: false,
      pageWizard:1,
      instanceObj:{}
    }

    this.getInitialInfo = this.getInitialInfo.bind(this)
  }

  componentDidMount() {
    document.body.classList.add('create-suborg')
    this.getInitialInfo()
  }
  componentWillUnmount() {
    document.body.classList.remove('create-suborg')
  }
  getInitialInfo = async () => {
    const initInfo = await getInfoSubOrg()
    if(initInfo.st){
      if(initInfo.status === 200){
        this.setState({
          Countries:initInfo.Countries,
          Plans:initInfo.Plan,
          ListDevices:initInfo.Devices,
          ListStatus:initInfo.Status
        })
      }
    }
  }
  handlerupdateForm = (form) => {
    this.information.suborg=form.suborg
    this.information.user=form.user
    this.setState({pageWizard:2})
  }
  handlerupdateShedule = async (infoSch) => {//Save
    this.information.schedule=infoSch.schedule
    this.information.suborg.gmt=infoSch.gmt
    this.setState({is_loading: true}, async()=>{
      let res = await createSubOrg(this.information)
      this.setState({is_loading: false}, ()=>{
        if(res.st && res.status===200){
          this.autoCloseSuccess()
        }
        else{
          this.errorValidation(res.message)
        }
      })
    })
  }
  
  returnFunction = () =>{        
    if(this.state.pageWizard===1) this.props.history.push('/admin/admin-operator')
    else this.state.instanceObj.previousStep()
  }

  changePage = (e) =>{ 
    this.setState({pageWizard:e.activeStep})
  }

  instance = (e) =>{
    this.setState({instanceObj:e})
  }

  render() {
    const {Countries,Plans,ListDevices,ListStatus} =  this.state
    return (
      <React.Fragment>
        {this.state.alert}
        <ResponsiveModal open={this.state.is_loading} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LoadingModal title="Loading..."/>
        </ResponsiveModal>
        <div  className="content">
          <Col md='12'>
            <Row>
              <Col md='12'>
                <CardHeader className="management-header">
                  <Col md='4'>
                    <CardTitle tag="h4" onClick={this.returnFunction}>
                      <img alt="" src={require("assets/img/icon_arrow.svg")}/>Create new Operador
                    </CardTitle>
                  </Col>  
                </CardHeader>            
              </Col>
            </Row>
            {/* <Row>
              <Col md={10}>
                <div className='navegation'>
                  <div className='progressBar'>
                    <div className='progressStep'></div>
                  </div>
                  <ul className='navSteps'>
                    <li className='step'>
                      <a className='stepLink active'>
                        <i className='tim-icons icon-single-02'></i>
                        <p>Information</p>
                      </a>
                    </li>
                    <li className='step'>
                      <a className='stepLink'>
                        <i className='tim-icons icon-settings-gear-63'></i>
                        <p>Schedule</p>
                      </a>
                    </li>
                  </ul>
                </div>
              </Col>
            </Row>
            <Row>
              <Col md='12'>
              <ReactWizard
                steps={[
                  {
                    stepName: "Information",
                    stepIcon: "tim-icons icon-single-02",
                    component: Step1
                  },
                  {
                    stepName: "SCHEDULE",
                    stepIcon: "tim-icons icon-settings-gear-63",
                    component: Step2
                  }
                ]}
                navSteps                
                description='Steps'                
                progressbar
                color="blue"
              /> 
              </Col>
            </Row> */}
            
            <Row>
              <Col md={12}>
                <Scrollbar
                  options={{suppressScrollX:true}}
                >
                  <Col className='into-Scroll' >
                    <StepWizard initialStep={1} onStepChange={this.changePage} instance={this.instance}>
                      <FormSubOrg 
                        hashKey={'form'}
                        Countries={Countries}
                        Plans={Plans}
                        ListDevices={ListDevices}
                        ListStatus={ListStatus}                
                        updateInfo={this.handlerupdateForm}
                        errorValidation={this.errorValidation}
                        />
                      <Schedule
                        hashKey={'schedule'}
                        onSave={this.handlerupdateShedule}
                        />              
                    </StepWizard>              
                  </Col>            
                </Scrollbar>
              </Col>
            </Row>
          </Col>
        </div>
      </React.Fragment>
    );
  }

  hideAlert = () => {
    this.setState({alert:null})
    this.props.history.push('/admin/admin-operator')
  };

  autoCloseSuccess = () => {
    this.setState({
      alert: (
        <SweetAlert 
          title=''
          custom
          customIcon={iconOKCou}          
          showConfirm={false}
        >   
        <hr className='Line'/>
        <div className='AlertCreate'>          
          <p>Sub-Organization has been created!</p>
        </div>       
        </SweetAlert >
      )
    });
    setTimeout(this.hideAlert, 2000);
  };

  errorValidation = (msj) => {
    this.setState({
      alert: (
        <SweetAlert 
          title=''
          custom
          customIcon={iconFailCou}
          showConfirm={false}
        >
          <hr className='Line'/>
          <div className='AlertCreate'>
            <p>{msj}</p>
          </div>
        </SweetAlert >
      )
    });
    setTimeout(() => this.setState({alert:null}), 2000);
  };
}

export default SubOrgCreate;

// class Step1 extends React.Component {
//   constructor(props) {
//     super(props);
//   }

//   render() {    
//     return (
//       <>
//       Paso 1
//       </>
//     )
//   }
// }


// class Step2 extends React.Component {
//   constructor(props) {
//     super(props);
//   }

//   render() {    
//     return (      
//       <>
//       Paso 2
//       </>
//     )
//   }
// }

// var steps = [
//   {
//     stepName: "Information",
//     stepIcon: "tim-icons icon-single-02",
//     component: Step1
//   },
//   {
//     stepName: "SCHEDULE",
//     stepIcon: "tim-icons icon-settings-gear-63",
//     component: Step2
//   }
// ];