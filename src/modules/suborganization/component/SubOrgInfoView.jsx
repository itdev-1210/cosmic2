import React from 'react';

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Label,
  Row,
  Col,
  Table
} from 'reactstrap';
import Moment from 'react-moment'
import Scrollbar from 'perfect-scrollbar-react';
import { ToastContainer, toast } from 'react-toastify'
import { getDetailSubOrg, editSubOrg,editSchedule, changeAccount} from '../../../api/subOrganizations';
import ResponsiveModal from 'react-responsive-modal';
import UpdateModalOp from './UpdateModalSubOrg'
import ConfirmModal from '../../../components/Cosmic/InAuthModal/InAuthModal'
import AlertModal from    '../../../components/Cosmic/AlertModal/AlertModal'
import ChangeModal from    '../ChangeModal'
import ModalSchedule from './ModalSchedule'
import LoadingModal from './../../../components/Navbars/LogoutModal'

class SubOrgInfoView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      SubOrganization:{},
      payments:[],   
      Devices:[],
      newSubOrg:null,
      namePlan:'',
      opeStatus:'Undefined',
      newSchedule:null,
      Schedule:{},
      idOp:'',
      tmpOperator: null,
      
      updateModal:false, 
      confirmModal:false,
      ScheduleModal:false,
      statusModal: false,
      authModal: false,
      alert:false,
      alertStatus:0,
      alertMsj:'',

      sort_direction_order_id: false,
      sort_direction_amount: true,
      sort_direction_currency: true,
      sort_direction_token: true,
      sort_direction_date: true,
      sort_direction_email: true,
      sort_direction_payment_method: true,
      sort_direction_service: true,

      is_loading: true,
    }
    
  }
  
  componentDidMount() {
    this.setStaticInfo()
  }

  setStaticInfo = async () => {
    if(this.props.infoOrganization){
      const data = this.props.infoOrganization      
      const subOrgDetail = await getDetailSubOrg(data.id_operator)
      if(subOrgDetail.st && subOrgDetail.status===200){        
        this.setState({
          is_loading: false,
          SubOrganization:data,
          payments:subOrgDetail.payments,
          Devices:subOrgDetail.Devices,
          opeStatus:subOrgDetail.operator.status,
          idOp:subOrgDetail.operator.id_operator,
          Schedule:subOrgDetail.operator_schedule,
          namePlan:data.id_plan===1?'Free':data.id_plan===2?'Basic':data.id_plan===3?'Pro':'Custom'
        }) 
      }else{
        this.setState({
          is_loading: false,
          SubOrganization:data,
          opeStatus:'Undefined',
          namePlan:data.id_plan===1?'Free':data.id_plan===2?'Basic':data.id_plan===3?'Pro':'Custom'
        }) 
      }
    }       
  }

  renderPayments = () => {
    const { payments } = this.state
    if(payments){
      return payments.map((payment, index) => (
        <tr key={index} onClick={()=>{this.props.showPaymentDetail(payment.id_payment)}} style={{cursor: 'pointer'}}>
          <td className="text-left">{payment.order_id}</td>
          <td>{new Intl.NumberFormat('us-US', {style: 'currency', currency: 'USD'}).format(payment.total_cost)}</td>
          <td className="upper-case">{payment.currency}</td>
          <td className="upper-case">{payment.brand + ' ' + payment.last_four}</td>
          <td className="text-left"><Moment format="MMM DD, YYYY h:mm A">{payment.date}</Moment></td>
          <td className="text-left">{payment.email}</td>          
          <td className="text-left">{payment.device}</td>
          <td className="text-left">
            <div className="status">
              <span>{payment.status}</span>
              {payment.status === 'Succeded' ? <img alt="" src={require("assets/img/icon_check.png")}/> : 
              payment.status === 'Failed' ? <img alt="" src={require("assets/img/icon_fail.png")}/> : 
              payment.status === 'Pending' ? <img alt="" src={require("assets/img/icon_pending.png")}/> : 
              payment.status === 'Refunded' ? <img alt="" src={require("assets/img/icon_reply.png")}/> : null}
            </div>
          </td>
        </tr>
      ));
    }
    else{
      return null
    }
  }

  sortData = (type) => {
    let payments = Object.assign([], this.state.payments)
    switch(type) {
      case "order_id":
        if (this.state.sort_direction_order_id) {
          payments.sort((a,b)=>(a.order_id > b.order_id) ? 1 : ((b.order_id > a.order_id) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.order_id < b.order_id) ? 1 : ((b.order_id < a.order_id) ? -1 : 0))
        }
        this.setState({sort_direction_order_id: !this.state.sort_direction_order_id, payments: payments})
        break;
      case "amount":
        if (this.state.sort_direction_amount) {
          payments.sort((a,b)=>(parseFloat(a.total_cost) > parseFloat(b.total_cost)) ? 1 : ((parseFloat(b.total_cost) > parseFloat(a.total_cost)) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(parseFloat(a.total_cost) < parseFloat(b.total_cost)) ? 1 : ((parseFloat(b.total_cost) < parseFloat(a.total_cost)) ? -1 : 0))
        }
        this.setState({sort_direction_amount: !this.state.sort_direction_amount, payments: payments})
        break;
      case "card":
        if (this.state.sort_direction_phone) {
          payments.sort((a,b)=>(a.brand + a.last_four > b.brand + b.last_four) ? 1 : ((b.brand + b.last_four > a.brand + a.last_four) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.brand + a.last_four < b.brand + b.last_four) ? 1 : ((b.brand + b.last_four < a.brand + a.last_four) ? -1 : 0))
        }
        this.setState({sort_direction_phone: !this.state.sort_direction_phone, payments: payments})
        break;
      case "currency":
        if (this.state.sort_direction_currency) {
          payments.sort((a,b)=>(a.currency > b.currency) ? 1 : ((b.currency > a.currency) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.currency < b.currency) ? 1 : ((b.currency < a.currency) ? -1 : 0))
        }
        this.setState({sort_direction_currency: !this.state.sort_direction_currency, payments: payments})
        break;
      case "payment_method":
        if (this.state.sort_direction_payment_method) {
          payments.sort((a,b)=>(a.payment_method > b.payment_method) ? 1 : ((b.payment_method > a.payment_method) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.payment_method < b.payment_method) ? 1 : ((b.payment_method < a.payment_method) ? -1 : 0))
        }
        this.setState({sort_direction_payment_method: !this.state.sort_direction_payment_method, payments: payments})
        break;
      case "service":
        if (this.state.sort_direction_service) {
          payments.sort((a,b)=>(a.service > b.service) ? 1 : ((b.service > a.service) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.service < b.service) ? 1 : ((b.service < a.service) ? -1 : 0))
        }
        this.setState({sort_direction_service: !this.state.sort_direction_service, payments: payments})
        break;
      case "date":
        if (this.state.sort_direction_date) {
          payments.sort((a,b)=>(Date.parse(a.date) > Date.parse(b.date)) ? 1 : ((Date.parse(b.date) > Date.parse(a.date)) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(Date.parse(a.date) < Date.parse(b.date)) ? 1 : ((Date.parse(b.date) < Date.parse(a.date)) ? -1 : 0))
        }
        this.setState({sort_direction_date: !this.state.sort_direction_date, payments: payments})
        break;
      default:
        break;
    }
  }

  onAuthenticate = async (res) => {
    const {newSchedule,newSubOrg,idOp}=this.state
    if (res.response.status !== 200)
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    else {      
      this.setState({is_loading: true}, async()=>{
        if(newSchedule!=={} && newSchedule!==null){
          const res = await editSchedule({
            id_operator:idOp,
            operator_schedule:newSchedule          
          })
          if(res.st && res.status===200){
            toast.success('Operator Schedule Update Success', {position: toast.POSITION_TOP_RIGHT})
            this.setState({
              is_loading: false,
              confirmModal: false,
              Schedule: newSchedule,
              newSchedule:{}
            })
          }
          else{
            toast.error('Error to update Schedule', {position: toast.POSITION_TOP_RIGHT})
            this.setState({
              is_loading: false,
              confirmModal: false,
            })
          }
        }
        else if(newSubOrg!=={} && newSubOrg!==null){
          const res = await editSubOrg(this.state.newSubOrg)
          if(res.st && res.status===200){
            this.setState({
              is_loading: false,
              alert:true,
              alertStatus:200,
              alertMsj:'The Operator has been updated',
              confirmModal: false,
              SubOrganization: newSubOrg,
            })
          }
          else{
            toast.error(res.message, {position: toast.POSITION_TOP_RIGHT})
            this.setState({is_loading: false})
          }
        }
      })
    }
  }

  changeStatus = () => {
    this.setState({statusModal: true})
  }

  closeStatusModal = () => {
    this.setState({statusModal: false})
  }

  confirmStatusModal = (new_operator) => {
    this.setState({statusModal: false, authModal: true, tmpOperator: new_operator})
  }

  closeAuthModal = () => {
    this.setState({authModal: false})
  }

  onCloseAlertModal = () => {
    this.setState({alert: false})
  }

  changeAuth = async (res) => {
    if (res.response.status !== 200)
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    else {
      const { tmpOperator } = this.state
      const result = await changeAccount({id_operator: tmpOperator.id_operator, id_status: tmpOperator.id_status})
      if (result.response.status !== 200) {
        this.setState({
          authModal: false, 
          alert: true, 
          alertMsj: 'An error has ocurred while updating the operator!', 
          alertStatus: result.response.status})
      } else {
        let newSub = this.state.SubOrganization;
        newSub.id_status = result.response.operator.id_status
        this.setState({
          authModal: false, 
          alert: true, 
          alertMsj: 'Operator has been updated!', 
          SubOrganization: newSub, 
          alertStatus: result.response.status})
      }
    }
  }
  
  render() {
    const { SubOrganization, Devices, namePlan,opeStatus,Schedule} = this.state
    const { Countries,onActionUpdate } = this.props
    return (
      <Row>
        <Col md="12">
          <CardHeader className="management-header">
            <Col md='12'>
              <CardTitle tag="h4" style={{cursor:'pointer'}}>
                <img alt="" src={require("assets/img/icon_arrow.svg")} onClick={()=>{onActionUpdate()}}/>
                <span className="icon_owner_white">{SubOrganization.name}</span>
                <button className={'btn-status '+opeStatus} onClick={this.changeStatus}>
                  <span>{opeStatus}</span>
                </button> 
              </CardTitle>
            </Col>  
          </CardHeader>
        </Col>
        <Col md={12}>
          <Scrollbar
            options={{suppressScrollX:true}}
          >
            <Col md="12" className='into-Scroll'>
              <Row>
                
                <Col md="6">
                  <Card>
                    <CardHeader>
                      <Row>
                        <Col md="4">
                          <CardTitle tag="h4" className="text-white">Account Information</CardTitle>
                        </Col>
                        <Col md="4">
                          <div onClick={(this.openEditModal)} style={{cursor:'pointer'}}><img alt="" src={require("assets/img/icon_edit.svg")}/></div>
                        </Col>
                      </Row>
                    </CardHeader>
                    <CardBody className="edit-suborg-body">
                      <Row>
                        <Label md="3" className="text-white">UUID</Label>
                        <Label md="9">{SubOrganization && SubOrganization.id_operator ? SubOrganization.id_operator : ''}</Label>
                      </Row>
                      <Row>
                        <Label md="3" className="text-white">Created</Label>
                        <Label md="9">{SubOrganization && SubOrganization.created_at ? new Date(SubOrganization.created_at).toLocaleString() : ''}</Label>
                      </Row>
                      <Row>
                        <Label md="3" className="text-white">Name</Label>
                        <Label md="9">{SubOrganization && SubOrganization.name ? SubOrganization.name: ''}</Label>
                      </Row>
                      <Row>
                        <Label md="3" className="text-white">Mobile Phone</Label>
                        <Label md="9">{SubOrganization && SubOrganization.mobile_phone ? SubOrganization.indicative + SubOrganization.mobile_phone : ''}</Label>
                      </Row>
                      <Row>
                        <Label md="3" className="text-white">Email</Label>
                        <Label md="9">{SubOrganization && SubOrganization.email ? SubOrganization.email : ''}</Label>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
                <Col md="6">
                  <Card>
                    <CardBody className="edit-owner-body">
                      <Row>
                        <Label md="3" className="text-white">Country</Label>
                        <Label md="9">{SubOrganization && SubOrganization.country ? SubOrganization.country : ''}</Label>
                      </Row>
                      <Row>
                        <Label md="3" className="text-white">State</Label>
                        <Label md="9">{SubOrganization && SubOrganization.state ? SubOrganization.state : ''}</Label>
                      </Row>
                      <Row>
                        <Label md="3" className="text-white">City</Label>
                        <Label md="9">{SubOrganization && SubOrganization.city ? SubOrganization.city : ''}</Label>
                      </Row>
                      <Row>
                        <Label md="3" className="text-white">Street</Label>
                        <Label md="9">{SubOrganization && SubOrganization.address ? SubOrganization.address : ''}</Label>
                      </Row>
                      <Row>
                        <Label md="3" className="text-white">ZIP Code</Label>
                        <Label md="9">{SubOrganization && SubOrganization.zip_code ? SubOrganization.zip_code : ''}</Label>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
                
                <Col md="12">
                  <Card>
                    <CardHeader>
                      <Row>
                        <Col md='2'>
                          <CardTitle tag="h4">Associated Services</CardTitle>
                        </Col>
                        <Col md='3'>
                          <button className='btn-cosmic' onClick={()=>this.setState({ScheduleModal:true})}>
                            <i className="tim-icons icon-calendar-60" />
                            <span>Edit Schedule</span>
                          </button> 
                        </Col>
                      </Row>
                    </CardHeader>
                    <CardBody>
                      <Row>
                        {Devices.map((device, index) => {
                          return (
                            <Col md={2} key={index}>
                              <img className='i-services' alt="" src={
                                device.id_device_type === 0 ? require("assets/img/icon_scooter_rover_gray.svg") :
                                device.id_device_type === 1 ? require("assets/img/icon_e_bike.svg") :
                                require("assets/img/icon_battery.svg")
                                }/>
                              <span className='t-services'>{device.count+'      |     '+device.device}</span>
                            </Col>)
                          })
                        }
                        {/*icon_e_bike.svg icon_battery.svg*/}                  
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
                <Col md="12">
                  <Card>
                    <CardHeader>
                      <Row>
                        <Col md="3" className="text-white">
                          <CardTitle tag="h4">Current Plan</CardTitle>
                        </Col>
                      </Row>
                    </CardHeader>
                    <CardBody className="edit-owner-body">
                      <Row>
                        <span className='t-services'>
                          {namePlan}
                        </span>
                        <Label md="2">
                          <button className='btn-add btn-cosmic-blue'>                        
                            <span>Upgrade your Plan</span>
                          </button> 
                        </Label>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
                <Col md="12">
                <Card>
                  <CardHeader className="show-more-payment">
                    <CardTitle tag="h4">Payments</CardTitle>
                    <div onClick={()=>{this.props.showMorePayment(SubOrganization.email)}}>Show more...</div>
                  </CardHeader>
                  <CardBody>
                    <Table>
                      <thead className="text-primary">
                        <tr>
                          <th className="text-left" onClick={()=>{this.sortData('order_id')}}>Order ID <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th onClick={()=>{this.sortData('amount')}}>Amount <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th onClick={()=>{this.sortData('currency')}}>Currency <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th className="text-left" onClick={()=>{this.sortData('card')}}>Card <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th className="text-left" onClick={()=>{this.sortData('date')}}>Date <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th className="text-left" onClick={()=>{this.sortData('email')}}>User <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>                          
                          <th className="text-left" onClick={()=>{this.sortData('service')}}>Service <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th className="text-left">STATUS</th>
                        </tr>
                      </thead>
                      <tbody className="edit-owner-payment">{this.renderPayments()}</tbody>
                    </Table>
                  </CardBody>
                </Card>
              </Col>
              </Row>
            </Col>
          </Scrollbar>
        </Col>
        <ResponsiveModal open={this.state.ScheduleModal} onClose={this.handlerCancelShedule} classNames={{'modal':'schModalDiv','closeButton':'modal-close-button'}}>
          <ModalSchedule   
            Schedule={Schedule}
            onSave={this.handlerUpdateShedule}
            firstStep={this.handlerCancelShedule}
          />
        </ResponsiveModal>
        <ResponsiveModal open={this.state.updateModal} onClose={this.onCloseEditModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <UpdateModalOp onClose={this.onCloseEditModal} updateData={this.updateData} SubOrganization={SubOrganization} Countries={Countries} Devices={Devices}/>
        </ResponsiveModal>
        <ResponsiveModal open={this.state.confirmModal} onClose={this.onCloseConfirmModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <ConfirmModal onClose={this.onCloseConfirmModal} onAuthenticate={this.onAuthenticate}/>
        </ResponsiveModal>
        <ResponsiveModal open={this.state.alert} onClose={this.closeAlert} center classNames={{'modal':'alert-modal','closeButton':'modal-close-button'}}>
          <AlertModal onClose={this.closeAlert} status={this.state.alertStatus} msj={this.state.alertMsj}/>
        </ResponsiveModal>
        <ResponsiveModal open={this.state.is_loading} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LoadingModal title="Loading..."/>
        </ResponsiveModal>
        <ResponsiveModal open={this.state.statusModal} onClose={this.closeStatusModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <ChangeModal onClose={this.closeStatusModal} onChange={this.confirmStatusModal} operator={this.state.SubOrganization} status={this.props.status_list}/>
        </ResponsiveModal>
        <ResponsiveModal open={this.state.authModal} onClose={this.closeAuthModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <ConfirmModal onClose={this.closeAuthModal} onAuthenticate={this.changeAuth}/>
        </ResponsiveModal>
        <ToastContainer />
      </Row>
    );
  }
  handlerUpdateShedule=(newSchedule)=>{this.setState({newSchedule:newSchedule, confirmModal: true, ScheduleModal:false})}
  handlerCancelShedule=()=>{this.setState({ScheduleModal:false, newSchedule:{}})}
  openEditModal = () => {this.setState({updateModal: true})}
  onCloseEditModal = () => {this.setState({updateModal: false})}
  updateData = (newSubOrg) => {this.setState({newSubOrg: newSubOrg, confirmModal: true, updateModal: false})}
  onCloseConfirmModal = () => {this.setState({confirmModal: false})}  
  closeAlert = () =>{
    this.props.onActionUpdate()
  }
  
  
  
}

export default SubOrgInfoView;

/*
  closeAlert = () =>{
    this.setState({alert:null})
  }

  updateUser = () => {
    this.setState({updateModal: true})
  }

 

  
  
  


  /*
  }*/



  /*setData = (res) => {
    var payments = res.payments ? res.payments : []
    payments.sort((a,b)=>(a.order_id > b.order_id) ? 1 : ((b.order_id > a.order_id) ? -1 : 0))

    this.setState({
      scooters: res.scooters ? res.scooters : "0",
      scooters_revenue: res.scooters_revenue ? res.scooters_revenue : "0",
      total_revenues: res.total_revenues ? res.total_revenues : "0",
      payments: res.payments ? res.payments : [],
      owner: res.owner ? res.owner : {}
    })
  }*/
