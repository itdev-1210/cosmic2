import React from 'react';
import { ToastContainer, toast } from 'react-toastify'
import {
  Button,
  CardTitle,
  CardBody,
  Row,
  Col,
} from "reactstrap";
import ScheduleDay from './ItemSchedule'

class ModalSchedule extends React.Component {
  constructor(props) {
    super(props);    
    this.infoDays = {
      Monday:{},
      Tuesday:{},
      Wednesday:{},
      Thursday:{},
      Friday:{},
      Saturday:{},
      Sunday:{},
    }
    this.state = {
      Schedules:[],
      timeZ:'+0000'
    }
  }

  setCurrent = async (current) =>{
    this.infoDays.Monday=current['monday_start']&&current['monday_end']?{start:current['monday_start'],end:current['monday_end'],index:null}:{}
    this.infoDays.Tuesday=current['tuesday_start']&&current['tuesday_end']?{start:current['tuesday_start'],end:current['tuesday_end'],index:null}:{}
    this.infoDays.Wednesday=current['wednesday_start']&&current['wednesday_end']?{start:current['wednesday_start'],end:current['wednesday_end'],index:null}:{}
    this.infoDays.Thursday=current['thursday_start']&&current['thursday_end']?{start:current['thursday_start'],end:current['thursday_end'],index:null}:{}
    this.infoDays.Friday=current['friday_start']&&current['friday_end']?{start:current['friday_start'],end:current['friday_end'],index:null}:{}
    this.infoDays.Saturday=current['saturday_start']&&current['saturday_end']?{start:current['saturday_start'],end:current['saturday_end'],index:null}:{}
    this.infoDays.Sunday=current['sunday_start']&&current['sunday_end']?{start:current['sunday_start'],end:current['sunday_end'],index:null}:{}
    
    let hours = []
    let Sch = []
    await Object.entries(this.infoDays).forEach(val => {
      if(Object.entries(val[1]).length>0){        
        if(hours.indexOf(val[1].start+val[1].end)===-1){
          this.infoDays[val[0]].index=hours.length
          hours.push(val[1].start+val[1].end)
        }else{
          this.infoDays[val[0]].index=hours.indexOf(val[1].start+val[1].end)
        }
      }
    })
    Sch=new Array(hours.length-1).fill(0,0)    
    this.setState({Schedules:Sch})
  }

  componentDidMount(){
    this.setCurrent(this.props.Schedule)
  }
  
  update = (data) =>{
    if(data.state){
      this.infoDays[data.day]=data      
    }else{this.infoDays[data.day]=null}
  }

  onBack = () =>{
    document.getElementById('admin-container').scrollTop = 0
    this.props.firstStep()
  }

  onSave = async () =>{
    let valid = true
    await Object.entries(this.infoDays).forEach(val => {
      if(Object.entries(val[1]).length>0 && valid===true){
        if(val[1].start.split(':')[0]>=val[1].end.split(':')[0]){
          toast.error('Start time should not be greater or equal than end time', {position: toast.POSITION_TOP_RIGHT})
          valid=false
        }
      }
    })
    const schedule = {
      monday_start:this.infoDays.Monday===null?null:this.infoDays.Monday.start,
      monday_end:this.infoDays.Monday===null?null:this.infoDays.Monday.end,
      tuesday_start:this.infoDays.Tuesday===null?null:this.infoDays.Tuesday.start,
      tuesday_end:this.infoDays.Tuesday===null?null:this.infoDays.Tuesday.end,
      wednesday_start:this.infoDays.Wednesday===null?null:this.infoDays.Wednesday.start,
      wednesday_end:this.infoDays.Wednesday===null?null:this.infoDays.Wednesday.end,
      thursday_start:this.infoDays.Thursday===null?null:this.infoDays.Thursday.start,
      thursday_end:this.infoDays.Thursday===null?null:this.infoDays.Thursday.end,
      friday_start:this.infoDays.Friday===null?null:this.infoDays.Friday.start,
      friday_end:this.infoDays.Friday===null?null:this.infoDays.Friday.end,
      saturday_start:this.infoDays.Saturday===null?null:this.infoDays.Saturday.start,
      saturday_end:this.infoDays.Saturday===null?null:this.infoDays.Saturday.end,
      sunday_start:this.infoDays.Sunday===null?null:this.infoDays.Sunday.start,
      sunday_end:this.infoDays.Sunday===null?null:this.infoDays.Sunday.end,
    } 
    if(valid){
      this.props.onSave(schedule)
    } 
  }

  update = (data) =>{  
    this.infoDays['Monday']=Object.getOwnPropertyNames(this.infoDays['Monday']).length===0?
    (data.days.indexOf('Monday')>=0?data.info:{}):
    (this.infoDays['Monday'].index===data.index?(data.days.indexOf('Monday')>=0?data.info:{}):this.infoDays['Monday'])
    this.infoDays['Tuesday']=Object.getOwnPropertyNames(this.infoDays['Tuesday']).length===0?
    (data.days.indexOf('Tuesday')>=0?data.info:{}):
    (this.infoDays['Tuesday'].index===data.index?(data.days.indexOf('Tuesday')>=0?data.info:{}):this.infoDays['Tuesday'])
    this.infoDays['Wednesday']=Object.getOwnPropertyNames(this.infoDays['Wednesday']).length===0?
    (data.days.indexOf('Wednesday')>=0?data.info:{}):
    (this.infoDays['Wednesday'].index===data.index?(data.days.indexOf('Wednesday')>=0?data.info:{}):this.infoDays['Wednesday'])
    this.infoDays['Thursday']=Object.getOwnPropertyNames(this.infoDays['Thursday']).length===0?
    (data.days.indexOf('Thursday')>=0?data.info:{}):
    (this.infoDays['Thursday'].index===data.index?(data.days.indexOf('Thursday')>=0?data.info:{}):this.infoDays['Thursday'])
    this.infoDays['Friday']=Object.getOwnPropertyNames(this.infoDays['Friday']).length===0?
    (data.days.indexOf('Friday')>=0?data.info:{}):
    (this.infoDays['Friday'].index===data.index?(data.days.indexOf('Friday')>=0?data.info:{}):this.infoDays['Friday'])
    this.infoDays['Saturday']=Object.getOwnPropertyNames(this.infoDays['Saturday']).length===0?
    (data.days.indexOf('Saturday')>=0?data.info:{}):
    (this.infoDays['Saturday'].index===data.index?(data.days.indexOf('Saturday')>=0?data.info:{}):this.infoDays['Saturday'])
    this.infoDays['Sunday']=Object.getOwnPropertyNames(this.infoDays['Sunday']).length===0?
    (data.days.indexOf('Sunday')>=0?data.info:{}):
    (this.infoDays['Sunday'].index===data.index?(data.days.indexOf('Sunday')>=0?data.info:{}):this.infoDays['Sunday'])  
    this.forceUpdate()
  }

  deleteItem = async (data) =>{
    await this.update(data)
    this.setState({
      Schedules:this.state.Schedules.slice(0,data.index-1)
    })
  }

  renderSchedules = () => {
    const {Schedules} = this.state
    if(Schedules.length>0){
      return Schedules.map((v,i) => 
        <Row key={i+1}><Col md={12}>
          <ScheduleDay key={i+1} update={this.update} 
            index={i+1} current={this.infoDays} 
            last={Schedules.length===i+1?true:false}
            deleteItem={this.deleteItem} 
          />
        </Col></Row>
      )
    }
  }

  addOtherTime = () =>{
    const {Schedules} = this.state
    let newSch = Schedules
    if(Schedules.length<6){
      newSch.push(Schedules.length)
      this.setState({
        Schedules:newSch
      })
    }
  }

  render() {
    return (
      <CardBody className='ScheduleModal'>
        <ToastContainer />
        <Row>
          <Col md={12}>
            <CardTitle>Operator Schedule</CardTitle>
          </Col>
        </Row>
        <hr className='Line'/>
        <Row>
          <Col md={12}>
            <ScheduleDay key={0} update={this.update} index={0} current={this.infoDays} last={false}/>
          </Col>
        </Row>
        {this.renderSchedules()}
        <Row><Col md={12}>
          <button className='btn-cosmic' onClick={this.addOtherTime}>
            <i className="tim-icons icon_add" />
            <span>Add Field Schedule</span>
          </button>
        </Col></Row>
        <Row>
          <Col md={12} className='cosmic-butons butonsSchedule'>
            <Button className='cosmic-cancel' onClick={this.onBack}>{'Cancel'}</Button>
            <Button className='cosmic-confirm' onClick={this.onSave}>{'Update Schedule'}</Button>
          </Col>
        </Row>  
      </CardBody>
    );
  }
}

export default ModalSchedule;