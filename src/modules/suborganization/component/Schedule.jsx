import React from 'react';
import { ToastContainer, toast } from 'react-toastify'
import {
  Button,
  Card,
  CardBody,
  Label,
  FormGroup,
  Input,  
  Row,
  Col,
} from "reactstrap";
// import ScheduleDay from './ScheduleDay'
import ScheduleDay from './ItemSchedule'

const gtm = [
  {text:'(+12:00 GMT)',value:'+1200'},
  {text:'(+11:00 GMT)',value:'+1100'},
  {text:'(+10:00 GMT)',value:'+1000'},
  {text:'(+09:00 GMT)',value:'+0900'},
  {text:'(+08:00 GMT)',value:'+0800'},
  {text:'(+07:00 GMT)',value:'+0700'},
  {text:'(+06:00 GMT)',value:'+0600'},
  {text:'(+05:00 GMT)',value:'+0500'},
  {text:'(+04:00 GMT)',value:'+0400'},
  {text:'(+03:00 GMT)',value:'+0300'},
  {text:'(+02:00 GMT)',value:'+0200'},
  {text:'(+01:00 GMT)',value:'+0100'},
  {text:'(+00:00 GMT)',value:'+0000'},
  {text:'(-01:00 GMT)',value:'-0100'},
  {text:'(-02:00 GMT)',value:'-0200'},
  {text:'(-03:00 GMT)',value:'-0300'},
  {text:'(-04:00 GMT)',value:'-0400'},
  {text:'(-05:00 GMT)',value:'-0500'},
  {text:'(-06:00 GMT)',value:'-0600'},
  {text:'(-07:00 GMT)',value:'-0700'},
  {text:'(-08:00 GMT)',value:'-0800'},
  {text:'(-09:00 GMT)',value:'-0900'},
  {text:'(-10:00 GMT)',value:'-1000'},
  {text:'(-11:00 GMT)',value:'-1100'},
  {text:'(-12:00 GMT)',value:'-1200'}
]

class Schedule extends React.Component {
  constructor(props) {
    super(props);    
    this.infoDays = {
      Monday:{},
      Tuesday:{},
      Wednesday:{},
      Thursday:{},
      Friday:{},
      Saturday:{},
      Sunday:{},
    }
    this.state ={
      Schedules:[],
      timeZ:'+0000'
    }
  }
  update = (data) =>{    
    this.infoDays['Monday']=Object.getOwnPropertyNames(this.infoDays['Monday']).length===0?
    (data.days.indexOf('Monday')>=0?data.info:{}):
    (this.infoDays['Monday'].index===data.index?(data.days.indexOf('Monday')>=0?data.info:{}):this.infoDays['Monday'])
    this.infoDays['Tuesday']=Object.getOwnPropertyNames(this.infoDays['Tuesday']).length===0?
    (data.days.indexOf('Tuesday')>=0?data.info:{}):
    (this.infoDays['Tuesday'].index===data.index?(data.days.indexOf('Tuesday')>=0?data.info:{}):this.infoDays['Tuesday'])
    this.infoDays['Wednesday']=Object.getOwnPropertyNames(this.infoDays['Wednesday']).length===0?
    (data.days.indexOf('Wednesday')>=0?data.info:{}):
    (this.infoDays['Wednesday'].index===data.index?(data.days.indexOf('Wednesday')>=0?data.info:{}):this.infoDays['Wednesday'])
    this.infoDays['Thursday']=Object.getOwnPropertyNames(this.infoDays['Thursday']).length===0?
    (data.days.indexOf('Thursday')>=0?data.info:{}):
    (this.infoDays['Thursday'].index===data.index?(data.days.indexOf('Thursday')>=0?data.info:{}):this.infoDays['Thursday'])
    this.infoDays['Friday']=Object.getOwnPropertyNames(this.infoDays['Friday']).length===0?
    (data.days.indexOf('Friday')>=0?data.info:{}):
    (this.infoDays['Friday'].index===data.index?(data.days.indexOf('Friday')>=0?data.info:{}):this.infoDays['Friday'])
    this.infoDays['Saturday']=Object.getOwnPropertyNames(this.infoDays['Saturday']).length===0?
    (data.days.indexOf('Saturday')>=0?data.info:{}):
    (this.infoDays['Saturday'].index===data.index?(data.days.indexOf('Saturday')>=0?data.info:{}):this.infoDays['Saturday'])
    this.infoDays['Sunday']=Object.getOwnPropertyNames(this.infoDays['Sunday']).length===0?
    (data.days.indexOf('Sunday')>=0?data.info:{}):
    (this.infoDays['Sunday'].index===data.index?(data.days.indexOf('Sunday')>=0?data.info:{}):this.infoDays['Sunday'])    
    this.forceUpdate()
  }

  deleteItem = async (data) =>{
    await this.update(data)    
    this.setState({
      Schedules:this.state.Schedules.slice(0,data.index-1)
    })
  }

  onBack = () =>{
    document.querySelector('.scrollbar-wrapper').scrollTop=0
    this.props.firstStep()
  }

  onSave = async () =>{
    let valid = true
    await Object.entries(this.infoDays).forEach(val => {
      if(Object.entries(val[1]).length>0 && valid===true){
        if(val[1].start.split(':')[0]>=val[1].end.split(':')[0]){
          toast.error('Start time should not be greater or equal than end time', {position: toast.POSITION_TOP_RIGHT})
          valid=false
        }
      }
    })
    const schedule = {
      monday_start:this.infoDays.Monday===null?null:this.infoDays.Monday.start,
      monday_end:this.infoDays.Monday===null?null:this.infoDays.Monday.end,
      tuesday_start:this.infoDays.Tuesday===null?null:this.infoDays.Tuesday.start,
      tuesday_end:this.infoDays.Tuesday===null?null:this.infoDays.Tuesday.end,
      wednesday_start:this.infoDays.Wednesday===null?null:this.infoDays.Wednesday.start,
      wednesday_end:this.infoDays.Wednesday===null?null:this.infoDays.Wednesday.end,
      thursday_start:this.infoDays.Thursday===null?null:this.infoDays.Thursday.start,
      thursday_end:this.infoDays.Thursday===null?null:this.infoDays.Thursday.end,
      fryday_start:this.infoDays.Friday===null?null:this.infoDays.Friday.start,
      fryday_end:this.infoDays.Friday===null?null:this.infoDays.Friday.end,
      saturday_start:this.infoDays.Saturday===null?null:this.infoDays.Saturday.start,
      saturday_end:this.infoDays.Saturday===null?null:this.infoDays.Saturday.end,
      sunday_start:this.infoDays.Sunday===null?null:this.infoDays.Sunday.start,
      sunday_end:this.infoDays.Sunday===null?null:this.infoDays.Sunday.end,
    }
    if(valid){
      const gmt = this.state.timeZ
      this.props.onSave({schedule:schedule,gmt:gmt})
    } 
  }

  renderSchedules = () => {
    const {Schedules} = this.state
    if(Schedules.length>0){
      return Schedules.map(i => 
        <Row key={i}><Col md={12}>
          <ScheduleDay key={i+1} update={this.update} 
            index={i+1} current={this.infoDays} 
            last={Schedules.length===i+1?true:false}
            deleteItem={this.deleteItem} 
          />
        </Col></Row>
      )
    }
  }

  addOtherTime = () =>{
    const {Schedules} = this.state
    let newSch = Schedules
    if(Schedules.length<6){
      newSch.push(Schedules.length)
      this.setState({
        Schedules:newSch
      })
    }
  }

  changeValue = (e) =>{
    this.setState({timeZ:e.target.value})
  }

  render() {    
    return (
      <>
      <ToastContainer />
        <Card>        
          <CardBody>
            <Row>
              <Col md={12}>
               <p>Operator Schedule</p>
               <label >The schedule defines the availability of the operator to deliver and pick up the vehicles.</label>                
              </Col>
            </Row>
            <Row>
              <Col md={8}>
                <Row>
                  <Col md={12}>
                    <ScheduleDay update={this.update} index={0} current={this.infoDays} last={false}/>
                  </Col>
                </Row>
                {this.renderSchedules()}
                <Row><Col>
                  <button className='btn-cosmic' onClick={this.addOtherTime}>
                    <i className="tim-icons icon_add" />
                    <span>Add Schedule</span>
                  </button>
                </Col></Row>
                
              </Col>
              <Col md={4}>
                <Row>
                  <p>Time Zone</p>
                  <label>Select the time zone that you want to use when dealing with dates and times.</label>
                </Row>
                <br></br>
                <Row>
                  <Col md={4}>
                    <Label>Time Zone</Label>
                  </Col>
                  <Col md={8}>
                    <FormGroup>
                      <Input type="select" value={this.state.timeZ} onChange={this.changeValue}>
                        {gtm.map((v,i) => {
                          return <option key={i} value={v.value}>{v.text}</option>
                        })}
                      </Input>
                    </FormGroup>
                  </Col>
                  
                </Row>                
              <Col>
              
            </Col>
                <Row className='cosmic-butons butonsSchedule'>
                  <Button className='cosmic-cancel' onClick={this.onBack}>{'Back'}</Button>
                  <Button className='cosmic-confirm' onClick={this.onSave}>{'Create Operator'}</Button>
                </Row>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </>
    );
  }
}

export default Schedule;