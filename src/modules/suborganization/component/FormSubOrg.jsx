import React from 'react';
import { Link } from 'react-router-dom';
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Label,
  FormGroup,
  Input,
  Row,
  Col,  
  InputGroupAddon,
  InputGroup,
  InputGroupText
} from "reactstrap";

import {getStates, getCities} from '../../../api/subOrganizations.js'
import Select from 'react-select';

class FormSubOrg extends React.Component {
  constructor(props) {
    super(props);    
    this.valid = {
      name:"",
      phone:"",
      indicative:"",
      mobile_phone:"",
      email:"",
      id_plan:0,

      id_country:"",
      id_state:"",
      id_city:"",
      address:"",
      zip_code:"",
		  
      devices:[],
      status:'',

      u_uuid:"",
      u_first_name: "",
      u_last_name: "",
      u_email: "",
      u_indicative: "",
      u_phone: "",
      u_password:"",
      u_c_password:"",
      u_twosteps:false,

      u_id_status: "",
      u_id_organization:"",
    }
    this.state = {
      type: 'password',
      name:"",
      phone:"",
      indicative:"",
      mobile_phone:"",
      email:"",
      id_plan:0,

      id_country:0,
      id_state:0,
      id_city:0,
      address:"",
      zip_code:"",
		  
      devices:[],
      status:'',
      
      geofence:{},      

      states:[],
      cities:[],
      
      u_uuid:"",
      u_first_name: "",
      u_last_name: "",
      u_email: "",
      u_indicative: "",
      u_phone: "",
      u_password:"",
      u_c_password:"",
      u_twosteps:false,

      u_id_status: "",
      u_id_organization:"",

      selectValue:''
    }
  }

  saveForm = async () => {
    if(!this.validForm()){
      this.props.errorValidation('Complete the form')
    }
    else{
      const object = {
        name:this.state.name,
        phone:this.state.phone,
        indicative:this.state.indicative,
        mobile_phone:this.state.mobile_phone,
        email:this.state.email,
        id_plan:this.state.id_plan,
        id_country:this.state.id_country,
        id_state:this.state.id_state,
        id_city:this.state.id_city,
        address:this.state.address,
        zip_code:this.state.zip_code,
        devices:this.state.devices,
        state:this.state.status,
        geofence:{},
      }

      const userObject = {          
        first_name:this.state.u_first_name,
        last_name:this.state.u_last_name,
        email:this.state.u_email,
        indicative:this.state.u_indicative,
        phone:this.state.u_phone,
        password:this.state.u_password,
        c_password:this.state.u_c_password,
        use_2fa:this.state.u_twosteps,        
        id_country:this.state.id_country,
        id_state:this.state.id_state,
        id_city:this.state.id_city,
        address:this.state.address,
        zip_code:this.state.zip_code,
      }
      await this.props.updateInfo({suborg:object,user:userObject})  
      document.querySelector('.scrollbar-wrapper').scrollTop=0
      await this.props.lastStep()
    }
  }
  
  validForm = () =>{
    let {
      status,name,phone,indicative,mobile_phone,email,id_plan,id_country,id_state,id_city,address,zip_code,devices,
      u_first_name,u_last_name,u_email,u_phone,u_password,u_c_password,u_indicative,c_password,password
    } = this.state
    let formValid = true
    name.trim()
    phone.trim()
    mobile_phone.trim()
    zip_code.trim()
    u_first_name.trim()
    u_phone.trim()

    if(status===''){
      this.valid.status = 'has-danger'
      formValid = false
    }else{this.valid.status = ''}

    if(name===''){
      this.valid.name = 'has-danger'
      formValid = false
    }else{this.valid.name = ''}

    if(phone===''){
      this.valid.phone = 'has-danger'
      formValid = false
    }else if(phone.length>10){
      this.valid.phone = 'has-danger'
      formValid = false
    }else{this.valid.phone = ''}  

    if(indicative==='' || indicative===0){
      this.valid.mobile_phone = 'has-danger'
      formValid = false
    }else{this.valid.mobile_phone = ''}    

    if(mobile_phone===''){
      this.valid.mobile_phone = 'has-danger'
      formValid = false
    }else if(mobile_phone.length>12){
      this.valid.mobile_phone = 'has-danger'
      formValid = false
    }else{this.valid.mobile_phone = ''}    


    if(email===''){
      this.valid.email = 'has-danger'
      formValid = false
    }else{this.valid.email = ''}

    if(id_plan===0){
      this.valid.id_plan = 'has-danger'
      formValid = false
    }else{this.valid.id_plan = ''}   

    if(id_country===0){
      this.valid.id_country = 'has-danger'
      formValid = false
    }else{
      this.valid.id_country = ''
    }   
    if(id_state===0){
      this.valid.id_state = 'has-danger'
      formValid = false
    }else{this.valid.id_state = ''}   
    
    if(id_city===0){
      this.valid.id_city = 'has-danger'
      formValid = false
    }else{this.valid.id_city = ''} 
    
    if(address===''){
      this.valid.address = 'has-danger'
      formValid = false
    }else{this.valid.address = ''}
    
    if(zip_code==='' || zip_code.length>10){
      this.valid.zip_code = 'has-danger'
      formValid = false
    }else{this.valid.zip_code = ''}

    if(devices.length===0){
      this.valid.devices = 'has-danger'
      formValid = false
    }else{this.valid.devices = ''}

    if(u_first_name===''){
      this.valid.u_first_name = 'has-danger'
      formValid = false
    }else{this.valid.u_first_name = ''}
    
    if(u_last_name===''){
      this.valid.u_last_name = 'has-danger'
      formValid = false
    }else{this.valid.u_last_name = ''}

    if(u_email===''){
      this.valid.u_email = 'has-danger'
      formValid = false
    }else{this.valid.u_email = ''}

    if(u_indicative===''){
      this.valid.u_phone = 'has-danger'
      formValid = false
    }else{this.valid.u_phone = ''}  

    if(u_phone===''){
      this.valid.u_phone = 'has-danger'
      formValid = false
    }else{this.valid.u_phone = ''}

    if(u_password==='' || u_password.length < 8){
      this.valid.u_password = 'has-danger'
      formValid = false
    }else{this.valid.u_password = ''} 

    if(u_c_password === '' || c_password !== password){
      this.valid.u_c_password = 'has-danger'
      formValid = false
    }else{this.valid.u_c_password = ''}
    
    return formValid
  }

  changeValue = async (e) => {
    const name = e.target.name
    const value = e.target.value
    switch(name) {
      case 'name':
        this.setState({name:value})
      break;
      case 'phone':
        this.setState({phone:value})
      break;
      case 'indicative':
        this.setState({indicative:value})
      break;
      case 'mobile_phone':
        this.setState({mobile_phone:value})
      break;
      case 'email':
        this.setState({email:value})
      break;
      case 'id_plan':
        this.setState({id_plan:value})
      break;
      case 'id_country':
        const infoStates = await getStates({id_country:value})
        if(infoStates.st && infoStates.status===200){
          this.setState({
            id_country:value,
            id_state:0,
            id_city:0,
            states:infoStates.States
          })
        }
      break;
      case 'id_state':             
        const infoCity = await getCities({id_state:value})
        if(infoCity.st && infoCity.status===200){
          this.setState({
            id_state:value,
            id_city:0,
            cities:infoCity.Cities
          })
        }        
      break;
      case 'id_city':             
          this.setState({id_city:value})
      break;
      case 'address':
          this.setState({address:value})
      break;
      case 'zipCode':
        this.setState({zip_code:value})
      break;
      case 'orgstate':
          this.setState({status:value})
        break;
      case 'geofence':
        this.setState({geofence:value})
      break;

      case 'u_first_name':
        this.setState({u_first_name:value})
      break;
      case 'u_last_name':
        this.setState({u_last_name:value})
      break;
      case 'u_email':
        this.setState({u_email:value})
      break;
      case 'u_indicative':
        this.setState({u_indicative:value})
      break;
      case 'u_phone':
        this.setState({u_phone:value})
      break;
      case 'u_password':
        this.setState({u_password:value})
      break;
      case 'u_c_password':             
        this.setState({u_c_password:value}) 
      break;
      case 'u_twosteps':        
        this.setState({u_twosteps:e.target.checked})
      break;
      default:
        break;
    }
  }

handleSelectChange = (selectValue) => {
  let devices = selectValue.map(val => val.value)  
  this.setState({ selectValue, devices});
}
showHide = () => {
  this.setState({
    type: this.state.type === 'input' ? 'password' : 'input'
  })
}
  
  render() {
    const LabelLong = 3
    const FieldLong = 9
    return (
      <>       
      <Card>
        <CardHeader>
          <Row>
            <Col md ='1'></Col>
            {this.props.New?null:
              <Col md ='4'>
                <CardTitle tag="h4">{this.props.CardTitle}</CardTitle>
              </Col>
            }
          </Row>                
        </CardHeader>
        <CardBody>
          <Row>
            <Col sm="4">
              <Row>
                <Label sm={LabelLong}>Name</Label>
                <Col sm={FieldLong}>
                  <FormGroup className={this.valid.name}>
                    <Input 
                      name='name'
                      type="text"
                      placeholder="Name"
                      value={this.state.name}
                      onChange={this.changeValue}
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Label sm={LabelLong}>Phone</Label>
                <Col sm={FieldLong}>
                  <FormGroup className={this.valid.phone}>
                    <Input 
                      name='phone'
                      type="text"
                      placeholder="Number Phone"
                      value={this.state.phone}
                      maxLength={10}
                      onChange={this.changeValue}
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Label sm={LabelLong}>Mobile</Label>
                <Col sm={FieldLong}>
                  <FormGroup className={this.valid.mobile_phone+' phone-cosmic'}>
                    <Input 
                      className='phone-cosmic-1'
                      name='indicative'
                      type="select"
                      value={this.state.indicative}
                      onChange={this.changeValue}
                    >
                      <option value=''>Sel...</option>
                      {this.props.Countries.map((country, index) => <option key={index} value={country.indicative}>{country.country}</option>)}
                    </Input >   
                    <Input
                      className='phone-cosmic-2'
                      type="text"
                      disabled 
                      placeholder="+00"
                      value={this.state.indicative} />
                    <Input                      
                      className='phone-cosmic-3'
                      name='mobile_phone'
                      type="text"
                      maxLength={12}
                      placeholder="3114511876"
                      value={this.state.mobile_phone}
                      onChange={this.changeValue}
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Label sm={LabelLong}>Email</Label>
                <Col sm={FieldLong}>
                  <FormGroup className={this.valid.email}>
                    <Input 
                      name='email'
                      type="text"
                      placeholder="Email address"
                      value={this.state.email}
                      onChange={this.changeValue}
                    />
                  </FormGroup>
                </Col>
              </Row>
            </Col>
            <div className="divider"></div>
            <Col sm="4">
              <Row>
                <Label sm={LabelLong}>Country</Label>
                <Col sm={FieldLong}>
                  <FormGroup className={this.valid.id_country}>
                    <Input                     
                      name='id_country'
                      type="select"                          
                      value={this.state.id_country}
                      onChange={this.changeValue}
                    >
                    <option value={0}>Select Country</option>
                    {this.props.Countries.map((country, index) => <option key={index} value={country.id_country}>{country.country}</option>)}
                    </Input>
                  </FormGroup>
                </Col>   
              </Row>

              <Row>
                <Label sm={LabelLong}>State</Label>
                <Col sm={FieldLong}>
                  <FormGroup className={this.valid.state}>
                    <Input
                      name='id_state'
                      type="select"                          
                      value={this.state.id_state}
                      onChange={this.changeValue}
                    >
                    <option value={0}>Select a state</option>
                    {this.state.states.map((state, index) => <option key={index} value={state.id_state}>{state.state}</option>)}
                    </Input>
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Label sm={LabelLong}>City</Label>
                <Col sm={FieldLong}>
                  <FormGroup className={this.valid.city}>
                    <Input
                      name='id_city'
                      type="select"                          
                      value={this.state.id_city}
                      onChange={this.changeValue}
                    >
                    <option value={0}>Select a city</option>
                    {this.state.cities.map((city, index) => <option key={index} value={city.id_city}>{city.city}</option>)}
                    </Input>
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Label sm={LabelLong}>Street</Label>
                <Col sm={FieldLong}>
                  <FormGroup className={this.valid.address}>
                    <Input                     
                      name='address'
                      type="text"                          
                      value={this.state.address}
                      onChange={this.changeValue}
                    >                  
                    </Input>
                  </FormGroup>
                </Col>
              </Row>
              
              <Row>
                <Label sm={LabelLong}>Zip Code</Label>
                <Col sm={FieldLong}>
                  <FormGroup className={this.valid.zip_code}>
                    <Input 
                      name='zipCode'
                      type="text"   
                      maxLength={10}                       
                      value={this.state.zip_code}
                      onChange={this.changeValue}                    
                    />
                  </FormGroup>
                </Col>
              </Row>
            </Col>
            <div className="divider"></div>
            <Col sm="4">
            </Col>
          </Row>
          <Row><br></br></Row>
          <Row>
            <Col sm="4">
              <p>Associated devices</p>
              <label >Select the devices associated whit the Operator</label>
              <FormGroup> 
              <Select
                closeMenuOnSelect={false}
                isMulti
                onChange={this.handleSelectChange}
                options={ this.props.ListDevices.map((device) => {
                  return ({label:device.device, value:device.id_device_type})
                })}
                placeholder="Select device(s)"
                className='multi-cosmic'
                classNamePrefix='MC'
                value={this.state.selectValue}
              />
              </FormGroup>              
            </Col>
            <Col sm="1"></Col>
            <Col sm="5">
              <p>Change status</p>
              <label >The members of the suborganization can log in according to the status assigned</label>  
              <FormGroup>
                <Input type="select" name="orgstate"                  
                  value={this.state.status}
                  onChange={this.changeValue}  
                >
                <option value=''>Select..</option>
                {this.props.ListStatus.map((status, index) => {
                  if(status.id_status===4){
                    return null
                  }else{
                    return <option key={index} value={status.id_status}>{status.status}</option>
                  }
                })}
                </Input>
              </FormGroup> 
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Card>
        <CardHeader>
          <Row>
            <Col md ='1'></Col>
            <Col md ='4'>
              <CardTitle tag="h4">Admin User</CardTitle>
            </Col>
          </Row>                
        </CardHeader>          
        <CardBody>          
            <Row>
              <Col sm="6">
                <Row>
                  <Label sm="3">Name</Label>
                  <Col sm="7">
                    <FormGroup className={this.valid.u_first_name}>
                      <Input
                        name='u_first_name'
                        type="text"
                        placeholder="Daniel R"
                        value={this.state.u_first_name}
                        onChange={this.changeValue}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Label sm="3">Last Name</Label>
                    <Col sm="7">
                      <FormGroup className={this.valid.u_last_name}>
                        <Input
                          name='u_last_name'
                          type="text"
                          placeholder="McMahon"
                          value={this.state.u_last_name}
                          onChange={this.changeValue}
                        />
                      </FormGroup>
                    </Col>
                </Row>
                <Row>
                  <Label sm="3">Email</Label>
                    <Col sm="7">
                      <FormGroup className={this.valid.u_email}>
                        <Input
                          name='u_email'
                          type="email"
                          placeholder="admin@mail.com"
                          value={this.state.u_email}
                          onChange={this.changeValue}
                        />
                      </FormGroup>
                    </Col>
                </Row>
              </Col>
              <Col sm="6">
                <Row>
                  <Label sm='3'>Mobile Number</Label>
                  <Col sm='7'>
                    <FormGroup className={this.valid.u_phone+' phone-cosmic'}>
                      <Input 
                        className='phone-cosmic-1'
                        name='u_indicative'
                        type="select"
                        value={this.state.u_indicative}
                        onChange={this.changeValue}
                      >
                        <option value={0}>Sel...</option>
                        {this.props.Countries.map((country, index) => <option key={index} value={country.indicative}>{country.country}</option>)}
                      </Input >   
                      <Input
                        className='phone-cosmic-2'
                        type="text"
                        disabled 
                        placeholder="+00"
                        value={this.state.u_indicative} />
                      <Input                      
                        className='phone-cosmic-3'
                        name='u_phone'
                        maxLength={12}
                        type="text"
                        placeholder="3114511876"
                        value={this.state.u_phone}
                        onChange={this.changeValue}
                      />
                    </FormGroup>
                  </Col>
                </Row>                
                <Row>
                  <Label sm="3">Password</Label>
                    <Col sm="7">
                      <FormGroup className={this.valid.u_password}>
                        <InputGroup>
                          <Input
                            name='u_password'
                            type={this.state.type}
                            placeholder="New Pass"
                            maxLength={12}
                            value={this.state.u_password}
                            onChange={this.changeValue}
                          />
                          <InputGroupAddon addonType="append" onClick={this.showHide}>
                            <InputGroupText>
                              {this.state.type === 'input' ? <i className="fa fa-eye-slash" /> : <i className="fa fa-eye" />}
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                </Row>
                <Row>
                  <Label sm="3">Confirm Password</Label>
                    <Col sm="7">
                      <FormGroup className={this.valid.u_c_password}>
                        <InputGroup>
                          <Input
                            name='u_c_password'
                            type={this.state.type}
                            placeholder="confirm new Pass"
                            value={this.state.u_c_password}
                            maxLength={12}
                            onChange={this.changeValue}
                          />
                          <InputGroupAddon addonType="append" onClick={this.showHide}>
                            <InputGroupText>
                              {this.state.type === 'input' ? <i className="fa fa-eye-slash" /> : <i className="fa fa-eye" />}
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                </Row>
              </Col>
            </Row> 
            <Row>
              <Col sm="6"></Col>
              <Col sm="6">
                <FormGroup check className="mt-3">
                  <Label check>
                    <Input 
                      type="checkbox"
                      name="u_twosteps"
                      checked={this.state.u_twosteps}
                      onChange={this.changeValue}
                    />
                    <span className="form-check-sign" />
                    Two-step authentication          
                    <p>Keep your account extra secure with a second authentication step.</p>
                  </Label>
                </FormGroup>
              </Col>
            </Row>        
          </CardBody>
          <CardFooter className="text-center">        
        
          </CardFooter>
        </Card>
      
      <Card>
        <CardBody>
          <Row>
            <Col sm="6">
              <Row><p>Change a plan</p></Row>
              <Row><label >The plan defines the number of users and records that can be managed by the suborganization.</label></Row>
              <Row><br></br></Row>              
              <Row>
                <Label sm='3'>Plan</Label>
                <Col sm='7'>
                  <FormGroup className={this.valid.plan}>
                    <Input                           
                      type="select"
                      name="id_plan"                    
                      value={this.state.id_plan}
                      onChange={this.changeValue}                    
                    >
                    <option value={0}>Select..</option>
                    {this.props.Plans.map((plan, index) => <option key={index} value={plan.id_plan}>{plan.name}</option>)}
                    </Input>
                  </FormGroup>
                </Col>
              </Row>              
            </Col>
            <Col sm="6" className='cosmic-butons butonsFormInfo'>
              <Link to='/admin/admin-operator'>
                <Button className='cosmic-cancel'>Cancel</Button>   
              </Link>    
              <div>
                <Button className='cosmic-confirm' onClick={this.saveForm}>Next</Button>
              </div>   
            </Col>
          </Row>
        </CardBody>
      </Card>
      </>
    );
  }
  
//   var MultiSelectField = createClass({
//     displayName: 'MultiSelectField',
//     propTypes: {
//       label: PropTypes.string,
//     },
//     getInitialState () {
//       return {
//         removeSelected: true,
//         disabled: false,
//         crazy: false,
//         stayOpen: false,
//         value: [],
//         rtl: false,
//       };
//     },
//     

//     toggleCheckbox (e) {
//       this.setState({
//         [e.target.name]: e.target.checked,
//       });
//     },

//     toggleRtl (e) {
//       let rtl = e.target.checked;
//       this.setState({ rtl });
//     },
  
//     render () {
//       const { crazy, disabled, stayOpen, value } = this.state;
//       const options = crazy ? WHY_WOULD_YOU : FLAVOURS;
//       return (
//         <div className="section">
//           <h3 className="section-heading">{this.props.label} <a href="https://github.com/JedWatson/react-select/tree/v1.x/examples/src/components/Multiselect.js">(Source)</a></h3>
//           <Select
//             closeOnSelect={!stayOpen}
//             disabled={disabled}
//             multi
//             onChange={this.handleSelectChange}
//             options={options}
//             placeholder="Select your favourite(s)"
//             removeSelected={this.state.removeSelected}
//             rtl={this.state.rtl}
//             simpleValue
//             value={value}
//           />

// removeSelected
// stayOpen
//       );
//     }
//   });
}

export default FormSubOrg;