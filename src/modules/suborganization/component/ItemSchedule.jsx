import React from 'react';
import {
  Label,
  FormGroup,
  Input,
  Row,
  Col,
  Button,
  Form,
} from "reactstrap";


class ScheduleDay extends React.Component {
  constructor(props) {
    super(props);    
    this.infoDays = {
      Monday:{},
      Tuesday:{},
      Wednesday:{},
      Thursday:{},
      Friday:{},
      Saturday:{},
      Sunday:{},
    }
    this.state = {
      Monday:false,
      Tuesday:false,
      Wednesday:false,
      Thursday:false,
      Friday:false,
      Saturday:false,
      Sunday:false,

      days:[],
      HStart:'1',
      MStart:'0',
      AMPMStart:'0',
      HEnd:'1',
      MEnd:'0',
      AMPMEnd:'0',
      printInfo:false
    }
  }

  componentDidMount(){
    this.setCurrent()
  }

  setCurrent = async() =>{
    let HS = '', HE = ''
    let days=[]
    const cur = await this.props.current
    await Object.entries(cur).map(val => {
      if(Object.entries(val[1]).length>0){
        if(val[1].index===this.props.index){
          this.infoDays[val[0]]=true
          days.push(val[0])
          if(HS==='' && HE==='') {
            HS=val[1].start
            HE=val[1].end
          }
        }else{          
          this.infoDays[val[0]]=false
        }
      }else{
        this.infoDays[val[0]]=false
      }
    })
    if(HS==='' && HE==='') {
      HS='1:0'
      HE='1:0'
    }
    HS=HS.split(':')
    HE=HE.split(':')    
    this.setState({
      days:days,
      HStart:HS[0]>12?(HS[0]-12<10?'0'+HS[0]-12:HS[0]-12):HS[0],
      MStart:HS[1],
      AMPMStart:HS[0]>12?'12':'0',
      HEnd:HE[0]>12?(HE[0]-12<10?'0'+(HE[0]-12).toString():HE[0]-12).toString():HE[0].toString(),
      MEnd:HE[1],
      AMPMEnd:HE[0]>12?'12':'0',
      Monday:this.infoDays['Monday'],
      Tuesday:this.infoDays['Tuesday'],
      Wednesday:this.infoDays['Wednesday'],
      Thursday:this.infoDays['Thursday'],
      Friday:this.infoDays['Friday'],
      Saturday:this.infoDays['Saturday'],
      Sunday:this.infoDays['Sunday'],
    })
  }

  updateUp = () =>{
    const {update} = this.props
    const hend = Number(this.state.HEnd)+Number(this.state.AMPMEnd)
    const hstr = Number(this.state.HStart)+Number(this.state.AMPMStart)
    update({
      days:this.state.days,
      info:{
        start:`${hstr>10?hstr:'0'+hstr}:${this.state.MStart}`,
        end:`${hend>10?hend:'0'+hend}:${this.state.MEnd}`,
        // start:`${(Number(this.state.HStart)+Number(this.state.AMPMStart))}:${Number(this.state.MStart)}`,
        // end:`${Number(this.state.HEnd)+Number(this.state.AMPMEnd)}:${Number(this.state.MEnd)}`,
        index:this.props.index
      },
      index:this.props.index      
    })
  }

  changeValue = async (e) => {
    const name = e.target.name    
    const value = e.target.value
    this.setState({[name]:value},this.updateUp)
  }

  changeState = async (e) => {
    let {days} = this.state
    const name = e.target.name    
    const value = e.target.checked
    if(days.indexOf(name)>=0){
      if(!value) days.splice(days.indexOf(name),1)
    }
    else{
      if(value) days.push(name)
    }
    this.setState({[name]:value,days},this.updateUp)
  }

  deleteItem = () => {
    const {deleteItem} = this.props
    deleteItem({
      days:[],
      index:this.props.index
    })
  }

  render() {
      const {StateDay,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,HStart,MStart,AMPMStart,HEnd,MEnd,AMPMEnd} = this.state
      const {current,last,index} = this.props
      const Hour =  new Array(12).fill(0,0)
      const Min = new Array(60).fill(0,0)
      const DMonday=Object.getOwnPropertyNames(current['Monday']).length===0?false:true
      const DTuesday=Object.getOwnPropertyNames(current['Tuesday']).length===0?false:true
      const DWednesday=Object.getOwnPropertyNames(current['Wednesday']).length===0?false:true
      const DThursday=Object.getOwnPropertyNames(current['Thursday']).length===0?false:true
      const DFriday=Object.getOwnPropertyNames(current['Friday']).length===0?false:true
      const DSaturday=Object.getOwnPropertyNames(current['Saturday']).length===0?false:true
      const DSunday=Object.getOwnPropertyNames(current['Sunday']).length===0?false:true
    return (  
      <Row className={'cenBtn'}>
        <Col md={11}>
          <Row>
            <Col>
              <FormGroup check>
                <Label check>
                  <Input type="checkbox" name='Monday' onChange={this.changeState} checked={Monday}
                    disabled={Monday?false:DMonday}
                  />
                  <span className="form-check-sign" />
                    Mon
                </Label>
              </FormGroup>
            </Col>
            <Col >
              <FormGroup check>
                <Label check>
                  <Input type="checkbox" name='Tuesday' onChange={this.changeState} checked={Tuesday}
                    disabled={Tuesday?false:DTuesday}
                  />
                  <span className="form-check-sign" />
                  Tue
                </Label>
              </FormGroup>
            </Col>
            <Col >
              <FormGroup check>
                <Label check>
                  <Input type="checkbox" name='Wednesday' onChange={this.changeState} checked={Wednesday}
                    disabled={Wednesday?false:DWednesday}
                  />
                  <span className="form-check-sign" />
                    Wed
                </Label>
              </FormGroup>
            </Col>
            <Col >
              <FormGroup check>
                <Label check>
                  <Input type="checkbox" name='Thursday' onChange={this.changeState} checked={Thursday}
                    disabled={Thursday?false:DThursday}
                  />
                  <span className="form-check-sign" />
                  Thu
                </Label>
              </FormGroup>
            </Col>
            <Col >
              <FormGroup check>
                <Label check>
                  <Input type="checkbox" name='Friday' onChange={this.changeState} checked={Friday}
                    disabled={Friday?false:DFriday}
                  />
                  <span className="form-check-sign" />
                    Fri
                </Label>
              </FormGroup>
            </Col>
            <Col >
              <FormGroup check>
                <Label check>
                  <Input type="checkbox" name='Saturday' onChange={this.changeState} checked={Saturday}
                    disabled={Saturday?false:DSaturday}
                  />
                  <span className="form-check-sign" />
                  Sat
                </Label>
              </FormGroup>
            </Col>
            <Col >
              <FormGroup check>
                <Label check>
                  <Input type="checkbox" name='Sunday' onChange={this.changeState} checked={Sunday}
                    disabled={Sunday?false:DSunday}
                  />
                  <span className="form-check-sign" />
                    Sun
                </Label>
              </FormGroup>
            </Col>
          </Row>
          <br></br>
          <Row className='lineDate'>            
            <Label className={!StateDay ? "text_color" : "disable_btn"}>Start Time</Label>
            <Col>
              <FormGroup>
                <Input className={'scheduleSelect'} type="select" onChange={this.changeValue} disabled={StateDay}
                  value={HStart} name='HStart'
                >
                    {Hour.map((v,i) => {
                      let formatNum = (i+1)<10?'0'+(i+1):(i+1)
                      return <option key={i} value={formatNum}>{formatNum}</option>
                    })}
                  </Input>
              </FormGroup>
            </Col>
            <Col>
              <FormGroup>
                <Input className={'scheduleSelect'} type="select" onChange={this.changeValue} disabled={StateDay}
                  value={MStart} name='MStart'
                >
                  {Min.map((v,i) => {
                    let formatNum = i<10?'0'+i:i
                    return <option key={i} value={formatNum}>{formatNum}</option>
                  })}
                </Input>
              </FormGroup>
            </Col>
            <Col>
              <FormGroup>
                <Input className={'scheduleSelect'} type="select" onChange={this.changeValue} disabled={StateDay}
                  value={AMPMStart} name='AMPMStart'
                >
                  <option value={0}>AM</option>
                  <option value={12}>PM</option>
                  })}
                </Input>
              </FormGroup>
            </Col>            
            <Label className={!StateDay ? "text_color" : "disable_btn"}>End Time</Label>
            <Col>
              <FormGroup>
                  <Input className={'scheduleSelect'} type="select" onChange={this.changeValue} disabled={StateDay}
                    value={HEnd} name='HEnd'
                  >
                    {Hour.map((v,i) => {
                      let formatNum = (i+1)<10?'0'+(i+1):(i+1)
                      return <option key={i} value={formatNum}>{formatNum}</option>
                    })}
                  </Input>
              </FormGroup>
            </Col>
            <Col>
              <FormGroup>
              <Input className={'scheduleSelect'} type="select" onChange={this.changeValue} disabled={StateDay}
                  value={MEnd} name='MEnd'
                >
                  {Min.map((v,i) => {
                    let formatNum = i<10?'0'+i:i
                    return <option key={i} value={formatNum}>{formatNum}</option>
                  })}
                </Input>
              </FormGroup>
            </Col>
            <Col>
              <FormGroup>
                <Input className={'scheduleSelect AP'} type="select" onChange={this.changeValue} disabled={StateDay}
                  value={AMPMEnd} name='AMPMEnd'
                >
                  <option value={0}>AM</option>
                  <option value={12}>PM</option>                  
                </Input>
              </FormGroup>
            </Col>            
          </Row>          
        </Col>
        <Col md={1}>
          {last?(
          <Button onClick={this.deleteItem} color="default" size="sm" className="btn-icon btn-link">
            <img alt="" src={require("assets/img/icon_delete.svg")}/>
          </Button>):null}          
        </Col>
        <hr className='Line'/>
      </Row>    
    );
  }
}

export default ScheduleDay;