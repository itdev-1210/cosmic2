import React from 'react';
import {
  Button,
  Label,
  Input,
  Row,
  Col,
  FormGroup,
} from 'reactstrap';
import {getStates, getCities} from '../../../api/subOrganizations.js'
import { ToastContainer, toast } from 'react-toastify'

class UpdateModalOp extends React.Component {
  constructor(props) {
    super(props)
    this.msjToast=''
    this.valid = {
      name:"",
      indicative:"",
      mobilePhone:"",
      email:"",
      id_country:"",
      id_state:"",
      id_city:"",
      address:"",
      zip_code:"",
    }
    this.state = {
      name:"",
      phone:"",
      indicative:"+57",
      mobilePhone:"",
      email:"",

      id_country:0,
      id_state:0,
      id_city:0,
      address:"",
      zip_code:"",

      states:[],
      cities:[],
      
      alert:null,
    }

    this.getInitialInfo = this.getInitialInfo.bind(this)
    this.changeValue = this.changeValue.bind(this)
  }

  validForm = () =>{
    let {name,phone,indicative,mobilePhone,email,id_country,id_state,id_city,address,zip_code} = this.state
    let formValid = true
    name.trim()
    phone.trim()
    mobilePhone.trim()
    zip_code.trim()    
    address.trim()
    email.trim()
    
    if(name===''){
      this.msjToast='Name is not valid'
      return false
    }
    if(indicative==='' || indicative===0){
      this.msjToast='Indicative is not valid'
      return false  
    }

    if(mobilePhone===''){
      this.msjToast='Mobile Pohne is not valid'
      return false
    }

    if(email===''){
      this.msjToast='Email is not valid'
      return false
    }

    if(id_country===0 || id_country==='0'){
      this.msjToast='Country is not valid'
      return false
    }   

    if(id_state===0 || id_state==='0'){
      this.msjToast='The State is not valid'
      return false
    }   
    
    if(id_city===0 || id_city==='0'){
      this.msjToast='City is not valid'
      return false
    } 
    
    if(address===''){
      this.msjToast='Address is not valid'
      return false
    }
    
    if(zip_code==='' || zip_code.length>8){
      this.msjToast='Zip Code is not valid'
      return false
    }
    
    return formValid
  }

  componentWillMount() {
    this.getInitialInfo()
  }

  getInitialInfo = async () => {    
    const {SubOrganization} = this.props    
    let infoStates = await getStates({id_country:SubOrganization.id_country})    
    infoStates.st ? infoStates=infoStates.States : infoStates = []
    let infoCity = await getCities({id_state:SubOrganization.id_state})
    infoCity.st ? infoCity = infoCity.Cities : infoCity = []
    this.setState({
      states:infoStates,
      cities:infoCity,
      name:SubOrganization.name,
      phone:SubOrganization.phone,
      indicative:SubOrganization.indicative,
      mobilePhone:SubOrganization.mobile_phone,
      email:SubOrganization.email,
      id_country:SubOrganization.id_country,
      id_state:SubOrganization.id_state,
      id_city:SubOrganization.id_city,
      address:SubOrganization.address,
      zip_code:SubOrganization.zip_code,      
    })  
  }

  changeStatus = async () => {
    if(!this.validForm()){
      console.log('error de validacion');
      toast.error(this.msjToast, {position: toast.POSITION_TOP_RIGHT})
    }
    else{
      let SubOrganization = {}
      let devices = []
      await this.props.Devices.map((device) => {
        devices.push(device.id_device_type)
      })
      SubOrganization['id_operator'] = this.props.SubOrganization.id_operator
      SubOrganization['name'] = this.state.name
      SubOrganization['email'] = this.state.email
      SubOrganization['address'] = this.state.address
      SubOrganization['id_country'] = this.state.id_country
      SubOrganization['id_state'] = this.state.id_state
      SubOrganization['id_city'] = this.state.id_city
      SubOrganization['geofence'] = this.props.SubOrganization.geofence
      SubOrganization['id_plan'] = this.props.SubOrganization.id_plan
      SubOrganization['indicative'] = this.props.SubOrganization.indicative
      SubOrganization['zip_code'] = this.state.zip_code
      SubOrganization['mobile_phone'] = this.state.mobilePhone
      SubOrganization['state'] = this.props.SubOrganization.id_status
      SubOrganization['devices'] = devices
      this.props.updateData(SubOrganization)
    }
  }

  changeValue = async (e) => {
    var obj = {}
    var name = e.target.name, value = e.target.value
    obj[name] = value
    if(name==='id_country') {
      let infoStates = await getStates({id_country:value})
      if(infoStates.st && infoStates.status===200){
        obj['states'] = infoStates.States
      }else{obj['states'] = []}
    }
    if(name==='id_state') {
      let infoCity = await getCities({id_state:value})
      if(infoCity.st && infoCity.status===200){
        obj['cities'] = infoCity.Cities
      }else{obj['cities'] = []}
    }
    this.setState(obj)
  }

  render() {
    const {name,mobilePhone,indicative,email,id_country,id_state,id_city,address,zip_code,states,cities} = this.state
    const { onClose , Countries} = this.props
    return (
      <Col md={12} id="update_operator">
        <ToastContainer />
        <Row className='field'>
          <Col md={12}>
            <span className="title">{'Update Operator details'}</span>
          </Col>
        </Row>
        <hr className='Line'/>
        <Row className='field'>
          <Label sm={3} className="id-label">Email</Label>
          <Col sm={7} >
            <Input className={this.valid.email} name="email" type="email" value={email} onChange={this.changeValue}/>              
          </Col>
        </Row>
        <Row className='field'>
        
          <Label sm={3} className="id-label">Name</Label>
          <Col sm={7}>
            <Input name="name" type="text" value={name} onChange={this.changeValue}/>
          </Col>
        </Row>
        <Row className='field'>          
          <Label sm={3} className="id-label">Phone</Label>
          <Col sm={7}>
            <FormGroup className={this.valid.mobilePhone+' phone-cosmic'}>
              <Input 
                className='phone-cosmic-1'
                name='indicative'
                type="select"
                value={indicative}
                onChange={this.changeValue}
              >
                <option value=''>Sel...</option>
                {this.props.Countries.map((country, index) => <option key={index} value={country.indicative}>{country.country}</option>)}
              </Input >   
              <Input
                className='phone-cosmic-2'
                type="text"
                disabled 
                placeholder="+00"
                value={indicative} />
              <Input                      
                className='phone-cosmic-3'
                name='mobilePhone'
                type="text"
                maxLength={10}
                placeholder="3114511876"
                value={mobilePhone}
                onChange={this.changeValue}
              />
            </FormGroup>            
          </Col>
        </Row>
           
        <Row className='field'>
          <Col md={12}>
            <span className="title">{'Address'}</span>
          </Col>
        </Row>
        <hr className='Line'/>
        <Row className='field'>
          <Label sm={3} className="id-label">Country</Label>
          <Col sm={7}>
            <Input
              name="id_country"
              type="select"
              value={id_country}
              onChange={this.changeValue}
            >
              <option className="dark-back" value={0}>Select Country</option>
              {Countries.map((country, index) => <option key={index} value={country.id_country} className="dark-back">{country.country}</option>)}
            </Input>
          </Col>
        </Row>
        <Row className='field'>
          <Label sm={3} className="id-label">State</Label>
          <Col sm={7}>
            <Input
              name="id_state"
              type="select"
              value={id_state}
              onChange={this.changeValue}
              onBlur={this.changeValue}
            >
              <option className="dark-back" value={0}>Select State</option>
              {states.map((state, index) => <option key={index} value={state.id_state} className="dark-back">{state.state}</option>)}
            </Input>
          </Col>
        </Row>
        <Row className='field'>
          <Label sm={3} className="id-label">City</Label>
          <Col sm={7}>
            <Input
              name="id_city"
              type="select"
              value={id_city}
              onChange={this.changeValue}
              onBlur={this.changeValue}
            >
              <option className="dark-back" value={0}>Select City</option>
              {cities.map((city, index) => <option key={index} value={city.id_city} className="dark-back">{city.city}</option>)}
            </Input>
          </Col>
        </Row>
        <Row className='field'>
          <Label sm={3} className="id-label">Street</Label>
          <Col sm={7}>
            <Input name="address" type="text" value={address} onChange={this.changeValue}/>              
          </Col>
        </Row>
        <Row className='field'>
          <Label sm={3} className="id-label">Zip code</Label>
          <Col sm={7}>
            <Input name="zip_code" type="text" value={zip_code} onChange={this.changeValue}/>              
          </Col>
        </Row>
        <Row className='cosmic-butons butonsUpdateOpe'>
          <Button className="cosmic-cancel" onClick={onClose}>Cancel</Button>
          <Button className="cosmic-confirm" onClick={this.changeStatus}>Update</Button>
        </Row>
      </Col>
    )
  }
}

export default UpdateModalOp;