import React from 'react';
import {
  Label,
  FormGroup,
  Input,
  Row,
  Col,
} from "reactstrap";


class ScheduleDay extends React.Component {
  constructor(props) {
    super(props);    
    this.state = {
      StateDay:true,
      HStart:1,
      MStart:0,
      AMPMStart:0,
      HEnd:1,
      MEnd:0,
      AMPMEnd:0,
      printInfo:false
    }
  }

  setCurrent = (cur) =>{    
    if(cur.start === null || cur.end === null){
      this.setState({
        StateDay:true,
        printInfo:true
      })
    }
    else{
      let HS = cur.start.split(':')[0]
      let HE = cur.end.split(':')[0]      
      this.setState({
        StateDay:false,
        HStart:HS>12?(HS-12):HS,
        MStart:cur.start.split(':')[1],
        AMPMStart:HS>12?12:0,
        HEnd:HE>12?(HE-12):HE,
        MEnd:cur.end.split(':')[1],
        AMPMEnd:HE>12?12:0,
        printInfo:true
      })
    }
  }

  componentDidUpdate(){    
    if(!this.state.printInfo && this.props.current!==null){
      this.setCurrent(this.props.current)
    }
  }

  changeValue = async (e) => {
    const name = e.target.name    
    const value = e.target.value
    this.setState({[name]:value})
  }

  changeState = async () => {
    this.setState({StateDay:!this.state.StateDay})
  }

  render() {
      const {nameDay} = this.props
      const {StateDay,HStart,MStart,AMPMStart,HEnd,MEnd,AMPMEnd} = this.state      
      const Hour =  new Array(12).fill(0,0)
      const Min = new Array(60).fill(0,0)
      this.props.update({
        state:!StateDay,
        day:nameDay,
        start:`${(Number(HStart)+Number(AMPMStart))}:${MStart}`,
        end:`${Number(HEnd)+Number(AMPMEnd)}:${MEnd}`,
      })
    return (  
      <Row>
        <Col md={this.props.colSize}>
          <Row>
            <Col sm={1}>
              <FormGroup check name='StateDay' onClick={this.changeState}>
                <Input type="checkbox" checked={!StateDay} readOnly/>
                <span className="form-check-sign"/>
              </FormGroup>
            </Col>
            <Label sm={3} className={!StateDay ? "text_color" : "disable_btn"}>{nameDay}</Label>
          </Row>
          <Row>            
            <Label sm={3} className={!StateDay ? "text_color" : "disable_btn"}>Start Time</Label>
            <Col md={3}>
              <FormGroup>
                <Input type="select" onChange={this.changeValue} disabled={StateDay}
                  value={HStart} name='HStart'
                >
                    {Hour.map((v,i) => {
                      let formatNum = (i+1)<10?'0'+(i+1):(i+1)
                      return <option key={i} value={i+1}>{formatNum}</option>
                    })}
                  </Input>
              </FormGroup>
            </Col>
            <Col md={3}>
              <FormGroup>
                <Input type="select" onChange={this.changeValue} disabled={StateDay}
                  value={MStart} name='MStart'
                >
                  {Min.map((v,i) => {
                    let formatNum = i<10?'0'+i:i
                    return <option key={i} value={i}>{formatNum}</option>
                  })}
                </Input>
              </FormGroup>
            </Col>
            <Col md={3}>
              <FormGroup>
                <Input type="select" onChange={this.changeValue} disabled={StateDay}
                  value={AMPMStart} name='AMPMStart'
                >
                  <option value={0}>AM</option>
                  <option value={12}>PM</option>
                  })}
                </Input>
              </FormGroup>
            </Col>            
          </Row>
          <Row>
            <Label sm={3} className={!StateDay ? "text_color" : "disable_btn"}>End Time</Label>
            <Col md={3}>
              <FormGroup>
                  <Input type="select" onChange={this.changeValue} disabled={StateDay}
                    value={HEnd} name='HEnd'
                  >
                    {Hour.map((v,i) => {
                      let formatNum = (i+1)<10?'0'+(i+1):(i+1)
                      return <option key={i} value={i+1}>{formatNum}</option>
                    })}
                  </Input>
              </FormGroup>
            </Col>
            <Col md={3}>
              <FormGroup>
              <Input type="select" onChange={this.changeValue} disabled={StateDay}
                  value={MEnd} name='MEnd'
                >
                  {Min.map((v,i) => {
                    let formatNum = i<10?'0'+i:i
                    return <option key={i} value={i}>{formatNum}</option>
                  })}
                </Input>
              </FormGroup>
            </Col>
            <Col md={3}>
              <FormGroup>
                <Input type="select" onChange={this.changeValue} disabled={StateDay}
                  value={AMPMEnd} name='AMPMEnd'
                >
                  <option value={0}>AM</option>
                  <option value={12}>PM</option>                  
                </Input>
              </FormGroup>
            </Col>            
          </Row>
        </Col>
      </Row>    
    );
  }
}

export default ScheduleDay;