import React from 'react';
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import {getSubOrg, deleteSubOrg, getInfoSubOrg, getNavSubOrg, changeAccount} from '../../api/subOrganizations.js'
import moment from 'moment'
import SweetAlert from 'react-bootstrap-sweetalert';
import Scrollbar from 'perfect-scrollbar-react';
import { Link } from 'react-router-dom';

// reactstrap components
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  CardFooter,
  Table,
  Button,
} from 'reactstrap';

import SubOrgInfoView from './component/SubOrgInfoView'
import okIcon from '../../assets/img/icon_ok_coupons.png'
import './SubOrg.scss'
import ResponsiveModal from 'react-responsive-modal';
import LoadingModal from './../../components/Navbars/LogoutModal'
import ConfirmModal from '../../views/pages/ConfirmModal'
import AlertModal from '../../components/Cosmic/AlertModal/AlertModal'
import ChangeModal from './ChangeModal'

class SubOrgAdmin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_loading: true,
      SubOrganizations:[],
      first: null,
      previous: null,
      next: null,
      last: null,
      total: 0,
      index: 0,
      alert:null,
      pageEdit:null,
      Countries:[],
      Plan:[],
      Status: [],
      pageTittle:'',
      
      active: false,
      inactive: false,
      developer: false,
      administrator: false,
      view_only: false,

      sort_direction_name: false,
      sort_direction_email: true,
      sort_direction_city: true,
      sort_direction_country: true,
      sort_direction_create: true,
      sort_direction_status: true,

      curOperator: null,
      statusModal: false,
      confirmModal: false,
      alertModal: false,

      error: '',
      c_res: -1,
    }

    this.renderSubOrg = this.renderSubOrg.bind(this)
    this.renderSubOrgList = this.renderSubOrgList.bind(this)
    this.getInitSubOrg = this.getInitSubOrg.bind(this)        
    this.editSuborg = this.editSuborg.bind(this)    
    this.deleteSubOrgAlert = this.deleteSubOrgAlert.bind(this)
    this.createAlert = this.createAlert.bind(this)
    this.closeAlert = this.closeAlert.bind(this)
    
  }

  createAlert = (e) =>{
    this.setState({
        alert: (
          <SweetAlert
            showCancel
            confirmBtnText="Yes, delete it!"
            confirmBtnBsStyle='default'
            title={<p>Delete Operator</p>}
            onConfirm={this.deleteSubOrgAlert.bind(this,e.target.id)}
            onCancel={this.closeAlert}
          >
            <hr className='Line'></hr>
            <p>Are you sure to delete this Operator?</p>
            <p>This action can not be undone</p>
          </SweetAlert>
        )
    });
  }

  closeAlert = () =>{
    this.setState({alert:null})
  }

  deleteSubOrgAlert = async (suborg) => {
    const res = await deleteSubOrg({id_operator:suborg})
    if(res.response.status===200){
      const data = this.state.SubOrganizations;
      await data.find((o, i) => {
        if(o.id_operator===suborg){
          data.splice(i, 1);
          return true
        }
        return false
      })
      this.setState({
        alert: (
          <SweetAlert 
            custom
            customIcon={okIcon}
            showConfirm={false}
            >            
            <hr className='Line'/>
            <div className='AlertCreate'>          
              <p>Your Sub-Organization has been deleted!</p>
            </div> 
          </SweetAlert>
        ),
        SubOrganizations:data
      })
      setTimeout(this.closeAlert, 2000);
    }  
  }

  editSuborg = (e) => {
    const data = this.state.SubOrganizations;
    data.find((o, i) => {
      if(o.id_operator===e.target.id){        
        this.setState({
           pageEdit:o
        })
        return true
      }
      else return false
    })
  }

  updateEditSubOrg = async () => {
    const result = await getSubOrg()
    if(result.st && result.status===200){
      this.setState({
        SubOrganizations:result.Operators,
        pageEdit:null
      })
    }
    else {
      this.setState({
        pageEdit:null
      });
    }
  }

  getInitSubOrg = async () => {
    const resSubOrg = await getSubOrg()
    const resInfo = await getInfoSubOrg()
    if(resInfo.st && resSubOrg.st){
      if(resInfo.status===200 && resSubOrg.status===200){
        let operators = resSubOrg.Operators ? resSubOrg.Operators : []
        operators.sort((a,b)=>(a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
        this.setState({
          is_loading: false,
          SubOrganizations: operators,
          Countries: resInfo.Countries,
          first: resSubOrg.first,
          previous: resSubOrg.previous,
          next: resSubOrg.next,
          last: resSubOrg.last,
          total: resSubOrg.total,
          index: resSubOrg.index,
          Status: resInfo.Status ? resInfo.Status : [],
        })
      } else { this.setState({is_loading: false}) }
    } else {
      this.setState({is_loading: false})
    }
  }

  setData = (resSubOrg) => {
    if (resSubOrg.status === 200) {
      let operators = resSubOrg.Operators ? resSubOrg.Operators : []
      if (this.state.sort_direction_id) {
        operators.sort((a,b)=>(a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
      } else {
        operators.sort((a,b)=>(a.name < b.name) ? 1 : ((b.name < a.name) ? -1 : 0))
      }
      this.setState({
        SubOrganizations: operators,
        first: resSubOrg.first,
        previous: resSubOrg.previous,
        next: resSubOrg.next,
        last: resSubOrg.last,
        total: resSubOrg.total,
        index: resSubOrg.index,
      })
    }
  }

  first = async() => {
    if (this.state.first === null)
      return

    const result = await getNavSubOrg(this.state.first)
    this.setData(result)
  }

  previous = async () => {
    if (this.state.previous === null)
      return
    const result = await getNavSubOrg(this.state.previous)
    this.setData(result)
  }

  next = async () => {
    if (this.state.next === null)
      return
    const result = await getNavSubOrg(this.state.next)
    this.setData(result)
  }

  last = async () => {
    if (this.state.last === null)
      return
    const result = await getNavSubOrg(this.state.last)
    this.setData(result)
  }

  sortData = (type) => {
    let SubOrganizations = Object.assign([], this.state.SubOrganizations)
    switch(type) {
      case "name":
        if (this.state.sort_direction_name) {
          SubOrganizations.sort((a,b)=>(a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
        } else {
          SubOrganizations.sort((a,b)=>(a.name < b.name) ? 1 : ((b.name < a.name) ? -1 : 0))
        }
        this.setState({sort_direction_name: !this.state.sort_direction_name, SubOrganizations: SubOrganizations})
        break;
      case "email":
        if (this.state.sort_direction_email) {
          SubOrganizations.sort((a,b)=>(a.email > b.email) ? 1 : ((b.email > a.email) ? -1 : 0))
        } else {
          SubOrganizations.sort((a,b)=>(a.email < b.email) ? 1 : ((b.email < a.email) ? -1 : 0))
        }
        this.setState({sort_direction_email: !this.state.sort_direction_email, SubOrganizations: SubOrganizations})
        break;
      case "city":
        if (this.state.sort_direction_city) {
          SubOrganizations.sort((a,b)=>(a.city > b.city) ? 1 : ((b.city > a.city) ? -1 : 0))
        } else {
          SubOrganizations.sort((a,b)=>(a.city < b.city) ? 1 : ((b.city < a.city) ? -1 : 0))
        }
        this.setState({sort_direction_city: !this.state.sort_direction_city, SubOrganizations: SubOrganizations})
        break;
      case "country":
        if (this.state.sort_direction_country) {
          SubOrganizations.sort((a,b)=>(a.country > b.country) ? 1 : ((b.country > a.country) ? -1 : 0))
        } else {
          SubOrganizations.sort((a,b)=>(a.country < b.country) ? 1 : ((b.country < a.country) ? -1 : 0))
        }
        this.setState({sort_direction_country: !this.state.sort_direction_country, SubOrganizations: SubOrganizations})
        break;
      case "create":
        if (this.state.sort_direction_create) {
          SubOrganizations.sort((a,b)=>(a.created_at > b.created_at) ? 1 : ((b.created_at > a.created_at) ? -1 : 0))
        } else {
          SubOrganizations.sort((a,b)=>(a.created_at < b.created_at) ? 1 : ((b.created_at < a.created_at) ? -1 : 0))
        }
        this.setState({sort_direction_create: !this.state.sort_direction_create, SubOrganizations: SubOrganizations})
        break;
      case "status":
        if (this.state.sort_direction_status) {
          SubOrganizations.sort((a,b)=>(a.id_status > b.id_status) ? 1 : ((b.id_status > a.id_status) ? -1 : 0))
        } else {
          SubOrganizations.sort((a,b)=>(a.id_status < b.id_status) ? 1 : ((b.id_status < a.id_status) ? -1 : 0))
        }
        this.setState({sort_direction_status: !this.state.sort_direction_status, SubOrganizations: SubOrganizations})
        break;
      default:
        break;
    }
  }

  changeStatus = (operator) => {
    this.setState({statusModal: true, curOperator: operator})
  }

  closeStatusModal = () => {
    this.setState({statusModal: false})
  }

  confirmStatusModal = (new_operator) => {
    this.setState({statusModal: false, confirmModal: true, curOperator: new_operator})
  }

  closeConfirmModal = () => {
    this.setState({confirmModal: false})
  }

  onCloseAlertModal = () => {
    this.setState({alertModal: false})
  }

  changeAuth = async (res) => {
    if (res.response.status !== 200)
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    else {
      let newSub = Object.assign([], this.state.SubOrganizations);
      const { curOperator } = this.state
      const result = await changeAccount({id_operator: curOperator.id_operator, id_status: curOperator.id_status})
      if (result.response.status !== 200) {
        this.setState({confirmModal: false, alertModal: true, error: 'An error has ocurred while updating the operator!', c_res: result.response.status})
      } else {
        const index = newSub.findIndex(user => user.id_operator === curOperator.id_operator)
        newSub[index].id_status = result.response.operator.id_status
        this.setState({confirmModal: false, alertModal: true, error: 'Operator has been updated!', SubOrganizations: newSub, c_res: result.response.status})
      }
    }
  }

  renderSubOrgList = () => {
    const { SubOrganizations } = this.state
    if(SubOrganizations.length>0){
      return SubOrganizations.map((suborg, index) => (
        <tr key={index} >
          <td>{suborg.name}</td>
          <td>{suborg.email}</td>
          <td>{suborg.city}</td>
          <td>{suborg.country}</td>          
          <td>{moment(suborg.created_at).format('DD-MM-YYYY')}</td>
          <td>{
            <div className={suborg.id_status === 1 ? 'Available btn-cosmic-ope' : 
            suborg.id_status === 2 ? 'Unavailable btn-cosmic-ope' : 
            suborg.id_status === 3 ? 'Demo btn-cosmic-ope' : 
            suborg.id_status === 4 ? 'Deleted btn-cosmic-ope' : 'Default btn-cosmic-ope'} 
              onClick={()=>this.changeStatus(suborg)}>
              <span>
                {suborg.id_status === 1 ? 'Available' :
                suborg.id_status === 2 ? 'Unavailable' :
                suborg.id_status === 3 ? 'Demo' :
                suborg.id_status === 4 ? 'Deleted' : 'Default'}
              </span>    
            </div>}
          </td>
          <td>
            <Button
              onClick={this.editSuborg}              
              color="default"
              size="sm"
              className="btn-icon btn-link"
            >
            <img id={suborg.id_operator} alt="" src={require("assets/img/icon_edit.svg")}/>
            </Button>
            <Button    
              onClick={this.createAlert}
              color="default"
              size="sm"
              className="btn-icon btn-link"
            >
            <img id={suborg.id_operator} alt="" src={require("assets/img/icon_delete.svg")}/>
            </Button>
            <Link to={'/admin/see-team/'+suborg.id_operator}>
              <button className='btn-add btn-cosmic'>
                <img alt="" src={require("assets/img/icon_molecule.svg")}/>
                <span>View Team</span>
              </button> 
            </Link>
          </td>          
        </tr>
      ));
    }
    else return null
  }

  showPaymentDetail = (id_payment) => {
    this.props.history.push('/admin/payment/' + id_payment)
  }

  showMorePayment = (email) => {
    this.props.history.push({pathname:'/admin/payment', state: {email: email}})
  }

  renderSubOrg= () => {
    const disableStyle = {
      color: '#9a9a9a'
    }
    const { SubOrganizations, Countries, pageEdit, Plan, first, next, previous, last, index, total } = this.state   
    const {childrens} = this.props 
    if(pageEdit){
      return (
        <SubOrgInfoView 
          infoOrganization={pageEdit}
          onActionUpdate={this.updateEditSubOrg}          
          CardTitle={pageEdit.name}
          CardState={pageEdit.status}
          Countries={Countries}
          Plan={Plan}
          showPaymentDetail={this.showPaymentDetail}
          showMorePayment={this.showMorePayment}
          status_list={this.state.Status}
          //New={false}          
        />
      )
    }
    else
    return (
      <>
      <ResponsiveModal open={this.state.is_loading} 
        onClose={()=>{}} center 
        classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
        <LoadingModal title="Loading..."/>
      </ResponsiveModal>
      <Col ms={12}>
        <Row>
          <Col md="12">
            <CardHeader className="management-header">
              <Col md='4'>
                <CardTitle tag="h4">Admin Operators</CardTitle>
              </Col>
              <Col md='4' className='funtionSpace'>
                {childrens.indexOf(5)>=0?
                <Link to='/admin/add-suborg'>
                  <button className='btn-add btn-cosmic'>
                    <i className="tim-icons icon_add" />
                    <span>Add Operator</span>
                  </button> 
                </Link>:null}
              </Col>                                
              <Col md='4'className='filterSpace'>                
              </Col>
            </CardHeader>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <Scrollbar
              options={{suppressScrollX:true}}
            >
              <Col md="12" className='into-Scroll'>
                <Card>
                  <CardBody>
                    <Table>
                      <thead className="text-primary">
                        <tr>
                          {/*<th className="text-center">#</th>*/}
                          <th onClick={()=>{this.sortData('name')}}>Name<img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th onClick={()=>{this.sortData('email')}}>Email<img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th onClick={()=>{this.sortData('city')}}>City<img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th onClick={()=>{this.sortData('country')}}>Country<img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th onClick={()=>{this.sortData('create')}}>Create<img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th onClick={()=>{this.sortData('status')}}>Status<img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>{this.renderSubOrgList()}</tbody>
                    </Table>
                    {SubOrganizations.length === 0 && <div className="no-result">No results found</div>}
                    {SubOrganizations.length > 0 &&
                    <div className="nav-group">
                      <div><span>Showing</span></div>
                      <div className="first" style={first===null?disableStyle:{}} onClick={this.first}><i className="fa fa-angle-double-left"></i></div>
                      <div className="previous" style={previous===null?disableStyle:{}} onClick={this.previous}><i className="fa fa-angle-left"></i></div>
                      <div className="index"><span>{index}</span></div>
                      <div><span>{'to'}</span></div>
                      <div className="count"><span>{SubOrganizations.length === 0 ? 0 : index + SubOrganizations.length - 1}</span></div>
                      <div><span>{'of'}</span></div>
                      <div className="total"><span>{new Intl.NumberFormat().format(total)}</span></div>
                      <div className="next" style={next===null?disableStyle:{}} onClick={this.next}><i className="fa fa-angle-right"></i></div>
                      <div className="last" style={last===null?disableStyle:{}} onClick={this.last}><i className="fa fa-angle-double-right"></i></div>
                    </div>
                    }
                  </CardBody>
                  <CardFooter className="text-center">              
                  </CardFooter>
                </Card>
              </Col>
            </Scrollbar>
          </Col>
        </Row>
      </Col>
      </>
    )
  }

  componentDidMount() {
    document.body.classList.add('admin-operator')
    this.getInitSubOrg()
  }
  componentWillUnmount() {
    document.body.classList.remove('admin-operator')
  }
  
  render() {
    return (
      <React.Fragment>
      {this.state.alert}
        <div className="content">
          {this.renderSubOrg()}
        </div>
        <ResponsiveModal open={this.state.statusModal} onClose={this.closeStatusModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <ChangeModal onClose={this.closeStatusModal} onChange={this.confirmStatusModal} operator={this.state.curOperator} status={this.state.Status}/>
        </ResponsiveModal>
        <ResponsiveModal open={this.state.confirmModal} onClose={this.closeConfirmModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <ConfirmModal onClose={this.closeConfirmModal} onAuthenticate={this.changeAuth}/>
        </ResponsiveModal>
        <ResponsiveModal open={this.state.alertModal} onClose={this.onCloseAlertModal} center classNames={{'modal':'alert-modal','closeButton':'modal-close-button'}}>
          <AlertModal onClose={this.onCloseAlertModal} status={this.state.c_res} msj={this.state.error}/>
        </ResponsiveModal>
        <ToastContainer />
      </React.Fragment>
    );
  }
}

export default SubOrgAdmin;