import React from 'react';
import CurrencyFormat from 'react-currency-format';
// reactstrap components
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Row,
  Table,
  Col,
} from 'reactstrap';
import { getDetail, refundPayment } from '../../api/PaymentApi';
import { transGeofence } from '../../api/LocationApi';
import './detail.scss'
import ResponsiveModal from 'react-responsive-modal';
import { ToastContainer, toast } from 'react-toastify'
import SweetAlert from 'react-bootstrap-sweetalert';
import AuthModal from '../../views/pages/ConfirmModal'
import RefundModal from './modal/RefundModal'
import ConfirmModal from './modal/ConfirmModal'
import SingleMarkerMap from '../../components/Maps/SingleMarkerMap/SingleMarkerMap'
import LoadingModal from './../../components/Navbars/LogoutModal'
import moment from 'moment';
import Moment from 'react-moment'

class PaymentDetail extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      events:[],
      amount: '',
      usd_cost: '',
      brand: '',
      currency: '',
      date: '',
      gateway_fee: '',
      id_service: '',
      last_four: '',
      net: '',
      order_id: '',
      origin: '',
      status: '',
      token: '',
      user: '',
      refund_amount: '',
      showRefund: false,
      confirmModal: false,
      authModal: false,
      alert: null,
      is_loading: true,
      endRideLocation:false,
    }
    this.fetchData = this.fetchData.bind(this)
    this.setData = this.setData.bind(this)
  }

  toast = (message) =>{
    toast.error(message, {position: toast.POSITION_TOP_RIGHT})
  }
  setData(data) {
    this.setState({
      is_loading: false,
      amount: data.amount,      
      brand: data.brand,
      currency: data.currency,
      date: data.date,      
      id_service: data.id_service,
      last_four: data.last_four,      
      order_id: data.order_id,
      origin: data.origin,
      status: data.status,
      token: data.token,
      user: data.user,
      //detail payment
      discount:data.discount,
      gateway_fee:data.gateway_fee,
      net:data.net,
      total_cost:data.total_cost,
      end_coords_lat:data.end_coords_lat,
      end_coords_lon:data.end_coords_lon,
      usd_cost:data.usd_cost,
      service_cost:data.service_cost,
      id_coupon:data.id_coupon,
      total_refunded:data.total_refunded,
      ride:data,
      penalty:0,//data.penalty_cost,
    })
  }

  setEvents = (events)=>{
    this.setState({events:events})
  }

  async fetchData(id_payment) {
    const result = await getDetail(id_payment);
    this.setData(result.response.payment)
    this.setEvents(result.response.payment_events)
    let geo = []//result.response.geofence ? await transGeofence(result.response.geofence) : []
    this.setState({geofence:geo})
  }
  componentDidMount() {
    const { match: { params } } = this.props;
    this.fetchData(params.id_payment)
  }

  refund = () => {
    this.setState({showRefund: true})
  }

  onCloseRefundModal = () => {
    this.setState({showRefund: false, refund_amount: ''})
  }

  refundTransaction = (refund_amount) => {
    this.setState({showRefund: false, confirmModal: true, refund_amount: refund_amount})
  }

  onCloseConfirmModal = () => {
    this.setState({confirmModal: false, refund_amount: ''})
  }

  onConfirmRefund = () => {
    this.setState({confirmModal: false, authModal: true})
  }

  onCloseAuthModal = () => {
    this.setState({authModal: false, refund_amount: ''})
  }

  closeAlert = () =>{
    this.setState({alert:null})
  }

  authenticate = async (res) => {
    if (res.response.status !== 200)
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    else {
      const result = await refundPayment({
        id_service: this.state.id_service,
        amount: parseInt(this.state.refund_amount)
      })
      if (result.response.status !== 200)
        toast.error(result.response.message, {position: toast.POSITION_TOP_RIGHT})
      else {
        this.setState({
          alert: (
            <SweetAlert success title="Payments Refunded!" onConfirm={this.closeAlert}>        
            </SweetAlert>
          ),
          authModal: false,
          status:'Refunded'
        })
      }
    }
  }

  renderMap = async(ride) => {
    let markers = []
    markers.push({
      lat:parseFloat(ride.start_coords_lat),
      lng:parseFloat(ride.start_coords_lon),
      urlIcon:require('assets/img/icon_map_start.svg'),
      address:ride.start_address,
      msj:'Ride Started'
    })
    markers.push({
      lat:parseFloat(ride.end_coords_lat),
      lng:parseFloat(ride.end_coords_lon),
      urlIcon:require('assets/img/icon_map_stop.svg'),
      address:ride.end_address,
      msj:'Ride Ended'
    })    
    this.setState({
      endRideLocation:!this.state.endRideLocation,
      currentMarkers:markers,
      currentCenter:{lat:ride.end_coords_lat,lng:ride.end_coords_lon},
      currenteDevice:ride.id_device
    });
  }
  
  toggleEndRideLocation = () => {
    this.setState({endRideLocation: !this.state.endRideLocation});
  }

  renderEvents = () => {
    const { events } = this.state
    return (
      events.map((events, index) => (
        <tr key={index}>
          <td className="text-left" >{events.account}</td>
          <td className="text-left" >{events.email}</td>
          <td className="text-left" >
            <CurrencyFormat 
              value={events.refund}
              displayType={'text'}
              thousandSeparator={true}
              decimalScale={2}
              fixedDecimalScale={true}
              renderText={value => <div className="upper-case">{`${value}`}</div>}/></td> 
          <td className="text-left upper-case">{events.currency}</td>
          <td className="text-left"><Moment format="MMM DD, YYYY h:mm A">{events.date}</Moment></td>
        </tr>
      ))
    )
  }

  render() {
    const { amount, currency, status, origin, last_four, order_id, brand, token, date, user, 
      events,discount,gateway_fee, net,total_cost,usd_cost,service_cost,id_coupon,total_refunded, 
      currentMarkers,geofence,endRideLocation,currentCenter,currenteDevice,ride,penalty} = this.state
    return (
      <React.Fragment>
        <SingleMarkerMap
          statusModal={endRideLocation}
          toggleModal={this.toggleEndRideLocation}
          markers={currentMarkers}
          geofence={geofence}
          center={currentCenter}
          title={'End Ride Location | ' + currenteDevice}
        />
        {this.state.alert}
        <ResponsiveModal open={this.state.is_loading} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LoadingModal title="Loading..."/>
        </ResponsiveModal>
        <div className="content" id="payment-detail">          
          <Row>
            <Col md='12'>
              <CardHeader className="management-header">
                <Col md='4'>
                  <CardTitle tag="h4" onClick={()=>{this.props.history.push('/admin/payment')}}>
                    <img alt="" src={require("assets/img/icon_arrow.svg")}/>Payments
                  </CardTitle>
                </Col>  
              </CardHeader>            
            </Col>
            <Col md="12">
              <Card>
                <CardBody>
                  <div className="id_service">
                    <div className="first_line">
                      <div className="amount-status">
                        <div className="amount">
                          <CurrencyFormat 
                            value={amount} 
                            displayType={'text'} 
                            thousandSeparator={true} 
                            prefix={'$'} 
                            decimalScale={2}
                            fixedDecimalScale={true}
                            renderText={value => <div>{value+' ' + currency.toUpperCase()}</div>}/>
                        </div>
                        <div className="upper-case">{currency}</div>
                        <div className="status">
                          <span>{status}</span>
                          {status === 'Succeded' ? <img alt="" src={require("assets/img/icon_check.png")}/> : 
                          status === 'Failed' ? <img alt="" src={require("assets/img/icon_fail.png")}/> : 
                          status === 'Pending' ? <img alt="" src={require("assets/img/icon_pending.png")}/> : 
                          status === 'Free' ? <img alt="" src={require("assets/img/icon_free.svg")}/> :
                          status === 'Partially Refunded' ? <img alt="" src={require("assets/img/icon_reply.png")}/> :
                          status === 'Refunded' ? <img alt="" src={require("assets/img/icon_reply.png")}/> : null}
                        </div>
                      </div>
                      <div className="order-id">
                        {status === 'Succeded' ? brand === 'Balance'?null: 
                          <div className="refund" onClick={this.refund}>
                            <img alt="" src={require("assets/img/icon_reply.png")}/> Refund
                          </div>
                        : null}
                        {order_id}
                      </div>
                    </div>
                    <div className="second-line">
                      <div className="one-child">
                        <span>Date</span>
                        <div>{moment.utc(date).format("MMM DD, YYYY h:mm A")}                          
                          </div>
                      </div>
                      <div className="one-child">
                        <span>User</span>
                        <div>{user}</div>
                      </div>
                      <div className="one-child">
                        <span>Payment method</span>
                        <div className="payment-method">
                          <img alt="" src={require("assets/img/icon_credit_card_visa.png")}/>
                          <div className="brand">{brand.toUpperCase()}</div>
                          {last_four}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="details">
                    <Row>
                      <Col md={12} className="sub-title">Payment details</Col>
                    </Row>
                    <Row className="one-line">
                      <Col  sm={6} md={3} xl={2} className="left-value">Ride Cost</Col>
                      <Col  sm={6} md={3} xl={2} className="right-value">
                        <CurrencyFormat 
                            value={service_cost} 
                            displayType={'text'} 
                            thousandSeparator={true} 
                            // prefix={'$'} 
                            decimalScale={2}
                            fixedDecimalScale={true}
                            renderText={value => <div className="upper-case">{`${value} ${currency}`}</div>}/>
                      </Col>
                      <Col  sm={6} md={3} xl={2} className="left-value">Amount Refunded</Col>
                      <Col  sm={6} md={3} xl={2} className="right-value">
                        <CurrencyFormat 
                            value={total_refunded}
                            displayType={'text'} 
                            thousandSeparator={true} 
                            // prefix={'$'} 
                            decimalScale={2}
                            fixedDecimalScale={true}
                            renderText={value => <div className="upper-case">{`${value} ${currency}`}</div>}/>
                      </Col>
                    </Row>
                    <Row className="one-line">
                      <Col  sm={6} md={3} xl={2} className="left-value">Extra Charge Fee</Col>
                      <Col  sm={6} md={3} xl={2} className="right-value">
                        <CurrencyFormat 
                          value={penalty} 
                          displayType={'text'} 
                          thousandSeparator={true} 
                          // prefix={'$'} 
                          decimalScale={2}
                          fixedDecimalScale={true}
                          renderText={value => <div className="upper-case">{`${value} ${currency}`}</div>}/>
                      </Col>
                      <Col  sm={6} md={3} xl={2} className="left-value">Promocode Used</Col>
                      <Col  sm={6} md={3} xl={2} className="right-value">
                        <span>{id_coupon?id_coupon:'None'}</span>
                      </Col>
                    </Row>
                    <Row className="one-line">
                      <Col  sm={6} md={3} xl={2} className="left-value">Discounts</Col>
                      <Col  sm={6} md={3} xl={2} className="right-value">
                        <CurrencyFormat 
                          value={discount} 
                          displayType={'text'} 
                          thousandSeparator={true} 
                          prefix={'-'} 
                          decimalScale={2}
                          fixedDecimalScale={true}
                          renderText={value => <div className="upper-case">{`${value} ${currency}`}</div>}/>
                      </Col>
                      <Col  sm={6} md={3} xl={2} className="left-value">Gateway Fee</Col>
                      <Col  sm={6} md={3} xl={2} className="right-value">
                        <CurrencyFormat 
                          value={gateway_fee} 
                          displayType={'text'} 
                          thousandSeparator={true}                           
                          decimalScale={2}
                          fixedDecimalScale={true}
                          renderText={value => <div className="upper-case">{`${value} ${currency}`}</div>}/> 
                      </Col>
                    </Row>
                    <Row className="one-line">
                      <Col  sm={6} md={3} xl={2} className="left-value">Total Cost</Col>
                      <Col  sm={6} md={3} xl={2} className="right-value">
                        <CurrencyFormat 
                          value={total_cost} 
                          displayType={'text'} 
                          thousandSeparator={true}
                          decimalScale={2}
                          fixedDecimalScale={true}
                          renderText={value => <div className="upper-case">{`${value} ${currency} -> ${usd_cost} USD`}</div>}/> 
                      </Col>
                      <Col  sm={6} md={3} xl={2} className="left-value">Net</Col>
                      <Col  sm={6} md={3} xl={2} className="right-value">
                        <CurrencyFormat 
                          value={net} 
                          displayType={'text'} 
                          thousandSeparator={true}                           
                          decimalScale={2}
                          fixedDecimalScale={true}
                          renderText={value => <div className="upper-case">{`${value} ${currency}`}</div>}/> 
                      </Col>
                    </Row>
                    <Row className="one-line">
                      <Col  sm={6} md={3} xl={2} className="left-value">Status</Col>
                      <Col  sm={6} md={3} xl={2} className="right-value">
                        <span>{status}</span>
                      </Col>
                      <Col  sm={6} md={3} xl={2} className="left-value" >End Ride Location</Col>
                      <Col  sm={6} md={3} xl={2} className="right-value">
                        <i className="tim-icons icon-gps-fixed" style={{'cursor':'pointer'}} onClick={()=>this.renderMap(ride)}/>
                      </Col>
                    </Row>
                  </div>
                  {/* <div className="details">
                    <div className="sub-title">Payment details</div>
                    <div className="one-line">
                      <div className="left-value">Amount</div>
                      <div className="right-value">
                        <div className="cop">
                          <CurrencyFormat 
                            value={amount} 
                            displayType={'text'} 
                            thousandSeparator={true} 
                            prefix={'$'} 
                            decimalScale={2}
                            fixedDecimalScale={true}
                            renderText={value => <div>{value+' ' + currency.toUpperCase()}</div>}/>
                        </div>
                        {
                          usd_cost?(
                            <>
                            <div className="right-arrow">&rarr;</div>
                            <div className="usd">
                              <CurrencyFormat 
                                value={usd_cost} 
                                displayType={'text'} 
                                thousandSeparator={true} 
                                prefix={'$'} 
                                decimalScale={2}
                                fixedDecimalScale={true}
                                renderText={value => <div>{value+' USD'}</div>}/>
                            </div>
                            </>
                          ):null
                        }
                      </div>
                    </div>
                    <div className="one-line">
                      <div className="left-value">Gateway Fee</div>
                      <div className="right-value">
                        <div className="cop">
                          <CurrencyFormat 
                            value={gateway_fee} 
                            displayType={'text'} 
                            thousandSeparator={true} 
                            prefix={'$'} 
                            decimalScale={2}
                            fixedDecimalScale={true}
                            renderText={value => <div>{value+' ' + currency.toUpperCase()}</div>}/>
                        </div>
                      </div>
                    </div>
                    <div className="one-line">
                      <div className="left-value">Net</div>
                      <div className="right-value">
                        <div className="cop">
                          <CurrencyFormat
                            value={net} 
                            displayType={'text'} 
                            thousandSeparator={true} 
                            prefix={'$'} 
                            decimalScale={2}
                            fixedDecimalScale={true}
                            renderText={value => <div>{'-'+value+' ' + currency.toUpperCase()}</div>}/>
                        </div>
                      </div>
                    </div>
                    <div className="one-line">
                      <div className="left-value">Status</div>
                      <div className="right-value">
                        <span>{status}</span>
                      </div>
                    </div>
                  </div> */}
                  <div className="method">
                    <div className="sub-title">Payment method</div>
                    <div className="one-line">
                      <div className="left-value">Origin</div>
                      <div className="right-value">
                        <span>{origin}</span>
                      </div>
                    </div>
                    <div className="one-line">
                      <div className="left-value">Number</div>
                      <div className="right-value">
                        <span>{'*** ' + last_four}</span>
                      </div>
                    </div>
                    <div className="one-line">
                      <div className="left-value">Order ID</div>
                      <div className="right-value">
                        <span>{order_id}</span>
                      </div>
                    </div>
                    <div className="one-line">
                      <div className="left-value">Type</div>
                      <div className="right-value">
                        <span>{brand==='visa' ? 'Visa Debit Card' : ''}</span>
                      </div>
                    </div>
                    <div className="one-line">
                      <div className="left-value">Token</div>
                      <div className="right-value">
                        <span>{token}</span>
                      </div>
                    </div>
                  </div>
                  <div className="events">
                    <Row>
                      <Col md={12} className="sub-title">Events</Col>
                    </Row>
                    <Row>
                      <Col md={12}>
                        <Table>
                          <thead className="text-primary">
                            <tr>
                              <th className="eventsTr">Account</th>
                              <th className="eventsTr" >Email</th>
                              <th className="eventsTr" >Amount Refunded</th>
                              <th className="eventsTr" >Currency</th>
                              <th className="eventsTr" >Date</th>
                            </tr>
                          </thead>
                          <tbody>{typeof events === 'object'?events.length>0?this.renderEvents():null:null}</tbody>
                        </Table>
                      </Col>
                    </Row>
                  </div>
                  {/* <div className="events">
                    <div className="sub-title">Events</div>
                    <div className="one-line">
                      <div className="right-value">
                        <span>{status==='Failed' ? "Payment declined by customer's bank" :
                        status==='Succeeded' ? "Succeeded" :
                        status==='Pending' ? "Pending" :
                        status==='Refunded' ? "Refunded" : ''}</span>
                      </div>
                    </div>
                  </div> */}
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
        <ResponsiveModal open={this.state.showRefund} onClose={this.onCloseRefundModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
        <RefundModal onClose={this.onCloseRefundModal} onRefund={this.refundTransaction} order_id={order_id} amount={amount} currency={currency} toast={this.toast}/>
        </ResponsiveModal>
        <ResponsiveModal open={this.state.confirmModal} onClose={this.onCloseConfirmModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <ConfirmModal onClose={this.onCloseConfirmModal} onRefund={this.onConfirmRefund}/>
        </ResponsiveModal>
        <ResponsiveModal open={this.state.authModal} onClose={this.onCloseAuthModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <AuthModal onClose={this.onCloseAuthModal} onAuthenticate={this.authenticate}/>
        </ResponsiveModal>
        <ToastContainer />
      </React.Fragment>
    );
  }
}

export default PaymentDetail;
