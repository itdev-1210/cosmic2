import React, { Component } from 'react';
import {
  Button,
  Label,
  Row,
} from 'reactstrap';
import './styles.scss'

class ConfirmModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  refund = () => {
    this.props.onRefund()
  }

  render() {
    const { onClose} = this.props
    return (
      <div className="change_status" id="confirm_refund">
        <div className="change-title">
          <span className="title">{'Refund a transaction'}</span>
        </div>
        <div className="input_group">
          <Row>
            <Label className="refund-alert">Are you sure to refund this transaction?
              <br/>This action is can not be undone
            </Label>
          </Row>
          <Row>
            <Label className="confirm-alert">This action requires additional privileges
            </Label>
          </Row>
        </div>
        <Row className='cosmic-butons butonsUpdateOpe'>
          <Button className="cosmic-cancel" onClick={onClose}>Cancel</Button>
          <Button className="cosmic-confirm" onClick={this.refund}>Refund</Button>
        </Row>
      </div>
    )
  }
}

export default ConfirmModal