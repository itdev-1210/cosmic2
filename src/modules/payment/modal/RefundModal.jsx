import React, { Component } from 'react';
import {
  Button,
  Label,
  FormGroup,
  Input,
  Row,
  Col,
} from 'reactstrap';
import './styles.scss'

class RefundModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      refund_amount: '',
      full_refund: false,
    }
  }

  refund = () => {
    if (!this.state.full_refund && this.state.refund_amount === '') this.props.toast('Select amount')
    else if (!this.state.full_refund && this.state.refund_amount>this.props.amount) this.props.toast('The amount is higher than the cost of the ride')
    else if (this.state.refund_amount<=0 && !this.state.full_refund) this.props.toast('The amount cannot be less than or equal to 0')
    else{
      this.props.onRefund(this.state.full_refund ? this.props.amount : this.state.refund_amount)
    }
  }

  changeValue = (e) => {
    this.setState({full_refund: !this.state.full_refund})
  }

  changeAmount = (e) => {
    this.setState({refund_amount: e.target.value})
  }

  render() {
    const { onClose, order_id, amount, currency} = this.props
    return (
      <div className="change_status" id="refund_modal">
        <div className="change-title">
          <span className="title">{'Refund a transaction'}</span>
        </div>
        <div className="input_group">
          <Row>
            <Label className="id-label">Order ID</Label>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Input
                  name="id"
                  type="text"
                  value={order_id}
                  disabled
                />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Label className="id-label">Amount transaction</Label>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Input
                  name="amount"
                  type="text"
                  value={new Intl.NumberFormat('us-US').format(amount) + ' ' + currency.toUpperCase()}
                  disabled
                />
              </FormGroup>
            </Col>
          </Row>
          <FormGroup check className="mt-3">
            <Label check>
              <Input 
                type="checkbox"
                name="full_refund"
                checked={this.state.full_refund}
                onChange={this.changeValue}
              />
              <span className="form-check-sign" />
              <span className={this.state.full_refund ? 'full-refund' : 'non-full-refund'}>Operate a full refund</span>
            </Label>
          </FormGroup>

          <FormGroup>
            <Label className={this.state.full_refund ? 'non-full-refund' : 'full-refund'}>{`Amount(${currency.toUpperCase()})`}</Label>
          </FormGroup>
          <Row>
            <Col>
              <FormGroup>
                <Input
                  name="real_amount"
                  type="number"
                  value={this.state.refund_amount}
                  onChange={this.changeAmount}
                  disabled={this.state.full_refund}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Label className="refund-alert">There might be a slight delay between the refund 
              and the availability on your customer's statement.
            </Label>
          </Row>
        </div>
        <Row className='cosmic-butons butonsUpdateOpe'>
          <Button className="cosmic-cancel" onClick={onClose}>Cancel</Button>
          <Button className="cosmic-confirm" onClick={this.refund}>Refund</Button>
        </Row>
      </div>
    )
  }
}

export default RefundModal