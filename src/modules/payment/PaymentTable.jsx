import React from 'react';
import CurrencyFormat from 'react-currency-format';
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  DropdownToggle,
  DropdownMenu,
  UncontrolledDropdown,
  Table,
  Row,
  Col,
} from 'reactstrap';
import Moment from 'react-moment'
import { getAllPayments, getFilter, getNav } from '../../api/PaymentApi';
import {emitter, subscribe,unSubscribe } from '../../utils/EventComponents';
import './styles.scss'
import LoadingModal from './../../components/Navbars/LogoutModal'
import ResponsiveModal from 'react-responsive-modal';

class PaymentTable extends React.Component {
  constructor(params) {
    super(params)
    this.state = {
      search:'',
      is_loading: true,
      payments: [],
      first: null,
      previous: null,
      next: null,
      last: null,
      total: 0,
      index: 0,
      status_payments: [],
      filters: {},

      sort_direction_order_id: false,
      sort_direction_amount: true,
      sort_direction_currency: true,
      sort_direction_token: true,
      sort_direction_date: true,
      sort_direction_user: true,
      sort_direction_status: true,
    }
    this.fetchData = this.fetchData.bind(this)
    this.handlePaymentDetails = this.handlePaymentDetails.bind(this)
    this.first = this.first.bind(this)
    this.previous = this.previous.bind(this)
    this.next = this.next.bind(this)
    this.last = this.last.bind(this)
    this.setData = this.setData.bind(this)
    this.filterCheck = this.filterCheck.bind(this)    
    this.doneFilter = this.doneFilter.bind(this)
    this.setParam = this.setParam.bind(this)
  }

  filterCheck(e) {
    const name = e.target.name
    let filters = Object.assign({}, this.state.filters)
    filters[name] = !filters[name]
    this.setState({filters: filters})
  }

  clearFilter = async () => {
    let filters = {}
    this.state.status_payments.map((status) => filters['status_'+status.id_status_payment] = false)
    this.setState({filters: filters, search:''}, () => {
      emitter('searchBar',true)
      this.fetchData()
    })
  }

  async doneFilter() {
    const { status_payments, filters } = this.state
    let result;
    var filter_status = 'filter_status='

    status_payments.map((status) => {
      if (filters['status_'+status.id_status_payment]) filter_status += status.id_status_payment + ','
      return status;
    })

    if (filter_status === 'filter_status=') {
      return;
    } else {
      result = await getFilter(filter_status.substr(0, filter_status.length-1))
    }
    this.setState({search:''}, () => {
      emitter('searchBar',true)
      this.setData(result.response)
    })
  }

  setData(data) {
    var payments = data.Payments ? data.Payments : []
    if (this.state.sort_direction_order_id) {
      payments.sort((a,b)=>(a.order_id > b.order_id) ? 1 : ((b.order_id > a.order_id) ? -1 : 0))
    } else {
      payments.sort((a,b)=>(a.order_id < b.order_id) ? 1 : ((b.order_id < a.order_id) ? -1 : 0))
    }
    this.setState({
      is_loading: false,
      payments: payments,
      first: data.first,
      previous: data.previous,
      next: data.next,
      last: data.last,
      total: data.total,
      index: data.index,
      status_payments: data.status_payments ? data.status_payments : [],
    })
  }
  setParam() {
    var filter = '&filter_status='
    const {status_payments, filters} = this.state
    status_payments.map((status) => {
      if (filters['status_'+status.id_status_payment]) filter += status.id_status_payment + ','
      return status;
    })
    if (filter === '&filter_status=') filter = ''
    if (this.state.search!=='') filter = '&query='+this.state.search
    return filter.substr(0, filter.length-1)
  }

  async first() {
    if (this.state.first === null)
      return
    const result = await getNav(this.state.first + this.setParam())
    this.setData(result.response)
  }

  async previous() {
    if (this.state.previous === null)
      return
    const result = await getNav(this.state.previous + this.setParam())
    this.setData(result.response)
  }

  async next() {
    if (this.state.next === null)
      return
    const result = await getNav(this.state.next + this.setParam())
    this.setData(result.response)
  }

  async last() {
    if (this.state.last === null)
      return
    const result = await getNav(this.state.last + this.setParam())
    this.setData(result.response)
  }

  async fetchData() {
    let result;
    if (this.state.search !== '')
      result = await getFilter('query='+this.state.search);
    else result = await getAllPayments();
    let data = result.response
    var payments = data.Payments ? data.Payments : []
    payments.sort((a,b)=>(a.order_id < b.order_id) ? 1 : ((b.order_id < a.order_id) ? -1 : 0))
    let status_payments = data.status_payments ? data.status_payments : []
    let filters = {}
    status_payments.map((status) => {
      filters['status_'+status.id_status_payment] = false
      return status
    })
    this.setState({
      is_loading: false,
      payments: payments,
      first: data.first,
      previous: data.previous,
      next: data.next,
      last: data.last,
      total: data.total,
      index: data.index,
      filters: filters,
      status_payments: status_payments,
    })
  }

  componentWillMount() {
    if (this.props.location.state)
      this.setState({
        search: this.props.location.state.email
      })
  }

  componentDidMount() {
    subscribe('search',(value)=> this.dinamycSearch(value))
    emitter('searchBar',true)
    this.fetchData()
  }
  componentWillUnmount(){
    unSubscribe('search')
    emitter('searchBar',false)
  }
  dinamycSearch = async (value) =>{
    const result = await getFilter('query='+value)
    this.setState({
      successful: false,
      failed: false,
      pending: false,
      refunded: false,
      search:value
    })
    this.setData(result.response)
  }

  handlePaymentDetails(id_payment) {
    this.props.history.push('/admin/payment/' + id_payment)
  }

  sortData = (type) => {
    let payments = Object.assign([], this.state.payments)
    switch(type) {
      case "order_id":
        if (this.state.sort_direction_order_id) {
          payments.sort((a,b)=>(a.order_id > b.order_id) ? 1 : ((b.order_id > a.order_id) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.order_id < b.order_id) ? 1 : ((b.order_id < a.order_id) ? -1 : 0))
        }
        this.setState({sort_direction_order_id: !this.state.sort_direction_order_id, payments: payments})
        break;
      case "amount":
        if (this.state.sort_direction_amount) {
          payments.sort((a,b)=>(parseFloat(a.amount) > parseFloat(b.amount)) ? 1 : ((parseFloat(b.amount) > parseFloat(a.amount)) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(parseFloat(a.amount) < parseFloat(b.amount)) ? 1 : ((parseFloat(b.amount) < parseFloat(a.amount)) ? -1 : 0))
        }
        this.setState({sort_direction_amount: !this.state.sort_direction_amount, payments: payments})
        break;
      case "currency":
        if (this.state.sort_direction_currency) {
          payments.sort((a,b)=>(a.currency > b.currency) ? 1 : ((b.currency > a.currency) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.currency < b.currency) ? 1 : ((b.currency < a.currency) ? -1 : 0))
        }
        this.setState({sort_direction_currency: !this.state.sort_direction_currency, payments: payments})
        break;
      case "token":
        if (this.state.sort_direction_token) {
          payments.sort((a,b)=>(a.token > b.token) ? 1 : ((b.token > a.token) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.token < b.token) ? 1 : ((b.token < a.token) ? -1 : 0))
        }
        this.setState({sort_direction_token: !this.state.sort_direction_token, payments: payments})
        break;
      case "date":
        if (this.state.sort_direction_date) {
          payments.sort((a,b)=>(a.date > b.date) ? 1 : ((b.date > a.date) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.date < b.date) ? 1 : ((b.date < a.date) ? -1 : 0))
        }
        this.setState({sort_direction_date: !this.state.sort_direction_date, payments: payments})
        break;
      case "user":
        if (this.state.sort_direction_user) {
          payments.sort((a,b)=>(a.user > b.user) ? 1 : ((b.user > a.user) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.user < b.user) ? 1 : ((b.user < a.user) ? -1 : 0))
        }
        this.setState({sort_direction_user: !this.state.sort_direction_user, payments: payments})
        break;
      case "status":
        if (this.state.sort_direction_status) {
          payments.sort((a,b)=>(a.status > b.status) ? 1 : ((b.status > a.status) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.status < b.status) ? 1 : ((b.status < a.status) ? -1 : 0))
        }
        this.setState({sort_direction_status: !this.state.sort_direction_status, payments: payments})
        break;
      default:
        break;
    }
  }

  renderPayments() {
    const { payments } = this.state
    return payments.map((payment, index) => (
      <tr key={index} onClick={() => this.handlePaymentDetails(payment.id_payment)} className="focus_on">
        <td>{payment.order_id}</td>
        <td>
          <CurrencyFormat 
            value={payment.amount} 
            displayType={'text'} 
            thousandSeparator={true} 
            // prefix={'$'}            
            decimalScale={2}
            fixedDecimalScale={true} 
            renderText={value => <div>{value+' '+payment.currency.toUpperCase()}</div>} />
        </td>        
        <td>{payment.payment_method.toUpperCase()}</td>
        <td><Moment format="MMM DD, YYYY h:mm A">{payment.date}</Moment></td>
        <td>{payment.user}</td>
        <td>
          <div className="payment-status">
            <span>{payment.status}</span>
            {payment.status === 'Succeded' ? <img alt="" src={require("assets/img/icon_check.png")}/> : 
            payment.status === 'Failed' ? <img alt="" src={require("assets/img/icon_fail.png")}/> : 
            payment.status === 'Pending' ? <img alt="" src={require("assets/img/icon_pending.png")}/> : 
            payment.status === 'Free' ? <img alt="" src={require("assets/img/icon_free.svg")}/> :
            payment.status === 'Partially Refunded' ? <img alt="" src={require("assets/img/icon_reply.png")}/> :
            payment.status === 'Refunded' ? <img alt="" src={require("assets/img/icon_reply.png")}/> : null}
          </div>
        </td>
      </tr>
    ));
  }

  render() {
    const { payments, first, previous, next, last, total, index, status_payments, filters} = this.state
    const disableStyle = {
      color: '#9a9a9a'
    }
    return (
      <React.Fragment>
        <ResponsiveModal open={this.state.is_loading} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LoadingModal title="Loading..."/>
        </ResponsiveModal>
        <div className="content" id="payments">
          <Row>
          <Col md="12">
              <CardHeader className="management-header">
                <Col md='4'>
                  <CardTitle tag="h4">Payments</CardTitle>
                </Col>
                <Col md='4' className='funtionSpace'>                  
                </Col>                                
                <Col md='4'className='filterSpace'>  
                  <div className="tools tools-group">
                    {/* <div className="export-btn">Export</div> */}
                    <UncontrolledDropdown>
                      <DropdownToggle
                        caret
                        className="btn-filter"
                        data-toggle="dropdown"
                      >
                        Filter
                      </DropdownToggle>
                      <DropdownMenu right>
                        <div className="filter-header">
                          <div className="filter-header-group">
                            <div className="filter-text">Filters</div>
                            <div className="filter-btn-group">
                              <div className="filter-clear" onClick={this.clearFilter}>Clear</div>
                              <div className="filter-done" onClick={this.doneFilter}>Done</div>
                            </div>
                          </div>
                        </div>

                        {status_payments.map((status, index) =>  <div className="filter-check" key={index}>
                          <input id={status.status} type="checkbox" name={'status_'+status.id_status_payment} checked={filters['status_'+status.id_status_payment]} onChange={this.filterCheck}/>
                          <label htmlFor={status.status}>{status.status}</label>
                        </div>)}
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </div>              
                </Col>
              </CardHeader>
            </Col>
            <Col md="12">
              <Card>
                <CardBody>
                  <Table>
                    <thead className="text-primary">
                      <tr>
                        <th onClick={()=>{this.sortData('order_id')}}>Order ID <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('amount')}}>Amount <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        {/* <th onClick={()=>{this.sortData('currency')}}>Currency <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th> */}
                        <th onClick={()=>{this.sortData('token')}}>Payment Method <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('date')}}>Date <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('user')}}>User <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('status')}}>Status <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                      </tr>
                    </thead>
                    <tbody className="payment-body">{this.renderPayments()}</tbody>
                  </Table>
                  {payments.length === 0 && <div className="no-result">No results found</div>}
                  {payments.length > 0 &&
                  <div className="nav-group">
                    <div><span>Showing</span></div>
                    <div className="first" style={first===null?disableStyle:{}} onClick={this.first}><i className="fa fa-angle-double-left"></i></div>
                    <div className="previous" style={previous===null?disableStyle:{}} onClick={this.previous}><i className="fa fa-angle-left"></i></div>
                    <div className="index"><span>{index}</span></div>
                    <div><span>{'to'}</span></div>
                    <div className="count"><span>{index + payments.length - 1}</span></div>
                    <div><span>{'of'}</span></div>
                    <div className="total"><span>{new Intl.NumberFormat().format(total)}</span></div>
                    <div className="next" style={next===null?disableStyle:{}} onClick={this.next}><i className="fa fa-angle-right"></i></div>
                    <div className="last" style={last===null?disableStyle:{}} onClick={this.last}><i className="fa fa-angle-double-right"></i></div>
                  </div>
                  }
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </React.Fragment>
    );
  }
}

export default PaymentTable;
