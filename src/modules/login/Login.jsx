import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { FormattedMessage } from 'react-intl.macro';
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Col
} from 'reactstrap';
import {loginUniverseUser, AuthTokenLogin, logoutUser} from '../../api/LoginApi';
import auth from '../../api/auth';
import './login.scss'
import ResponsiveModal from 'react-responsive-modal';
import LogoutModal from '../../components/Navbars/LogoutModal'
import logoalt from '../../assets/img/logoAlt.png'
import {logo} from '../../environment'

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      redirectSMSValidation: false,
      email: '',
      pass: '',
      email_state: '',
      pass_state: '',
      res_state: false,
      status: '',
      showModal: false,
      type: 'password'
    };

    this.login = this.login.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePass = this.handleChangePass.bind(this);
    this.renderLogin = this.renderLogin.bind(this);
  }

  componentDidMount() {    
    document.body.classList.toggle('login-page');
  }

  componentWillUnmount() {
    document.body.classList.toggle('login-page');
  }

  handleChangeEmail(event) {
    var isValid = event.target.value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
    this.setState({
      email: event.target.value,
      email_state: isValid ? '' : 'has-danger'
    });
  }

  handleChangePass(event) {
    let valid = '';
    if (event.target.value.length < 8)
      valid = 'has-danger'
    this.setState({
      pass: event.target.value,
      pass_state: valid
    });
  }

  async login(e) {
    e.preventDefault()
    const {email_state , pass_state,email,pass} = this.state
    if(pass_state==='' && email_state==='' && pass!=='' && email!==''){
      this.setState({showModal: true})
      const userLogin = {
        email: this.state.email,
        password: this.state.pass
      };
      await AuthTokenLogin();
      const res = await loginUniverseUser(userLogin);
      this.setState({showModal: false} ,()=>{
        if (res.response.status === 400) {
          this.setState({res_state: true})
          return;
        }
        if (res.response.status === 404) {
          this.setState({status: 'inactive'})
          return
        }
        if (auth.hasLogin()) {
          this.props.history.push('/admin/Dashboard')
        }
        else { 
          if (auth.hasSMSBeenSent()) {
            this.props.history.push('/auth/verification-code')
          }
        }
      })
    }    
  }

  showHide = () => {
    this.setState({
      type: this.state.type === 'input' ? 'password' : 'input'
    })
  }

  reEnter = () =>{    
    this.setState({status:''})
  }

  renderLogin() {
    const {email, email_state, pass, pass_state, status} = this.state
    if (auth.hasLogin()) {
      return <Redirect from="/" to="/admin/Dashboard" />;
    } else if (auth.hasSMSBeenSent()) {
      if(localStorage.getItem('login')){
        return <Redirect from="/" to="/auth/verification-code" />
      }
      else{
        logoutUser()
      }
    } 
    return (
      <div className="content">
        <Container>
          <Col className="ml-auto mr-auto" lg="6" md="6">
            <Form className="form" onSubmit={this.login}>
              {status!=='inactive' ? (
                <div className="logo">
                  <img alt={logoalt} src={logo}/>
                </div>
              ) : null}
              <Card className="card-login">
                <CardHeader>
                  <div className="login-title">{status==='inactive' ? 
                    <FormattedMessage
                      id="auth.login.disTitle"
                      defaultMessage='Your account is inactive'
                    />:
                    <FormattedMessage
                      id="auth.login.welcome"
                      defaultMessage='Welcome back!'
                    />}
                  </div>
                  <div className="title-border"></div>
                </CardHeader>
                {status==='inactive' ? (
                  <CardBody>
                    <InputGroup>
                      <label className="error log_error inactive">
                        <FormattedMessage
                          id="auth.login.dis1"
                          defaultMessage="Your account has been disabled by the administrator."
                        />
                      </label>
                    </InputGroup>
                    <InputGroup>
                      <label className="contact-support">
                        <FormattedMessage
                          id="auth.login.dis2"
                          defaultMessage="Please, contact customer support."
                        />
                      </label>
                    </InputGroup>
                  </CardBody>
                ) : (
                  <CardBody>
                    <InputGroup>
                      {this.state.res_state ? (
                        <label className="error log_error">
                          <FormattedMessage
                            id="auth.login.err1"
                            defaultMessage="Wrong email or password"
                          />
                        </label>
                      ) : null}                      
                    </InputGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="tim-icons icon-email-85" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input placeholder="Email" type="text" value={email} onChange={this.handleChangeEmail} onBlur={this.handleChangeEmail}/>
                    </InputGroup>
                    <InputGroup>
                      {email_state === "has-danger" ? (
                        <label className="error">
                          <FormattedMessage
                            id="auth.login.emailWrong"
                            defaultMessage="Enter a valid email address"
                          />
                        </label>
                      ) : null}                      
                    </InputGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="tim-icons icon-lock-circle" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input placeholder="Password" type={this.state.type} value={pass} onChange={this.handleChangePass} onBlur={this.handleChangePass}/>
                      <InputGroupAddon addonType="append" onClick={this.showHide}>
                        <InputGroupText>
                          {this.state.type === 'input' ? <i className="fa fa-eye-slash" /> : <i className="fa fa-eye" />}
                        </InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    <InputGroup>
                      {pass_state === "has-danger" ? (
                        <label className="error">
                          <FormattedMessage
                            id="auth.login.passShort"
                            defaultMessage="Your password must have at least 8 characters"
                          />
                        </label>
                      ) : null}
                    </InputGroup>
                  </CardBody>
                )}
                {status === 'inactive' ? (
                  <CardFooter>
                    <div className="got_it">
                      <Button
                        type="button"
                        block
                        onClick={this.reEnter}
                      >
                        <FormattedMessage
                          id="auth.login.btnGotit"
                          defaultMessage=" Got it!"
                        />
                      </Button>
                    </div>
                  </CardFooter>
                ) : (
                  <CardFooter>
                    <div className="first-line">
                      {/* <div>
                        <input id="remember_me" type="checkbox" disabled/>
                        <label htmlFor="remember_me">
                          <FormattedMessage
                            id="auth.login.rememberme"
                            defaultMessage="Remember me"
                          />
                        </label>
                      </div> */}
                      <div>
                        <Link to={"/auth/reset-password"} className="forgot_password">
                          <FormattedMessage
                            id="auth.login.forgotPass"
                            defaultMessage="Forgot your password?"
                          />
                        </Link>
                      </div>
                    </div>
                    <div className="second-line">
                      <Button
                        type="submit"
                        block
                        onClick={this.login}
                      >
                        <FormattedMessage
                          id="auth.login.btnLogin"
                          defaultMessage="Login"
                        />
                      </Button>
                    </div>
                  </CardFooter>                    
                )}
              </Card>
            </Form>
          </Col>
        </Container>
        <ResponsiveModal open={this.state.showModal} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LogoutModal title="Logging In"/>
        </ResponsiveModal>
      </div>
    );
  }
  
  render() {
    return <React.Fragment>{this.renderLogin()}</React.Fragment>;
  }
}

export default Login;
