// reactstrap components
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormGroup,
  Input,
  Row,
  Label
} from 'reactstrap';
import React from 'react';
import './change.scss'
import { recoveryPasswordView, recoverPasswordLogin ,AuthTokenLogin} from './../../api/LoginApi'
import SweetAlert from 'react-bootstrap-sweetalert';
import logoalt from '../../assets/img/logoAlt.png'
import {logo} from '../../environment'

class ChangePassword extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      pass: '',
      pass_state: '',
      confirm: '',
      confirm_state: '',
      token: '',
      uuid: null,
      alert: null,
    }
  }
  passChange = (e) => {
    this.setState({
      pass: e.target.value,
      pass_state: e.target.value.length < 8 ? 'has-danger' : ''
    })
  }

  confirmChange = (e) => {
    this.setState({
      confirm: e.target.value,
      confirm_state: e.target.value !== this.state.pass ? 'has-danger' : ''
    })
  }

  closeAlert = () => {
    this.setState({alert: null})
  }

  closeSuccessAlert = () => {
    this.setState({alert: null})
    this.props.history.push('/')
  }
  fetchData = async() => {
    const { match: { params } } = this.props; 
    let token = params.token?params.token:null

    //this.setState({token: params.token})
    const res = await recoveryPasswordView(token)
    let uuid = ''
    if (res.response.status === 200) {
      uuid = res.response.uuid
       this.setState({
         uuid:uuid,
         token:res.response.token,
         alert: null,
       })
       return;
    }
    else{
      this.setState({
        uuid:uuid,
        token:token,
        alert: (
         <SweetAlert warning title={res.response.message} onConfirm={this.closeAlert}>
         </SweetAlert>
       )
      })
    }
    this.setState({token:token,uuid:uuid})
  }
 
  componentDidMount() {
    this.fetchData()
  }

  changePassword = async () => {
    const {uuid, pass, confirm, token} = this.state
    if (!uuid)
      return

    var pass_state = true, confirm_state = true
    if (pass.length < 8)
      pass_state = false
    if (pass !== confirm)
      confirm_state = false

    if (!pass_state || !confirm_state) {
      this.setState({
        pass_state: pass_state ? '' : 'has-danger',
        confirm_state: confirm_state ? '' : 'has-danger',
      })
      return;
    }
    await AuthTokenLogin()
    const res = await recoverPasswordLogin({uuid:uuid, password: pass, token: token})
    if (res.response.status !== 200) {
      this.setState({
        alert: (
          <SweetAlert warning title={res.response.message} onConfirm={this.closeAlert}>
          </SweetAlert>
        )
      })
    } else {
      this.setState({
        alert: (
          <SweetAlert success title={res.response.message} onConfirm={this.closeSuccessAlert}>
          </SweetAlert>
        )
      })
    }
  }

  render() {
    const {pass, pass_state, confirm, confirm_state} = this.state
    return (
      <React.Fragment>
        {this.state.alert}
        <div className="content">
          <Row className="justify-content-md-center">
            <Col md="4">
              <div className="logo">
                <img alt={logoalt} src={logo}/>
              </div>
              <Card>
                <Form>
                  <CardHeader>
                    <div className="login-title">Change password</div>
                    <div className="title-border"></div>
                  </CardHeader>
                  <CardBody>
                    <Row>
                      <Label sm="4">New password</Label>
                      <Col sm="8">
                        <FormGroup className={pass_state}>
                          <Input
                            type="password"
                            placeholder="********"
                            value={pass}
                            onChange={this.passChange}
                            onBlur={this.passChange}
                          />
                          {pass_state === "has-danger" ? (
                            <label className="error">
                              This field is required.
                            </label>
                          ) : null}
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Label sm="4">Confirm password</Label>
                      <Col sm="8">
                        <FormGroup className={confirm_state}>
                          <Input
                            type="password"
                            placeholder="********"
                            value={confirm}
                            onChange={this.confirmChange}
                            onBlur={this.confirmChange}
                          />
                          {confirm_state === "has-danger" ? (
                            <label className="error">
                              This field is required.
                            </label>
                          ) : null}
                        </FormGroup>
                      </Col>
                    </Row>
                  </CardBody>
                  <CardFooter>
                    <div className="cosmic-butons butonsFormInfo">
                      <Button className="ccosmic-cancelancel" onClick={()=>{this.props.history.push('/')}}>
                        Cancel
                      </Button>
                      <Button className="cosmic-confirm" onClick={this.changePassword}>
                        Change password
                      </Button>
                    </div>
                  </CardFooter>
                </Form>
              </Card>
            </Col>
          </Row>
        </div>
      </React.Fragment>
    );
  }
}

export default ChangePassword;
