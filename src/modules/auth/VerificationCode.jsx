// reactstrap components
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  InputGroup,
  Row
} from 'reactstrap';
import React from 'react';
import { Redirect } from 'react-router-dom';
import auth from './../../api/auth';
import { verifySMSCode, requestSMSCode, AuthTokenLogin } from './../../api/LoginApi';
import './verification.scss'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import logoalt from '../../assets/img/logoAlt.png'
import {logo} from '../../environment'

const MAX_LENGTH = 1

class VerificationCode extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      lastDigits: auth.getLastDigits(),
      code: [],
      status: '',
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount() {
    if (!auth.hasLogin())
      this.codeInput0.focus()
  }

  async handleSubmit(event) {
    event.preventDefault();
    const res = await verifySMSCode(this.state.code.join(""));
    if (res.response.status !== 200) {
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
      this.setState({status: 'error'})
    }
    if (auth.hasLogin()) {
      this.props.history.push('/admin/Dashboard')
    }
  }

  handleChangeCode = (e) => {
    let newCode = this.state.code
    newCode[parseInt(e.target.name)-1] = e.target.value
    this.setState({code: newCode})
    if (e.target.name==="1" && e.target.value.length===0) return;
    if (e.target.name==="6" && e.target.value.length===MAX_LENGTH) {
      return;
    }
    if (e.target.value.length === MAX_LENGTH) {
      this[`codeInput${parseInt(e.target.name)}`].focus();
      return
    }
    const prev = parseInt(e.target.name)-1
    if (e.target.value.length === 0) {
      this[`codeInput${prev}`].focus();
      return
    }
  }

  tryAgain = async () => {
    await AuthTokenLogin()
    const res = await requestSMSCode()
    this.codeInput0.value="";this.codeInput1.value="";this.codeInput2.value="";
    this.codeInput3.value="";this.codeInput4.value="";this.codeInput5.value="";
    if (res.response.status !== 201) {
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    } else {
      toast.success(res.response.message, {position: toast.POSITION_TOP_RIGHT})
      this.setState({lastDigits: auth.getLastDigits(), status: ''})
    }
  }

  goBack = () => {
    localStorage.clear()
    auth.removeApiKey();
    auth.removeOrganization();
    auth.removeLastDigits();
    auth.removeRole();
    auth.removeUserInfo();
    auth.removeUserInfo()
    auth.removeOperator()
    auth.removeAuthTokenUniverseUser();
    auth.removeUUID();
    auth.removeBrand();
    auth.removeUserType();
    localStorage.clear()
    this.props.history.push('/auth/login');
  }

  render() {
    const {status, lastDigits} = this.state
    if (auth.hasLogin()) {
      return <Redirect from="/" to="/admin/Dashboard" />;
    } else {
      return (
        <React.Fragment>
          <div className="content">
            <Row className="justify-content-md-center">
              <Col md="4">
                <div className="logo">
                  <img alt={logoalt} src={logo}/>
                </div>
                <Card>
                  <Form onSubmit={this.handleSubmit}>
                    <CardHeader>
                      <div className="login-title">Enter your verification code.</div>
                      <div className="title-border"></div>
                    </CardHeader>
                    <CardBody>
                      {status === 'error' &&
                      <InputGroup>
                        <label className="error">
                          Wrong verification code
                        </label>
                      </InputGroup>}
                      <InputGroup>
                        <label className="description">
                          Let's make sure it's really you. We've just sent a text message with a fresh verification code to
                          the phone number ending in *********{lastDigits}.                        
                        </label>
                      </InputGroup>
                      <div className="code-input-group">
                        <div className="code-input">
                          <input ref={(input)=>{this.codeInput0 = input}} 
                            type="text" maxLength="1" name="1" onChange={this.handleChangeCode}/>
                          <input ref={(input)=>{this.codeInput1 = input}} 
                            type="text" maxLength="1" name="2" onChange={this.handleChangeCode}/>
                          <input ref={(input)=>{this.codeInput2 = input}} 
                            type="text" maxLength="1" name="3" onChange={this.handleChangeCode}/>
                        </div>
                        <div className="dash">-</div>
                        <div className="code-input">
                          <input ref={(input)=>{this.codeInput3 = input}} 
                            type="text" maxLength="1" name="4" onChange={this.handleChangeCode}/>
                          <input ref={(input)=>{this.codeInput4 = input}} 
                            type="text" maxLength="1" name="5" onChange={this.handleChangeCode}/>
                          <input ref={(input)=>{this.codeInput5 = input}} 
                            type="text" maxLength="1" name="6" onChange={this.handleChangeCode}/>
                        </div>
                      </div>
                      <div className="try-again">
                        <p>
                          Did not receive a code? <span onClick={this.tryAgain}>Try again.</span>
                        </p>
                      </div>
                    </CardBody>
                    <CardFooter>
                      <Row className='cosmic-butons butonsVerification'>
                        <Button className="cosmic-cancel" onClick={()=>{this.goBack()}}>Back</Button>
                        <Button className="cosmic-confirm" type="submit">Submit</Button>
                      </Row>
                    </CardFooter>
                  </Form>
                </Card>
              </Col>
            </Row>
            <ToastContainer />
          </div>
        </React.Fragment>
      );
    }
  }
}

export default VerificationCode;
