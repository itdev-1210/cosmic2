// reactstrap components
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  InputGroupAddon,
  InputGroupText,
  Input,
  InputGroup,
  Row
} from 'reactstrap';
import React from 'react';
import './reset.scss'
import Recaptcha from 'react-recaptcha'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { recoveryPassword,AuthTokenLogin } from './../../api/LoginApi'
import logoalt from '../../assets/img/logoAlt.png'
import {logo} from '../../environment'
import {captcha} from '../../environment'

class ResetPassword extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      email_state: '',
      status: '',
      captcha: false,
    }
  }
  handleChangeEmail = (e) => {
    var isValid = e.target.value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
    this.setState({
      email: e.target.value,
      email_state: isValid ? '' : 'has-danger'
    });
  }
  callback = () => {
  }
  verifyCallback = (response) => {
    this.setState({captcha: true})
  }
  
  resetPassword = async () => {
    var isValid = this.state.email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
    if (!isValid) {
      this.setState({email_state: 'has-danger'})
      return;
    }
    if (!this.state.captcha)
      return;
    await AuthTokenLogin();
    const res = await recoveryPassword({email: this.state.email})
    if (res.response.status === 200)
      toast.success(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    else
      this.setState({status: 'error'})
      // toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
  }
  render() {
    const {email, email_state, status} = this.state
    return (
      <React.Fragment>
        <div className="content">
          <Row className="justify-content-md-center">
            <Col md="4">
              <div className="logo">
                <img alt={logoalt} src={logo}/>
              </div>
              <Card>
                <CardHeader>
                  <div className="login-title">Reset your password</div>
                  <div className="title-border"></div>
                </CardHeader>
                <CardBody>
                  {status === 'error' &&
                  <InputGroup>
                    <label className="error">
                      We could not find your email. Try another?
                    </label>
                  </InputGroup>}
                  <InputGroup>
                    <label className="description">
                      Enter your email address below and we'll send you a link to reset your password.
                    </label>
                  </InputGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="tim-icons icon-email-85" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Your Email" type="text" value={email} onChange={this.handleChangeEmail} onBlur={this.handleChangeEmail}/>                        
                  </InputGroup>
                  <Recaptcha 
                    sitekey={captcha}
                    render="explicit"
                    onloadCallback={this.callback}
                    verifyCallback={this.verifyCallback}
                    />
                  <InputGroup>
                    {email_state === "has-danger" ? (
                      <label className="error">
                        Enter a valid email address
                      </label>
                    ) : null}
                  </InputGroup>
                </CardBody>
                <CardFooter>
                  <Row className='cosmic-butons butonsVerification'>
                    <Button className="cosmic-cancel" onClick={()=>{this.props.history.goBack();}}>Back</Button>
                    <Button className="cosmic-confirm" onClick={this.resetPassword} disabled={!this.state.captcha}>Send reset password email</Button>
                  </Row>
                </CardFooter>
              </Card>
            </Col>
          </Row>
          <ToastContainer />
        </div>
      </React.Fragment>
    );
  }
}

export default ResetPassword;
