import React from 'react';
import classNames from 'classnames';
import { Line } from 'react-chartjs-2';
import ReactFlagsSelect from 'react-flags-select';
import 'react-flags-select/css/react-flags-select.css';
import moment from 'moment'
import { ToastContainer, toast } from 'react-toastify'

import Scooter from '../../assets/img/Scooter.svg';
import Guy from '../../assets/img/Guy.svg';
import Active from '../../assets/img/Active.svg';
import Inactive from '../../assets/img/Inactive.svg';
import CurrencyFormat from 'react-currency-format';
import {
  Button,
  ButtonGroup,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
} from 'reactstrap';

import { canvasInfo , getAnalyticsBy , canvasOption, getScootersInfoBy} from '../../api/analyticsApi'

import './dashboard.scss'

class Dashboard extends React.Component {
  constructor(props) {    
    super(props);
    this.reloadIcon = <i className={`tim-icons icon-refresh-01`} />
    this.clickReloadIcon = <i className={`tim-icons icon-refresh-01 fa-spin`} />
    
    this.state = {
      bigChartData: 'daily',
      sOperation:0,
      sActiveRides:0,
      sAvailable:0,
      sUnavailable:0,
      countryRevenue:[],
      activeCountry:'0',
      icon1:this.reloadIcon,
      icon2:this.reloadIcon,
      icon3:this.reloadIcon,
      icon4:this.reloadIcon,
      startDate:'',
      endDate:'',
    };
  }

  startDateChanged = async (d) => {
    if(moment.isMoment(d)){
      const start = d.format('YYYY-MM-DD')
      const res = await getAnalyticsBy({filter:'date',country:this.state.activeCountry,start:start,end:this.state.endDate})
      this.setState({startDate:start,
        countryRevenue:res.country,
        bigChartData:'date'
      });
    }
    else{
      this.setState({startDate:''});
    }
  }

  endDateChanged = async (d) => {
    if(moment.isMoment(d)){
      const end = d.format('YYYY-MM-DD')
      const res = await getAnalyticsBy({filter:'date',country:this.state.activeCountry,start:this.state.startDate,end:end})
      this.setState({endDate:end,
        countryRevenue:res.country,   
        bigChartData:'date'   
      });
    }
    else{
      this.setState({endDate:''});
    }
  }

  componentDidMount() {
    this.getAllInfoScooters()
    this.setBgChartData('daily')
  }

  setBgChartData = async (name) => {
    const res = await getAnalyticsBy({filter:name,country:this.state.activeCountry,start:'',end:''})
    this.setState({
      bigChartData:name,
      countryRevenue:res.country,
      startDate:'',
      endDate:''
    }); 
  };

  getAllInfoScooters = async ()=>{
    const allDeviceInfo = {}
    let deviceInfo = {}
    deviceInfo = await getScootersInfoBy('operation')
    if(deviceInfo.st && deviceInfo.status===200){      
      allDeviceInfo.operation=deviceInfo.Devices_in_operation
    }
    deviceInfo = await getScootersInfoBy('onRide')
    if(deviceInfo.st && deviceInfo.status===200){      
      allDeviceInfo.onRide=deviceInfo.Devices_on_ride
    }
    deviceInfo = await getScootersInfoBy('available')
    if(deviceInfo.st && deviceInfo.status===200){      
      allDeviceInfo.available=deviceInfo.Devices_available
    }
    deviceInfo = await getScootersInfoBy('unavailable')
    if(deviceInfo.st && deviceInfo.status===200){
      allDeviceInfo['unavailable']=deviceInfo['Devices_unavailable']      
    }
    this.setState({
      sOperation:allDeviceInfo.operation?allDeviceInfo.operation:0,
      sActiveRides:allDeviceInfo.onRide?allDeviceInfo.onRide:0,
      sAvailable:allDeviceInfo.available?allDeviceInfo.available:0,
      sUnavailable:allDeviceInfo.unavailable?allDeviceInfo.unavailable:0,
    });
  }

  updateInfoScooter = async (filter) =>{
    let icons = {};
    switch(filter) {
      case 'operation':
        icons['icon1'] = this.clickReloadIcon;
        break;
      case 'onRide':
        icons['icon2'] = this.clickReloadIcon;
        break;
      case 'available':
        icons['icon3'] = this.clickReloadIcon;
        break;
      case 'unavailable':
        icons['icon4'] = this.clickReloadIcon;
        break;
      default:
        break;
    }
    this.setState(icons, async () => {
      const scooterInfo = await getScootersInfoBy(filter)
      if(scooterInfo.st && scooterInfo.status===200){
        toast.success(scooterInfo.message, {position: toast.POSITION_TOP_RIGHT})
        if(scooterInfo['Devices_in_operation']){
          this.setState({
            sOperation:scooterInfo['Devices_in_operation'],
            icon1:this.reloadIcon
          });
        }
        else if(scooterInfo['Devices_on_ride']){
          this.setState({
            sActiveRides:scooterInfo['Devices_on_ride'],
            icon2:this.reloadIcon
          });
        }
        else if(scooterInfo['Devices_available']){
          this.setState({
            sAvailable:scooterInfo['Devices_available'],
            icon3:this.reloadIcon
          });
        }
        else if(scooterInfo['Devices_unavailable']){
          this.setState({
            sUnavailable:scooterInfo['Devices_unavailable'],
            icon4:this.reloadIcon
          });
        }
      } else {
        toast.error(scooterInfo.message, {position: toast.POSITION_TOP_RIGHT})
        this.setState({
          icon1:this.reloadIcon,
          icon2:this.reloadIcon,
          icon3:this.reloadIcon,
          icon4:this.reloadIcon,
        });   
      }
    })
  }

  changeCountry = async (e) => {
    const value = e.target.value
    const res = await getAnalyticsBy({filter:this.state.bigChartData,country:value,start:this.state.startDate,end:this.state.endDate})
    this.setState({
      countryRevenue:res.country,
      activeCountry:value
    });
  }

  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col xs="12">
              <Card className="card-chart">
                <CardHeader>
                  <Row className="mobile_row">
                    <Col className="text-left" sm="4" md="4" xs="2">
                      <h5 className="card-category">Total Revenue</h5>
                      <CardTitle tag="h2">Revenue</CardTitle>
                    </Col>
                    <Col sm="8" md="8" xs="10" className="dashboard-filters">
                      <Col sm="6" className='dash-filters1'>
                        <ButtonGroup className={"btn-group-toggle dash-filter"} data-toggle="buttons">                        
                          <Button
                            color="info"
                            id="0"
                            size="sm"
                            tag="label"
                            className={classNames('btn-simple', {
                              active: this.state.bigChartData === 'daily'
                            })}
                            onClick={() => this.setBgChartData('daily')}
                          >
                            <input defaultChecked name="options" type="radio" />
                            <span className="d-none d-sm-block d-md-block d-lg-block d-xl-block">DAILY</span>
                            <span className="d-block d-sm-none">
                              <i className="tim-icons icon-single-02" />
                            </span>
                          </Button>

                          <Button
                            color="info"
                            id="1"
                            size="sm"
                            tag="label"
                            className={classNames('btn-simple', {
                              active: this.state.bigChartData === 'montly'
                            })}
                            onClick={() => this.setBgChartData('montly')}
                          >
                            <input name="options" type="radio" />
                            <span className="d-none d-sm-block d-md-block d-lg-block d-xl-block">MONTHLY</span>
                            <span className="d-block d-sm-none">
                              <i className="tim-icons icon-gift-2" />
                            </span>
                          </Button>

                          <Button
                            color="info"
                            id="2"
                            size="sm"
                            tag="label"
                            className={classNames('btn-simple', {
                              active: this.state.bigChartData === 'annual'
                            })}
                            onClick={() => this.setBgChartData('annual')}
                          >
                            <input name="options" type="radio" />
                            <span className="d-none d-sm-block d-md-block d-lg-block d-xl-block">ANNUAL</span>
                            <span className="d-block d-sm-none">
                              <i className="tim-icons icon-tap-02" />
                            </span>
                          </Button>
                          
                          {/* <Button
                            color="info"
                            id="3"
                            size="sm"
                            tag="label"
                            className={classNames('btn-simple', {
                              active: this.state.bigChartData === 'mtd'
                            })}
                            onClick={() => this.setBgChartData('mtd')}
                          >
                            <input name="options" type="radio" />
                            <span className="d-none d-sm-block d-md-block d-lg-block d-xl-block">MTD</span>
                            <span className="d-block d-sm-none">
                              <i className="tim-icons icon-tap-02" />
                            </span>
                          </Button>

                          <Button
                            color="info"
                            id="4"
                            size="sm"
                            tag="label"
                            className={classNames('btn-simple', {
                              active: this.state.bigChartData === 'qtd'
                            })}
                            onClick={() => this.setBgChartData('qtd')}
                          >
                            <input name="options" type="radio" />
                            <span className="d-none d-sm-block d-md-block d-lg-block d-xl-block">QTD</span>
                            <span className="d-block d-sm-none">
                              <i className="tim-icons icon-tap-02" />
                            </span>
                          </Button>
                          
                          <Button
                            color="info"
                            id="5"
                            size="sm"
                            tag="label"
                            className={classNames('btn-simple', {
                              active: this.state.bigChartData === 'ytd'
                            })}
                            onClick={() => this.setBgChartData('ytd')}
                          >
                            <input name="options" type="radio" />
                            <span className="d-none d-sm-block d-md-block d-lg-block d-xl-block">YTD</span>
                            <span className="d-block d-sm-none">
                              <i className="tim-icons icon-tap-02" />
                            </span>
                          </Button>
                          
                          <Button
                            color="info"
                            id="6"
                            size="sm"
                            tag="label"
                            className={classNames('btn-simple', {
                              active: this.state.bigChartData === 'all'
                            })}
                            onClick={() => this.setBgChartData('all')}
                          >
                            <input name="options" type="radio" />
                            <span className="d-none d-sm-block d-md-block d-lg-block d-xl-block">ALL</span>
                            <span className="d-block d-sm-none">
                              <i className="tim-icons icon-tap-02" />
                            </span>
                          </Button> */}

                        </ButtonGroup>
                      </Col>
                      <Col sm="6" className='dash-filters2'>
                          {/* <FormGroup>
                            <Datetime className='dates'
                              inputProps={{placeholder:"Select Day"}}
                              timeFormat={false}
                              closeOnSelect={true}
                              value={this.state.startDate}
                              onChange={this.startDateChanged}
                            />
                          </FormGroup> */}
                        {/* <i className={`tim-icons icon-minimal-right`} />                      
                          <FormGroup>
                            <Datetime className='dates'
                              inputProps={{placeholder:"End"}}
                              timeFormat={false}
                              closeOnSelect={true}
                              value={this.state.endDate}
                              onChange={this.endDateChanged}
                            />
                          </FormGroup>                       */}
                          <FormGroup>
                            <Input
                              className='country'
                              name="country"
                              type="select"
                              value={this.state.activeCountry}
                              onChange={this.changeCountry}
                            >
                            <option key={0} value={0}>Countries</option>
                            {this.state.countryRevenue.map((country, index) => <option key={index+1} value={country.id_country}>{country.country}</option>)}
                            </Input>
                          </FormGroup>                     

                      </Col>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <div className="chart-area">
                    <Line
                      data={                        
                        canvasInfo
                      }
                      options={canvasOption}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>

            <Col lg="3" md="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col xs="5">
                      <div className="info-icon text-center icon-warning">
                        <img src={Scooter} className="icon-cosmicgo" alt=""/>
                      </div>
                    </Col>
                    <Col xs="7">
                      <div className="numbers">
                        <p className="card-category">Scooters in Operation</p>
                        <CardTitle tag="h3">{this.state.sOperation}</CardTitle>
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats" onClick={() => this.updateInfoScooter('operation')}>
                      <Row style={{cursor:'pointer'}}>
                        <Col sm={1}>
                          {this.state.icon1}
                        </Col>
                        <Col sm={8}>
                        Update Now
                        </Col>
                      </Row>
                  </div>
                </CardFooter>
              </Card>
            </Col>
            
            <Col lg="3" md="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col xs="5">
                      <div className="info-icon text-center icon-warning" >
                        <img src={Guy} className="icon-cosmicgo" alt=""/>
                      </div>
                    </Col>
                    <Col xs="7">
                      <div className="numbers">
                        <p className="card-category">Active Rides</p>
                        <CardTitle tag="h3">{this.state.sActiveRides}</CardTitle>
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats" onClick={() => this.updateInfoScooter('onRide')}>
                    <Row style={{cursor:'pointer'}}>
                      <Col sm={1}>
                        {this.state.icon2}
                      </Col>
                      <Col sm={8}>
                      Update Now
                      </Col>
                    </Row>
                  </div>
                </CardFooter>
              </Card>
            </Col>
            
            <Col lg="3" md="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col xs="5">
                      <div className="info-icon text-center icon-warning">
                        <img src={Active} className="icon-cosmicgo" alt=""/>
                      </div>
                    </Col>
                    <Col xs="7">
                      <div className="numbers">
                        <p className="card-category">Available Scooters</p>
                        <CardTitle tag="h3">{this.state.sAvailable}</CardTitle>
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats" onClick={() => this.updateInfoScooter('available')}> 
                    <Row style={{cursor:'pointer'}}>
                      <Col sm={1}>
                        {this.state.icon3}
                      </Col>
                      <Col sm={8}>
                      Update Now
                      </Col>
                    </Row>  
                  </div>
                </CardFooter>
              </Card>
            </Col>
            
            <Col lg="3" md="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col xs="5">
                      <div className="info-icon text-center icon-warning">
                        <img src={Inactive} className="icon-cosmicgo" alt=""/>
                      </div>
                    </Col>
                    <Col xs="7">
                      <div className="numbers">
                        <p className="card-category">Total Fleet</p>
                        <CardTitle tag="h3">{this.state.sUnavailable}</CardTitle>
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats"onClick={() => this.updateInfoScooter('unavailable')}>
                    <Row style={{cursor:'pointer'}}>
                      <Col sm={1}>
                        {this.state.icon4}
                      </Col>
                      <Col sm={8}>
                      Update Now
                      </Col>
                    </Row>
                  </div>
                </CardFooter>
              </Card>
            </Col>
          </Row>
          <Row>
                  
            <Col lg="12">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Global Daily by Locations</CardTitle>
                  <p className="card-category">All products that were shipped</p>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col md="6">
                      <Table>
                        <thead>
                          <tr>
                            <th >COUNTRY</th>
                            <th />
                            <th className="text-right">TOTAL REVENUES</th>
                            <th className="text-right">TOTAL RIDES</th>
                          </tr>
                        </thead>                        
                        <tbody>
                          {this.state.countryRevenue.map(country => {
                            return (<tr key={country.id_country}>
                              <td>
                                <div className="flag">                                
                                  <ReactFlagsSelect
                                    defaultCountry={country.iso}
                                    showSelectedLabel={false}
                                    disabled={true} 
                                  />
                                </div>
                              </td>
                              <td>{country.country}</td>
                              <td className="text-right">{
                                <CurrencyFormat 
                                  value={country.total_revenues} 
                                  displayType={'text'} 
                                  thousandSeparator={true} 
                                  prefix={'$'}
                                  decimalScale={2}
                                  fixedDecimalScale={true}
                                  renderText={value => <div>{value+' '+'USD'}</div>} />
                                }
                              </td>
                              <td className="text-right">{country.total_rides}</td>
                            </tr>)
                          })}                                                 
                        </tbody>
                      </Table>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
        <ToastContainer />
      </>
    );
  }
}

export default Dashboard;
