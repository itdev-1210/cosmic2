import React from 'react';
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  DropdownToggle,
  DropdownMenu,
  UncontrolledDropdown,
  Table,
  Row,
  UncontrolledTooltip,
  Col,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import SweetAlert from 'react-bootstrap-sweetalert';
import { getInventoriDevices,getInventoriDevicesFil, getNavAdmin, getFilterAdmin, deleteDeviceAdmin, ownerAssignDevices, operatorAssignDevices } from '../../api/ScooterApi';
import {emitter, subscribe,unSubscribe } from '../../utils/EventComponents';
import './styles/adminInventory.scss'
import ResponsiveModal from 'react-responsive-modal';
import Moment from 'react-moment'
import Datetime from 'react-datetime';
import ConfirmModal from '../../views/pages/ConfirmModal'
import OperatorModal from './modals/OperatorModal'
import UrlModal from './modals/UrlModal'
import {CopyToClipboard} from 'react-copy-to-clipboard';
import LoadingModal from './../../components/Navbars/LogoutModal'

class AdminInventory extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      search:'',
      is_loading: true,
      Devices: [],
      first: null,
      previous: null,
      next: null,
      last: null,
      total: 0,
      index: 0,
      sort_direction_id: false,
      sort_direction_serial: true,
      sort_direction_date: true,
      sort_direction_owner: true,
      sort_direction_operator: true,

      urlModal: false,
      transferModal: false,
      assignModal: false,
      confirmModal: false,
      curDevice: null,
      alert: null,
    }
    this.setData = this.setData.bind(this)
    this.fetchData = this.fetchData.bind(this)
    this.first = this.first.bind(this)
    this.previous = this.previous.bind(this)
    this.next = this.next.bind(this)
    this.last = this.last.bind(this)
    this.setParam = this.setParam.bind(this)
  }
  
  setParam() {
    if (!this.state.create_date)
      return '';
    if (this.state.filter_start === '' && this.state.filter_end === '')
      return '';
    var filter_start = 'filter_start_date='
    var filter_end = 'filter_end_date='

    if (this.state.filter_start !== '') {
      filter_start += this.state.filter_start
    }
    if (this.state.filter_end !== '') {
      filter_end += this.state.filter_end
    }
    if (filter_start === 'filter_start_date=') {
      if (filter_end === 'filter_end_date=') return;
      return filter_end
    } else if (filter_end === 'filter_end_date=') {
      if (filter_start === 'filter_start_date=') return;
      return filter_start
    } else {
      return filter_start+'&' +filter_end
    }
  }

  async fetchData() {
    const result = await getInventoriDevices();
    this.setData(result.response);
  }

  async first() {
    if (this.state.first === null)
      return

    const result = await getNavAdmin(this.state.first + this.setParam())
    this.setData(result.response)
  }

  async previous() {
    if (this.state.previous === null)
      return
    const result = await getNavAdmin(this.state.previous + this.setParam())
    this.setData(result.response)
  }

  async next() {
    if (this.state.next === null)
      return
    const result = await getNavAdmin(this.state.next + this.setParam())
    this.setData(result.response)
  }

  async last() {
    if (this.state.last === null)
      return
    const result = await getNavAdmin(this.state.last + this.setParam())
    this.setData(result.response)
  }

  componentDidMount() {
    subscribe('search',(value)=> this.dinamycSearch(value))
    emitter('searchBar',true)
    this.fetchData()
  }  
  componentWillUnmount =() => {
    unSubscribe('search')
    emitter('searchBar',false)
  }
  dinamycSearch = async (value) =>{
    const result = await getInventoriDevicesFil('query='+value)
    this.setState({      
      search:value
    })
    this.setData(result.response)
  }


  editInventory = (device) => {
    this.props.history.push({pathname:'/admin/edit-inventory', state: {device: device}})
  }

  setData(data) {
    let devices = data.Devices ? data.Devices : []
    devices.sort((a,b)=>(a.id_device > b.id_device) ? 1 : ((b.id_device > a.id_device) ? -1 : 0))
    this.setState({
      is_loading: false,
      Devices: devices,
      first: data.first,
      previous: data.previous,
      next: data.next,
      last: data.last,
      total: data.total,
      index: data.index,
    })
  }

  transferOwn = (device) => {
    this.setState({curDevice: device, transferModal: true})
  }

  reassignOperator = (device) => {
    this.setState({curDevice: device, assignModal: true})
  }

  showUrl = (device) => {
    this.setState({curDevice: device, urlModal: true})
  }

  onCloseUrlModal = () => {
    this.setState({urlModal: false})
  }

  renderDevices() {
    const { Devices } = this.state        
    const type = localStorage.getItem('role_type')
    if(type.toString() === '1'){      
      return Devices.map((device, index) => {
        return (
          <tr key={index}>
            <td>{device.id_device}</td>
            <td>{device.serial}</td>
            <td><Moment format="MMM DD, YYYY h:mm A">{device.created_at}</Moment></td>
            <td>
              <div className="action">
                <div onClick={()=>{this.showUrl(device)}}>
                  <img alt="" src={require("assets/img/icon_show_password.svg")}/>
                </div>                
                <div onClick={()=> toast.success('Copy to Clipboard', {position: toast.POSITION_TOP_RIGHT})}>
                  <CopyToClipboard text={device.qr_code}>
                    <img alt="" src={require("assets/img/icon_copy.svg")}/>
                  </CopyToClipboard>
                </div>
              </div>
            </td>
            <td>{device.device_type}</td>
            <td>{device.owner}</td>
            <td>{device.operator}</td>
            <td>
              <div className="action">
                <div id={`editDe${index}`} onClick={()=>{this.editInventory(device)}}>
                  <img alt="" src={require("assets/img/icon_edit.svg")}/>
                </div>
                <UncontrolledTooltip
                  delay={0}
                  placement="top"
                  target={`editDe${index}`}
                >
                  Edit Device
                </UncontrolledTooltip>
                <div id={`deleteDe${index}`} onClick={()=>{this.deleteDevice(device.id_device)}}>
                  <img alt="" src={require("assets/img/icon_delete.svg")}/>
                </div>
                <UncontrolledTooltip
                  delay={0}
                  placement="top"
                  target={`deleteDe${index}`}
                >
                  Delete Device
                </UncontrolledTooltip>
                <div id={`transferOw${index}`} onClick={()=>{this.transferOwn(device)}}>
                  <img alt="" src={require("assets/img/icon_transfer.svg")}/>
                </div>
                <UncontrolledTooltip
                  delay={0}
                  placement="top"
                  target={`transferOw${index}`}
                >
                  Transfer Ownership
                </UncontrolledTooltip>
                <div id={`transferOp${index}`} onClick={()=>{this.reassignOperator(device)}}>
                  <img alt="" src={require("assets/img/icon_reassign.svg")}/>
                </div>
                <UncontrolledTooltip
                  delay={0}
                  placement="top"
                  target={`transferOp${index}`}
                >
                  Assign Operator
                </UncontrolledTooltip>
              </div>
            </td>
          </tr>
        );
      });
    }else{      
      return Devices.map((device, index) => {      
        return (
          <tr key={index}>
            <td>{device.id_device}</td>
            <td>{device.serial}</td>
            <td><Moment format="MMM DD, YYYY h:mm A">{device.created_at}</Moment></td>
            <td>
              <div className="action">
                <div onClick={()=>{this.showUrl(device)}}>
                  <img alt="" src={require("assets/img/icon_show_password.svg")}/>
                </div>
                <div><CopyToClipboard text={device.qr_code}>
                  <img alt="" src={require("assets/img/icon_copy.svg")}/>
                </CopyToClipboard></div>
              </div>
            </td>
            <td>{device.device_type}</td>
            <td>{device.owner}</td>
            <td>{device.operator}</td>
            <td>- - - -</td>
          </tr>
        );
      });
    }
    
  }

  startDateChanged = (e) => {
    this.setState({filter_start: e.format('YYYY-MM-DD')});
  }

  endDateChanged = (e) => {
    this.setState({filter_end: e.format('YYYY-MM-DD')});
  }

  onCloseConfirmModal = () => {
    this.setState({confirmModal: false})
  }

  onCloseTransferModal = () => {
    this.setState({transferModal: false})
  }

  onCloseAssignModal = () => {
    this.setState({assignModal: false})
  }

  changeAssign = async (id_operator, operator) => {
    let devices = []
    // devices.push({id_device_type: 1, serial: this.state.curDevice.serial})
    devices.push(this.state.curDevice.serial)
    const res = await operatorAssignDevices({id_operator: id_operator, devices: devices})
    if (res.response.status !== 200) toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    else {
      toast.success(res.response.message, {position: toast.POSITION_TOP_RIGHT})
      let newDevices = Object.assign([], this.state.Devices)
      const index = this.state.Devices.findIndex(status => status.id_device === this.state.curDevice.id_device)
      newDevices[index].operator = operator
      this.setState({assignModal: false, Devices: newDevices})
    }
  }

  changeTransfer = async (uuid, owner) => {
    let devices = []
    devices.push({id_device_type: 1, serial: this.state.curDevice.serial})
    const res = await ownerAssignDevices({uuid: uuid, devices: devices})
    if (res.response.status !== 200) toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    else {
      toast.success(res.response.message, {position: toast.POSITION_TOP_RIGHT})
      let newDevices = Object.assign([], this.state.Devices)
      const index = this.state.Devices.findIndex(status => status.id_device === this.state.curDevice.id_device)
      newDevices[index].owner = owner
      this.setState({transferModal: false, Devices: newDevices})
    }
  }

  deleteDevice = (id_device) => {
    this.setState({
      alert: (
        <SweetAlert
          warning
          showCancel
          confirmBtnText="Yes, delete it!"
          title="Are you sure to delete this device?"
          onConfirm={this.deleteDeviceAlert.bind(this, id_device)}
          onCancel={this.closeAlert}
        >
        This action can not be undone
        </SweetAlert>
      )
    });
  }

  closeAlert = () =>{
    this.setState({alert:null})
  }

  deleteDeviceAlert = (id_device) => {
    this.setState({curDevice: id_device, confirmModal: true, alert: null})
  }

  authenticate = async (res) => {
    let newDevices = this.state.Devices
    if (res.response.status !== 200)
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    else {
      const result = await deleteDeviceAdmin({id_device: this.state.curDevice})
      if (result.response.status !== 200)
        toast.error(result.response.message, {position: toast.POSITION_TOP_RIGHT})
      else {
        const index = newDevices.findIndex(device => device.id_device === this.state.curDevice)
        newDevices.splice(index, 1)
        this.setState({
          alert: (
            <SweetAlert success title="device has been deleted!" onConfirm={this.closeAlert}>        
            </SweetAlert>
          ),
          confirmModal: false, Devices: newDevices, curUuid: null, total: this.state.total-1
        })
      }
    }
  }

  sortData = (type) => {
    let Devices = Object.assign([], this.state.Devices)
    switch(type) {
      case "id":
        if (this.state.sort_direction_id) {
          Devices.sort((a,b)=>(a.id_device > b.id_device) ? 1 : ((b.id_device > a.id_device) ? -1 : 0))
        } else {
          Devices.sort((a,b)=>(a.id_device < b.id_device) ? 1 : ((b.id_device < a.id_device) ? -1 : 0))
        }
        this.setState({sort_direction_id: !this.state.sort_direction_id, Devices: Devices})
        break;
      case "serial":
        if (this.state.sort_direction_serial) {
          Devices.sort((a,b)=>(a.serial > b.serial) ? 1 : ((b.serial > a.serial) ? -1 : 0))
        } else {
          Devices.sort((a,b)=>(a.serial < b.serial) ? 1 : ((b.serial < a.serial) ? -1 : 0))
        }
        this.setState({sort_direction_serial: !this.state.sort_direction_serial, Devices: Devices})
        break;
      case "date":
        if (this.state.sort_direction_date) {
          Devices.sort((a,b)=>(a.created_at > b.created_at) ? 1 : ((b.created_at > a.created_at) ? -1 : 0))
        } else {
          Devices.sort((a,b)=>(a.created_at < b.created_at) ? 1 : ((b.created_at < a.created_at) ? -1 : 0))
        }
        this.setState({sort_direction_date: !this.state.sort_direction_date, Devices: Devices})
        break;
      case "owner":
        if (this.state.sort_direction_owner) {
          Devices.sort((a,b)=>(a.owner > b.owner) ? 1 : ((b.owner > a.owner) ? -1 : 0))
        } else {
          Devices.sort((a,b)=>(a.owner < b.owner) ? 1 : ((b.owner < a.owner) ? -1 : 0))
        }
        this.setState({sort_direction_owner: !this.state.sort_direction_owner, Devices: Devices})
        break;
      case "operator":
        if (this.state.sort_direction_operator) {
          Devices.sort((a,b)=>(a.operator > b.operator) ? 1 : ((b.operator > a.operator) ? -1 : 0))
        } else {
          Devices.sort((a,b)=>(a.operator < b.operator) ? 1 : ((b.operator < a.operator) ? -1 : 0))
        }
        this.setState({sort_direction_operator: !this.state.sort_direction_operator, Devices: Devices})
        break;
      default:
        break;
    }
  }

  render() {
    const disableStyle = {
      color: '#9a9a9a'
    }
    const { create_date, filter_start, filter_end,
      first, next, previous, last, index, total, Devices } = this.state
      const {childrens} = this.props
    return (
      <React.Fragment>
        <ToastContainer />
        <ResponsiveModal open={this.state.is_loading} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LoadingModal title="Loading..."/>
        </ResponsiveModal>
        {this.state.alert}
        <div className="content" id="admin-inventory">
          <Row>
            <Col md="12">
              <CardHeader className="management-header">
                <Col md='4'>
                  <CardTitle tag="h4">Admin Inventory</CardTitle>
                </Col>
                <Col md='4' className='funtionSpace'>
                  {childrens.indexOf(10)>=0?
                  <Link to='/admin/add-inventory'>
                    <button className='btn-add btn-cosmic'>
                      <i className="tim-icons icon_add" />
                      <span>Add Vehicles</span>
                    </button> 
                  </Link>:null}
                </Col>                                
                <Col md='4'className='filterSpace'> 
                </Col>
              </CardHeader>
            </Col>            
            <Col md="12">
              <Card>
                <CardBody>
                  <Table>
                    <thead className="text-primary">
                      <tr>
                        <th onClick={()=>{this.sortData('id')}}>ID Device <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('serial')}}>Serial Device <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('date')}}>Created Date <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th>URL</th>
                        <th onClick={()=>{this.sortData('device')}}>Device Type <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('owner')}}>Owner <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('operator')}}>Operator <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody className="management-body">{this.renderDevices()}</tbody>
                  </Table>
                  {Devices.length === 0 && <div className="no-result">No results found</div>}
                  {Devices.length > 0 &&
                  <div className="nav-group">
                    <div><span>Showing</span></div>
                    <div className="first" style={first===null?disableStyle:{}} onClick={this.first}><i className="fa fa-angle-double-left"></i></div>
                    <div className="previous" style={previous===null?disableStyle:{}} onClick={this.previous}><i className="fa fa-angle-left"></i></div>
                    <div className="index"><span>{index}</span></div>
                    <div><span>{'to'}</span></div>
                    <div className="count"><span>{Devices.length === 0 ? 0 : index + Devices.length - 1}</span></div>
                    <div><span>{'of'}</span></div>
                    <div className="total"><span>{new Intl.NumberFormat().format(total)}</span></div>
                    <div className="next" style={next===null?disableStyle:{}} onClick={this.next}><i className="fa fa-angle-right"></i></div>
                    <div className="last" style={last===null?disableStyle:{}} onClick={this.last}><i className="fa fa-angle-double-right"></i></div>
                  </div>
                  }
                </CardBody>
              </Card>
            </Col>
          </Row>
          <ResponsiveModal open={this.state.confirmModal} onClose={this.onCloseConfirmModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
            <ConfirmModal onClose={this.onCloseConfirmModal} onAuthenticate={this.authenticate}/>
          </ResponsiveModal>

          <ResponsiveModal open={this.state.transferModal} onClose={this.onCloseTransferModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
            <OperatorModal device={this.state.curDevice} onSubmit={this.changeTransfer} onClose={this.onCloseTransferModal} title="Transfer Property" submitBtn="Transfer"/>
          </ResponsiveModal>

          <ResponsiveModal open={this.state.assignModal} onClose={this.onCloseAssignModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
            <OperatorModal device={this.state.curDevice} onSubmit={this.changeAssign} onClose={this.onCloseAssignModal} title="Reassign Operator" submitBtn="Reassign"/>
          </ResponsiveModal>
          <ResponsiveModal open={this.state.urlModal} onClose={this.onCloseUrlModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
            <UrlModal device={this.state.curDevice} onClose={this.onCloseUrlModal}/>
          </ResponsiveModal>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminInventory;
