import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify'
import './modal.scss'
import {
  Button,
  FormGroup,
  Input,
  Row,
  Col,
} from 'reactstrap';

import {CopyToClipboard} from 'react-copy-to-clipboard';

class UrlModal extends Component {

  render() {
    const { onClose, device } = this.props
    return (
      <div className="change_status show_url">
        <ToastContainer />
        <div className="change-title">
          <span className="title">{device.id_device}</span>
        </div>
        <div className="input_group">
          <Row>
            <Col>
              <FormGroup>
                <Input
                  name="qr_code"
                  type="text"
                  value={device.qr_code}
                  disabled
                />
              </FormGroup>
            </Col>
          </Row>
        </div>
        <Row className='cosmic-butons butonsUpdateOpe'>
          <CopyToClipboard text={device.qr_code}><Button className="cosmic-cancel" onClick={()=>toast.success('Copy to Clipboard', {position: toast.POSITION_TOP_RIGHT})}>Copy to clipboard</Button></CopyToClipboard>
          <Button className="cosmic-confirm" onClick={onClose}>Done</Button>
        </Row>
      </div>
    )
  }
}

export default UrlModal