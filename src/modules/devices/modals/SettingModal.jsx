import React, { Component } from 'react';
import './ModalSettings.scss'
import {
  Button,
  FormGroup,
  Input,
  Row,
} from 'reactstrap';

class SettingModal extends Component {

  constructor(props) {
    super(props)
    this.state = {
      heartbeat: 0,
      low_speed: 0,
      medium_speed: 0,
      high_speed: 0,
    }
  }

  applyChange = () => {
    let data = {
      serial: this.props.device.serial,
      heartbeat: this.state.heartbeat,
      low_speed: this.state.low_speed,
      medium_speed: this.state.medium_speed,
      high_speed: this.state.high_speed,
    }
    this.props.onApplyChanges(data)
  }
  defaultSet = ()=>{
    let data = {
      serial: this.props.device.serial,
      heartbeat: '120',
      low_speed: '15',
      medium_speed: '18',
      high_speed: '22',
    }
    this.props.onApplyChanges(data)
  }

  changeValue = (e) => {
    let new_state = {}
    new_state[e.target.name] = e.target.value
    this.setState(new_state)
  }

  render() {
    const { onClose, device} = this.props
    const { heartbeat, low_speed, medium_speed, high_speed } = this.state
    return (
      <div className="default_setting" id="device_setting">
        <div className="change-title">
          <span className="title">{'Settings'}</span>
        </div>
        <div className="input_group">
          <div className="group_input">
            <Row>
              <span className="input-label">ID Scooter</span>
            </Row>
            <Row>
              <FormGroup className="input-setting">
                <Input
                  name="id"
                  type="text"
                  value={device.id_device}
                  disabled
                />
              </FormGroup>
            </Row>

            <Row>
              <span className="input-label">Heartbeat</span>
            </Row>
            <Row>
              <FormGroup className="input-setting">
                <Input
                  name="heartbeat"
                  type="number"
                  value={heartbeat}
                  onChange={this.changeValue}
                />
              </FormGroup>
            </Row>

            <Row>
              <span className="input-label">Low Speed</span>
            </Row>
            <Row>
              <FormGroup className="input-setting">
                <Input
                  name="low_speed"
                  type="number"
                  value={low_speed}
                  onChange={this.changeValue}
                />
              </FormGroup>
            </Row>

            <Row>
              <span className="input-label">Medium Speed</span>
            </Row>
            <Row>
              <FormGroup className="input-setting">
                <Input
                  name="medium_speed"
                  type="number"
                  value={medium_speed}
                  onChange={this.changeValue}
                />
              </FormGroup>
            </Row>

            <Row>
              <span className="input-label">High Speed</span>
            </Row>
            <Row>
              <FormGroup className="input-setting">
                <Input
                  name="high_speed"
                  type="number"
                  value={high_speed}
                  onChange={this.changeValue}
                />
              </FormGroup>
            </Row>

          </div>
        </div>
        <Row className="require-action">
          <div>{'This action requires additional privileges'}</div>
        </Row>
        <Row className='cosmic-butons buttonsSetting'>
          <Button className="cosmic-confirm" onClick={this.defaultSet}>Set default settings</Button>
          <div>
            <Button className="cosmic-cancel" onClick={onClose}>Cancel</Button>
            <Button className="cosmic-confirm" onClick={this.applyChange}>Apply changes</Button>
          </div>
        </Row>
      </div>
    )
  }
}

export default SettingModal