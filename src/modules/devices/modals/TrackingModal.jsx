import React, { Component } from 'react';
import './modal.scss'
import {
  Button,
  Label,
  FormGroup,
  Input,
  Row,
  Col,
} from 'reactstrap';
import { deviceTracking } from '../../../api/ScooterApi';
const seconds = [
  '5',
  '10',
  '15',
  '20',
  '25',
  '60',
  '120',
]
class TrackingModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      second: '5'
    }
    this.change = this.change.bind(this)
    this.setTracking = this.setTracking.bind(this)
  }

  change(e) {
    this.setState({second: e.target.value})
  }
  async setTracking() {
    const res = await deviceTracking({serial: this.props.serial, interval: this.state.second})
    this.props.updateTracking(res.response)
  }
  render() {
    const { onClose, deviceId} = this.props
    return (
      <div className="change_status">
        <div className="change-title">
          <span className="title">{'Change status'}</span>
        </div>
        <div className="input_group">
          <Row>
            <Label className="id-label">ID device</Label>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Input
                  name="id"
                  type="text"
                  value={deviceId}
                  disabled
                />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Label className="status-label">Seconds</Label>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Input
                  name="status"
                  type="select"
                  value={this.state.second}
                  className={'status_in_use'}
                  onChange={this.change}
                >
                {seconds.map((second, index) => <option key={index} value={second} className={'status_in_use'}>{second}</option>)}
                </Input>
              </FormGroup>
            </Col>
          </Row>
        </div>
        <Row className='cosmic-butons butonsUpdateOpe'>
          <Button className="cosmic-cancel" onClick={onClose}>Cancel</Button>
          <Button className="cosmic-confirm" onClick={this.setTracking}>Set tracking</Button>
        </Row>
      </div>
    )
  }
}

export default TrackingModal