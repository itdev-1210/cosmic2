import React, { Component } from 'react';
import './modal.scss'
import {
  Button,
  Label,
  FormGroup,
  Input,
  Row,
  Col,
} from 'reactstrap';

class StatusModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      deviceIdStatus: this.props.deviceIdStatus
    }
    this.change = this.change.bind(this)
    this.changeStatus = this.changeStatus.bind(this)
  }

  change(e) {
    this.setState({deviceIdStatus: e.target.value})
  }
  async changeStatus() {
    this.props.updateData({id_device:this.props.deviceId, id_status_device: this.state.deviceIdStatus})
  }
  render() {
    const { onClose, deviceId, status} = this.props
    const { deviceIdStatus } = this.state
    return (
      <div className="change_status">
        <div className="change-title">
          <span className="title">{'Change status'}</span>
        </div>
        <div className="input_group">
          <Row>
            <Label className="id-label">ID device</Label>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Input
                  name="id"
                  type="text"
                  value={deviceId}
                  disabled
                />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Label className="status-label">Status</Label>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Input
                  name="status"
                  type="select"
                  value={deviceIdStatus}
                  className={deviceIdStatus.toString() === '2' ? 'status_in_use' :
                      deviceIdStatus.toString() === '1' ? 'status_available' :
                      deviceIdStatus.toString() === '6' ? 'status_unavailable' :
                      deviceIdStatus.toString() === '3' ? 'status_transport' :
                      deviceIdStatus.toString() === '7' ? 'status_maintenance' :
                      deviceIdStatus.toString() === '4' ? 'status_broken' :
                      deviceIdStatus.toString() === '5' ? 'status_stolen' :
                      deviceIdStatus.toString() === '8' ? 'status_logistic' : ''}
                      onChange={this.change}
                >
                {status.map((status, index) => {
                  if(status.id_status_device !== 2){
                    return (
                      <option key={index} value={status.id_status_device} className={
                        // status.id_status_device === 2 ? 'status_in_use' :
                        status.id_status_device === 1 ? 'status_available' :
                        status.id_status_device === 6 ? 'status_unavailable' :
                        status.id_status_device === 3 ? 'status_transport' :
                        status.id_status_device === 7 ? 'status_maintenance' :
                        status.id_status_device === 4 ? 'status_broken' :
                        status.id_status_device === 5 ? 'status_stolen' :
                        status.id_status_device === 8 ? 'status_logistic' : ''}>{status.status}
                      </option>
                    )
                  }
                })}
                </Input>
              </FormGroup>
            </Col>
          </Row>
        </div>
        <Row className='cosmic-butons butonsUpdateOpe'>
          <Button className="cosmic-cancel" onClick={onClose}>Cancel</Button>
          <Button className="cosmic-confirm" onClick={this.changeStatus}>Change status</Button>
        </Row>
      </div>
    )
  }
}

export default StatusModal