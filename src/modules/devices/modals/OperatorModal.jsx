import React, { Component } from 'react';
import './modal.scss'
import {
  Button,
  Label,
  FormGroup,
  Input,
  Row,
  Col,
} from 'reactstrap';
import { getOperators } from '../../../api/ScooterApi';
import { getOwnersAdmin } from '../../../api/OwnerApi'

class OperatorModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user_id: '-1',
      operators: [],
      owners: [],
      name: '',
      // operator: this.props.scooter.operator
    }
    this.change = this.change.bind(this)
  }

  componentWillMount() {
    this.getOperators()
  }

  getOperators = async () => {
    if (this.props.submitBtn === "Transfer") {
      const res = await getOwnersAdmin()
      if (res.response.status === 200) {
        let owners = res.response.Owners;        
        this.setState({operators: owners})
      }
    } else {
      const res = await getOperators()
      if (res.status === 200 && res.st) {
        let operators = res.Operators;                
        this.setState({operators: operators})
      }
    }
  }

  change(e) {
    let name = ''
    if (this.props.submitBtn === "Transfer") {
      const index = this.state.operators.findIndex(operator => operator.uuid === e.target.value)
      if (index !== -1) name = this.state.operators[index].name
    } else {
      const index = this.state.operators.findIndex(operator => operator.id_operator === e.target.value)
      if (index !== -1) name = this.state.operators[index].name
    }
    this.setState({user_id: e.target.value, name: name})
  }

  onSubmit = () => {
    if (this.state.user_id.toString() === '-1') return;
    this.props.onSubmit(this.state.user_id, this.state.name)
  }

  render() {
    const { onClose, device, title, submitBtn} = this.props
    const { user_id, operators } = this.state
    return (
      <div className="change_status">
        <div className="change-title">
          <span className="title">{title}</span>
        </div>
        <div className="input_group">
          <Row>
            <Label className="id-label">ID Device</Label>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Input
                  name="id"
                  type="text"
                  value={device.id_device}
                  disabled
                />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Label className="status-label">Status</Label>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Input
                  name="status"
                  type="select"
                  value={user_id}
                  onChange={this.change}
                >
                {this.props.submitBtn==='Transfer'?<option key={0} value={''}>--Select Owner--</option>:<option key={0} value={''}>--Select Operator--</option>}
                {operators.map((operator, index) => <option key={index} value={this.props.submitBtn === 'Transfer' ? operator.uuid : operator.id_operator}>{operator.name}</option>)}
                </Input>
              </FormGroup>
            </Col>
          </Row>
        </div>
        <Row className='cosmic-butons butonsUpdateOpe'>
          <Button className="cosmic-cancel" onClick={onClose}>Cancel</Button>
          <Button className="cosmic-confirm" onClick={this.onSubmit}>{submitBtn}</Button>
        </Row>
      </div>
    )
  }
}

export default OperatorModal