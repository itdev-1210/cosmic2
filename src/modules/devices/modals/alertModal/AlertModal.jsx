import React, { Component } from 'react';
import './alert.scss'

class AlertModal extends Component {
  render() {
    const { status, error } = this.props
    return (
      <div className="alert-body">
        <div className="close-button">
          <div onClick={() => this.props.onClose()}>&#10005;</div>
        </div>
        <div className="status-icon">
          <img alt="" src={status===200 ? require("assets/img/icon_check.svg") : require("assets/img/icon-fail.svg")}/>
        </div>
        <div className="bottom-border"><div></div></div>
        <div className="alert-description">
          {error}
        </div>
      </div>
    )
  }
}

export default AlertModal