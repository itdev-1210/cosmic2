import React, { Component } from 'react';
import './modal.scss'
import {
  Button,
  FormGroup,
  Input,
  Row,
} from 'reactstrap';

class DefaultSettingModal extends Component {
  render() {
    const { onClose, onSetDefault, deviceId} = this.props
    return (
      <div className="default_setting">
        <div className="change-title">
          <span className="title">{'Set default settings'}</span>
        </div>
        <div className="input_group">
          <div className="group_input">
            <Row>
              <span className="id-label">ID Device</span>
            </Row>
            <Row>
              <div className="device_id">
                <FormGroup>
                  <Input
                    name="id"
                    type="text"
                    value={deviceId}
                    disabled
                  />
                </FormGroup>
              </div>
            </Row>
          </div>
        </div>
        <Row className="description">
          <div>{'Do you want to set the default settings for this device?'}</div>
          <div>{'This action is can not be undone'}</div>
        </Row>
        <Row className="require-action">
          <div>{'This action requires additional privileges'}</div>
        </Row>
        <Row className='cosmic-butons butonsUpdateOpe'>
          <Button className="cosmic-cancel" onClick={onClose}>Cancel</Button>
          <Button className="cosmic-confirm" onClick={onSetDefault}>Set default</Button>
        </Row>
      </div>
    )
  }
}

export default DefaultSettingModal