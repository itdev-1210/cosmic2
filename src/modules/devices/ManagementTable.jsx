import React from 'react';
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import moment from 'moment'
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  DropdownToggle,
  DropdownMenu,
  UncontrolledDropdown,
  Table,
  Modal,
  ModalBody,
  Row,
  Col,
} from 'reactstrap';
import {getNav, scooterLock, scooterUnlock, scooterSetting, scooterInstructions, changeStatus,changeBulkStatus, getControlDevices,getControlDevicesFil } from '../../api/ScooterApi';
import {emitter, subscribe,unSubscribe } from '../../utils/EventComponents';
import './ManagementTable.scss'
import ResponsiveModal from 'react-responsive-modal';
import StatusModal from './modals/StatusModal'
import TrackingModal from './modals/TrackingModal'
// import DefaultSettingModal from './modals/DefaultSettingModal'
import SettingModal from './modals/SettingModal'
// import ConfirmModal from '../../views/pages/ConfirmModal'
import LoadingModal from './../../components/Navbars/LogoutModal'
import LoaderModal from '../../components/Cosmic/LoaderModal/LoaderModal'

import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import { object } from 'prop-types';

const defaultMapOptions = {
  disableDefaultUI: true
}
const RegularMap = withScriptjs(
  withGoogleMap((props) => {
    return (
      <GoogleMap
        defaultZoom={16}
        defaultCenter={{ lat: props.lat, lng: props.lon }}
        defaultOptions={defaultMapOptions}
      >
        <Marker position={{ lat: props.lat, lng: props.lon }} 
          icon={props.status === 'In use' ? require("assets/img/mark-scooter-inuse.svg") :
            props.status === 'Available' ? require("assets/img/mark-scooter-available.svg") :
            props.status === 'Unavailable' ? require("assets/img/mark-scooter-inactive.svg") :
            props.status === 'In transport' ? require("assets/img/mark-scooter-transport.svg") :
            props.status === 'Maintenance' ? require("assets/img/mark-scooter-maintenance.svg") :
            props.status === 'Broken' ? require("assets/img/mark-scooter-broken.svg") :
            props.status === 'Stolen' ? require("assets/img/mark-scooter-stolen.svg") :
            props.status === 'Logistics user' ? require("assets/img/mark-scooter-inlogistic.svg") :''}/>
      </GoogleMap>
    );
  })
);

class ManagementTable extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      arrayBulk:[],
      search:'',
      loader:true,
      devices: [],
      first: null,
      previous: null,
      next: null,
      last: null,
      total: 0,
      index: 0,
      status: [],
      inUse: false,
      available: false,
      unavailable: false,
      transport: false,
      maintenance: false,
      broken: false,
      stolen: false,
      statusModal: false,
      trackingModal: false,
      defaultSettingModal: false,
      confirmModal: false,
      settingModal: false,
      curDevice: null,
      curId: null,
      curSerial: null,
      curStatusId: null,
      selId: null,
      deviceStatus: null,

      sort_direction_id: false,
      sort_direction_imei: true,
      sort_direction_status: true,
    }
    this.renderModalPosition = this.renderModalPosition.bind(this)
    this.renderDevices = this.renderDevices.bind(this)
    this.toggleModalPosition = this.toggleModalPosition.bind(this)
    this.setData = this.setData.bind(this)
    this.fetchData = this.fetchData.bind(this)
    this.filterCheck = this.filterCheck.bind(this)
    this.clearFilter = this.clearFilter.bind(this)
    this.doneFilter = this.doneFilter.bind(this)
    this.first = this.first.bind(this)
    this.previous = this.previous.bind(this)
    this.next = this.next.bind(this)
    this.last = this.last.bind(this)
    this.setParam = this.setParam.bind(this)    
    this.setTracking = this.setTracking.bind(this)
    // this.setDefaultSetting = this.setDefaultSetting.bind(this)
    // this.onSetDefault = this.onSetDefault.bind(this)
    this.updateData = this.updateData.bind(this)
    this.lockScooter = this.lockScooter.bind(this)
    this.unlockScooter = this.unlockScooter.bind(this)
    this.updateTracking = this.updateTracking.bind(this)
    this.authenticate = this.authenticate.bind(this)
  }

  async lockScooter(serial) {
    this.setState({loader: true}, async()=>{
      const res = await scooterLock({serial: serial})
      if (res.response.status !== 200) {
        toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
        this.setState({loader: false})
      }
      else {
        toast.success(res.response.message, {position: toast.POSITION_TOP_RIGHT})
        this.setState({loader: false})
      }
    })
  }
  async unlockScooter(serial) {
    this.setState({loader: true}, async()=>{
      const res = await scooterUnlock({serial: serial})
      if (res.response.status !== 200) {
        toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
        this.setState({loader: false})
      }
      else {
        toast.success(res.response.message, {position: toast.POSITION_TOP_RIGHT})
        this.setState({loader: false})
      }
    })
  }
  updateTracking(response) {    
    if (response.status !== 200)
      toast.error(response.message, {position: toast.POSITION_TOP_RIGHT})
    else {
      toast.success(`Set interval ${response.message.interval}`, {position: toast.POSITION_TOP_RIGHT})
    }
    this.setState({ trackingModal: false });
  }

  async updateData(data) {//change status
    this.setState({loader: true}, async()=>{
      if(typeof data.id_device === 'object'){
        // const res = await changeBulkStatus(data)
        // if (res.status !== 200) {
        //   toast.error(res.message, {position: toast.POSITION_TOP_RIGHT})
        // }
        // else {
        //   toast.success(res.message, {position: toast.POSITION_TOP_RIGHT})
        // }
        this.setState({ statusModal: false, loader: false })
        this.fetchData();
      } 
      else{
        const res = await changeStatus(data)
        var newDevices = this.state.devices 
        if (res.status !== 200) {
          toast.error(res.message, {position: toast.POSITION_TOP_RIGHT})
        }
        else {
          toast.success(res.message, {position: toast.POSITION_TOP_RIGHT})
          const new_device = res.device
          const device = this.state.devices.findIndex(device => device.id_device === new_device.id_device)
          const new_status = this.state.status.findIndex(status => status.id_status_device === new_device.id_status_device)
          newDevices[device].id_status_device = new_device.id_status_device
          newDevices[device].status = this.state.status[new_status].status
        }
        this.setState({ statusModal: false, devices: newDevices, loader: false });
      }
    })
  }

  changeStatusModal = (id, id_status_device) => {
    this.setState({statusModal: true, curId: id, curStatusId: id_status_device})
  }

  setTracking(id, serial) {
    this.setState({trackingModal: true, curId: id, curSerial: serial})
  }

  setSetting = (device) => {
    this.setState({settingModal: true, curDevice: device})
  }

  onCloseStatusModal = () => {
    this.setState({ statusModal: false });
  };

  onCloseTrackingModal = () => {
    this.setState({ trackingModal: false });
  }

  onCloseDefaultSettingModal = () => {
    this.setState({ defaultSettingModal: false });
  }

  onCloseConfirmModal = () => {
    this.setState({ confirmModal: false });
  };

  openModalSetting(device) {
    this.setState({settingModal:true, curDevice:device})
  }
  onCloseSettingModal = () => {
    this.setState({ settingModal: false});
  }
  onApplyChanges = async(data) => {
    this.setState({loader:true})
    const ins_res = await scooterInstructions({
      serial: data.serial,
      display: '2',
      crusie: '1',
      startup:'1',
      button:'1',
      ligth: '1',
      speed_low: data.low_speed,
      speed_medium: data.medium_speed,
      speed_high: data.high_speed,
    })
    if(ins_res.st){
      if (ins_res.status !== 200) toast.error(ins_res.message, {position: toast.POSITION_TOP_RIGHT})
      else{
        const set_res = await scooterSetting({
          serial:data.serial,
          acc:'2',
          heartbeat: data.heartbeat
        });
        if(set_res.st){
          if (set_res.status !== 200) toast.error(set_res.message, {position: toast.POSITION_TOP_RIGHT})
          else toast.success('Successful Settings', {position: toast.POSITION_TOP_RIGHT})
        }
        else{
          toast.error('Settings Failed', {position: toast.POSITION_TOP_RIGHT})
        }
      }
    }else{
      toast.error('Settings Failed', {position: toast.POSITION_TOP_RIGHT})
    }
    this.setState({settingModal: false, loader:false})
  }

  authenticate(res) {
    if (res.response.status !== 200)
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    else {
      this.setState({confirmModal: false})
      // Deafult Setting API
    }
  }
  filterCheck(e) {
    const name = e.target.name
    let state = {}
    state[name] = !this.state[name]
    this.setState(state)
  }

  clearFilter() {
    this.setState({
      'inUse': false,
      'available': false,
      'unavailable': false,
      'transport': false,
      'maintenance': false,
      'broken': false,
      'stolen': false,
      search: '',
    }, ()=>{
      emitter('searchBar',true)
      this.fetchData()
    })
  }

  async doneFilter() {
    var filter = 'filter='
    if (this.state.inUse)
      filter += '2,'
    if (this.state.available)
      filter += '1,'
    if (this.state.unavailable)
      filter += '6,'
    if (this.state.transport)
      filter += '3,'
    if (this.state.maintenance)
      filter += '7,'
    if (this.state.broken)
      filter += '4,'
    if (this.state.stolen)
      filter += '5,'
    if (filter === 'filter=')
      return;
    const result = await getControlDevicesFil(filter.substr(0, filter.length-1))
    this.setData(result.response)
    this.setState({search:''},emitter('searchBar',true))
  }

  setParam() {
    var filter = 'filter='
    if (this.state.inUse) filter += '2,'
    if (this.state.available) filter += '1,'
    if (this.state.unavailable) filter += '6,'
    if (this.state.transport) filter += '3,'
    if (this.state.maintenance) filter += '7,'
    if (this.state.broken) filter += '4,'
    if (this.state.stolen) filter += '5,'
    if (filter === 'filter=') filter=''
    if (this.state.search!=='') filter = '&query='+this.state.search  
    return filter.substr(0, filter.length-1)
  }

  async fetchData() {
    const result = await getControlDevices();
    this.setData(result.response);
  }

  async first() {
    if (this.state.first === null)
      return

    const result = await getNav(this.state.first + this.setParam())
    this.setData(result.response)
  }

  async previous() {
    if (this.state.previous === null)
      return
    const result = await getNav(this.state.previous + this.setParam())
    this.setData(result.response)
  }

  async next() {
    if (this.state.next === null)
      return
    const result = await getNav(this.state.next + this.setParam())
    this.setData(result.response)
  }

  async last() {
    if (this.state.last === null)
      return
    const result = await getNav(this.state.last + this.setParam())
    this.setData(result.response)
  }

  componentDidMount() {
    subscribe('search',(value)=> this.dinamycSearch(value))
    emitter('searchBar',true)
    this.fetchData()
  }
  componentWillUnmount =() => {
    unSubscribe('search')
    emitter('searchBar',false)
  }
  dinamycSearch = async (value) =>{
    const result = await getControlDevicesFil('query='+value)
    this.setState({
      inUse: false,
      available: false,
      unavailable: false,
      transport: false,
      maintenance: false,
      broken: false,
      stolen: false,
      search:value
    })
    this.setData(result.response)
  }

  setData(data) {    
    let devices = data.Devices ? data.Devices : []
    devices.sort((a,b)=>(a.id_device > b.id_device) ? 1 : ((b.id_device > a.id_device) ? -1 : 0))
    this.setState({
      loader: false,
      devices: devices,
      first: data.first,
      previous: data.previous,
      next: data.next,
      last: data.last,
      total: data.total,
      index: data.index,
      status: data.Status ? data.Status : [],
    })
  }

  toggleModalPosition(lat, lon, selId, status) {
    this.setState({ lat: parseFloat(lat), lon: parseFloat(lon), selId: selId, modalPosition: !this.state.modalPosition, deviceStatus: status });
  }

  renderModalPosition() {
    return (
      <Modal isOpen={this.state.modalPosition} toggle={this.toggleModalPosition}>
        <div className="modal-header">
          <button aria-hidden={true} className="close" data-dismiss="modal" type="button" onClick={this.toggleModalPosition}>
            <i className="tim-icons icon-simple-remove" />
          </button>
          <h6 className="title title-up">{'Position | ' + this.state.selId}</h6>
        </div>
        <ModalBody className="text-center">
          <div className="map" id="regularMap">
            <RegularMap
              lat={this.state.lat}
              lon={this.state.lon}
              status={this.state.deviceStatus}
              googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDASQ1gDtuxVXsoeJZHXqsNRASQ17yVoEI"
              loadingElement={<div style={{ height: `100%` }} />}
              containerElement={
                <div
                  style={{
                    height: `500px`,
                    borderRadius: `.2857rem`,
                    overflow: `hidden`
                  }}
                />
              }
              mapElement={<div style={{ height: `100%` }} />}
            />
          </div>
        </ModalBody>
      </Modal>
    );
  }

  sortData = (type) => {
    let Devices = Object.assign([], this.state.devices)
    switch(type) {
      case "id":
        if (this.state.sort_direction_id) {
          Devices.sort((a,b)=>(a.id_device > b.id_device) ? 1 : ((b.id_device > a.id_device) ? -1 : 0))
        } else {
          Devices.sort((a,b)=>(a.id_device < b.id_device) ? 1 : ((b.id_device < a.id_device) ? -1 : 0))
        }
        this.setState({sort_direction_id: !this.state.sort_direction_id, devices: Devices})
        break;
      case "imei":
        if (this.state.sort_direction_imei) {
          Devices.sort((a,b)=>(a.serial > b.serial) ? 1 : ((b.serial > a.serial) ? -1 : 0))
        } else {
          Devices.sort((a,b)=>(a.serial < b.serial) ? 1 : ((b.serial < a.serial) ? -1 : 0))
        }
        this.setState({sort_direction_imei: !this.state.sort_direction_imei, devices: Devices})
        break;
      case "status":
        if (this.state.sort_direction_status) {
          Devices.sort((a,b)=>(a.status > b.status) ? 1 : ((b.status > a.status) ? -1 : 0))
        } else {
          Devices.sort((a,b)=>(a.status < b.status) ? 1 : ((b.status < a.status) ? -1 : 0))
        }
        this.setState({sort_direction_status: !this.state.sort_direction_status, devices: Devices})
        break;
      default:
        break;
    }
  }

  checkBox = async (e)=>{
    const {arrayBulk,devices} = this.state
    let newArrayBulk = arrayBulk
    if(e.target.checked){
      if(e.target.id==='0'){
        newArrayBulk = await  devices.map(device => device.id_device)
      }else newArrayBulk.push(e.target.id)
    }else{
      if(e.target.id==='0'){
        newArrayBulk = []
      }else{
        if(newArrayBulk.indexOf(e.target.id)>=0) newArrayBulk.splice(newArrayBulk.indexOf(e.target.id),1)
      }
    }
    this.setState({arrayBulk:newArrayBulk})
  }

  renderDevices() {
    const { devices, arrayBulk } = this.state
    return devices.map((device, index) => {
      return (
        <tr key={index}>
          <td className='two-step'><input id={device.id_device} type="checkbox" onChange={this.checkBox} checked={arrayBulk.indexOf(device.id_device)>=0?true:false}/></td>
          <td>{device.id_device}</td>
          <td>{device.serial}</td>
          <td>
            <div className={device.status === 'In use' ? 'in_use' :
                    device.status === 'Available' ? 'available' :
                    device.status === 'Unavailable' ? 'unavailable' :
                    device.status === 'In transport' ? 'transport' :
                    device.status === 'Maintenance' ? 'maintenance' :
                    device.status === 'Broken' ? 'broken' :
                    device.status === 'Stolen' ? 'stolen' :
                    device.status === 'Logistics user' ? 'logistic' : ''} onClick={()=>this.changeStatusModal(device.id_device, device.id_status_device)}>
              {device.status}
            </div>
          </td>
          <td>
            <div
              onClick={() => this.toggleModalPosition(device.lat, device.lon, device.id_device, device.status)}
              className="position"
            >
              <i className="tim-icons icon-gps-fixed" />
            </div>
          </td>
          <td>{moment.utc(device.updated_at).format('MMM DD, YYYY h:mm:ss A')}</td>
          <td>
            <div className="unlock">
              <div style={{cursor:'pointer'}} className="locked" onClick={()=>this.lockScooter(device.serial)}>
              <img alt="" src={require("assets/img/icon_padlock_open.svg")}/>
              </div>
              <div style={{cursor:'pointer'}} className="unlocked" onClick={()=>this.unlockScooter(device.serial)}>
              <img alt="" src={require("assets/img/icon_padlock_close.svg")}/>
              </div>
            </div>
          </td>
          <td>
            <div style={{cursor:'pointer'}} className="tracking" onClick={()=>this.setTracking(device.id_device, device.serial)}>
              <img alt="" src={require("assets/img/icon_magic_wand.svg")}/>
            </div>
          </td>
          <td>
            <div style={{cursor:'pointer'}} className="settings" onClick={()=>this.setSetting(device)}>
              <img alt="" src={require("assets/img/icon_settings_gears.svg")}/>
            </div>
          </td>
        </tr>
      );
    });
  }

  render() {
    const disableStyle = {
      color: '#9a9a9a'
    }
    const { inUse, available, unavailable, transport, maintenance, broken, stolen, 
        first, next, previous, last, index, total, devices, status, arrayBulk } = this.state
    return (
      <React.Fragment>
        {this.renderModalPosition()}
        <div className="content" id="management">
          <ResponsiveModal open={this.state.loader}
            onClose={()=>{}} center 
            classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
            <LoaderModal title="Loading..."/>
          </ResponsiveModal>   
          <Row>
            <Col md="12">
              <CardHeader className="management-header">
                <Col md='4'>
                  <CardTitle tag="h4">Control Device</CardTitle>
                </Col>
                <Col md='4' className='funtionSpace'>
                  <label htmlFor="bulkStatus">{`${arrayBulk.length} items selected`}</label>
                  <button id='bulkStatus' className='btn-add btn-cosmic' onClick={()=>this.changeStatusModal(arrayBulk,'1')}>
                    <i className="tim-icons icon_add" />
                    <span>Bulk Status</span>
                  </button>                  
                </Col>                                
                <Col md='4'className='filterSpace'> 
                  <div className="tools tools-group">
                    <UncontrolledDropdown>
                      <DropdownToggle
                        caret
                        className="btn-filter"
                        data-toggle="dropdown"
                      >
                        Filter
                      </DropdownToggle>
                      <DropdownMenu right>
                        <div className="filter-header">
                          <div className="filter-header-group">
                            <div className="filter-text">Filters</div>
                            <div className="filter-btn-group">
                              <div className="filter-clear" onClick={this.clearFilter}>Clear</div>
                              <div className="filter-done" onClick={this.doneFilter}>Done</div>
                            </div>
                          </div>
                        </div>

                        <div className="filter-check">
                          <input id="check_use" type="checkbox" name="inUse" checked={inUse} onChange={this.filterCheck}/>
                          <label htmlFor="check_use">In Use</label>
                        </div>
                        <div className="filter-check">
                          <input id="check_available" type="checkbox" name="available" checked={available} onChange={this.filterCheck}/>
                          <label htmlFor="check_available">Available</label>
                        </div>
                        <div className="filter-check">
                          <input id="check_unavailable" type="checkbox" name="unavailable" checked={unavailable} onChange={this.filterCheck}/>
                          <label htmlFor="check_unavailable">Unavailable</label>
                        </div>
                        <div className="filter-check">
                          <input id="check_transport" type="checkbox" name="transport" checked={transport} onChange={this.filterCheck}/>
                          <label htmlFor="check_transport">Transport</label>
                        </div>
                        <div className="filter-check">
                          <input id="check_maintenance" type="checkbox" name="maintenance" checked={maintenance} onChange={this.filterCheck}/>
                          <label htmlFor="check_maintenance">Maintenance</label>
                        </div>
                        <div className="filter-check">
                          <input id="check_broken" type="checkbox" name="broken" checked={broken} onChange={this.filterCheck}/>
                          <label htmlFor="check_broken">Broken</label>
                        </div>
                        <div className="filter-check">
                          <input id="check_stolen" type="checkbox" name="stolen" checked={stolen} onChange={this.filterCheck}/>
                          <label htmlFor="check_stolen">Stolen</label>
                        </div>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </div>             
                </Col>
              </CardHeader>
            </Col>
            
            <Col md="12">
              <Card>
                <CardBody>
                  <Table>
                    <thead className="text-primary">
                      <tr>
                        <th className='two-step'><input id={0} type="checkbox" onChange={this.checkBox}/></th>
                        <th onClick={()=>{this.sortData('id')}}>ID <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('imei')}}>IMEI <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('status')}}>STATUS <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th>POSITION</th>
                        <th>LAST TRANSMITION</th>
                        <th>UNLOCK</th>
                        <th>TRACKING</th>
                        <th>SETTINGS</th>
                      </tr>
                    </thead>
                    <tbody className="management-body">{this.renderDevices()}</tbody>
                  </Table>
                  {devices.length === 0 && <div className="no-result">No results found</div>}
                  {devices.length > 0 &&
                  <div className="nav-group">
                    <div><span>Showing</span></div>
                    <div className="first" style={first===null?disableStyle:{}} onClick={this.first}><i className="fa fa-angle-double-left"></i></div>
                    <div className="previous" style={previous===null?disableStyle:{}} onClick={this.previous}><i className="fa fa-angle-left"></i></div>
                    <div className="index"><span>{index}</span></div>
                    <div><span>{'to'}</span></div>
                    <div className="count"><span>{devices.length === 0 ? 0 : index + devices.length - 1}</span></div>
                    <div><span>{'of'}</span></div>
                    <div className="total"><span>{new Intl.NumberFormat().format(total)}</span></div>
                    <div className="next" style={next===null?disableStyle:{}} onClick={this.next}><i className="fa fa-angle-right"></i></div>
                    <div className="last" style={last===null?disableStyle:{}} onClick={this.last}><i className="fa fa-angle-double-right"></i></div>
                  </div>
                  }
                </CardBody>
              </Card>
            </Col>
          </Row>          
          <ResponsiveModal open={this.state.statusModal} onClose={this.onCloseStatusModal} center classNames={{'modal':'dark-modal-status'}}>
            <StatusModal history={this.props.history} onClose={this.onCloseStatusModal} 
                deviceId={this.state.curId} status={status}
                updateData={this.updateData} deviceIdStatus={this.state.curStatusId}
                />
          </ResponsiveModal>
          <ResponsiveModal open={this.state.trackingModal} onClose={this.onCloseTrackingModal} center classNames={{'modal':'dark-modal-status'}}>
            <TrackingModal history={this.props.history} onClose={this.onCloseTrackingModal} 
                deviceId={this.state.curId} serial={this.state.curSerial} updateTracking={this.updateTracking}/>
          </ResponsiveModal>
         
          <ResponsiveModal open={this.state.settingModal} onClose={this.onCloseSettingModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
            <SettingModal onClose={this.onCloseSettingModal} onApplyChanges={this.onApplyChanges} device={this.state.curDevice}/>
          </ResponsiveModal>          
          <ToastContainer />
        </div>
      </React.Fragment>
    );
  }
}
export default ManagementTable;
