import React from 'react';
import {
  Button,
  Card,
  CardBody,
  Label,
  FormGroup,
  Input,
  Row,
  CardHeader,
  CardTitle,
  Col,
} from 'reactstrap';
import ResponsiveModal from 'react-responsive-modal';
import AlertModal from '../../components/Cosmic/AlertModal/AlertModal'
import './styles/addInventory.scss'
import { editScooter } from './../../api/ScooterApi'

class EditInventory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id_device: '',
      serial: '',
      type: '0',
      version_a: '0',
      version_b: '1',
      url: '',
      error: -1,
      errMsg: '',
      showModal: false,
      userRole: true,

      operators:[],
      owners:[],
      types:[],
    };
  }
  componentDidMount() {
    this.setState({
      id_device: this.props.location.state.device.id_device,
      serial: this.props.location.state.device.serial,
      url: this.props.location.state.device.qr_code,
      // userRole: auth.checkRole('SCOOTERS', 'Admin Inventory'),
    })
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.location.state !== nextProps.location.state) {
      this.setState({
        id_device: nextProps.location.state.device.id_device,
        serial: nextProps.location.state.device.serial,
        url: nextProps.location.state.device.qr_code,
      })
    }
  }
  valueChange = (e) => {
    var obj = {}
    obj[e.target.name] = e.target.value
    this.setState(obj)
  }
  editItem = async () => {
    const res = await editScooter({id_device: this.state.id_device, serial: this.state.serial})
    this.setState({
      showModal: true, 
      error: res.response.status, 
      errMsg: res.response.status===200 ? 'Your scooter has been updated!' : 'An error has occurred while updating the scooter!'})
  }
  onCloseModal = () => {
    this.setState({showModal: false})
    if (this.state.error === 200) this.props.history.goBack()
  }
  render() {
    const { id_device, serial, type, version_a, version_b, url, userRole, operator, owner, operators, owners, types } = this.state
    return (
      <React.Fragment>
      <div className="content" id="add-inventory">        
        <Row>
          <Col md='12'>
            <CardHeader className="management-header">
              <Col md='4'>
                <CardTitle tag="h4" onClick={()=>{this.props.history.push('/admin/admin-inventory')}}>
                  <img alt="" src={require("assets/img/icon_arrow.svg")}/>Edit Scooter
                </CardTitle>
              </Col>  
            </CardHeader>            
          </Col>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="assign_field">
                  <div className="import_title">Manually Assign Fields</div>
                  <div className="import_section">
                    <Col md="6">
                      <Row>
                        <Label md="3" className="text_color">QR Field Name</Label>
                      </Row>
                      <Row>
                        <Label md="3" className="disable_btn">ID Scooter</Label>
                        <Col md="6">
                          <FormGroup>
                            <Input className="disable_btn" type="text" value={id_device} name="id_device" readOnly/>
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Label md="3" className="text_color">Serial Scooter</Label>
                        <Col md="6">
                          <FormGroup>
                            <Input type="text" value={serial} onChange={this.valueChange} name="serial" disabled={ userRole ? false : true }/>
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Label md="3" >Operator</Label>
                        <Col md="6">
                          <FormGroup>
                            <Input                              
                              name="type"
                              type="select"
                              value={operator}
                            >
                              <option value={0}>--Select operator--</option>
                              {operators.map((obj, index) => {
                                return <option key={index} value={obj.id}>{obj.name}</option>
                              })}                            
                            </Input>
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Label md="3" >Owner</Label>
                        <Col md="6">
                          <FormGroup>
                            <Input                              
                              name="type"
                              type="select"
                              value={owner}
                            >
                              <option value={0}>--Select owner--</option>
                              {owners.map((obj, index) => {
                                return <option key={index} value={obj.id}>{obj.name}</option>
                              })}                            
                            </Input>
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Label md="3" className="disable_btn">Device Type</Label>
                        <Col md="6">
                          <FormGroup>
                            <Input
                              className="disable_btn"
                              name="type"
                              type="select"
                              value={type}
                              readOnly
                            >
                              <option value={0}>--Select type--</option>
                              {types.map((obj, index) => {
                                return <option key={index} value={obj.id}>{obj.name}</option>
                              })}                            
                            </Input>
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Label md="3" className="disable_btn">Device Version</Label>
                        <Col md="3">
                          <FormGroup>
                            <Input
                              className="disable_btn"
                              name="version_a"
                              type="select"
                              value={version_a}
                              readOnly
                            >
                            <option value={version_a}>{version_a}</option>
                            </Input>
                          </FormGroup>
                        </Col>
                        <Col md="3">
                          <FormGroup>
                            <Input
                              className="disable_btn"
                              name="version_b"
                              type="select"
                              value={version_b}
                              readOnly
                            >
                            <option value={version_b}>{version_b}</option>
                            </Input>
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Label md="3" className="disable_btn">URL QR Code</Label>
                        <Col md="9">
                          <FormGroup>
                            <Input className="disable_btn" type="text" value={url} name="url" readOnly/>
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col md="4">
                          <Button className="add_new_item" onClick={this.editItem} disabled={ userRole ? false : true }>SAVE CHANGES</Button>
                        </Col>
                      </Row>
                    </Col>
                  </div>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <ResponsiveModal open={this.state.showModal} onClose={this.onCloseModal} center classNames={{'modal':'alert-modal','closeButton':'modal-close-button'}}>
          <AlertModal onClose={this.onCloseModal} status={this.state.error} msj={this.state.errMsg}/>
        </ResponsiveModal>
      </div>
    </React.Fragment>
    )
  }
}

export default EditInventory;
