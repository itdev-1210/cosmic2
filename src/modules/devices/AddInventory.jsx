import React from 'react';
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Label,
  FormGroup,
  Input,
  Row,
  Col,
} from 'reactstrap';
import './styles/addInventory.scss'

import { createScooterCsv, createScooter, getDeviceInfo,getOperators,getOwnersAdmin } from '../../api/ScooterApi'
import auth from '../../api/auth';

const versions = [
  0,1,2,3,4,5,6,7,8,9
]
class AddInventory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      enableOp:false,
      enableOw:false,
      load_csv: false,
      upload_success: false,
      id_device: '',
      serial: '',
      version_a: 0,
      version_b: 0,
      type: 1,
      types: [],
      id_operator: '',
      operators: [],
      id_owner: '',
      owners: [],
      services:[],
      service:'',
      file: null,
    };
  }
  componentDidMount() {
    this.getDeviceInfo();
  }
  componentWillUnmount() {
  }

  getDeviceInfo = async() => {
    let res = await getDeviceInfo();
    if (res.status === 200 && res.st){
      this.setState({types: res.Devices,operators:res.Operators,owners:res.Owners,services:res.Services_type})
    } 
  }

  changeValue = (e) => {
    this.setState({
      load_csv: !this.state.load_csv,
      enableOp: this.state.load_csv,
      enableOw: this.state.load_csv
    })
  }

  changeValueCheck = (e)=>{
    let obj = {}
    let name = e.target.name
    let value = e.target.checked    
    if(name==='enableOp' && !value){
      this.setState({
        enableOp:value,
        id_operator:''
      })
    }else if(name==='enableOw' && !value){
      this.setState({
        enableOw:value,
        id_owner:''
      })
    }
    else {
      obj[name]=value
      this.setState(obj)
    }    
  }

  onSave = (e) => {
    this.setState({file: e.target.files[0]})
  }
  onUpload = async () => {
    if (!this.state.file || !this.state.load_csv) {
      toast.error('Please choose file or Check right way', {position: toast.POSITION_TOP_RIGHT})
      return;
    }
    let formData = new FormData();
    await formData.append('devices',this.state.file)
    const res = await createScooterCsv(formData)
    if (res.response.status === 200)
      toast.success(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    else
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    this.setState({file: null});
  }

  valueChange = (e) => {
    var obj = {}
    obj[e.target.name] = e.target.value
    this.setState(obj)
  }

  addItem = async () => {
    const {id_operator,id_owner,service,type} = this.state
    const res = await createScooter({
      id_device: this.state.id_device,
      serial: this.state.serial,
      id_service_type:service,
      id_device_type:type,
      id_operator:id_operator===''?null:id_operator,
      id_owner:id_owner===''?null:id_owner,
      version: this.state.type.toString() + this.state.version_a + this.state.version_b
    })
    if (res.response.status === 200){
      toast.success(res.response.message, {position: toast.POSITION_TOP_RIGHT})
      this.props.history.goBack()}
    else
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
  }

  render() {
    const { load_csv, upload_success, id_device, serial, version_a, version_b, type, types, operators,id_operator,id_owner,owners,service,services,enableOp,enableOw } = this.state
    return (
      <React.Fragment>
      <div className="content" id="add-inventory">        
        <Row>
          <Col md='12'>
            <CardHeader className="management-header">
              <Col md='4'>
                <CardTitle tag="h4" onClick={()=>{this.props.history.push('/admin/admin-inventory')}}>
                  <img alt="" src={require("assets/img/icon_arrow.svg")}/>Add to Inventory
                </CardTitle>
              </Col>  
            </CardHeader>            
          </Col>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="assign_field">
                  <div className={!load_csv ? "import_title" : "import_title disable_btn"}>Manually Assign Fields</div>
                  <div className="import_section">
                    <Row>
                      <Col md="6">
                        <Row>
                          <Label md="3" className={!load_csv ? "text_color" : "disable_btn"}>QR Field Name</Label>
                        </Row>
                        <Row>
                          <Label md="3" className={!load_csv ? "text_color" : "disable_btn"}>ID Device</Label>
                          <Col md="6">
                            <FormGroup>
                              <Input type="text" value={id_device} onChange={this.valueChange} maxLength="5" name="id_device" disabled={load_csv}/>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Label md="3" className={!load_csv ? "text_color" : "disable_btn"}>Serial Device</Label>
                          <Col md="6">
                            <FormGroup>
                              <Input type="text" value={serial} onChange={this.valueChange} maxLength="20" name="serial" disabled={load_csv}/>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Label md="3" className={!load_csv ? "text_color" : "disable_btn"}>Device Type</Label>
                          <Col md="6">
                            <FormGroup>
                              <Input
                                name="type"
                                type="select"
                                value={type}
                                disabled={load_csv}
                                onChange={this.valueChange}
                              >                               
                              {types.map((type, index) => <option key={index} value={type.id_device_type}>{type.device}</option>)}
                              </Input>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Label md="3" className={!load_csv ? "text_color" : "disable_btn"}>Device Version</Label>
                          <Col md="6">
                            <FormGroup className='deviceVersion'>
                              <Input
                                className='fieldVersion'
                                name="version_a"
                                type="select"
                                value={version_a}
                                disabled={load_csv}
                                onChange={this.valueChange}
                              >
                                {versions.map((version, index) => <option key={index} value={version}>{version}</option>)}
                              </Input>
                              <Input
                                className='fieldVersion'
                                name="version_b"
                                type="select"
                                value={version_b}
                                disabled={load_csv}
                                onChange={this.valueChange}
                              >
                                {versions.map((version, index) => <option key={index} value={version}>{version}</option>)}
                              </Input>                              
                            </FormGroup>
                          </Col>
                        </Row>
                      </Col>
                      <Col md="6">
                        <Row>
                          <Label md="12" className={!load_csv ? "text_color" : "disable_btn"}>Device Operation</Label>
                        </Row>
                        <Row>
                          <Col sm="12">
                            <FormGroup check>
                              <Label check className={!load_csv ? "text_color" : "disable_btn"}>
                                <Input 
                                  className='enaCheck'
                                  type="checkbox"
                                  name="enableOp"
                                  checked={enableOp}
                                  disabled={load_csv}
                                  onChange={this.changeValueCheck}
                                  />
                                <span className="form-check-sign" />
                                Add Operator                                
                              </Label>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Label md="3" className={!load_csv ? "text_color" : "disable_btn"}>Operator</Label>
                          <Col md="6">
                            <FormGroup>
                              <Input
                                name="id_operator"
                                type="select"
                                value={id_operator}
                                disabled={!enableOp}
                                onChange={this.valueChange}
                              >
                                <option key={0} value={''}>{'--Select Operator--'}</option>
                              {operators?operators.map((obj, index) => <option key={index} value={obj.id_operator}>{obj.name}</option>):null}
                              </Input>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col sm="12">
                            <FormGroup check >
                              <Label check className={!load_csv ? "text_color" : "disable_btn"}>
                                <Input 
                                  className='enaCheck'
                                  type="checkbox"
                                  name="enableOw"
                                  checked={enableOw}
                                  disabled={load_csv}
                                  onChange={this.changeValueCheck}
                                  />
                                <span className="form-check-sign" />
                                Add Owner                           
                              </Label>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Label md="3" className={!load_csv ? "text_color" : "disable_btn"}>Owner</Label>
                          <Col md="6">
                            <FormGroup>
                              <Input
                                name="id_owner"
                                type="select"
                                value={id_owner}
                                disabled={!enableOw}
                                onChange={this.valueChange}
                              >
                                <option key={0} value={''}>{'--Select Owner--'}</option>                                
                              {owners?owners.map((obj, index) => <option key={index} value={obj.id_owner}>{obj.name}</option>):null}
                              </Input>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Label md="3" className={!load_csv ? "text_color" : "disable_btn"}>Service Type</Label>
                          <Col md="6">
                            <FormGroup>
                              <Input
                                name="service"
                                type="select"
                                value={service}
                                disabled={load_csv}
                                onChange={this.valueChange}
                              >
                                <option key={0} value={''}>{'--Select service--'}</option>
                              {services.map((obj, index) => <option key={index} value={obj.id_service_type}>{obj.service}</option>)}
                              </Input>
                            </FormGroup>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="4">
                        <Button className="add_new_item" disabled={load_csv} onClick={this.addItem}>ADD NEW ITEM</Button>
                      </Col>
                    </Row>
                    
                  </div>
                </div>
                <div className="import_data">
                  <div className="import_title">Import Data</div>
                  <div className="import_section">
                    <div className="import_check">
                      <input id="load_csv" type="checkbox" name="load_csv" checked={load_csv} onChange={this.changeValue}/>
                      <label htmlFor="load_csv">Load CSV file</label>
                    </div>
                    <div className="upload_download">
                      <div className="fileupload">
                        <input type="file" onChange={this.onSave} disabled={!load_csv}/>
                        choose file
                      </div>
                      <div className="fileupload" onClick={this.onUpload}>
                        upload csv
                      </div>
                      <a className={load_csv ? "download_btn" : "download_btn disable_btn"} 
                        href="https://prod-img-csmc.s3-us-west-2.amazonaws.com/templates/devices.csv">
                        <img src={require("assets/img/icon-download.svg")} alt=""/>
                        Download sheet template
                      </a>
                    </div>
                    <div className={load_csv ? "import_desc" : "import_desc disable_btn"}>CSVI files should have one sheet without references and functions</div>
                    {upload_success && load_csv && <div className="alert_upload"><img src={require("assets/img/check.svg")} alt=""/><span>Upload Successful</span></div>}
                  </div>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <ToastContainer />
      </div>
    </React.Fragment>
    )
  }
}

export default AddInventory;
