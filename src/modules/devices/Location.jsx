// import FullScreenMap from './Map/FullScreenMap';
import FullScreenMap from '../../views/maps/FullScreenMap';
import { scooterLock, scooterUnlock, changeStatus } from '../../api/ScooterApi';
import { getGeneralView, getNav, getDetailInfo, transGeofence } from '../../api/LocationApi';
import {subscribe,unSubscribe} from '../../utils/EventComponents'
import StatusModal from './modals/StatusModal'
import LoadingModal from './../../components/Navbars/LogoutModal'
import './styles/location.scss'

import React from 'react';
// import moment from 'moment'
import {
  Row,
  Col,
} from 'reactstrap';

import { ToastContainer, toast } from 'react-toastify'
import Moment from 'react-moment'
import ResponsiveModal from 'react-responsive-modal';

class Location extends React.Component {
  constructor(props) {    
    super(props)
    this.update = ''
    this.upScooters=[]
    this.state = {
      is_loading:false,
      geofence:[],
      view: true,
      filter: 1,
      first: null,
      previous: null,
      next: null,
      last: null,
      total: 0,
      index: 0,
      scooters: [],      
      users: [],
      status: [],
      img_scooter: '',
      last_ride_date: '',
      last_ride_time: '',
      last_rider_full_name: '',
      last_rider_email: '',
      last_rider_phone: '',
      center: {lat: 4.7134, lng: -74.0653},

      curId: null,
      curStatus: null,
      statusModal: false
    }
    
  }

  

    setFilter = async(filter)=> {
    const res = await getNav("?filter="+filter)
    this.setState({filter: filter}, ()=>{this.setData(res.response)})
  }

  setData = async(data) => {
    var scooters = data.Devices ? data.Devices : []
      scooters.sort((a,b)=>(a.id_device > b.id_device) ? 1 : ((b.id_device > a.id_device) ? -1 : 0))
    const geofence = data.geofence?transGeofence(data.geofence):[]
    this.upScooters = scooters
    this.setState({
      is_loading: false,
      geofence:geofence,
      scooters: scooters,
      first: data.first,
      previous: data.previous,
      next: data.next,
      last: data.last,
      total: data.total,
      index: data.index,
      status: data.Status ? data.Status : [],
      users: data.Users ? data.Users : []
    })
  }

  async = async() => {
    if (this.state.first === null)
      return

    const result = await getNav(this.state.first + '&filter='+this.state.filter)
    this.setData(result.response)
  }

  previous = async() => {
    if (this.state.previous === null)
      return
    const result = await getNav(this.state.previous + '&filter='+this.state.filter)
    this.setData(result.response)
  }

  next = async() => {
    if (this.state.next === null)
      return
    const result = await getNav(this.state.next + '&filter='+this.state.filter)
    this.setData(result.response)
  }

  last = async() => {
    if (this.state.last === null)
      return
    const result = await getNav(this.state.last + '&filter='+this.state.filter)
    this.setData(result.response)
  }

  fetchData = async() => {
    const res = await getNav('?filter='+this.state.filter)
    this.setData(res.response)
  }
  changeView = async() => {
    var res = null
    if (!this.state.view)
      res = await getNav('?filter='+this.state.filter)
    else
      res = await getGeneralView()
    this.setState({view: !this.state.view}, ()=>{this.setData(res.response)})
  }

  showIndent = async (scooter) => {
    let res = await getDetailInfo(scooter.id_device);
    if (res.st){
      if(res.status!==200){
        toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
      }
      else {        
        const data = res;
        let name='', phone='', email='',date=''

        if (data.ride) {
          // Full Name
          name = data.ride.first_name ? data.ride.first_name : ''
          name += ' ' + data.ride.last_name ? data.ride.last_name : ''

          // Phone Number
          phone = data.ride.indicative ? data.ride.indicative : ''
          phone += data.ride.phone ? data.ride.phone : ''

          // Email
          email = data.ride.email ? data.ride.email : ''

          date = data.ride.last_ride?data.ride.last_ride:''
        }

        this.setState({
          img_scooter: data.img_scooter ? 'data:image/png;base64, ' + data.img_scooter : require("assets/img/Image_not_found.png"),
          last_ride_date: date,
          last_rider_full_name: name,
          last_rider_email: email,
          last_rider_phone: phone,
          center: {lat:parseFloat(scooter.lat.replace('"','').replace('"','')), lng: parseFloat(scooter.lon.replace('"','').replace('"',''))}
        }, ()=> {
          let rect = this.refs['indent-'+scooter.id_device].getBoundingClientRect();
          this.refs.popup.style.left = rect.right + 40 + 'px';
          this.refs.popup.style.top = rect.top - 95 + 'px';
          this.refs.popup.style.display = 'flex';
        })
      }
    }
  }

  closePopup = () => {
    this.refs.popup.style.display = 'none';
  }

  setCenter = (scooter) => {
    this.setState({center: {lat:parseFloat(scooter.lat.replace('"','').replace('"','')), lng: parseFloat(scooter.lon.replace('"','').replace('"',''))}})
  }
  renderScooters() {
    const { scooters } = this.state
    return scooters.map((scooter, index) => {
      return (
        <div className="scooter-detail" key={index}>
          <div className="first-line">
            <div className="scooter-id">{scooter.id_device}</div>
            <div className="scooter-status">
            <div className={
                scooter.id_status_device === 1 ? "ellipse available-ellipse":
                scooter.id_status_device === 2 ? "ellipse use-ellipse":
                scooter.id_status_device === 3 ? "ellipse transport-ellipse":
                scooter.id_status_device === 4 ? "ellipse broken-ellipse":
                scooter.id_status_device === 5 ? "ellipse stolen-ellipse":
                scooter.id_status_device === 6 ? "ellipse unavailable-ellipse":
                scooter.id_status_device === 7 ? "ellipse maintenance-ellipse":
                scooter.id_status_device === 8 ? "ellipse logistic-ellipse":"ellipse"}>
              </div>{
                scooter.id_status_device === 1 ? 'Available':
                scooter.id_status_device === 2 ? 'In Use':
                scooter.id_status_device === 3 ? 'In Transport' :
                scooter.id_status_device === 4 ? 'Broken' :
                scooter.id_status_device === 5 ? 'Stolen' :
                scooter.id_status_device === 6 ? 'Unavailable' :
                scooter.id_status_device === 7 ? 'Maintenance' :
                scooter.id_status_device === 8 ? 'Logistic User' : 'Other'}
            </div>
          </div>
          <div className="second-line">
            <div className="location">
              <span className="location-cursor" onClick={()=>this.setCenter(scooter)}>
                <i className="fas fa-map-marker-alt" ></i>
              <span>Location</span>
              </span>
              <i className="fas fa-sync refresh" onClick={this.fetchData}></i>
              <i className="fas fa-indent" ref={"indent-"+scooter.id_device} onClick={()=>this.showIndent(scooter)}></i>
            </div>
            <div className="battery">
              <img alt="" src={parseInt(scooter.level_charge) > 20 ? require("assets/img/battery_high.svg") : require("assets/img/battery_low.svg")}/>
              {scooter.level_charge}%
            </div>
          </div>
        </div>
      );
    });
  }

  componentDidMount() {
    var _this = this    
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        }; 
        _this.setState({center: pos})
      })
    }
    this.initdata()
  }

  componentWillUnmount(){
    unSubscribe('socket')
    clearInterval(this.update);
  }

  initdata = async () => {    
    const res = await getGeneralView()
    await this.setData(res.response)
    subscribe('socket',(data)=>{
      // if(data.id_device==='A0001') console.log(data);
      let newScooters = Object.assign([], this.upScooters)
      const scooter = newScooters.findIndex(scooter => scooter.id_device === data.id_device)
      if(scooter>=0){
        newScooters[scooter] = data
      }
      this.upScooters = newScooters
    })
    this.update = setInterval(()=>{
      this.setState({scooters:this.upScooters})
    }, 1000);
  }


  historicView = (id_device) => {
    this.props.history.push({pathname:'/admin/rides', state: {id_device: id_device}})
  }

  scooterLock = async (serial, id_device) => {
    const res = await scooterLock({serial: serial})
    if (res.response.status !== 200) {
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    }
    else {
      toast.success(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    }
    // toast.success(`Command LOCK has sent to ${id_scooter}`, {position: toast.POSITION_TOP_RIGHT})
  }

  scooterUnlock = async (serial, id_device) => {
    const res = await scooterUnlock({serial: serial})
    if (res.response.status !== 200) {
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    }
    else {
      toast.success(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    }
    // toast.success(`Command UNLOCK has sent to ${id_scooter}`, {position: toast.POSITION_TOP_RIGHT})
  }

  statusModalUpdateData = async(data) => {
    this.setState({is_loading: true}, async()=>{
    const res = await changeStatus(data)
    if (res.status === 200) {
      toast.success(res.message, {position: toast.POSITION_TOP_RIGHT})
    }
    else toast.error(res.message, {position: toast.POSITION_TOP_RIGHT})      
      this.setState({ statusModal: false, is_loading: false },()=>this.fetchData());
    })
  }

  scooterSetting = (id_device, id_status_device) => {
    this.setState({
      curId: id_device,
      curStatus: id_status_device,
      statusModal: true
    })
  }
  onCloseStatusModal = () => {
    this.setState({
      statusModal: false
    })
  }

  render() {
    const disableStyle = {
      color: '#9a9a9a'
    }
    const { view, filter, first, previous, next, last, total, index, scooters, users, status ,geofence} = this.state
     return (
      <React.Fragment>
        <ResponsiveModal open={this.state.is_loading}
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}
        >
          <LoadingModal title="Loading..."/>
        </ResponsiveModal>
        <div className="indent-popup" ref={"popup"}>
          <div className="popup-photo">
            <img alt="" src={this.state.img_scooter}/>
          </div>
          <div className="popup-info">
            <div className="close-btn" onClick={this.closePopup}>
              <span className="close">&#215;</span>
            </div>
            <div className="info">
              <div className="ride">
                <div className="title">Last Ride:</div>
                <div className="infoRide">{this.state.last_ride_date==='' ? '' : (<Moment format="DD/MM/YYYY">{this.state.last_ride_date}</Moment>)}</div>
                <div className="infoRide">{this.state.last_ride_date==='' ? '' : (<Moment format="hh:mm">{this.state.last_ride_date}</Moment>)}</div>
              </div>
              <div className="ride">
                <div className="title">Last Rider:</div>
                <div className="infoRide">{this.state.last_rider_full_name}</div>
                <div className="infoRide">{this.state.last_rider_email}</div>
                <div className="infoRide">{this.state.last_rider_phone}</div>
              </div>
            </div>
          </div>
        </div>
        <div className="content" id="location">          
          <Row>
            <Col md="12">
              <div className="view-group">
                <div className={view===false ? "general-view active-view" : "general-view"} onClick={this.changeView}>General View</div>
                <div className={view===true ? "detail-view active-view" : "detail-view"} onClick={this.changeView}>Detailed View</div>
              </div>
            </Col>
          </Row>
          {view ? (
            <Row className="main-view">
              <Col md="5">
                <div className="left-side">
                  <div className="left-header">
                    <div className={filter===2 ? "status active-status" : "status"} onClick={()=>this.setFilter(2)}>on ride</div>
                    <div className={filter===1 ? "status active-status" : "status"} onClick={()=>this.setFilter(1)}>available</div>
                    <div className={filter===4 ? "status active-status" : "status"} onClick={()=>this.setFilter(4)}>unavailable</div>
                    <div className={filter===3 ? "status active-status" : "status"} onClick={()=>this.setFilter(3)}>on transport</div>
                    <div className={filter===5 ? "status active-status" : "status"} onClick={()=>this.setFilter(5)}>low battery</div>
                  </div>
                  <div className="left-body">
                    {this.renderScooters()}
                  </div>
                  <div className="filter-count">
                    {scooters.length + ' items filtered'}
                  </div>
                  <div className="left-footer">
                    <div className="nav-group">
                      <div><span>Showing</span></div>
                      <div className="first" style={first===null?disableStyle:{}} onClick={this.first}><i className="fa fa-angle-double-left"></i></div>
                      <div className="previous" style={previous===null?disableStyle:{}} onClick={this.previous}><i className="fa fa-angle-left"></i></div>
                      <div className="index"><span>{index}</span></div>
                      <div><span>{'to'}</span></div>
                      <div className="count"><span>{scooters.length === 0 ? '0' : (index + scooters.length - 1).toString()}</span></div>
                      <div><span>{'of'}</span></div>
                      <div className="total"><span>{new Intl.NumberFormat().format(total)}</span></div>
                      <div className="next" style={next===null?disableStyle:{}} onClick={this.next}><i className="fa fa-angle-right"></i></div>
                      <div className="last" style={last===null?disableStyle:{}} onClick={this.last}><i className="fa fa-angle-double-right"></i></div>
                    </div>
                  </div>
                </div>
              </Col>
              <Col md="7">
                <div className="google-map">
                  <FullScreenMap 
                    scooters={scooters}
                    users={users}
                    center={this.state.center}
                    view={view}
                    scooterLock={this.scooterLock}
                    scooterUnlock={this.scooterUnlock}
                    historicView={this.historicView}
                    scooterSetting={this.scooterSetting}
                    />
                </div>
              </Col>
            </Row>
          ) : (
            <div>
              <FullScreenMap 
                scooters={scooters} 
                users={users} 
                center={this.state.center} 
                geofence={geofence}
                view={view}
                scooterLock={this.scooterLock}
                scooterUnlock={this.scooterUnlock}
                historicView={this.historicView}
                scooterSetting={this.scooterSetting}
              />
            </div>
          // ):(null)}
          )}
        </div>
        <ResponsiveModal open={this.state.statusModal} onClose={this.onCloseStatusModal} center classNames={{'modal':'dark-modal-status'}}>
          <StatusModal onClose={this.onCloseStatusModal} 
            deviceId={this.state.curId} status={status}
            updateData={this.statusModalUpdateData} deviceIdStatus={this.state.curStatus}
          />
        </ResponsiveModal>
        <ToastContainer />
      </React.Fragment>
    )
  }
}

export default Location;
