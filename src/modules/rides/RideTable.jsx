import React from 'react';
import CurrencyFormat from 'react-currency-format';
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
  FormGroup,
  Input,
} from 'reactstrap';
// import Moment from 'react-moment'
import ResponsiveModal from 'react-responsive-modal';
import { getAllRides, getFilter, getNav } from '../../api/RideApi';
import { transGeofence } from '../../api/LocationApi';
import { endRideLock} from '../../api/ScooterApi';
import {emitter, subscribe,unSubscribe } from '../../utils/EventComponents';
import './styles.scss'
import { ToastContainer, toast } from 'react-toastify'
import SweetAlert from 'react-bootstrap-sweetalert';
import moment from 'moment';

import ConfirmModal from '../../components/Cosmic/InAuthModal/InAuthModal'
import LoaderModal from '../../components/Cosmic/LoaderModal/LoaderModal'
import SingleMarkerMap from '../../components/Maps/SingleMarkerMap/SingleMarkerMap'

class RideTable extends React.Component {
  constructor(params) {
    super(params)
    this.state = {
      search:'',
      currentMarkers:[],
      currentCenter:'',
      currenteDevice:'',
      geofence:'',
      endRideLocation:false,
      centerLat:'',
      centerLon:'',
      is_loading: true,
      rides: [],
      first: null,
      previous: null,
      next: null,
      last: null,
      total: 0,
      index: 0,
      countries: [],
      filter: null,
      active: false,
      country: 'null',
      showModal: false,
      selectId: null,
      selectDevice: null,

      alert: null,
      loader:false,

      sort_direction_date: false,
      sort_direction_start: true,
      sort_direction_end: true,
      sort_direction_time: true,
      sort_direction_email: true,
      sort_direction_phone: true,
      sort_direction_id: true,
      sort_direction_cost: true,
      sort_direction_status: true,
      sort_direction_payment: true,
    }
    this.fetchData = this.fetchData.bind(this)
    this.first = this.first.bind(this)
    this.previous = this.previous.bind(this)
    this.next = this.next.bind(this)
    this.last = this.last.bind(this)    
    this.filterParam = this.filterParam.bind(this)
    this.endRide = this.endRide.bind(this)
    this.setFilter = this.setFilter.bind(this)
    this.change = this.change.bind(this)
    this.onAuthenticate = this.onAuthenticate.bind(this)
    this.setParam = this.setParam.bind(this)
  }

  async onAuthenticate(res) {
    const {selectId,selectDevice} = this.state
    this.setState({
      loader:true,
      showModal: false
    })
    if (res.response.status !== 200) {
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
      this.setState({
        loader:false
      })    
    }
    else{      
      const result = await endRideLock({
        id_service:selectId.service,
        socket_id:selectId.socket_id,
        serial:selectDevice
      })      
      if (result.st) {
        if (result.status !== 200 && result.status !== 406) {
          toast.error(result.message, {position: toast.POSITION_TOP_RIGHT})
          this.setState({
            loader:false
          })    
        }
        else {
          let newRides = this.state.rides;
          const index = newRides.findIndex(ride => ride.id_service === selectId.service)
          newRides.splice(index,1)
          this.setState({
            alert: (<SweetAlert success title="The ride has been finished!" onConfirm={this.closeAlert}></SweetAlert>),
            showModal: false,
            rides: newRides,
            loader:false,
            selectId:null,
            selectDevice:null
          })                    
        }
      }
      else{      
        toast.error(result.message, {position: toast.POSITION_TOP_RIGHT})
        this.setState({
          loader:false
        }) 
      }
    }
  }

  closeAlert = () =>{
    this.setState({alert:null})
  }

  setParam() {
    var filter = ''
    if (this.state.active) 
      filter += 'filter_active_rides=1&'
    if (this.state.filter && this.state.filter !== 'ALL') {
      filter += 'filter_date=' + this.state.filter + '&'
    }
    if (this.state.country !== 'null')
      filter += 'country=' + this.state.country + '&'
    
    if (this.state.search!=='') return filter = '&query='+this.state.search

    return filter.substr(0, filter.length-1)  
  }
  async setFilter() {
    var filter = ''
    if (this.state.active) 
      filter += 'filter_active_rides=1&'
    if (this.state.filter && this.state.filter !== 'ALL') {
      filter += 'filter_date=' + this.state.filter + '&'
    }
    if (this.state.country !== 'null')
      filter += 'country=' + this.state.country + '&'

    const result = await getFilter(filter.substr(0, filter.length-1))
    this.setState({search:''},() => {
      emitter('searchBar',true)
      this.setData(result.response)
    })
  }

  change(e) {
    this.setState({country: e.target.value}, ()=>{this.setFilter()})
  }

  endRide(obj) {
    this.setState({selectId:{service:obj.id,socket_id:obj.socket_id},selectDevice:obj.serial, showModal: true})
  }

  onCloseConfirmModal = () => {
    this.setState({ showModal: false });
  };
  filterParam(type) {
    if (type === 'active')
      this.setState({active: !this.state.active, is_loading: true, filter:''}, ()=> {this.setFilter()})
    else {
      this.setState({filter: type}, ()=>{this.setFilter()})
    }
  }

  setData = async data => {
    var rides = data.Rides ? data.Rides : []
    //rides.sort((a,b)=>(a.start_time_service > b.start_time_service) ? 1 : ((b.start_time_service > a.start_time_service) ? -1 : 0))
    let geo = data.Geofence ? await transGeofence(data.Geofence) : []
    this.setState({
      is_loading: false,
      rides: rides,
      first: data.first,
      previous: data.previous,
      next: data.next,
      last: data.last,
      total: data.total,
      index: data.index,
      countries: data.Countries ? data.Countries : [],  
      geofence:geo,    
    })
  }
  async first() {
    if (this.state.first === null)
      return

    const result = await getNav(this.state.first + this.setParam())
    this.setData(result.response)
  }

  async previous() {
    if (this.state.previous === null)
      return
    const result = await getNav(this.state.previous + this.setParam())
    this.setData(result.response)
  }

  async next() {
    if (this.state.next === null)
      return
    const result = await getNav(this.state.next + this.setParam())
    this.setData(result.response)
  }

  async last() {
    if (this.state.last === null)
      return
    const result = await getNav(this.state.last + this.setParam())
    this.setData(result.response)
  }

  async fetchData() {
    let result;
    if (this.state.search !== '')
      result = await getFilter('query='+this.state.search);
    else result = await getAllRides();
    this.setData(result.response)
  }

  componentWillMount() {
    if (this.props.location.state)
      this.setState({
        search: this.props.location.state.id_device        
      })
  }

  componentDidMount() {
    subscribe('search',(value)=> this.dinamycSearch(value))
    emitter('searchBar',true)
    this.fetchData()
  }
  componentWillUnmount =() => {
    unSubscribe('search')
    emitter('searchBar',false)
  }
  dinamycSearch = async (value) =>{
    const result = await getFilter('query='+value)
    this.setState({
      search:value
    })
    this.setData(result.response)
  }

  goToDetailUser = (cuid) => {
    this.props.history.push(`/admin/user-detail/${cuid}`);
  }

  renderRides() {
    const { active, rides } = this.state
    return rides.map((ride, index) => (
      <tr key={index} style={{cursor:'pointer'}}>
        <td>{moment.utc(ride.start_time_service).format('MMM DD, YYYY')}</td>
        <td>{moment.utc(ride.start_time_service).format('h:mm A')}</td>
        <td>
          {ride.end_time_service!=="null"?moment.utc(ride.end_time_service).format('h:mm A'):'-'}
        </td>
        {this.state.active?null:
        <td>
          {ride.end_time_service!=="null"?
            ride.time==="1"?`${ride.time} Minute`:`${ride.time} Minutes`:
            moment.utc(ride.start_time_service).fromNow()
          }
        </td>}
        <td onClick={() => this.goToDetailUser(ride.cuid)}>{ride.email}</td>
        <td>{ride.phone}</td>
        <td>{ride.id_device}</td>
        <td>
          <CurrencyFormat 
            value={ride.total_cost} 
            displayType={'text'} 
            thousandSeparator={true} 
            prefix={'$'}            
            decimalScale={2}
            fixedDecimalScale={true} 
            renderText={value => <div>{value}</div>} />
        </td> 
        <td>{ride.status}</td>
        <td>
          <div
            onClick={()=>this.renderMap(ride)}
            className="position"
          >
            <i className="tim-icons icon-gps-fixed" />
          </div>
        </td>
        <td>
          <div className="payment-status">
            <span>{ride.status_payment}</span>
            {ride.status_payment === 'Succeded' ? <img alt="" src={require("assets/img/icon_check.png")}/> : 
            ride.status_payment === 'Failed' ? <img alt="" src={require("assets/img/icon_fail.png")}/> : 
            ride.status_payment === 'Free' ? <img alt="" src={require("assets/img/icon_free.svg")}/> :
            ride.status_payment === 'Partially Refunded' ? <img alt="" src={require("assets/img/icon_reply.png")}/> :
            ride.status_payment === 'Pending' ? <img alt="" src={require("assets/img/icon_pending.png")}/> : 
            ride.status_payment === 'Refunded' ? <img alt="" src={require("assets/img/icon_reply.png")}/> : null}
          </div>
        </td>
        {active ? <td>
          <div className="end-ride" onClick={() => {this.endRide({id:ride.id_service,serial:ride.serial,socket_id:ride.socket_id})}}>end service</div>
        </td> : null}
      </tr>
    ));
  }

  sortData = (type) => {
    let rides = Object.assign([], this.state.rides)
    switch(type) {
      case "id":
        if (this.state.sort_direction_id) {
          rides.sort((a,b)=>(a.id_device > b.id_device) ? 1 : ((b.id_device > a.id_device) ? -1 : 0))
        } else {
          rides.sort((a,b)=>(a.id_device < b.id_device) ? 1 : ((b.id_device < a.id_device) ? -1 : 0))
        }
        this.setState({sort_direction_id: !this.state.sort_direction_id, rides: rides})
        break;
      case "start":
        if (this.state.sort_direction_start) {
          rides.sort((a,b)=>(a.start_time_service > b.start_time_service) ? 1 : ((b.start_time_service > a.start_time_service) ? -1 : 0))
        } else {
          rides.sort((a,b)=>(a.start_time_service < b.start_time_service) ? 1 : ((b.start_time_service < a.start_time_service) ? -1 : 0))
        }
        this.setState({sort_direction_start: !this.state.sort_direction_start, rides: rides})
        break;
      case "date":
        if (this.state.sort_direction_date) {
          rides.sort((a,b)=>(a.start_time_service > b.start_time_service) ? 1 : ((b.start_time_service > a.start_time_service) ? -1 : 0))
        } else {
          rides.sort((a,b)=>(a.start_time_service < b.start_time_service) ? 1 : ((b.start_time_service < a.start_time_service) ? -1 : 0))
        }
        this.setState({sort_direction_date: !this.state.sort_direction_date, rides: rides})
        break;
      case "end":
        if (this.state.sort_direction_end) {
          rides.sort((a,b)=>(a.end_time_service > b.end_time_service) ? 1 : ((b.end_time_service > a.end_time_service) ? -1 : 0))
        } else {
          rides.sort((a,b)=>(a.end_time_service < b.end_time_service) ? 1 : ((b.end_time_service < a.end_time_service) ? -1 : 0))
        }
        this.setState({sort_direction_end: !this.state.sort_direction_end, rides: rides})
        break;
      case "time":
        if (this.state.sort_direction_time) {
          rides.sort( function(a,b) {
            let start = new Date(a.start_time_service)
            let end = a.end_time_service ? new Date(a.end_time_service) : new Date()
            let diff_a = end.getTime() - start.getTime()

            start = new Date(b.start_time_service)
            end = b.end_time_service ? new Date(b.end_time_service) : new Date()
            let diff_b = end.getTime() - start.getTime()
            return (diff_a > diff_b) ? 1 : ((diff_b > diff_a) ? -1 : 0);
          })
        } else {
          rides.sort( function(a,b) {
            let start = new Date(a.start_time_service)
            let end = a.end_time_service ? new Date(a.end_time_service) : new Date()
            let diff_a = end.getTime() - start.getTime()

            start = new Date(b.start_time_service)
            end = b.end_time_service ? new Date(b.end_time_service) : new Date()
            let diff_b = end.getTime() - start.getTime()
            return (diff_a < diff_b) ? 1 : ((diff_b < diff_a) ? -1 : 0);
          })
        }
        this.setState({sort_direction_time: !this.state.sort_direction_time, rides: rides})
        break;
      case "email":
        if (this.state.sort_direction_email) {
          rides.sort((a,b)=>(a.email > b.email) ? 1 : ((b.email > a.email) ? -1 : 0))
        } else {
          rides.sort((a,b)=>(a.email < b.email) ? 1 : ((b.email < a.email) ? -1 : 0))
        }
        this.setState({sort_direction_email: !this.state.sort_direction_email, rides: rides})
        break;
      case "phone":
        if (this.state.sort_direction_phone) {
          rides.sort((a,b)=>(a.phone > b.phone) ? 1 : ((b.phone > a.phone) ? -1 : 0))
        } else {
          rides.sort((a,b)=>(a.phone < b.phone) ? 1 : ((b.phone < a.phone) ? -1 : 0))
        }
        this.setState({sort_direction_phone: !this.state.sort_direction_phone, rides: rides})
        break;
      case "cost":
        if (this.state.sort_direction_cost) {
          rides.sort((a,b)=>(a.total_cost > b.total_cost) ? 1 : ((b.total_cost > a.total_cost) ? -1 : 0))
        } else {
          rides.sort((a,b)=>(a.total_cost < b.total_cost) ? 1 : ((b.total_cost < a.total_cost) ? -1 : 0))
        }
        this.setState({sort_direction_cost: !this.state.sort_direction_cost, rides: rides})
        break;
      case "status":
        if (this.state.sort_direction_status) {
          rides.sort((a,b)=>(a.status > b.status) ? 1 : ((b.status > a.status) ? -1 : 0))
        } else {
          rides.sort((a,b)=>(a.status < b.status) ? 1 : ((b.status < a.status) ? -1 : 0))
        }
        this.setState({sort_direction_status: !this.state.sort_direction_status, rides: rides})
        break;
      case "payment":
        if (this.state.sort_direction_payment) {
          rides.sort((a,b)=>(a.status_payment > b.status_payment) ? 1 : ((b.status_payment > a.status_payment) ? -1 : 0))
        } else {
          rides.sort((a,b)=>(a.status_payment < b.status_payment) ? 1 : ((b.status_payment < a.status_payment) ? -1 : 0))
        }
        this.setState({sort_direction_payment: !this.state.sort_direction_payment, rides: rides})
        break;
      default:
        break;
    }
  }

  renderMap = async(ride) => {    
    let markers = []
    markers.push({
      lat:parseFloat(ride.start_coords_lat),
      lng:parseFloat(ride.start_coords_lon),
      urlIcon:require('assets/img/icon_map_start.svg'),
      address:ride.start_address,
      msj:'Ride Started'
    })
    markers.push({
      lat:parseFloat(ride.end_coords_lat),
      lng:parseFloat(ride.end_coords_lon),
      urlIcon:require('assets/img/icon_map_stop.svg'),
      address:ride.end_address,
      msj:'Ride Ended'
    })    
    this.setState({
      endRideLocation:!this.state.endRideLocation,
      currentMarkers:markers,
      currentCenter:{lat:ride.end_coords_lat,lng:ride.end_coords_lon},
      currenteDevice:ride.id_device
    });
  }
  toggleEndRideLocation = () => {
    this.setState({endRideLocation: !this.state.endRideLocation});
  }

  render() {
    const { rides, first, previous, next, last, total, index,
           filter, active, country, countries,
           currentMarkers,geofence,endRideLocation,currentCenter,currenteDevice} = this.state
    const disableStyle = {
      color: '#9a9a9a'
    }
    return (
      <React.Fragment>
        <SingleMarkerMap
          statusModal={endRideLocation}
          toggleModal={this.toggleEndRideLocation}
          markers={currentMarkers}
          geofence={geofence}
          center={currentCenter}
          title={'End Ride Location | ' + currenteDevice}
        />
        <ResponsiveModal open={this.state.loader} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LoaderModal title="Loading..."/>
        </ResponsiveModal>
        {this.state.alert}
        <div id="ride" className="content">          
          <Row>
            <Col md="12">
              <CardHeader className="management-header">
                <Col md='4'>
                  <CardTitle tag="h4">Historic Rides</CardTitle>
                </Col>
                {/* <Col md='4' className='funtionSpace'>                  
                </Col>                                 */}
                <Col md='8'className='filterSpace'> 
                  <div className="tools tools-group">
                    <div className={active ? "active-btn active" : "active-btn"} onClick={() => {this.filterParam('active')}}>ACTIVE RIDES</div>
                    <div className="filter-group">
                      <div className={filter==='daily' ? "active" : ''} onClick={() => {this.filterParam('daily')}}>Daily</div>
                      <div className={filter==='montly' ? "active" : ''} onClick={() => {this.filterParam('montly')}}>Monthly</div>
                      <div className={filter==='annual' ? "active" : ''} onClick={() => {this.filterParam('annual')}}>ANNUAL</div>
                      {/* <div className={filter==='MTD' ? "active" : ''} onClick={() => {this.filterParam('MTD')}}>MTD</div>
                      <div className={filter==='QTD' ? "active" : ''} onClick={() => {this.filterParam('QTD')}}>QTD</div>
                      <div className={filter==='YTD' ? "active" : ''} onClick={() => {this.filterParam('YTD')}}>YTD</div> */}
                      <div className={filter==='ALL' ? "active" : ''} onClick={() => {this.filterParam('ALL')}}>ALL</div>
                      <div className={filter==='ALL' ? "active" : ''} onClick={() => {this.filterParam('')}}>CLEAR</div>
                    </div>
                    <div className="country">
                      <FormGroup>
                        <Input
                          name="country"
                          type="select"
                          value={country}
                          onChange={this.change}
                        >
                        <option key={index} value='null'>country</option>
                        {countries.map((country, index) => <option key={index} value={country.country}>{country.iso}</option>)}
                        </Input>
                      </FormGroup>
                    </div>
                  </div>               
                </Col>
              </CardHeader>
            </Col>
            <Col md="12">
              <Card>
                {/* <CardHeader className="payment-header">
                  
                </CardHeader> */}
                <CardBody>
                  <Table>
                    <thead className="text-primary">
                      <tr>
                        <th onClick={()=>{this.sortData('date')}}>Ride date <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('start')}}>Start ride <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        {this.state.active?null:
                        <th onClick={()=>{this.sortData('end')}}>End ride <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>}
                        <th onClick={()=>{this.sortData('time')}}>Time ride <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('email')}}>Rider email <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('phone')}}>Rider phone <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('id')}}>ID <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('cost')}}>Cost ride <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('status')}}>Status <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th>Location</th>
                        <th onClick={()=>{this.sortData('payment')}}>Status payment <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        {active ? <th></th> : null}
                      </tr>
                    </thead>
                    <tbody className="payment-body">{this.renderRides()}</tbody>
                  </Table>
                  {rides.length === 0 && <div className="no-result">No results found</div>}
                  {rides.length > 0 &&
                  <div className="nav-group">
                    <div><span>Showing</span></div>
                    <div className="first" style={first===null?disableStyle:{}} onClick={this.first}><i className="fa fa-angle-double-left"></i></div>
                    <div className="previous" style={previous===null?disableStyle:{}} onClick={this.previous}><i className="fa fa-angle-left"></i></div>
                    <div className="index"><span>{index}</span></div>
                    <div><span>{'to'}</span></div>
                    <div className="count"><span>{rides.length}</span></div>
                    <div><span>{'of'}</span></div>
                    <div className="total"><span>{new Intl.NumberFormat().format(total)}</span></div>
                    <div className="next" style={next===null?disableStyle:{}} onClick={this.next}><i className="fa fa-angle-right"></i></div>
                    <div className="last" style={last===null?disableStyle:{}} onClick={this.last}><i className="fa fa-angle-double-right"></i></div>
                  </div>
                  }
                </CardBody>
              </Card>
            </Col>
          </Row>
          <ResponsiveModal open={this.state.showModal} onClose={this.onCloseConfirmModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
            <ConfirmModal history={this.props.history} onClose={this.onCloseConfirmModal} onAuthenticate={this.onAuthenticate} type={'isRide'}/>
          </ResponsiveModal>
        </div>
        <ToastContainer />
      </React.Fragment>
    );
  }
}

export default RideTable;
