import React, { Component } from 'react';
import {
  Button,
  Input,
  Row,
} from 'reactstrap';

import './modal.scss'
class ChangeModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user_status: this.props.user.id_status
    }
    this.change = this.change.bind(this)
  }

  change(e) {
    this.setState({user_status: e.target.value})
  }

  changeStatus = () => {
    let newUser = Object.assign([], this.props.user);
    newUser.id_status = this.state.user_status
    this.props.onChange(newUser)
  }
  render() {
    const { user, status, onClose } = this.props
    const { user_status } = this.state
    return (
      <div className="user_status">
        <div className="change-title">
          <span className="title">{'Change status'}</span>
        </div>
        <div className="status-group">
          <div className="input_group">
            <div>User</div>
            <div className="text-center">{user.first_name + ' ' + user.last_name}</div>

            <div>Status</div>
            <div>
              <Input
                name="status"
                type="select"
                value={user_status}
                className={user_status.toString() === '1' ? 'available' :
                    user_status.toString() === '4' ? 'inactive' : ''}
                    onChange={this.change}
              >
              {status.map((status, index) => <option key={index} value={status.id_status} className={status.id_status === 1 ? 'available' :
                    status.id_status === 4 ? 'inactive' : ''}>{status.status}</option>)}
              </Input>
            </div>
          </div>
        </div>
        <Row className='cosmic-butons butonsUpdateOpe'>
          <Button className="cosmic-cancel" onClick={onClose}>Cancel</Button>
          <Button className="cosmic-confirm" onClick={this.changeStatus}>Change status</Button>
        </Row>
      </div>
    )
  }
}

export default ChangeModal