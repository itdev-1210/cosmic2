import React from 'react';
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
} from 'reactstrap';
import { Link } from 'react-router-dom';

import { getTeams } from '../../api/subOrganizations';
import './style.scss'

class SeeTeam extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      users: [],
      first: null,
      previous: null,
      next: null,
      last: null,
      total: 0,
      index: 0,
    }
    this.setData = this.setData.bind(this)
    this.fetchData = this.fetchData.bind(this)
    this.first = this.first.bind(this)
    this.previous = this.previous.bind(this)
    this.next = this.next.bind(this)
    this.last = this.last.bind(this)
  }
  
  async fetchData() {
    const { match: { params } } = this.props;
    const result = await getTeams(params.id_operator);
    this.setData(result.response);
  }

  async first() {
    if (this.state.first === null)
      return

    // const result = await getNav(this.state.first + this.setParam())
    // this.setData(result.response)
  }

  async previous() {
    if (this.state.previous === null)
      return
    // const result = await getNav(this.state.previous + this.setParam())
    // this.setData(result.response)
  }

  async next() {
    if (this.state.next === null)
      return
    // const result = await getNav(this.state.next + this.setParam())
    // this.setData(result.response)
  }

  async last() {
    if (this.state.last === null)
      return
    // const result = await getNav(this.state.last + this.setParam())
    // this.setData(result.response)
  }

  componentDidMount() {
    this.fetchData()
  }

  setData(data) {
    this.setState({
      'users': data.team ? data.team : [],
      'first': data.first,
      'previous': data.previous,
      'next': data.next,
      'last': data.last,
      'total': data.total,
      'index': data.index,
    })
  }
  renderUsers() {
    const { users } = this.state
    return users.map((user, index) => {
      return (
        <tr key={index}>
          <td>{user.first_name + ' ' + user.last_name}</td>
          <td>{user.role}</td>
          <td>{user.email}</td>
          <td>
            <div className={user.status === 'Available' ? 'available' :
              user.status === 'Inactive' ? 'inactive' : ''}>
              {user.status}
            </div>
          </td>
        </tr>
      );
    });
  }

  render() {
    const disableStyle = {
      color: '#9a9a9a'
    }
    const { first, next, previous, last, index, total, users } = this.state
    return (
      <React.Fragment>
        {this.state.alert}
        <div className="content" id="admin-team">
          <Row>
            <Col md="12">
              <CardHeader className="management-header">
                <Col md='4'>
                  <CardTitle tag="h4" onClick={()=>{this.props.history.push('/admin/admin-operator')}}>
                    <img alt="" src={require("assets/img/icon_arrow.svg")}/>View Team
                  </CardTitle>
                </Col>
                <Col md='4' className='funtionSpace'>
                  <Link to={'/admin/add-operator/'+this.props.match.params.id_operator}>
                    <button className='btn-add btn-cosmic'>
                      <i className="tim-icons icon_add" />
                      <span>Add Member</span>
                    </button> 
                  </Link>
                </Col>                                
                <Col md='4'className='filterSpace'>                
                </Col>
              </CardHeader>
            </Col>
          </Row>
          <Row>
            <Col md="12">
              <Card>
                <CardBody>
                  <Table>
                    <thead className="text-primary">
                      <tr>
                        {/* <th>#</th> */}
                        <th>Name</th>
                        <th>Role</th>
                        <th>Email</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody className="management-body">{this.renderUsers()}</tbody>
                  </Table>
                  {users.length === 0 && <div className="no-result">No results found</div>}
                  {users.length > 0 && 
                  <div className="nav-group">
                    <div><span>Showing</span></div>
                    <div className="first" style={first===null?disableStyle:{}} onClick={this.first}><i className="fa fa-angle-double-left"></i></div>
                    <div className="previous" style={previous===null?disableStyle:{}} onClick={this.previous}><i className="fa fa-angle-left"></i></div>
                    <div className="index"><span>{index}</span></div>
                    <div><span>{'to'}</span></div>
                    <div className="count"><span>{users.length === 0 ? 0 : index + users.length - 1}</span></div>
                    <div><span>{'of'}</span></div>
                    <div className="total"><span>{new Intl.NumberFormat().format(total)}</span></div>
                    <div className="next" style={next===null?disableStyle:{}} onClick={this.next}><i className="fa fa-angle-right"></i></div>
                    <div className="last" style={last===null?disableStyle:{}} onClick={this.last}><i className="fa fa-angle-double-right"></i></div>
                  </div>
                  }
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </React.Fragment>
    );
  }
}

export default SeeTeam;
