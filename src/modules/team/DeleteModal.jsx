import React, { Component } from 'react';
import './modal.scss'
import {
  Button,
  Row,
} from 'reactstrap';

class DeleteModal extends Component {
  render() {
    const { onClose, onSetDefault } = this.props
    return (
      <div className="delete_modal">
        <div className="change-title">
          <span className="title">{'Delete account'}</span>
        </div>
        <Row className="description">
          <div>{'Are you sure to delete this account?'}</div>
          <div>{'This action is can not be undone'}</div>
        </Row>
        <Row className="require-action">
          <div>{'This action requires additional privileges'}</div>
        </Row>
        <Row className='cosmic-butons butonsUpdateOpe'>
          <Button className="cosmic-cancel" onClick={onClose}>Cancel</Button>
          <Button className="cosmic-confirm" onClick={onSetDefault}>Delete account</Button>
        </Row>
      </div>
    )
  }
}

export default DeleteModal