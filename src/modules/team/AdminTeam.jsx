import React from 'react';
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  DropdownToggle,
  DropdownMenu,
  UncontrolledDropdown,
  Table,
  Row,
  Col,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import SweetAlert from 'react-bootstrap-sweetalert';

import { getTeams, getNav, getFilter, deleteAccount, changeAccount } from '../../api/TeamApi';
import {emitter, subscribe,unSubscribe } from '../../utils/EventComponents';
import './style.scss'
import ResponsiveModal from 'react-responsive-modal';
import ConfirmModal from '../../views/pages/ConfirmModal'
import AlertModal from '../../components/Cosmic/AlertModal/AlertModal'
import ChangeModal from './ChangeModal'
import LoadingModal from './../../components/Navbars/LogoutModal'

class AdminTeam extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      search:'',
      is_loading: true,
      users: [],

      first: null,
      previous: null,
      next: null,
      last: null,
      total: 0,
      index: 0,
      status: [],
      roles: [],
      filters: {},

      error: '',
      c_res: -1,
      showModal: false,
      changeModal: false,
      confirmModal: false,
      alertModal: false,
      curUuid: null,
      alert: null,
      activeUser: null,

      sort_direction_name: false,
      sort_direction_role: true,
      sort_direction_email: true,
      sort_direction_status: true,
    }
    
    this.setData = this.setData.bind(this)
    this.fetchData = this.fetchData.bind(this)
    this.filterCheck = this.filterCheck.bind(this)
    this.clearFilter = this.clearFilter.bind(this)
    this.doneFilter = this.doneFilter.bind(this)
    this.first = this.first.bind(this)
    this.previous = this.previous.bind(this)
    this.next = this.next.bind(this)
    this.last = this.last.bind(this)
    this.setParam = this.setParam.bind(this)
  }
  filterCheck(e) {
    const name = e.target.name
    let filters = Object.assign({}, this.state.filters)
    filters[name] = !filters[name]
    this.setState({filters: filters})
  }

  clearFilter() {
    let filters = {}
    this.state.roles.map((role) => filters['role_'+role.id_role] = false)
    this.state.status.map((status) => filters['status_'+status.id_status] = false)
    this.setState({filters: filters,search:''}, () => {
      emitter('searchBar',true)
      this.fetchData()
    })
  }

  async doneFilter() {
    const { roles, status, filters } = this.state
    let result;
    var filter_status = 'filter_status='
    var filter_role = 'filter_role='

    roles.map((role) => {
      if (filters['role_'+role.id_role]) filter_role += role.id_role + ','
      return role;
    })

    status.map((status) => {
      if (filters['status_'+status.id_status]) filter_status += status.id_status + ','
      return status;
    })

    if (filter_status === 'filter_status=') {
      if (filter_role === 'filter_role=') return;
      result = await getFilter(filter_role.substr(0, filter_role.length-1))
    } else if (filter_role === 'filter_role=') {
      if (filter_status === 'filter_status=') return;
      result = await getFilter(filter_status.substr(0, filter_status.length-1))
    } else {
      result = await getFilter(filter_status.substr(0, filter_status.length-1)+'&' +filter_role.substr(0, filter_role.length-1))
    }
    this.setData(result.response)
    this.setState({search:''},emitter('searchBar',true))
  }

  setParam() {
    const { roles, status, filters ,search} = this.state
    var filter_status = 'filter_status='
    var filter_role = 'filter_role='
    roles.map((role) => {
      if (filters['role_'+role.id_role]) filter_role += role.id_role + ','
      return role;
    })
    status.map((status) => {
      if (filters['status_'+status.id_status]) filter_status += status.id_status + ','
      return status;
    })

    if (filter_status === 'filter_status=') {
      if (filter_role === 'filter_role=') return '';
      return filter_role.substr(0, filter_role.length-1)
    } else if (filter_role === 'filter_role=') {
      if (filter_status === 'filter_status=') return '';
      return filter_status.substr(0, filter_status.length-1)
    } else if(search!==''){
      return ('&query='+this.state.search)
    }else {
      return filter_status.substr(0, filter_status.length-1)+'&' +filter_role.substr(0, filter_role.length-1)
    }
  }

  async fetchData() {
    const result = await getTeams();
    let data = result.response;
    let status = data.Status ? data.Status : []
    let roles = data.Roles ? data.Roles : []
    let filters = {}
    status.map((status) => {
      filters['status_'+status.id_status] = false
      return status
    })

    roles.map((role) => {
      filters['role_'+role.id_role] = false
      return role
    })
    
    let users = data.Users ? data.Users : []
    users.sort((a,b)=>(a.first_name + a.last_name > b.first_name + b.last_name) ? 1 : ((b.first_name + b.last_name > a.first_name + a.last_name) ? -1 : 0))
    this.setState({
      is_loading: false,
      users: users,
      first: data.first,
      previous: data.previous,
      next: data.next,
      last: data.last,
      total: data.total,
      index: data.index,
      status: status,
      roles: roles,
      filters: filters,
    })
  }

  async first() {
    if (this.state.first === null)
      return

    const result = await getNav(this.state.first + this.setParam())
    this.setData(result.response)
  }

  async previous() {
    if (this.state.previous === null)
      return
    const result = await getNav(this.state.previous + this.setParam())
    this.setData(result.response)
  }

  async next() {
    if (this.state.next === null)
      return
    const result = await getNav(this.state.next + this.setParam())
    this.setData(result.response)
  }

  async last() {
    if (this.state.last === null)
      return
    const result = await getNav(this.state.last + this.setParam())
    this.setData(result.response)
  }

  changeUser = (user) => {
    this.setState({activeUser: user, showModal: true})
  }

  onCloseShowModal = () => {
    this.setState({showModal: false})
  }

  onCloseChangeModal = () => {
    this.setState({changeModal: false, activeUser: null})
  }

  changeStatus = (changeUser) => {
    this.setState({activeUser: changeUser, showModal: false, changeModal: true})
  }

  onCloseAlertModal = () => {
    this.setState({alertModal: false})
  }

  changeAuth = async (res) => {
    if (res.response.status !== 200)
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    else {
      let newUsers = Object.assign([], this.state.users);
      const { activeUser } = this.state
      const result = await changeAccount({uuid: activeUser.uuid, id_status: activeUser.id_status})
      if (result.response.status !== 200) {
        this.setState({changeModal: false, alertModal: true, error: 'An error has ocurred while updating the member team!', activeUser: null, c_res: result.response.status})
      } else {
        const index = newUsers.findIndex(user => user.uuid === activeUser.uuid)
        newUsers[index].id_status = result.response.user.id_status
        newUsers[index].status = result.response.user.id_status===1 ? 'Available' : 'Inactive'
        this.setState({changeModal: false, alertModal: true, error: 'Your member team has been updated!', users: newUsers, activeUser: null, c_res: result.response.status})
      }
    }
  }
  deleteUser = (uuid) => {
    this.setState({
      alert: (
        <SweetAlert
          warning
          showCancel
          confirmBtnText="Yes, delete it!"
          title="Are you sure to delete this user?"
          onConfirm={this.deleteUserAlert.bind(this, uuid)}
          onCancel={this.closeAlert}
        >
        This action can not be undone
        </SweetAlert>
      )
    });
  }

  closeAlert = () =>{
    this.setState({alert:null})
  }

  deleteUserAlert = (uuid) => {
    this.setState({curUuid: uuid, confirmModal: true, alert: null})
  }

  componentDidMount() {    
    subscribe('search',(value)=> this.dinamycSearch(value))
    emitter('searchBar',true)
    this.fetchData()
  }
  componentWillUnmount =() => {
    unSubscribe('search')
    emitter('searchBar',false)
  }
  dinamycSearch = async (value) =>{
    const result = await getFilter('query='+value)
    this.setState({
      filters: {},
      search:value
    })
    this.setData(result.response)
  }

  setData(data) {    
    let status = data.Status ? data.Status : []
    let roles = data.Roles ? data.Roles : []    
    let users = data.Users ? data.Users : []
    if (this.state.sort_direction_name) {
      users.sort((a,b)=>(a.first_name + a.last_name > b.first_name + b.last_name) ? 1 : ((b.first_name + b.last_name > a.first_name + a.last_name) ? -1 : 0))
    } else {
      users.sort((a,b)=>(a.first_name + a.last_name < b.first_name + b.last_name) ? 1 : ((b.first_name + b.last_name < a.first_name + a.last_name) ? -1 : 0))
    }
    this.setState({
      users: users,
      first: data.first,
      previous: data.previous,
      next: data.next,
      last: data.last,
      total: data.total,
      index: data.index,
      status: status,
      roles: roles,
    })
  }

  sortData = (type) => {
    let users = Object.assign([], this.state.users)
    switch(type) {
      case "name":
        if (this.state.sort_direction_name) {
          users.sort((a,b)=>(a.first_name + a.last_name > b.first_name + b.last_name) ? 1 : ((b.first_name + b.last_name > a.first_name + a.last_name) ? -1 : 0))
        } else {
          users.sort((a,b)=>(a.first_name + a.last_name < b.first_name + b.last_name) ? 1 : ((b.first_name + b.last_name < a.first_name + a.last_name) ? -1 : 0))
        }
        this.setState({sort_direction_name: !this.state.sort_direction_name, users: users})
        break;
      case "role":
        if (this.state.sort_direction_role) {
          users.sort((a,b)=>(a.role > b.role) ? 1 : ((b.role > a.role) ? -1 : 0))
        } else {
          users.sort((a,b)=>(a.role < b.role) ? 1 : ((b.role < a.role) ? -1 : 0))
        }
        this.setState({sort_direction_role: !this.state.sort_direction_role, users: users})
        break;
      case "email":
        if (this.state.sort_direction_email) {
          users.sort((a,b)=>(a.email > b.email) ? 1 : ((b.email > a.email) ? -1 : 0))
        } else {
          users.sort((a,b)=>(a.email < b.email) ? 1 : ((b.email < a.email) ? -1 : 0))
        }
        this.setState({sort_direction_email: !this.state.sort_direction_email, users: users})
        break;
      case "status":
        if (this.state.sort_direction_status) {
          users.sort((a,b)=>(a.status > b.status) ? 1 : ((b.status > a.status) ? -1 : 0))
        } else {
          users.sort((a,b)=>(a.status < b.status) ? 1 : ((b.status < a.status) ? -1 : 0))
        }
        this.setState({sort_direction_status: !this.state.sort_direction_status, users: users})
        break;
      default:
        break;
    }
  }

  renderUsers() {
    const { users } = this.state
    return users.map((user, index) => {
      return (
        <tr key={index}>
          <td>{user.first_name + ' ' + user.last_name}</td>
          <td>{user.role}</td>
          <td>{user.email}</td>
          <td>
            <div className={user.status === 'Available' ? 'available' :
              user.status === 'Inactive' ? 'inactive' : ''} onClick={()=>{this.changeUser(user)}}>
              {user.status}
            </div>
          </td>
          <td>
            <div className="action">
              <div className="edit" onClick={()=>{this.props.history.push('/admin/add-member/' + user.uuid)}}>
              <img alt="" src={require("assets/img/icon_edit.svg")}/>
              </div>
              <div className="delete" onClick={()=>{this.deleteUser(user.uuid)}}>
              <img alt="" src={require("assets/img/icon_delete.svg")}/>
              </div>
            </div>
          </td>
        </tr>
      );
    });
  }

  onCloseConfirmModal = () => {
    this.setState({confirmModal: false})
  }

  authenticate = async (res) => {
    let newUsers = Object.assign([], this.state.users);
    if (res.response.status !== 200)
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    else {
      const result = await deleteAccount({uuid: this.state.curUuid})
      if (result.response.status !== 200)
        toast.error(result.response.message, {position: toast.POSITION_TOP_RIGHT})

      const index = newUsers.findIndex(user => user.uuid === this.state.curUuid)
      newUsers.splice(index, 1)
      this.setState({
        alert: (
          <SweetAlert success title="User has been deleted!" onConfirm={this.closeAlert}>        
          </SweetAlert>
        ),
        confirmModal: false, users: newUsers, curUuid: null, total: this.state.total-1
      })
    }
  }
  render() {
    const disableStyle = {
      color: '#9a9a9a'
    }
    const { first, next, previous, last, index, total, users, status, roles, filters } = this.state
    const { childrens } = this.props
    return (
      <React.Fragment>
        <ResponsiveModal open={this.state.is_loading} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LoadingModal title="Loading..."/>
        </ResponsiveModal>
        {this.state.alert}
        <div className="content" id="admin-team">
          <Row>
            <Col md="12">
              <CardHeader className="management-header">
                <Col md='4'>
                  <CardTitle tag="h4">Admin Team</CardTitle>
                </Col>
                <Col md='4' className='funtionSpace'>
                  {childrens.indexOf(3)>=0?
                  <Link to='/admin/add-member'>
                    <button className='btn-add btn-cosmic'>
                      <i className="tim-icons icon_add" />
                      <span>Add Team Member</span>
                    </button> 
                  </Link>:null}
                </Col>                                
                <Col md='4'className='filterSpace'>
                  <div className="tools tools-group">
                    <UncontrolledDropdown>
                      <DropdownToggle
                        caret
                        className="btn-filter"
                        data-toggle="dropdown"
                      >
                        Filter
                      </DropdownToggle>
                      <DropdownMenu right>
                        <div className="filter-header">
                          <div className="filter-header-group">
                            <div className="filter-text">Filters</div>
                            <div className="filter-btn-group">
                              <div className="filter-clear" onClick={this.clearFilter}>Clear</div>
                              <div className="filter-done" onClick={this.doneFilter}>Done</div>
                            </div>
                          </div>
                        </div>

                        {status.map((status, index) =>  <div className="filter-check" key={index}>
                          <input id={status.status} type="checkbox" name={'status_'+status.id_status} checked={filters['status_'+status.id_status]} onChange={this.filterCheck}/>
                          <label htmlFor={status.status}>{status.status}</label>
                        </div>)}

                        {roles.map((role, index) =>  <div className="filter-check" key={index}>
                          <input id={role.role} type="checkbox" name={'role_'+role.id_role} checked={filters['role_'+role.id_role]} onChange={this.filterCheck}/>
                          <label htmlFor={role.role}>{role.role}</label>
                        </div>)}
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </div>
                </Col>
              </CardHeader>
            </Col>
            <Col md="12">
              <Card>                
                <CardBody>
                  <Table>
                    <thead className="text-primary">
                      <tr>
                        {/* <th>#</th> */}
                        <th onClick={()=>{this.sortData('name')}}>Name <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('role')}}>Role <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('email')}}>Email <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('status')}}>Status <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody className="management-body">{this.renderUsers()}</tbody>
                  </Table>
                  {users.length === 0 && <div className="no-result">No results found</div>}
                  {users.length > 0 && 
                  <div className="nav-group">
                    <div><span>Showing</span></div>
                    <div className="first" style={first===null?disableStyle:{}} onClick={this.first}><i className="fa fa-angle-double-left"></i></div>
                    <div className="previous" style={previous===null?disableStyle:{}} onClick={this.previous}><i className="fa fa-angle-left"></i></div>
                    <div className="index"><span>{index}</span></div>
                    <div><span>{'to'}</span></div>
                    <div className="count"><span>{users.length === 0 ? 0 : index + users.length - 1}</span></div>
                    <div><span>{'of'}</span></div>
                    <div className="total"><span>{new Intl.NumberFormat().format(total)}</span></div>
                    <div className="next" style={next===null?disableStyle:{}} onClick={this.next}><i className="fa fa-angle-right"></i></div>
                    <div className="last" style={last===null?disableStyle:{}} onClick={this.last}><i className="fa fa-angle-double-right"></i></div>
                  </div>
                  }
                </CardBody>
              </Card>
            </Col>
          </Row>
          <ResponsiveModal open={this.state.showModal} onClose={this.onCloseShowModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
            <ChangeModal onClose={this.onCloseShowModal} onChange={this.changeStatus} user={this.state.activeUser} status={this.state.status}/>
          </ResponsiveModal>
          <ResponsiveModal open={this.state.changeModal} onClose={this.onCloseChangeModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
            <ConfirmModal onClose={this.onCloseChangeModal} onAuthenticate={this.changeAuth}/>
          </ResponsiveModal>
          <ResponsiveModal open={this.state.alertModal} onClose={this.onCloseAlertModal} center classNames={{'modal':'alert-modal','closeButton':'modal-close-button'}}>
            <AlertModal onClose={this.onCloseAlertModal} status={this.state.c_res} msj={this.state.error}/>
          </ResponsiveModal>
          <ResponsiveModal open={this.state.confirmModal} onClose={this.onCloseConfirmModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
            <ConfirmModal onClose={this.onCloseConfirmModal} onAuthenticate={this.authenticate}/>
          </ResponsiveModal>
          <ToastContainer />
        </div>
      </React.Fragment>
    );
  }
}

export default AdminTeam;
