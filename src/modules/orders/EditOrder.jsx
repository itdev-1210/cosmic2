import React from 'react';

// reactstrap components
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Row,
  Col,
  Table,
  Modal,
  ModalBody,
} from 'reactstrap';
import Moment from 'react-moment'
import ResponsiveModal from 'react-responsive-modal';
import LoadingModal from './../../components/Navbars/LogoutModal'
import {getOrder} from '../../api/OrdersApi'
import './detail.scss'

import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow } from 'react-google-maps';

const defaultMapOptions = {
  disableDefaultUI: true
}

const RegularMap = withScriptjs(
  withGoogleMap((props) => {
    return (
      <GoogleMap
        defaultZoom={16}
        defaultCenter={{ lat: parseFloat(props.type === 'start' ? props.data.start_coords_lat : props.data.end_coords_lat), lng: parseFloat(props.type === 'start' ? props.data.start_coords_lon : props.data.end_coords_lon) }}
        defaultOptions={defaultMapOptions}
        onClick={props.onMapClick}
      >
        <Marker 
          position={{ lat: parseFloat(props.type === 'start' ? props.data.start_coords_lat : props.data.end_coords_lat), lng: parseFloat(props.type === 'start' ? props.data.start_coords_lon : props.data.end_coords_lon) }} 
          icon={props.type === 'start' ? require("assets/img/mark_start.svg") : require("assets/img/mark_end.svg")}
          onClick={props.onMarkerClick}>
          {props.show_info ? 
            <InfoWindow 
              onCloseClick={props.onMarkerClose}
              >
              <div>
                <div className="info-window">
                  <div className="info-address">{props.type === 'start' ? props.data.start_address : props.data.end_address}</div>
                  <div className="info-type">{props.type === 'start' ? 'Pick Up Address' : 'Delivery Information'}</div>
                </div>
              </div>
            </InfoWindow>
          : null}
        </Marker>
      </GoogleMap>
    );
  })
);

class EditOrder extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      service: {},
      service_events: [],
      modalPosition: false,
      type: '',
      show_info: false,

      is_loading: true,
    }
    this.fetchData = this.fetchData.bind(this)
    this.setData = this.setData.bind(this)
  }

  setData(data) {
    this.setState({
      is_loading: false,
      service: data.service,
      service_events: data.service_events ? data.service_events : []
    })
  }

  async fetchData(id_service) {
    const result = await getOrder(id_service);
    if (result.status === 200) {
      this.setData(result)
    }
  }
  componentWillMount() {
    const { match: { params } } = this.props;
    this.fetchData(params.id_service)
  }

  renderEvents = () => {
    const { service_events } = this.state;
    // return service_events.map((event, index) => (
    //   <tr key={index}>
    //     <td className="text-left"><Moment format="MMM DD, YYYY h:mm A">{event.date}</Moment></td>
    //     <td>{event.first_name + ' ' + event.last_name}</td>
    //     <td>{event.email}</td>
    //     <td>{event.phone}</td>
    //     <td>{event.status}</td>
    //   </tr>
    // ));
  }

  handleMarkerClick = () => {
    this.setState({show_info: true})
  }

  handleMarkerClose = () => {
    this.setState({show_info: false})
  }

  renderModalPosition() {
    return (
      <Modal isOpen={this.state.modalPosition} toggle={()=>{this.setState({modalPosition: false, show_info: false})}}>
        <div className="modal-header">
          <button aria-hidden={true} className="close" data-dismiss="modal" type="button" onClick={()=>{this.setState({modalPosition: false, show_info: false})}}>
            <i className="tim-icons icon-simple-remove" />
          </button>
          <h6 className="title title-up">{this.state.service.order_id + ' | ' + this.state.service.device + ' | ' + this.state.service.id_device}</h6>
        </div>
        <ModalBody className="text-center">
          <div className="map" id="order-map">
            <RegularMap
              type={this.state.type}
              data={this.state.service}
              show_info={this.state.show_info}
              onMarkerClick={this.handleMarkerClick}
              onMarkerClose={this.handleMarkerClose}
              onMapClick={this.handleMarkerClose}
              googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDASQ1gDtuxVXsoeJZHXqsNRASQ17yVoEI"
              loadingElement={<div style={{ height: `100%` }} />}
              containerElement={
                <div
                  style={{
                    height: `500px`,
                    borderRadius: `.2857rem`,
                    overflow: `hidden`
                  }}
                />
              }
              mapElement={<div style={{ height: `100%` }} />}
            />
          </div>
        </ModalBody>
      </Modal>
    );
  }

  showModal = (type) => {
    this.setState({type: type, modalPosition: true})
  }
  render() {
    const { service } = this.state
    return (
      <React.Fragment>
        <ResponsiveModal open={this.state.is_loading} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LoadingModal title="Loading..."/>
        </ResponsiveModal>
        {this.renderModalPosition()}
        <div className="content" id="edit-order">
          <Row>
            <Col md='12'>
              <CardHeader className="management-header">
                <Col md='4'>
                  <CardTitle tag="h4" onClick={()=>{this.props.history.push('/admin/orders')}}>
                    <img alt="" src={require("assets/img/icon_arrow.svg")}/>
                    <img alt="" src={require("assets/img/icon_orders.svg")}/>
                    {service.order_id}
                  </CardTitle>
                </Col>  
              </CardHeader>            
            </Col>
            <Col md="12">
              <Card>
                <CardBody>
                  <div className="one-content">
                    <div className="sub-title">Lessee Information</div>
                    <div className="sub-content">
                      <Row>
                        <Col md="6">
                          <Row className="one-content">
                            <Col md="2" className="sub-header">CUID</Col>
                            <Col md="8" className="sub-data">{service.cuid}</Col>
                          </Row>
                          <Row className="one-content">
                            <Col md="2" className="sub-header">Name</Col>
                            <Col className="sub-data">{service.first_name + ' ' + service.last_name}</Col>
                          </Row>
                          <Row className="one-content">
                            <Col md="2" className="sub-header">Email</Col>
                            <Col md="8" className="sub-data">{service.email}</Col>
                          </Row>
                          <Row className="one-content">
                            <Col md="2" className="sub-header">Phone</Col>
                            <Col md="8" className="sub-data">{service.phone}</Col>
                          </Row>
                          <Row className="one-content">
                            <Col md="2" className="sub-header">Country</Col>
                            <Col md="8" className="sub-data">{service.user_country}</Col>
                          </Row>
                        </Col>
                        <Col md="6">
                          <Row className="one-content">
                            <Col md="4" className="sub-header">Payment method</Col>
                          </Row>
                          <Row className="one-content">
                            <Col md="8" className="sub-data">
                              <img alt="" src={require("assets/img/icon_credit_card_visa.png")}/>{(service.brand ? service.brand : '') + ' ' + (service.last_four ? service.last_four : '')}                              
                            </Col>
                          </Row>
                          <Row className="one-content">
                            <Col md="4" className="sub-header">Payment Status</Col>
                          </Row>
                          <Row className="one-content">
                            <Col md="8" className="sub-data">{service.status}</Col>
                          </Row>
                        </Col>
                      </Row>
                    </div>
                  </div>
                  <div className="dash-line"></div>
                  <div className="one-content">
                    <Row>
                      <Col md="6">
                        <div className="sub-title">Delivery Information</div>
                        <div className="sub-content">
                          <Row>
                            <Col md="12">
                              <Row className="one-content">
                                <Col md="2" className="sub-header">Country</Col>
                                <Col md="8" className="sub-data">{service.service_country}</Col>
                              </Row>
                              <Row className="one-content">
                                <Col md="2" className="sub-header">State</Col>
                                <Col className="sub-data">{service.service_state}</Col>
                              </Row>
                              <Row className="one-content">
                                <Col md="2" className="sub-header">City</Col>
                                <Col md="8" className="sub-data">{service.service_city}</Col>
                              </Row>
                              <Row className="one-content">
                                <Col md="2" className="sub-header">Street</Col>
                                <Col md="8" className="sub-data">
                                  {service.end_address}
                                  <img alt="" src={require("assets/img/icon_gps_fixed.svg")} onClick={()=>this.showModal('end')}/>
                                </Col>
                              </Row>
                              <Row className="one-content">
                                <Col md="2" className="sub-header">Date</Col>
                                <Col md="8" className="sub-data"><Moment format="MMM DD, YYYY hh:MM A">{service.end_time_service}</Moment></Col>
                              </Row>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                      <Col md="6">
                        <div className="sub-title">Pick up Information</div>
                        <div className="sub-content">
                          <Row>
                            <Col md="12">
                              <Row className="one-content">
                                <Col md="2" className="sub-header">Country</Col>
                                <Col md="8" className="sub-data">{service.service_country}</Col>
                              </Row>
                              <Row className="one-content">
                                <Col md="2" className="sub-header">State</Col>
                                <Col className="sub-data">{service.service_state}</Col>
                              </Row>
                              <Row className="one-content">
                                <Col md="2" className="sub-header">City</Col>
                                <Col md="8" className="sub-data">{service.service_city}</Col>
                              </Row>
                              <Row className="one-content">
                                <Col md="2" className="sub-header">Street</Col>
                                <Col md="8" className="sub-data">
                                  {service.start_address}
                                  <img alt="" src={require("assets/img/icon_gps_fixed.svg")} onClick={()=>this.showModal('start')}/>
                                </Col>
                              </Row>
                              <Row className="one-content">
                                <Col md="2" className="sub-header">Date</Col>
                                <Col md="8" className="sub-data"><Moment format="MMM DD, YYYY hh:MM A">{service.start_time_service}</Moment></Col>
                              </Row>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                    </Row>
                  </div>

                  <div className="dash-line"></div>

                  <div className="one-content">
                    <div className="sub-title">Events</div>
                    <div className="sub-content">
                      <Table>
                        <thead>
                          <tr>
                            <th>Date <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                            <th>Staff Assigned <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                            <th>Email <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                            <th>Phone <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                            <th>Status <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          </tr>
                        </thead>
                        <tbody>{this.renderEvents()}</tbody>
                      </Table>
                    </div>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </React.Fragment>
    );
  }
}

export default EditOrder;
