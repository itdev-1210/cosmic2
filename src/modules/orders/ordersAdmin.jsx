import React from 'react';

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Table,
  Row,
  Button,
  Col,
  Modal,
  ModalBody,
  DropdownToggle,
  DropdownMenu,
  UncontrolledDropdown,
} from 'reactstrap';
import { ToastContainer, toast } from 'react-toastify'
import Moment from 'react-moment'
import Scrollbar from 'perfect-scrollbar-react';
import ResponsiveModal from 'react-responsive-modal';
import ConfirmModal from './../../views/pages/ConfirmModal'
import StatusModal from './Modals/statusModal'
import LoadingModal from './../../components/Navbars/LogoutModal'
import {getAllOrders, getNav, getFailedOrders, getFilter} from '../../api/OrdersApi';
import './orders.scss'
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow } from 'react-google-maps';
import auth from '../../api/auth';

const defaultMapOptions = {
  disableDefaultUI: true
}
const RegularMap = withScriptjs(
  withGoogleMap((props) => {
    return (
      <GoogleMap
        defaultZoom={16}
        defaultCenter={{ lat: parseFloat(props.curOrder.start_coords_lat), lng: parseFloat(props.curOrder.start_coords_lon) }}
        defaultOptions={defaultMapOptions}
        onClick={()=>{props.onMapClick('any')}}
      >
        <Marker 
          position={{ lat: parseFloat(props.curOrder.start_coords_lat), lng: parseFloat(props.curOrder.start_coords_lon) }} 
          icon={require("assets/img/mark_start.svg")}
          onClick={()=>{props.onMarkerClick('start')}}>
          {props.show_start_info ? 
            <InfoWindow 
              onCloseClick={()=>props.onMarkerClose('start')}
              >
              <div>
                <div className="info-window">
                  <div className="info-address">{props.curOrder.start_address}</div>
                  <div className="info-type">{'Pick Up Address'}</div>
                </div>
              </div>
            </InfoWindow>
          : null}
        </Marker>
        <Marker 
          position={{ lat: parseFloat(props.curOrder.end_coords_lat), lng: parseFloat(props.curOrder.end_coords_lon) }} 
          icon={require("assets/img/mark_end.svg")}
          onClick={()=>{props.onMarkerClick('end')}}>>
          {props.show_end_info ? 
            <InfoWindow 
              onCloseClick={()=>props.onMarkerClose('end')}
              >
              <div>
                <div className="info-window">
                  <div className="info-address">{props.curOrder.end_address}</div>
                  <div className="info-type">{'Delivery Address'}</div>
                </div>
              </div>
            </InfoWindow>
          : null}
        </Marker>
      </GoogleMap>
    );
  })
);

class AdminOrders extends React.Component {
  constructor(params) {
    super(params)
    this.state = {
      orders: [],
      status: [],
      filters: {},

      sort_direction_order_id: false,
      sort_direction_name: true,
      sort_direction_start_date: true,
      sort_direction_end_date: true,
      sort_direction_device: true,
      sort_direction_id_device: true,
      sort_direction_status: true,

      show_order_failed: false,

      curOrder: {},
      modalPosition: false,
      show_start_info: false,
      show_end_info: false,
      first:null,
      index:0,
      last:null,
      next: null,
      previous: null,
      total: 13,
      
      curOrderStatus:null,
      statusModal:false,
      is_loading: true,
    }
    this.fetchInitialData = this.fetchInitialData.bind(this)
    this.first = this.first.bind(this)
    this.previous = this.previous.bind(this)
    this.next = this.next.bind(this)
    this.last = this.last.bind(this)
    this.setData = this.setData.bind(this)
    this.filterCheck = this.filterCheck.bind(this)
    this.clearFilter = this.clearFilter.bind(this)
    this.doneFilter = this.doneFilter.bind(this)
    this.setParam = this.setParam.bind(this)
  }

  setData(data) {
    let orders = data.Orders ? data.Orders : []
    orders.sort((a,b)=>(a.order_id > b.order_id) ? 1 : ((b.order_id > a.order_id) ? -1 : 0))
    this.setState({
      orders: orders,
      first: data.first,
      previous: data.previous,
      next: data.next,
      last: data.last,
      total: data.total,
      index: data.index,
      status: data.Status ? data.Status : [],
    })
  }

  filterCheck(e) {
    const name = e.target.name
    let filters = Object.assign({}, this.state.filters)
    filters[name] = !filters[name]
    this.setState({filters: filters})
  }

  clearFilter() {
    let filters = {}
    this.state.status.map((status) => filters['status_'+status.id_status_service] = false)
    this.setState({filters: filters}, () => {
      this.state.show_order_failed ? this.fetchFailedData() : this.fetchInitialData()
    })
  }

  async doneFilter() {
    const { status, filters } = this.state
    let result;
    var filter_status = 'filter_status='

    status.map((status) => {
      if (filters['status_'+status.id_status_service]) filter_status += status.id_status_service + ','
      return status;
    })

    if (filter_status === 'filter_status=') {
      return;
    } else {
      result = await getFilter(filter_status.substr(0, filter_status.length-1))
    }
    this.setData(result)
  }

  setParam() {
    return ''
  }

  async first() {
    if (this.state.first === null)
      return
  
    const result = await getNav(this.state.first + this.setParam())
    this.setData(result)
  }
  
  async previous() {
    if (this.state.previous === null)
      return
    const result = await getNav(this.state.previous + this.setParam())
    this.setData(result)
  }
  
  async next() {
    if (this.state.next === null)
      return
    const result = await getNav(this.state.next + this.setParam())
    this.setData(result)
  }
  
  async last() {
    if (this.state.last === null)
      return
    const result = await getNav(this.state.last + this.setParam())
    this.setData(result)
  }

  async fetchInitialData() {
    const res = await getAllOrders();
    if(res.st && res.status===200){
      let orders = res.Orders ? res.Orders : []
      orders.sort((a,b)=>(a.order_id > b.order_id) ? 1 : ((b.order_id > a.order_id) ? -1 : 0))
      let status = res.Status ? res.Status : []
      let filters = {}
      status.map((status) => {
        filters['status_'+status.id_status_service] = false
        return status
      })
      this.setState({
        is_loading: false,
        orders:   orders,
        first:    res.first,
        previous: res.previous,
        next:     res.next,
        last:     res.last,
        total:    res.total,
        index:    res.index,
        status:   status,
        filters:  filters,
        show_order_failed: false
      })
    }
    else{
      toast.error(res.message, {position: toast.POSITION_TOP_RIGHT})     
      this.setState({
        is_loading: false, 
      })
    }
  }

  fetchFailedData = async () => {
    const res = await getFailedOrders();
    if(res.st && res.status===200){
      let orders = res.Orders ? res.Orders : []
      orders.sort((a,b)=>(a.order_id > b.order_id) ? 1 : ((b.order_id > a.order_id) ? -1 : 0))
      let status = res.Status ? res.Status : []
      let filters = {}
      status.map((status) => {
        filters['status_'+status.id_status] = false
        return status
      })
      this.setState({
        is_loading: false,
        orders:   orders,
        first:    res.first,
        previous: res.previous,
        next:     res.next,
        last:     res.last,
        total:    res.total,
        index:    res.index,
        status:   status,
        filters:  filters,
        show_order_failed: true,
      })
    }
    else{
      toast.error(res.message, {position: toast.POSITION_TOP_RIGHT})      
    }
  }
  
  componentDidMount() {
    this.fetchInitialData()
  }

  showLocation = (curOrder) => {
    this.setState({curOrder: curOrder, modalPosition: true})
  }

  handleMarkerClick = (type) => {
    type === 'start' ? this.setState({show_start_info: true, show_end_info: false}) : this.setState({show_end_info: true, show_start_info: false})
  }

  handleMarkerClose = (type) => {
    type === 'start' ? this.setState({show_start_info: false}) : type === 'end' ? this.setState({show_end_info: false}) : this.setState({show_start_info: false, show_end_info: false})
  }

  renderModalPosition() {
    return (
      <Modal isOpen={this.state.modalPosition} toggle={()=>{this.setState({modalPosition: false, show_start_info: false, show_end_info: false})}}>
        <div className="modal-header">
          <button aria-hidden={true} className="close" data-dismiss="modal" type="button" onClick={()=>{this.setState({modalPosition: false, show_start_info: false, show_end_info: false})}}>
            <i className="tim-icons icon-simple-remove" />
          </button>
          <h6 className="title title-up">{this.state.curOrder.order_id + ' | ' + this.state.curOrder.device + ' | ' + this.state.curOrder.id_device}</h6>
        </div>
        <ModalBody className="text-center">
          <div className="map" id="order-map">
            <RegularMap
              curOrder={this.state.curOrder}
              show_start_info={this.state.show_start_info}
              show_end_info={this.state.show_end_info}
              onMarkerClick={this.handleMarkerClick}
              onMarkerClose={this.handleMarkerClose}
              onMapClick={this.handleMarkerClose}
              googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDASQ1gDtuxVXsoeJZHXqsNRASQ17yVoEI"
              loadingElement={<div style={{ height: `100%` }} />}
              containerElement={
                <div
                  style={{
                    height: `500px`,
                    borderRadius: `.2857rem`,
                    overflow: `hidden`
                  }}
                />
              }
              mapElement={<div style={{ height: `100%` }} />}
            />
          </div>
        </ModalBody>
      </Modal>
    );
  }

  changeStatus = (order) =>{
    this.setState({curOrderStatus:order,statusModal:true})
  }

  renderOrders() {
    const { orders } = this.state
    return orders.map((order, index) => (
      <tr key={index}>
        <td>{order.order_id}</td>
        <td>{order.first_name+' '+order.last_name}</td>
        <td><Moment format="MMM DD, YYYY">{order.start_time_service}</Moment></td>
        <td>{order.end_time_service?<Moment format="MMM DD, YYYY">{order.end_time_service}</Moment>:'No End'}</td>
        <td>{order.device}</td>
        <td>{order.id_device}</td>
        <td>
          <div style={{'cursor':'pointer'}} className={
            order.status === 'Pending Delivery' ? 'status_pending_d' :
            order.status === 'On Delivery' ? 'status_on_delivery' :
            order.status === 'Delivered' ? 'status_delivered' :
            order.status === 'Pending return' ? 'status_pending_r' :
            order.status === 'On Return' ? 'status_on_return' :
            order.status === 'Returned' ? 'status_returned' :
            order.status === 'Canceled' ? 'cancel' :
            ''} 
            onClick={() => this.changeStatus(order)}>
          {order.status}
          </div>
        </td>
        <td>
            <Button
              onClick={()=>{this.props.history.push('/admin/edit-order/'+order.id_service);}}              
              color="default"
              size="sm"
              className="btn-icon btn-link"
            >
            <img id={order.id_operator} alt="" src={require("assets/img/icon_edit.svg")}/>
            </Button>
            <Button    
              onClick={()=>{this.showLocation(order)}}
              color="default"
              size="sm"
              className="btn-icon btn-link"
            >
            <i className="tim-icons icon-gps-fixed" />
            </Button>
        </td>
      </tr>
    ));
  }

  sortData = (type) => {
    let orders = Object.assign([], this.state.orders)
    switch(type) {
      case "order_id":
        if (this.state.sort_direction_order_id) {
          orders.sort((a,b)=>(a.order_id > b.order_id) ? 1 : ((b.order_id > a.order_id) ? -1 : 0))
        } else {
          orders.sort((a,b)=>(a.order_id < b.order_id) ? 1 : ((b.order_id < a.order_id) ? -1 : 0))
        }
        this.setState({sort_direction_order_id: !this.state.sort_direction_order_id, orders: orders})
        break;
      case "name":
        if (this.state.sort_direction_name) {
          orders.sort((a,b)=>(a.first_name + a.last_name > b.first_name + b.last_name) ? 1 : ((b.first_name + b.last_name > a.first_name + a.last_name) ? -1 : 0))
        } else {
          orders.sort((a,b)=>(a.first_name + a.last_name < b.first_name + b.last_name) ? 1 : ((b.first_name + b.last_name < a.first_name + a.last_name) ? -1 : 0))
        }
        this.setState({sort_direction_name: !this.state.sort_direction_name, orders: orders})
        break;
      case "start_date":
        if (this.state.sort_direction_start_date) {
          orders.sort((a,b)=>(a.start_time_service > b.start_time_service) ? 1 : ((b.start_time_service > a.start_time_service) ? -1 : 0))
        } else {
          orders.sort((a,b)=>(a.start_time_service < b.start_time_service) ? 1 : ((b.start_time_service < a.start_time_service) ? -1 : 0))
        }
        this.setState({sort_direction_start_date: !this.state.sort_direction_start_date, orders: orders})
        break;
      case "end_date":
        if (this.state.sort_direction_end_date) {
          orders.sort((a,b)=>(a.end_time_service > b.end_time_service) ? 1 : ((b.end_time_service > a.end_time_service) ? -1 : 0))
        } else {
          orders.sort((a,b)=>(a.end_time_service < b.end_time_service) ? 1 : ((b.end_time_service < a.end_time_service) ? -1 : 0))
        }
        this.setState({sort_direction_end_date: !this.state.sort_direction_end_date, orders: orders})
        break;
      case "device":
        if (this.state.sort_direction_device) {
          orders.sort((a,b)=>(a.device > b.device) ? 1 : ((b.device > a.device) ? -1 : 0))
        } else {
          orders.sort((a,b)=>(a.device < b.device) ? 1 : ((b.device < a.device) ? -1 : 0))
        }
        this.setState({sort_direction_device: !this.state.sort_direction_device, orders: orders})
        break;
      case "id_device":
        if (this.state.sort_direction_id_device) {
          orders.sort((a,b)=>(a.id_device > b.id_device) ? 1 : ((b.id_device > a.id_device) ? -1 : 0))
        } else {
          orders.sort((a,b)=>(a.id_device < b.id_device) ? 1 : ((b.id_device < a.id_device) ? -1 : 0))
        }
        this.setState({sort_direction_id_device: !this.state.sort_direction_id_device, orders: orders})
        break;
      case "status":
        if (this.state.sort_direction_status) {
          orders.sort((a,b)=>(a.status > b.status) ? 1 : ((b.status > a.status) ? -1 : 0))
        } else {
          orders.sort((a,b)=>(a.status < b.status) ? 1 : ((b.status < a.status) ? -1 : 0))
        }
        this.setState({sort_direction_status: !this.state.sort_direction_status, orders: orders})
        break;
      default:
        break;
    }
  }

  showFailedOrder = () => {
    this.setState({is_loading: true}, ()=>{
      this.fetchFailedData()
    })
  }

  showAllOrder = () => {
    this.fetchInitialData()
  }

  render() {
    const { orders, first, previous, next, last, total, index,
           filters, show_order_failed,status,curOrderStatus} = this.state
    const disableStyle = {
      color: '#9a9a9a'
    }
    return (
      <React.Fragment>
        {this.renderModalPosition()}
        <Col md='12' className="content">
          <Row>
            <Col md="12">
              <CardHeader className="management-header">
                <Col md='4'>
                  {show_order_failed ? 
                  <CardTitle tag="h4" onClick={this.showAllOrder}>
                    <img alt="" src={require("assets/img/icon_arrow.svg")}/>
                    Orders Failed
                  </CardTitle>
                  :
                  <CardTitle tag="h4">
                    Admin Orders
                  </CardTitle>}
                </Col>
                {!show_order_failed ? 
                <Col md='2' className='funtionSpace'>
                  <button className='btn-add btn-cosmic' onClick={this.showFailedOrder}>
                    <img alt="" src={require("assets/img/icon_failed_order.svg")}/>
                    <span>Orders Failed</span>
                  </button>
                </Col>
                : null}
                <Col md='4' className='filterSpace'>
                  <div className="tools tools-group">
                    <UncontrolledDropdown>
                      <DropdownToggle
                        caret
                        className="btn-filter"
                        data-toggle="dropdown"
                      >
                        Filter
                      </DropdownToggle>
                      <DropdownMenu right>
                        <div className="filter-header">
                          <div className="filter-header-group">
                            <div className="filter-text">Filters</div>
                            <div className="filter-btn-group">
                              <div className="filter-clear" onClick={this.clearFilter}>Clear</div>
                              <div className="filter-done" onClick={this.doneFilter}>Done</div>
                            </div>
                          </div>
                        </div>

                        {status.map((status, index) =>  <div className="filter-check" key={index}>
                          <input id={status.status} type="checkbox" name={'status_'+status.id_status_service} checked={filters['status_'+status.id_status_service]} onChange={this.filterCheck}/>
                          <label htmlFor={status.status}>{status.status}</label>
                        </div>)}
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </div>
                </Col>
              </CardHeader>
            </Col>
          </Row>
          <Row style={{flexDirection: 'column'}}>
            <Scrollbar
              options={{suppressScrollX:true}}
            >
              <Col md="12" className='into-Scroll'>
                <Card>
                  <CardBody>
                    <Table>
                      <thead>
                        <tr>
                          <th onClick={()=>{this.sortData('order_id')}}>Order ID <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th onClick={()=>{this.sortData('name')}}>Lessee Name <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th onClick={()=>{this.sortData('start_date')}}>Start Date <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th onClick={()=>{this.sortData('end_date')}}>End Date <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th onClick={()=>{this.sortData('device')}}>Vehicle Type <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th onClick={()=>{this.sortData('id_device')}}>ID Vehicle <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th onClick={()=>{this.sortData('status')}}>Status Service <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>{this.renderOrders()}</tbody>
                    </Table>
                    {orders.length === 0 && <div className="empty-table">No orders to show yet</div>}
                  </CardBody>
                  <CardFooter>
                    {orders.length > 0 &&
                      <div className="nav-group" style={{padding: '7px 15px'}}>
                        <div><span>Showing</span></div>
                        <div className="first" style={first===null?disableStyle:{}} onClick={this.first}><i className="fa fa-angle-double-left"></i></div>
                        <div className="previous" style={previous===null?disableStyle:{}} onClick={this.previous}><i className="fa fa-angle-left"></i></div>
                        <div className="index"><span>{index}</span></div>
                        <div><span>{'to'}</span></div>
                        <div className="count"><span>{orders.length}</span></div>
                        <div><span>{'of'}</span></div>
                        <div className="total"><span>{new Intl.NumberFormat().format(total)}</span></div>
                        <div className="next" style={next===null?disableStyle:{}} onClick={this.next}><i className="fa fa-angle-right"></i></div>
                        <div className="last" style={last===null?disableStyle:{}} onClick={this.last}><i className="fa fa-angle-double-right"></i></div>
                      </div>
                    }
                  </CardFooter>
                </Card>
              </Col>
            </Scrollbar>
          </Row>
        </Col>
        <ToastContainer />
        <ResponsiveModal open={this.state.statusModal} onClose={this.onCloseStatusModal} center classNames={{'modal':'dark-modal-status'}}>
          <StatusModal history={this.props.history} onClose={this.onCloseStatusModal}
            orderId={curOrderStatus?curOrderStatus.id_service:''}
            status={status}
            uuid={auth.getUUID()}
            updateData={this.updateData} 
            orderIdStatus={'6'}
          />
        </ResponsiveModal>
        <ResponsiveModal open={this.state.is_loading} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LoadingModal title="Loading..."/>
        </ResponsiveModal>
      </React.Fragment>
    );
  }

  updateData = (res)=>{
    if(res.st && res.status===200){
      toast.success(res.message, {position: toast.POSITION_TOP_RIGHT})
      this.setState({statusModal:false})
    }
    else{toast.error(res.message, {position: toast.POSITION_TOP_RIGHT})}
  }

  onCloseStatusModal = () =>{
    this.setState({statusModal:false})
  }
}

export default AdminOrders;
