import React, { Component } from 'react';
import {
  Button,
  Label,
  FormGroup,
  Input,
  Row,
  Col,
} from 'reactstrap';
import { changeStatus } from '../../../api/OrdersApi';
import './statusModal.scss'

class StatusModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
        orderIdStatus: this.props.orderIdStatus
    }
    this.change = this.change.bind(this)
    this.changeStatus = this.changeStatus.bind(this)
  }

  change(e) {
    this.setState({orderIdStatus: e.target.value})
  }

  async changeStatus() {
    const res = await changeStatus({id_service:this.props.orderId, id_status_service: this.state.orderIdStatus, uuid:this.props.uuid})
    this.props.updateData(res)
  }

  render() {
    const { onClose, orderId, status} = this.props
    const { orderIdStatus } = this.state
    return (
      <div className="change_status">
        <div className="change-title">
          <span className="title">{'Change status'}</span>
        </div>
        <div className="input_group">
          <Row>
            <Label className="id-label">Order ID</Label>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Input
                  name="id"
                  type="text"
                  value={orderId}
                  disabled
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Label className="status-label">Status</Label>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Input
                    name="status"
                    type="select"
                    value={orderIdStatus}
                    className={
                        orderIdStatus.toString() === '6' ? 'status_pending_d':
                        orderIdStatus.toString() === '7' ? 'status_on_delivery':
                        orderIdStatus.toString() === '8' ? 'status_delivered':
                        orderIdStatus.toString() === '9' ? 'status_pending_r':
                        orderIdStatus.toString() === '10' ? 'status_on_return':
                        orderIdStatus.toString() === '11' ? 'status_returned':''}
                    onChange={this.change}
                >
                {status.map((status, index) => <option key={index} value={status.id_status_service} className={
                    status.id_status_service === 6 ? 'status_pending_d':
                    status.id_status_service === 7 ? 'status_on_delivery':
                    status.id_status_service === 8 ? 'status_delivered':
                    status.id_status_service === 9 ? 'status_pending_r':
                    status.id_status_service === 10 ? 'status_on_return':
                    status.id_status_service === 11 ? 'status_returned':
                     ''}>{status.status}</option>)}
                </Input>
              </FormGroup>
            </Col>
          </Row>
        </div>
        <Row className='cosmic-butons butonsUpdateOpe'>
          <Button className="cosmic-cancel" onClick={onClose}>Cancel</Button>
          <Button className="cosmic-confirm" onClick={this.changeStatus}>Change status</Button>
        </Row>
      </div>
    )
  }
}

export default StatusModal