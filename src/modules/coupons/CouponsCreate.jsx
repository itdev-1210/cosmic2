import React from 'react';
import {
  CardHeader,
  CardTitle,
  Row,
  Col
} from "reactstrap";

import FromCoupon from './FormCoupon'

class CouponsCreate extends React.Component {
  constructor(props) {
    super(props)
    this.handler = this.handler.bind(this);
  }

  handler () {this.props.history.push('/admin/admin-coupons')}
    
  render() {
    return (
      <React.Fragment>        
        <div className="content" id="create-coupon">
        <Row> 
          <Col md='12'>
            <CardHeader className="management-header">
              <Col md='4'>
                <CardTitle tag="h4" onClick={()=>{this.props.history.push('/admin/admin-coupons')}}>
                  <img alt="" src={require("assets/img/icon_arrow.svg")}/>Create Coupons
                </CardTitle>
              </Col>  
            </CardHeader>            
          </Col>
          <Col md="12">
            <FromCoupon
              CardTitle='Create New Coupone'
              New={true}
              onActionUpdate={this.handler}
            />
          </Col>
        </Row>
      </div>
      </React.Fragment>
    );
  }
}

export default CouponsCreate;
