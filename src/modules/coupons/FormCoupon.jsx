import React from 'react';
//import classNames from 'classnames';
//import { VectorMap } from 'react-jvectormap';
import Datetime from 'react-datetime';

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Label,
  FormGroup,
  Form,
  Input,
  Row,
  Col
} from "reactstrap";

import {getInfoCoupons, createCoupons, editCoupons} from './../../api/couponsApi'
import SweetAlert from 'react-bootstrap-sweetalert';
import moment from 'moment'
import iconFailCou from '../../assets/img/icon_fail_coupons.png' 
import iconOKCou from '../../assets/img/icon_ok_coupons.png' 

class FormCoupon extends React.Component {
  constructor(props) {
    super(props);    
    this.valid = {
      name:'',
      start:'',
      end:'',
      type:'',
      value:'',
      cur:'',
      country:'',
      id_device_type:'',
    }
    this.state = {
      name:'',
      start_date:'',
      expiration_date:'',
      id_coupon_type:'',
      id_device_type:'',
      value:'',
      id_country:'',
      id_currency:'',
      currency:'',
      nameValue:'Amount*',

      Countries:[],
      Coupons_type:[],
      Device_types:[],

      id_coupon:'',
      alert:null,
      userRole: true,
    }

    this.getInitialInfo   = this.getInitialInfo.bind(this)
    this.clear            = this.clear.bind(this);

    this.autoCloseSuccess = this.autoCloseSuccess.bind(this);
    this.hideAlert = this.hideAlert.bind(this);

    this.handleCancel = this.handleCancel.bind(this)
    this.validForm = this.validForm.bind(this)
    this.errorValidation = this.errorValidation.bind(this);
  }

  getInitialInfo = async () => {
    const initInfo = await getInfoCoupons()
    if(initInfo.st){
      if(initInfo.status === 200){
        if(this.props.infoCoupon){
          const data = this.props.infoCoupon
          this.setState({
            Countries:initInfo.Countries,
            Coupons_type:initInfo.Coupons_type,
            Device_types:initInfo.Devices_type,
            name:data.name,
            value:data.value,
            id_coupon:data.id_coupon, 
            id_device_type:data.id_device_type,         
            start_date:moment(data.start_date).format('YYYY-MM-DD'),
            expiration_date:moment(data.expiration_date,).format('YYYY-MM-DD'),
            id_coupon_type:data.id_coupon_type, 
            id_country:data.id_country,
            id_currency:data.id_coupon_type===2?data.id_country:data.id_coupon_type,
            currency:data.currency,
            nameValue:data.id_coupon_type==='2'?'Amount*':data.id_coupon_type==='3'?'Percentage*':'Minutes*'
          })
        } 
        else{
          this.setState({
            Countries:initInfo.Countries,
            Coupons_type:initInfo.Coupons_type,
            Device_types:initInfo.Devices_type,
          })
        }     
      }
    }
  }

  startDateChanged = (d) => {
    if(moment.isMoment(d)){
      this.setState({start_date: d.format('YYYY-MM-DD')});
    }
    else{
      this.setState({start_date:''});
    }
  }

  expirationDateChanged = (d) => {
    if(moment.isMoment(d)){
      this.setState({expiration_date: d.format('YYYY-MM-DD')});
    }
    else{
      this.setState({expiration_date:''});
    }
  }

  changeValue = (e) => {
    const name = e.target.name
    const value = e.target.value    
    switch(name) {
      case 'name':
        this.setState({name:value})
      break;
      case 'type':        
        if(value === '1'){
          this.setState({
            id_coupon_type:value,
            id_currency:'1',
            currency:'',
            nameValue:'Minutes*',
          })
        }  
        else if(value === '2'){
          this.setState({
            id_coupon_type:value,
            id_currency:this.state.id_country,
            currency:'',
            id_country:'',
            nameValue:'Amount*',
          })
        }
        else if(value === '3'){
          this.setState({
            id_coupon_type:value,
            currency:'',
            id_currency:value,
            nameValue:'Percentage*',
          })
        }
        else if(value === '4'){
          this.setState({
            id_coupon_type:value,
            currency:value,
            nameValue:'Minutes*',
          })
        }
        else{
          this.setState({
            id_coupon_type:value,
          })
        }
      break;
      case 'amount':
        if(!isNaN(value)){
          this.setState({value:value})  
        }   
      break;
      case 'campaing':
        const cur = this.state.Countries.find(function(element) {
          if(element.id_country===value) return element
        });
        this.setState({
          id_country:value,
          id_currency:this.state.id_coupon_type==='2'?value:this.state.id_coupon_type,          
          currency:this.state.id_coupon_type==='2'?cur.currency:this.state.currency
        })      
      break;
      case 'id_device_type':
        this.setState({id_device_type:value})
      break;
      default:
      break;
    }
  }

  clear(){
    this.setState({
      start_date:'',
      expiration_date:''
    });
  }

  validForm = () =>{
    let formValid = true
    if(this.state.name===''){
      this.valid.name = 'has-danger'
      formValid = false
    }else{this.valid.name = ''}
    if(this.state.start_date===''){
      this.valid.start = 'has-danger'
      formValid = false
    }else{this.valid.start = ''}  
    if(this.state.expiration_date===''){
      this.valid.end = 'has-danger'
      formValid = false
    }else{this.valid.end = ''}    
    if(this.state.id_coupon_type==='' || this.state.id_coupon_type==='0'){
      this.valid.type = 'has-danger'
      formValid = false
    }else{this.valid.type = ''}
    if(this.state.value===''){
      this.valid.value = 'has-danger'
      formValid = false
    }else{this.valid.value = ''}
    if(this.state.id_country===''|| this.state.id_country==='0'){
      this.valid.country = 'has-danger'
      formValid = false
    }else{this.valid.country = ''}
    return formValid
  }

  onSave = async () => {
    if(this.props.New){
      if(!this.validForm()){
        this.errorValidation()
      }else{
        const object = {
          name:this.state.name,
          start_date:this.state.start_date,
          expiration_date:this.state.expiration_date,
          id_coupon_type:this.state.id_coupon_type,
          id_device_type:this.state.id_device_type,
          value:this.state.value,
          id_country:this.state.id_country,
          currency:this.state.currency,
        }
        const response = await createCoupons(object)
        if(response.response.status===200){
          this.autoCloseSuccess('Your coupon has been created!')
          this.setState({
            name:'',
            start_date:'',
            expiration_date:'',
            id_coupon_type:'',
            value:'',
            id_country:'',
            currency:'',
            nameValue:'Amount*',
          })
        }
      }
    }
  }

  onUpdate = async () => {
    if(!this.props.New){      
      if(!this.validForm()){
        this.errorValidation()
      }else{
        const object = await {
          id_coupon:this.state.id_coupon,
          name:this.state.name,
          start_date:this.state.start_date,
          expiration_date:this.state.expiration_date,
          id_coupon_type:this.state.id_coupon_type,
          value:this.state.value,
          id_country:this.state.id_country,
          currency:this.state.currency,
        }
        const response = await editCoupons(object)
        if(response.response.status===200){
          this.autoCloseSuccess('Coupon has been updated successfully!')
        }
      }
    }
  }

  hideAlert = () => {
    if(!this.props.New){
      this.handleCancel()
    }
    this.setState({
      alert:null
    }, ()=>{this.props.onActionUpdate()})
  };

  errorValidation = () => {
    this.setState({
      alert: (
        <SweetAlert 
          title=''
          custom          
          customIcon={iconFailCou}
          onConfirm={() => this.hideAlert()}
          showConfirm={false}
        >
          <hr className='Line'/>
          <div className='AlertCreate'>
            <p>Complete the Form</p>
          </div>
        </SweetAlert >
      )
    });
    setTimeout(() => this.setState({alert:null}), 2000);
  };

  autoCloseSuccess = (message) => {
    this.setState({
      alert: (
        <SweetAlert 
          title=''  
          custom
          customIcon={iconOKCou}
          onConfirm={() => this.hideAlert()}
          showConfirm={false}
        >   
          <hr className='Line'/>
          <div className='AlertCreate'>          
            <p>{message}</p>
          </div>       
        </SweetAlert >
      )
    });
    setTimeout(this.hideAlert, 2000);
  };

  handleCancel(e) {
    this.props.onActionUpdate(e)
  }

  componentDidMount() {
    document.body.classList.add('formCoupon-page')
    this.getInitialInfo()
  }

  componentWillUnmount() {
    document.body.classList.remove('formCoupon-page')
  }

  beforeValid = (current) => {
    if (this.state.expiration_date) {
      let yesterday = moment(this.state.expiration_date).add(1, 'days');
      return current.isBefore(yesterday)
    } else {
      return true;
    }
  }

  afterValid = (current) => {
    if (this.state.start_date) {
      let yesterday = moment(this.state.start_date).subtract(1, 'days');
      return current.isAfter(yesterday)
    } else {
      return true;
    }
  }
  
  render() {
    const {userRole} = this.state
    return (
      <>
      {this.state.alert}      
      <Card>
        <CardHeader>
          <Row>
            <Col md ='1'></Col>
            <Col md ='2'>
              <CardTitle tag="h4">{this.props.CardTitle}</CardTitle>
            </Col>
            <Col md ='2'>
              <div className={this.props.New?'':"couponeState"}>
                <span>{this.props.CardState}</span>
                  {this.props.CardState === 'Available' ? <img alt="" src={require("assets/img/icon_check.png")}/> :
                  this.props.CardState === 'Unavailable' ? <img alt="" src={require("assets/img/icon_fail.png")}/>:
                  this.props.CardState === 'Expired' ? <img alt="" src={require("assets/img/icon_fail.png")}/>:null}                 
              </div>
            </Col>
          </Row>                
        </CardHeader>
        <CardBody>
          <Form action="/" className="form-horizontal" method="get">
            <Row>
            <Label sm="2">Coupone Name*</Label>
              <Col sm="4">
                <FormGroup className={this.valid.name}>
                  <Input 
                    name='name'
                    type="text"
                    placeholder="Text"
                    value={this.state.name}
                    onChange={this.changeValue}
                    disabled={ userRole ? false : true }
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Label sm="2">Start Date*</Label>
              <Col sm="4">
                <FormGroup className={this.valid.start}>
                  <Datetime 
                    inputProps={{placeholder:"Start", disabled: !userRole}}
                    timeFormat={false}
                    closeOnSelect={true}
                    value={this.state.start_date}
                    onChange={this.startDateChanged}
                    isValidDate={this.beforeValid}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Label sm="2">Expiration Date*</Label>
              <Col sm="4">
                <FormGroup className={this.valid.end}>                      
                  <Datetime 
                    inputProps={{placeholder:"End", disabled: !userRole }}
                    timeFormat={false}
                    closeOnSelect={true}
                    value={this.state.expiration_date}
                    onChange={this.expirationDateChanged}
                    isValidDate={this.afterValid}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Label sm="2">Coupone Type*</Label>
              <Col sm="4">
                <FormGroup className={this.valid.type}>
                  <Input                     
                    name='type'
                    type="select"
                    value={this.state.id_coupon_type}
                    onChange={this.changeValue}
                    disabled={ userRole ? false : true }
                  >
                  <option value={0}>--Select coupon type--</option>                         
                  {this.state.Coupons_type.map((type, index) => {
                      return <option key={index} value={type.id_coupon_type}>{type.type}</option>
                  })}
                  </Input>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Label sm="2">Device Type*</Label>
              <Col sm="4">
                <FormGroup className={this.valid.id_device_type}>
                  <Input                     
                    name='id_device_type'
                    type="select"
                    value={this.state.id_device_type}
                    onChange={this.changeValue}
                    disabled={ userRole ? false : true }
                  >
                  <option value={''}>--Select Device type--</option>                         
                  {this.state.Device_types.map((type, index) => {                    
                      return <option key={index} value={type.id_device_type}>{type.device}</option>
                  })}
                  </Input>
                </FormGroup>
              </Col>
            </Row>
            
            <Row>
              <Label md="2" >{this.state.nameValue}</Label>
              <Col md="4">
                <FormGroup className={`${this.valid.value} couponValue`}>
                  <Input 
                    name='amount'
                    type="text"                          
                    value={this.state.value}
                    onChange={this.changeValue}
                    disabled={ userRole ? false : true }
                    placeholder="" 
                  />
                  <Input                           
                    type="select"
                    name="currency"
                    placeholder=""
                    value={this.state.id_currency}
                    onChange={this.changeValue}
                    disabled={true}
                  >                        
                    <option value='3'>%</option>
                    <option value='1'>Minutes</option>
                    <option value='4'>Minutes</option>
                    {this.state.Countries.map((country, index) => <option key={index} value={country.id_country}>{country.currency.toUpperCase()}</option>)}
                  </Input>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Label sm="2">Campaign Country*</Label>
              <Col sm="4">
                <FormGroup className={this.valid.country}>
                  <Input 
                      name='campaing'                      
                      value={this.state.id_country}
                      onChange={this.changeValue}
                      disabled={ userRole ? false : true }
                      placeholder="Select Country" 
                      type="select">
                      <option value={0}>Select country</option>
                    {this.state.Countries.map((country, index) => <option key={index} value={country.id_country}>{country.country}</option>)}
                  </Input>
                </FormGroup>
              </Col>
            </Row>

          </Form>          
        </CardBody>
        <CardFooter className="text-center">
        <Row>
          <p className="card-category">*These fields are required</p>
        </Row> 
        <Col sm='6' className='cosmic-butons butonsFormInfo'>
          <Button className="cosmic-cancel" onClick={this.handleCancel}>Cancel</Button>
          <Button className="cosmic-confirm" onClick={this.props.New?this.onSave:this.onUpdate} disabled={ userRole ? false : true }>Save</Button>
        </Col>
        </CardFooter>
      </Card>
      </>
    );
  }
}

export default FormCoupon;
