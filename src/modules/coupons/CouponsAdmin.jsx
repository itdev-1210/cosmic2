import React from 'react';

import {getCoupons, deleteCoupon, getNavCoupon, getFilter} from './../../api/couponsApi'
import moment from 'moment'
import SweetAlert from 'react-bootstrap-sweetalert';
import Datetime from 'react-datetime';

// reactstrap components
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Table,
  Input,
  DropdownMenu,
  UncontrolledDropdown,
  DropdownToggle,
  
} from 'reactstrap';
import { Link } from 'react-router-dom';

import FormCoupon from './FormCoupon'
import './coupons.scss'
import okIcon from '../../assets/img/icon_ok_coupons.png'
import LoadingModal from './../../components/Navbars/LogoutModal'
import ResponsiveModal from 'react-responsive-modal';
import { ToastContainer, toast } from 'react-toastify'
import ConfirmModal from '../../views/pages/ConfirmModal'
import AlertModal from '../../components/Cosmic/AlertModal/AlertModal'
import ChangeModal from './ChangeModal'

class CouponsAdmin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_loading: true,
      pageCoupons:[],
      alert:null,
      pageEdit:null,
      first: null,
      previous: null,
      next: null,
      last: null,
      total: 0,
      index: 0,

      sort_direction_id: false,
      sort_direction_name: true,
      sort_direction_start: true,
      sort_direction_expire: true,
      sort_direction_type: true,
      sort_direction_benefit: true,
      sort_direction_country: true,
      sort_direction_status: true,

      active: false,
      country: false,
      type: false,
      start_date: false,
      expired: false,
      filter_start: '',
      filter_end: '',
      id_country: '-1',
      id_type: -1,
      countries: [],
      types: [],
      Status: [],

      statusModal: false,
      curCoupon: null,
      confirmModal: false,
      alertModal: false,

      error: '',
      c_res: -1,
    }
    this.getInitCoupons = this.getInitCoupons.bind(this)
    this.renderCoupons = this.renderCoupons.bind(this)
    this.deleteCouponAlert = this.deleteCouponAlert.bind(this)
    this.createAlert = this.createAlert.bind(this)
    this.closeAlert = this.closeAlert.bind(this)    
    this.editCoupon = this.editCoupon.bind(this)

    this.renderAdmin = this.renderAdmin.bind(this)
    this.updateEditCoupon = this.updateEditCoupon.bind(this)

    this.first = this.first.bind(this)
    this.previous = this.previous.bind(this)
    this.next = this.next.bind(this)
    this.last = this.last.bind(this)
    this.setParam = this.setParam.bind(this)
    this.setData = this.setData.bind(this)
  }

  setData(data) {
    var coupons = data.Coupons ? data.Coupons : []
    coupons.sort((a,b)=>(a.id_coupon > b.id_coupon) ? 1 : ((b.id_coupon > a.id_coupon) ? -1 : 0))

    var types = data.Coupons_Type ? data.Coupons_Type : []
    types.sort((a,b)=>(a.id_coupon_type > b.id_coupon_type) ? 1 : ((b.id_coupon_type > a.id_coupon_type) ? -1 : 0))
    types.unshift({id_coupon_type: -1, 'type':'--Select coupon type--'})

    var countries = data.Countries ? data.Countries : []
    countries.sort((a,b)=>(a.country > b.country) ? 1 : ((b.country > a.country) ? -1 : 0))
    countries.unshift({id_country: -1, 'country':'--Select a country--', 'indicative': '', 'iso': 'NO'})
    this.setState({
      is_loading: false,
      pageCoupons:coupons,
      first: data.first,
      previous: data.previous,
      next: data.next,
      last: data.last,
      total: data.total,
      index: data.index,
      countries: countries,
      types: types,
      Status: data.Status ? data.Status : [],
    })
  }

  setParam() {
    return '';
  }

  async first() {
    if (this.state.first === null)
      return

    const result = await getNavCoupon(this.state.first + this.setParam())
    this.setData(result.response)
  }

  async previous() {
    if (this.state.previous === null)
      return
    const result = await getNavCoupon(this.state.previous + this.setParam())
    this.setData(result.response)
  }

  async next() {
    if (this.state.next === null)
      return
    const result = await getNavCoupon(this.state.next + this.setParam())
    this.setData(result.response)
  }

  async last() {
    if (this.state.last === null)
      return
    const result = await getNavCoupon(this.state.last + this.setParam())
    this.setData(result.response)
  }

  sortData = (type) => {
    let coupons = Object.assign([], this.state.pageCoupons)
    switch(type) {
      case "name":
        if (this.state.sort_direction_name) {
          coupons.sort((a,b)=>(a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
        } else {
          coupons.sort((a,b)=>(a.name < b.name) ? 1 : ((b.name < a.name) ? -1 : 0))
        }
        this.setState({sort_direction_name: !this.state.sort_direction_name, pageCoupons: coupons})
        break;
      case "id":
        if (this.state.sort_direction_id) {
          coupons.sort((a,b)=>(a.id_coupon > b.id_coupon) ? 1 : ((b.id_coupon > a.id_coupon) ? -1 : 0))
        } else {
          coupons.sort((a,b)=>(a.id_coupon < b.id_coupon) ? 1 : ((b.id_coupon < a.id_coupon) ? -1 : 0))
        }
        this.setState({sort_direction_id: !this.state.sort_direction_id, pageCoupons: coupons})
        break;
      case "start":
        if (this.state.sort_direction_start) {
          coupons.sort((a,b)=>(a.start_date > b.start_date) ? 1 : ((b.start_date > a.start_date) ? -1 : 0))
        } else {
          coupons.sort((a,b)=>(a.start_date < b.start_date) ? 1 : ((b.start_date < a.start_date) ? -1 : 0))
        }
        this.setState({sort_direction_start: !this.state.sort_direction_start, pageCoupons: coupons})
        break;
      case "country":
        if (this.state.sort_direction_country) {
          coupons.sort((a,b)=>(a.country > b.country) ? 1 : ((b.country > a.country) ? -1 : 0))
        } else {
          coupons.sort((a,b)=>(a.country < b.country) ? 1 : ((b.country < a.country) ? -1 : 0))
        }
        this.setState({sort_direction_country: !this.state.sort_direction_country, pageCoupons: coupons})
        break;
      case "expire":
        if (this.state.sort_direction_expire) {
          coupons.sort((a,b)=>(a.expiration_date > b.expiration_date) ? 1 : ((b.expiration_date > a.expiration_date) ? -1 : 0))
        } else {
          coupons.sort((a,b)=>(a.expiration_date < b.expiration_date) ? 1 : ((b.expiration_date < a.expiration_date) ? -1 : 0))
        }
        this.setState({sort_direction_expire: !this.state.sort_direction_expire, pageCoupons: coupons})
        break;
      case "type":
        if (this.state.sort_direction_type) {
          coupons.sort((a,b)=>(a.type > b.type) ? 1 : ((b.type > a.type) ? -1 : 0))
        } else {
          coupons.sort((a,b)=>(a.type < b.type) ? 1 : ((b.type < a.type) ? -1 : 0))
        }
        this.setState({sort_direction_type: !this.state.sort_direction_type, pageCoupons: coupons})
        break;
      case "benefit":
        if (this.state.sort_direction_benefit) {
          coupons.sort((a,b)=>(a.value > b.value) ? 1 : ((b.value > a.value) ? -1 : 0))
        } else {
          coupons.sort((a,b)=>(a.value < b.value) ? 1 : ((b.value < a.value) ? -1 : 0))
        }
        this.setState({sort_direction_benefit: !this.state.sort_direction_benefit, pageCoupons: coupons})
        break;
      case "status":
        if (this.state.sort_direction_status) {
          coupons.sort((a,b)=>(a.status > b.status) ? 1 : ((b.status > a.status) ? -1 : 0))
        } else {
          coupons.sort((a,b)=>(a.status < b.status) ? 1 : ((b.status < a.status) ? -1 : 0))
        }
        this.setState({sort_direction_status: !this.state.sort_direction_status, pageCoupons: coupons})
        break;
      default:
        break;
    }
  }

  createAlert = (id_coupon) =>{
    this.setState({
        alert: (
          <SweetAlert            
            showCancel
            confirmBtnText="Yes, delete it!"
            confirmBtnBsStyle='default'
            title={<p>Delete coupon</p>}
            onConfirm={this.deleteCouponAlert.bind(this,id_coupon)}
            onCancel={this.closeAlert}
          >
            <hr className='Line'></hr>
            <p>Are you sure to delete this coupon?</p>
            <p>This action can not be undone</p>
          </SweetAlert>
        )
    });
  }

  closeAlert = () =>{
    this.setState({alert:null})
  }

  deleteCouponAlert = async (coupon) => {
    const res = await deleteCoupon({id_coupon:coupon})
    if(res.response.status===200){
      const data = this.state.pageCoupons;
      await data.find((o, i) => {
        if(o.id_coupon===coupon){
          data.splice(i, 1);
          return true
        }
        return false
      })
      this.setState({
        alert: (
          <SweetAlert 
            onConfirm={this.closeAlert}
            title='' 
            showConfirm={false}
            custom
            customIcon={okIcon}
            >            
            <hr className='Line'/>
            <div className='AlertCreate'>          
              <p>Your coupon has been deleted!</p>
            </div> 
          </SweetAlert>
        ),
        pageCoupons:data
      })
      setTimeout(this.closeAlert, 2000);
    }  
  }

  
  editCoupon = (id_coupon) => {
    const data = this.state.pageCoupons;
    data.find((o, i) => {
      if(o.id_coupon===id_coupon){
        this.setState({
          pageEdit:o
        })
        return o        
      }
    })
  }

  getInitCoupons = async () => {
    const result = await getCoupons()
    if(result.response.status===200){
      this.setData(result.response);
    } else {
      this.setState({is_loading: false})
    }
  }

  changeStatus = (coupon) => {
    this.setState({statusModal: true, curCoupon: coupon})
  }

  closeStatusModal = () => {
    this.setState({statusModal: false})
  }

  confirmStatusModal = (newCoupon) => {
    this.setState({statusModal: false, confirmModal: true, curCoupon: newCoupon})
  }

  closeConfirmModal = () => {
    this.setState({confirmModal: false})
  }

  onCloseAlertModal = () => {
    this.setState({alertModal: false})
  }

  changeAuth = async (res) => {
    // if (res.response.status !== 200)
    //   toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    // else {
    //   let newCoupons = Object.assign([], this.state.pageCoupons);
    //   const { curCoupon } = this.state
    //   const result = await changeAccount({id_operator: curCoupon.id_operator, id_status: curCoupon.id_status})
    //   if (result.response.status !== 200) {
    //     this.setState({confirmModal: false, alertModal: true, error: 'An error has ocurred while updating the coupon!', c_res: result.response.status})
    //   } else {
    //     const index = newCoupons.findIndex(coupon => coupon.id_operator === curCoupon.id_operator)
    //     newCoupons[index].id_status = result.response.operator.id_status
    //     this.setState({confirmModal: false, alertModal: true, error: 'Coupon has been updated!', pageCoupons: newCoupons, c_res: result.response.status})
    //   }
    // }
  }

  renderCoupons = () => {
    if(this.state.pageCoupons.length>0){
      return this.state.pageCoupons.map((coupon, index) => (
        <tr key={index} >
          <td>{coupon.id_coupon}</td>
          <td>{coupon.name}</td>          
          <td>{moment.utc(coupon.start_date).format('MMM DD YYYY')}</td>
          <td>{moment.utc(coupon.expiration_date).format('MMM DD YYYY')}</td>
          <td>{coupon.type}</td>
          <td>{typeof coupon.benefit === 'string'?coupon.benefit.toUpperCase():coupon.benefit}</td>
          <td>{coupon.country}</td>
          <td>{
            <div className="couponeState" style={{cursor: "pointer"}} onClick={()=>this.changeStatus(coupon)}>
              <span>{coupon.status}</span>
                {coupon.status === 'Available' ? <img alt="" src={require("assets/img/icon_check.png")}/> :
                coupon.status === 'Unavailable' ? <img alt="" src={require("assets/img/icon_fail.png")}/>:
                coupon.status === 'Expired' ? <img alt="" src={require("assets/img/icon_fail.png")}/>:null}                 
            </div>}
          </td>
          <td>
            <div className="action">
              <div className="edit" onClick={()=>{this.editCoupon(coupon.id_coupon)}}>
              <img alt="" src={require("assets/img/icon_edit.svg")}/>
              </div>
              <div className="delete" onClick={()=>{this.createAlert(coupon.id_coupon)}}>
              <img alt="" src={require("assets/img/icon_delete.svg")}/>
              </div>
            </div>
          </td>
        </tr>
      ));
    }
    else return null
    
  }
  
  componentDidMount() {
    this.getInitCoupons()
  }

  componentWillUpdate() {
  }

  updateEditCoupon = async () => {
    const result = await getCoupons()
    if(result.response.status===200){
      this.setState({
        pageCoupons:result.response.Coupons,
        pageEdit:null
      })
    }
    else {
      this.setState({
        pageEdit:null
      });
    }
  }

  startDateChanged = (e) => {
    this.setState({filter_start: e.format('YYYY-MM-DD')});
  }

  endDateChanged = (e) => {
    this.setState({filter_end: e.format('YYYY-MM-DD')});
  }

  changeValue = (e) => {
    var obj = {}
    var name = e.target.name, value = e.target.value
    obj[name] = value
    this.setState(obj);
  }

  clearFilter = () => {
    this.setState({
      active: false,
      country: false,
      type: false,
      start_date: false,
      expired: false,
      filter_start: '',
      filter_end: '',
      id_country: '-1',
      id_type: -1,
    })
  }

  filterCheck = (e) => {
    const name = e.target.name
    let state = {}
    state[name] = !this.state[name]
    this.setState(state)
  }

  doneFilter = async () => {
    var filter = 'filter=', type='', start_date='', end_date='', country=''
    if (this.state.active)
      filter += '1,'
    if (this.state.expired)
      filter += '3,'
    filter = filter !== 'filter=' ? filter.substr(0, filter.length-1) + '&' : ''

    if (this.state.country && this.state.id_country !== '-1') {
      country += 'country=' + this.state.id_country
    }
    country = country !== '' ? country + '&' : ''

    if (this.state.type && this.state.id_type !== -1) {
      type += 'type=' + this.state.id_type
    }
    type = type !== '' ? type + '&' : ''

    if (this.state.start_date) {
      if (this.state.filter_start !== '') start_date += 'start_date=' + this.state.filter_start
      if (this.state.filter_end !== '') end_date += 'end_date=' + this.state.filter_end
    }
    start_date = start_date !== '' ? start_date + '&' : ''
    end_date = end_date !== '' ? end_date + '&' : ''
    
    
    if (filter === '' && type === '' 
      && start_date === '' && end_date === '' && country === '')
      return;

    var filter_str = filter + country + type + start_date + end_date
    const result = await getFilter(filter_str.substr(0, filter_str.length-1))
    this.setData(result.response)
  }

  renderAdmin= () => {
    const { first, next, previous, last, index, total, pageCoupons } = this.state
    const dataCouponEdit = this.state.pageEdit
    const disableStyle = {
      color: '#9a9a9a'
    }
    if(dataCouponEdit){
      return (
        <FormCoupon 
          infoCoupon={this.state.pageEdit}          
          onActionUpdate={this.updateEditCoupon}
          CardTitle={dataCouponEdit.type}
          CardState={dataCouponEdit.status}
          New={false}
        />
      )
    }
    else 
    return (
      <Card>
        <CardBody>
          <Table>
            <thead className="text-primary">
              <tr>
                <th onClick={()=>{this.sortData('id')}}>ID Coupon <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                <th onClick={()=>{this.sortData('name')}}>Coupon Name <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                <th onClick={()=>{this.sortData('start')}}>Start Date <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                <th onClick={()=>{this.sortData('expire')}}>Expiration Date <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                <th onClick={()=>{this.sortData('type')}}>Coupon Type <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                <th onClick={()=>{this.sortData('benefit')}}>Benefit <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                <th onClick={()=>{this.sortData('country')}}>Country <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                <th onClick={()=>{this.sortData('status')}}>Status <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>{this.renderCoupons()}</tbody>
          </Table>
          {pageCoupons.length === 0 && <div className="no-result">No results found</div>}
          {pageCoupons.length > 0 &&
          <div className="nav-group">
            <div><span>Showing</span></div>
            <div className="first" style={first===null?disableStyle:{}} onClick={this.first}><i className="fa fa-angle-double-left"></i></div>
            <div className="previous" style={previous===null?disableStyle:{}} onClick={this.previous}><i className="fa fa-angle-left"></i></div>
            <div className="index"><span>{index}</span></div>
            <div><span>{'to'}</span></div>
            <div className="count"><span>{pageCoupons.length === 0 ? 0 : index + pageCoupons.length - 1}</span></div>
            <div><span>{'of'}</span></div>
            <div className="total"><span>{new Intl.NumberFormat().format(total)}</span></div>
            <div className="next" style={next===null?disableStyle:{}} onClick={this.next}><i className="fa fa-angle-right"></i></div>
            <div className="last" style={last===null?disableStyle:{}} onClick={this.last}><i className="fa fa-angle-double-right"></i></div>
          </div>
          }
        </CardBody>
      </Card>
    )
  }


  render() {
    const { active, country, type, start_date, expired, filter_start, filter_end,
      id_country, countries, id_type, types} = this.state
    const {childrens} = this.props
    return (
      <React.Fragment>
      {!this.state.pageEdit ? (
        <ResponsiveModal open={this.state.is_loading} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LoadingModal title="Loading..."/>
        </ResponsiveModal>
      ) : null}
      {this.state.alert}
      <div className="content" id="adminCoupon">        
      <Row>
        {this.state.pageEdit?  
        <Col md='12'>
          <CardHeader className="management-header">
            <Col md='4'>
              <CardTitle tag="h4" onClick={()=>{this.setState({pageEdit:null})}}><img alt="" src={require("assets/img/icon_arrow.svg")}/><span></span>Edit Coupon</CardTitle>
            </Col>  
          </CardHeader>            
        </Col>:
        <Col md="12">
          <CardHeader className="management-header">
            <Col md='4'>
              <CardTitle tag="h4">Admin Coupons</CardTitle>
            </Col>
            <Col md='4' className='funtionSpace'>
              {childrens.indexOf(18)>=0?
              <Link to='/admin/create-coupons'>
                <button className='btn-add btn-cosmic'>
                  <i className="tim-icons icon_add" />
                  <span>Add Coupon</span>
                </button> 
              </Link>:null}
            </Col>                                
            <Col md='4'className='filterSpace'> 
              <div className="tools tools-group">
                <UncontrolledDropdown>
                  <DropdownToggle
                    caret
                    className="btn-filter"
                    data-toggle="dropdown"
                  >
                    Filter
                  </DropdownToggle>
                  <DropdownMenu right>
                    <div className="filter-header">
                      <div className="filter-header-group">
                        <div className="filter-text">Filters</div>
                        <div className="filter-btn-group">
                          <div className="filter-clear" onClick={this.clearFilter}>Clear</div>
                          <div className="filter-done" onClick={this.doneFilter}>Done</div>
                        </div>
                      </div>
                    </div>

                    <div className="filter-check">
                      <input id="country" type="checkbox" name="country" checked={country} onChange={this.filterCheck}/>
                      <label htmlFor="country">Campaign Country</label>
                    </div>
                    <div className="filter-check">
                      <Input
                        name="id_country"
                        type="select"
                        value={id_country}
                        onChange={this.changeValue}
                        onBlur={this.changeValue}
                      >
                        {countries.map((country, index) => <option key={index} value={country.id_country}>{country.country}</option>)}
                      </Input>
                    </div>

                    <div className="filter-check">
                      <input id="type" type="checkbox" name="type" checked={type} onChange={this.filterCheck}/>
                      <label htmlFor="type">Coupon Type</label>
                    </div>
                    <div className="filter-check">
                      <Input
                        name="id_type"
                        type="select"
                        value={id_type}
                        onChange={this.changeValue}
                        onBlur={this.changeValue}
                      >
                        {types.map((type, index) => <option key={index} value={type.id_coupon_type}>{type.type}</option>)}
                      </Input>
                    </div>

                    <div className="filter-check">
                      <input id="start_date" type="checkbox" name="start_date" checked={start_date} onChange={this.filterCheck}/>
                      <label htmlFor="start_date">Start Date</label>
                    </div>
                    <div className="filter-check">
                      <Datetime 
                        className="date_time"
                        inputProps={{placeholder:"Start Date"}}                          
                        timeFormat={false}
                        closeOnSelect={true}
                        value={filter_start}
                        onChange={this.startDateChanged}
                      />
                      <div>-</div>
                      <Datetime 
                        className="date_time"
                        inputProps={{placeholder:"End Date"}}                          
                        timeFormat={false}
                        closeOnSelect={true}
                        value={filter_end}
                        onChange={this.endDateChanged}
                      />
                    </div>
                    <div className="filter-check">
                      <input id="active" type="checkbox" name="active" checked={active} onChange={this.filterCheck}/>
                      <label htmlFor="active">Active</label>
                    </div>
                    <div className="filter-check">
                      <input id="expired" type="checkbox" name="expired" checked={expired} onChange={this.filterCheck}/>
                      <label htmlFor="expired">Expired</label>
                    </div>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </div>               
            </Col>
          </CardHeader>
        </Col>
        }
        <Col md="12">
          {this.renderAdmin()}
        </Col>
      </Row>
    </div>
      <ResponsiveModal open={this.state.statusModal} onClose={this.closeStatusModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
        <ChangeModal onClose={this.closeStatusModal} onChange={this.confirmStatusModal} coupon={this.state.curCoupon} status={this.state.Status}/>
      </ResponsiveModal>
      <ResponsiveModal open={this.state.confirmModal} onClose={this.closeConfirmModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
        <ConfirmModal onClose={this.closeConfirmModal} onAuthenticate={this.changeAuth}/>
      </ResponsiveModal>
      <ResponsiveModal open={this.state.alertModal} onClose={this.onCloseAlertModal} center classNames={{'modal':'alert-modal','closeButton':'modal-close-button'}}>
        <AlertModal onClose={this.onCloseAlertModal} status={this.state.c_res} msj={this.state.error}/>
      </ResponsiveModal>
      <ToastContainer />
    </React.Fragment>    
    );
  }
}

export default CouponsAdmin;
