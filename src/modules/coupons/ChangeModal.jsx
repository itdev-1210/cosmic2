import React, { Component } from 'react';
import {
  Button,
  Input,
  Row,
} from 'reactstrap';

import './modal.scss'
class ChangeModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      coupon_status: this.props.coupon.status
    }
    this.change = this.change.bind(this)
  }

  change(e) {
    this.setState({coupon_status: e.target.value})
  }

  changeStatus = () => {
    let newCoupon = Object.assign([], this.props.coupon);
    newCoupon.status = this.state.coupon_status
    this.props.onChange(newCoupon)
  }
  render() {
    const { coupon, status, onClose } = this.props
    const { coupon_status } = this.state
    return (
      <div className="coupon_status">
        <div className="change-title">
          <span className="title">{'Change status'}</span>
        </div>
        <div className="status-group">
          <div className="input_group">
            <div>Coupon</div>
            <div className="text-center">{coupon.name}</div>

            <div>Status</div>
            <div>
              <Input
                name="status"
                type="select"
                value={coupon_status}
                className={coupon_status.toString() === '1' ? 'available' :
                coupon_status.toString() === '2' ? 'unavailable' : 
                coupon_status.toString() === '3' ? 'demo' : 
                coupon_status.toString() === '4' ? 'deleted' : 
                coupon_status.toString() === '5' ? 'default' : ''}
                    onChange={this.change}
              >
              {status.map((status, index) => 
                <option key={index} value={status.id_status} 
                  className={status.id_status === 1 ? 'available' :
                    status.id_status === 2 ? 'unavailable' :
                    status.id_status === 3 ? 'demo' :
                    status.id_status === 4 ? 'deleted' :
                    status.id_status === 5 ? 'default' : ''}>{status.status}</option>)}
              </Input>
            </div>
          </div>
        </div>
        <Row className='cosmic-butons butonsUpdateOpe'>
          <Button className="cosmic-cancel" onClick={onClose}>Cancel</Button>
          <Button className="cosmic-confirm" onClick={this.changeStatus}>Change status</Button>
        </Row>
      </div>
    )
  }
}

export default ChangeModal