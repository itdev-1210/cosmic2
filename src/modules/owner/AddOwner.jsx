import React from 'react';
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
// reactstrap components
import {
  Card,
  Label,
  Input,
  CardBody,
  CardHeader,
  CardTitle,
  Row,
  Col,
  FormGroup,
  CardFooter,
  Button,
} from 'reactstrap';
import {getInfoSubOrg, getStates, getCities} from '../../api/subOrganizations.js'
import { createOwnerUser } from '../../api/OwnerApi'
import auth from '../../api/auth';

import AlertModal from    '../../components/Cosmic/AlertModal/AlertModal'
import ResponsiveModal from 'react-responsive-modal';
import LoadingModal from './../../components/Navbars/LogoutModal'

import './styles/addOwner.scss'

class AddOwner extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      alert: false,
      alertMsj:'Owner Create',
      alertStatus:200,

      first_name_state: "",
      last_name_state: "",
      email_state: "",
      indicative_state: "",
      phone_state: "",
      password_state: "",
      confirm_state: "",
      id_country_state: "",
      id_state_state: "",
      id_city_state: "",
      zip_code_state:"",

      owner_status_state: "",

      first_name: "",
      last_name: "",
      email: "",
      indicative: "",
      phone: "",
      password: "",
      confirm: "",
      id_country: "",
      id_state: "",
      id_city: "",
      zip_code:"",
      use_2fa:false,
      devices: [],
      owner_status: "",

      countries: [],
      listDevices: [],
      listStatus: [],      
      states: [],
      cities: [],

      is_loading: false,
    }    
  }

  changeValue = (e) => {
    let obj = {}
    let name = e.target.name, value = e.target.value
    if(name==='use_2fa'){
      value = e.target.checked  
    }
    obj[name] = value
    obj[name+'_state'] = ''
    if (value === '') {
      obj[name+'_state'] = 'has-danger'
    }

    switch(name) {
      case 'indicative':
        obj['phone_state'] = ''
        if (value === '' || this.state.phone === '')
          obj['phone_state'] = 'phone-error'
        break;
      case 'phone':
        obj['phone_state'] = ''
        if (value === '' || this.state.indicative === '')
          obj['phone_state'] = 'phone-error'
        break;
      case 'password':
        obj['password_state'] = ''
        if (value.length < 8)
          obj['password_state'] = 'has-danger'
        if (this.state.confirm.length > 0 && value !== this.state.confirm)
          obj['confirm_state'] = 'has-danger'
        break;
      case 'confirm':
        obj['confirm_state'] = ''
        if (value !== this.state.password)
          obj['confirm_state'] = 'has-danger'
        break;
      default:
        break;
    }

    this.setState(obj, ()=>{
      if(name==='id_country' && value !== '') {
        this.getAllStates(value)
      }
      if(name==='id_state' && value !== '') {
        this.getAllCities(value)
      }
    })
  }

  componentDidMount() {
    this.getInitialInfo();
    document.addEventListener('click', this.handleOutsideIndicative, false);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleOutsideIndicative, false);
  }

  getInitialInfo = async () => {
    const initInfo = await getInfoSubOrg()    
    if(initInfo.st){
      if(initInfo.status === 200){
        // for(var i=0;i<initInfo.Devices.length; i ++){
        //   devices.push(false)
        // }
        this.setState({
          countries:initInfo.Countries,
          listDevices:initInfo.Devices,
          listStatus:initInfo.Status,
          // devices: devices,
        })
      }
      else{
        toast.error(initInfo.message, {position: toast.POSITION_TOP_RIGHT})
      }
    }
  }

  async getAllStates(id_country) {
    const res = await getStates({id_country: id_country})
    if(res.st){
      if (res.status === 200) {        
        this.setState({states: res.States ? res.States : []})
      }
    }
  }

  async getAllCities(id_state) {
    const res = await getCities({id_state: id_state})
    if(res.st){
      if (res.status === 200) {
        this.setState({cities: res.Cities ? res.Cities : []})
      }
    }
  }

  checkBox = (e) => {
    this.setState({use_2fa: !this.state.use_2fa})
  }

  serviceCheck = (index) => {
    let devices = this.state.devices
    devices[index] = !devices[index]
    this.setState({devices: devices})
  }

  validForm = () =>{
    let first_name_state = '', last_name_state = '',
      email_state= '', phone_state = "",
      zip_code_state = "",
      id_city_state = "", id_country_state = "",
      id_state_state = "", owner_status_state = "",
      password_state = "", confirm_state = ""
    const { first_name, last_name, email, 
      indicative, phone, zip_code, 
      id_city, id_country, id_state, 
      password, confirm, owner_status} = this.state
    let valid = true
    if (first_name === '') {
      first_name_state = 'has-danger'
      valid = false
    }
    if (last_name === '') {
      last_name_state = 'has-danger'
      valid = false
    }
    if (email === '') {
      email_state = 'has-danger'
      valid = false
    }
    if (indicative === '') {
      phone_state = 'phone-error'
      valid = false
    }
    if (phone === '') {
      phone_state = 'phone-error'
      valid = false
    }
    if (owner_status === '') {
      owner_status_state = 'has-danger'
      valid = false
    }
    if (zip_code === '') {
      zip_code_state = 'has-danger'
      valid = false
    }
    if (id_city === '') {
      id_city_state = 'has-danger'
      valid = false
    }
    if (id_country === '') {
      id_country_state = 'has-danger'
      valid = false
    }
    if (id_state === '') {
      id_state_state = 'has-danger'
      valid = false
    }
    if (password === '') {
      password_state = 'has-danger'
      valid = false
    }
    if (confirm !== password) {
      confirm_state = 'has-danger'
      valid = false
    }
    if(!valid){
      this.setState({
        first_name_state: first_name_state,
        last_name_state: last_name_state,
        email_state: email_state,
        phone_state: phone_state,
        owner_status_state: owner_status_state,
        zip_code_state: zip_code_state,
        id_city_state: id_city_state,
        id_country_state: id_country_state,
        id_state_state: id_state_state,
        password_state: password_state,
        confirm_state: confirm_state,
      })
    }
    return valid
  }

  onSave = async () => {
    const { first_name, last_name, email, 
      indicative, phone, zip_code, 
      id_city, id_country, id_state, 
      password, owner_status,use_2fa} = this.state
    if(this.validForm){
      this.setState({is_loading: true}, async()=>{
        let own = {
          first_name: first_name,
          last_name: last_name,
          email: email,
          indicative: indicative,
          phone: phone,
          id_status: owner_status,
          zip_code: zip_code,
          id_country: id_country,
          id_state: id_state,
          id_city: id_city,
          password: password,        
          use_2fa: use_2fa,
          //id_service_type: 1 /* Will change after */
        }
        const res = await createOwnerUser(own)
        if (res.status !== 201) {
          toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
          this.setState({is_loading: false})
        }
        else {
          this.setState({
            alert: true,
            alertMsj:'The owner has been created!',
            alertStatus:200,
            is_loading: false
          })
        }
      })
    }
  }

  closeAlert = () =>{
    this.setState({      
      alert: false,
      alertMsj:'',
      alertStatus:'' 
    },()=>{
      this.props.history.push('/admin/admin-owner')
    })
  }

  render() {
    const { first_name, last_name, email, indicative, phone, password, confirm, pre_country,
        id_country, countries, id_state, states, id_city, cities, zip_code, owner_status, listStatus,use_2fa,phone_state } = this.state
    return (
      <React.Fragment>
        <ResponsiveModal open={this.state.is_loading} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LoadingModal title="Loading..."/>
        </ResponsiveModal>
        <div className="content" id="add-owner">          
          <Row>
            <Col md='12'>
              <CardHeader className="management-header">
                <Col md='4'>
                  <CardTitle tag="h4" onClick={()=>{this.props.history.push('/admin/admin-owner')}}>
                    <img alt="" src={require("assets/img/icon_arrow.svg")}/>Add Owner
                  </CardTitle>
                </Col>  
              </CardHeader>            
            </Col>
            <Col md="12">
              <Card>
                <CardBody>
                  <Row>
                    <Label sm="2">Owner Profile</Label>
                  </Row>
                  <Row>
                    {/* Name, Last Name, Email, Mobile Number */}
                    <Col md="5">
                      <Row>
                        <Label sm="4">Name</Label>
                        <Col sm="8">
                          <FormGroup className={this.state.first_name_state}>
                            <Input
                              name="first_name"
                              type="text"
                              placeholder="Daniel"
                              value={first_name}
                              onChange={this.changeValue}
                              onBlur={this.changeValue}
                            />
                            {this.state.first_name_state === "has-danger" ? (
                              <label className="error">
                                This field is required.
                              </label>
                            ) : null}
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Label sm="4">Last Name</Label>
                        <Col sm="8">
                          <FormGroup className={this.state.last_name_state}>
                            <Input
                              name="last_name"
                              type="text"
                              placeholder="Rogers"
                              value={last_name}
                              onChange={this.changeValue}
                              onBlur={this.changeValue}
                            />
                            {this.state.last_name_state === "has-danger" ? (
                              <label className="error">
                                This field is required.
                              </label>
                            ) : null}
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Label sm="4">Email</Label>
                        <Col sm="8">
                          <FormGroup className={this.state.email_state}>
                            <Input
                              name="email"
                              type="email"
                              placeholder="admin@gmail.com"
                              value={email}
                              onChange={this.changeValue}
                              onBlur={this.changeValue}
                            />
                            {this.state.email_state === "has-danger" ? (
                              <label className="error">
                                This field is required.
                              </label>
                            ) : null}
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Label sm={4}>Mobile</Label>
                        <Col sm={8}>
                          <FormGroup className={phone_state+' phone-cosmic'}>
                            <Input 
                              className='phone-cosmic-1'
                              name='indicative'
                              type="select"
                              value={indicative}
                              onChange={this.changeValue}
                            >
                              <option value=''>-Country-</option>
                              {countries.map((country, index) => <option key={index} value={country.indicative}>{country.country}</option>)}
                            </Input >   
                            <Input
                              className='phone-cosmic-2'
                              type="text"
                              disabled 
                              placeholder="+00"
                              value={indicative} />
                            <Input                      
                              className='phone-cosmic-3'
                              name='phone'
                              type="text"
                              maxLength={10}
                              placeholder="3114511876"
                              value={phone}
                              onChange={this.changeValue}
                            />
                          </FormGroup>
                          {phone_state === "phone-error" ? (
                            <label className="error">
                              This field is required.
                            </label>
                          ) : null}
                        </Col>
                      </Row>
                    </Col>

                    {/* Country, State, City, Street */}
                    <Col md="5">
                      <Row>
                        <Label sm="4">Country</Label>
                        <Col sm="8">
                          <FormGroup className={this.state.id_country_state}>
                            <Input
                              name="id_country"
                              type="select"
                              value={id_country}
                              onChange={this.changeValue}
                              onBlur={this.changeValue}
                            >
                              <option key={0} value='' className="dark-back">--Select Country--</option>
                            {countries.map((country, index) => <option key={index} value={country.id_country} className="dark-back">{country.country}</option>)}
                            </Input>
                            {this.state.id_country_state === "has-danger" ? (
                              <label className="error">
                                This field is required.
                              </label>
                            ) : null}
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Label sm="4">State</Label>
                        <Col sm="8">
                          <FormGroup className={this.state.id_state_state}>
                            <Input
                              name="id_state"
                              type="select"
                              value={id_state}
                              onChange={this.changeValue}
                              onBlur={this.changeValue}
                            >
                              <option key={0} value='' className="dark-back">--Select State--</option>
                            {states.map((state, index) => <option key={index} value={state.id_state} className="dark-back">{state.state}</option>)}
                            </Input>
                            {this.state.id_state_state === "has-danger" ? (
                              <label className="error">
                                This field is required.
                              </label>
                            ) : null}
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Label sm="4">City</Label>
                        <Col sm="8">
                          <FormGroup className={this.state.id_city_state}>
                            <Input
                              name="id_city"
                              type="select"
                              value={id_city}
                              onChange={this.changeValue}
                              onBlur={this.changeValue}
                            >
                              <option key={0} value='' className="dark-back">--Select City--</option>
                            {cities.map((city, index) => <option key={index} value={city.id_city} className="dark-back">{city.city}</option>)}
                            </Input>
                            {this.state.id_city_state === "has-danger" ? (
                              <label className="error">
                                This field is required.
                              </label>
                            ) : null}
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Label sm="4">Zip Code</Label>
                        <Col sm="8">
                          <FormGroup className={this.state.zip_code_state}>
                            <Input
                              name="zip_code"
                              type="text"
                              placeholder="12511"
                              value={zip_code}
                              onChange={this.changeValue}
                              onBlur={this.changeValue}
                            />
                            {this.state.zip_code_state === "has-danger" ? (
                              <label className="error">
                                This field is required.
                              </label>
                            ) : null}
                          </FormGroup>
                        </Col>
                      </Row>
                    </Col>
                  </Row>

                  
                  <Row className="padding-div">
                    <Col md="5">
                      <Row>
                        <Label sm="4">Password</Label>
                        <Col sm="8">
                          <FormGroup className={this.state.password_state}>
                            <Input
                              name="password"
                              type="password"
                              placeholder="********"
                              value={password}
                              onChange={this.changeValue}
                              onBlur={this.changeValue}
                            />
                            {this.state.password_state === "has-danger" ? (
                              <label className="error">
                                This field is required.
                              </label>
                            ) : null}
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Label sm="4">Confirm Password</Label>
                        <Col sm="8">
                          <FormGroup className={this.state.confirm_state}>
                            <Input
                              name="confirm"
                              type="password"
                              placeholder="********"
                              value={confirm}
                              onChange={this.changeValue}
                              onBlur={this.changeValue}
                            />
                            {this.state.confirm_state === "has-danger" ? (
                              <label className="error">
                                This field is required.
                              </label>
                            ) : null}
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <div className="col-sm-12 two-step">
                          <input id="use_2fa" type="checkbox" name="use_2fa" checked={use_2fa} onChange={this.checkBox}/>
                          <label htmlFor="use_2fa">Two-step authentication <br/>
                          Keep your account extra secure with a second authentication step.</label>
                        </div>
                      </Row>
                    </Col>

                    {/* Associated devices */}
                    <Col md="5">
                      <Row>
                        <Col sm="12"><div className="service-title">Associated devices</div></Col>
                      </Row>
                      <Row>
                        <Col sm="12"><div className="service-sub-title">Select the devices associated with the suborganization.</div></Col>
                      </Row>
                      <Row className="padding-div">
                        <Col sm="8">
                          <div className="service-list">
                            {this.state.listDevices.map((list, index) => 
                            <div key={index} onClick={()=>{this.serviceCheck(index)}}>
                              <label>{list.device}</label>
                              <div>
                                {this.state.devices[index] ? <img alt="" src={require("assets/img/filter_check.svg")}/> : <img alt="" src={require("assets/img/filter_uncheck.svg")}/>}
                              </div>
                            </div>
                            )}
                          </div>                          
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                  
                  <Row className="padding-div">
                    {/* Change Status */}
                    <Col md="5">
                      <Row>
                        <Col sm="12"><div className="service-title">Change status</div></Col>
                      </Row>
                      <Row>
                        <Col sm="12"><div className="service-sub-title">The members of the suborganization can log in according to the status assigned</div></Col>
                      </Row>
                      <Row className="padding-div">
                        <Label sm="4">Status</Label>
                        <Col sm="8">
                          <FormGroup className={this.state.owner_status_state}>
                            <Input
                              name="owner_status"
                              type="select"
                              value={owner_status}
                              onChange={this.changeValue}
                              onBlur={this.changeValue}
                            >
                              <option key={0} value='' className="dark-back">--Select Status--</option>
                              {listStatus.map((status, index) => {
                                if(status.id_status === 4){
                                  return null
                                }else{
                                  return <option key={index} value={status.id_status} className="dark-back">{status.status}</option>
                                }
                              })}                            
                            </Input>
                            {this.state.owner_status_state === "has-danger" ? (
                              <label className="error">
                                This field is required.
                              </label>
                            ) : null}
                          </FormGroup>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter className="text-center cosmic-butons">
                  <Button className="cosmic-cancel" onClick={()=>{this.props.history.goBack()}}>Cancel</Button>
                  <Button className="cosmic-confirm" onClick={this.onSave}>Save</Button>
                </CardFooter>
              </Card>
            </Col>
          </Row>
          <ResponsiveModal open={this.state.alert} onClose={this.closeAlert} center classNames={{'modal':'alert-modal','closeButton':'modal-close-button'}}>
            <AlertModal onClose={this.closeAlert} status={this.state.alertStatus} msj={this.state.alertMsj}/>
          </ResponsiveModal>
          <ToastContainer />
        </div>
      </React.Fragment>
    );
  }
}

export default AddOwner;
