import React from 'react';

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Label,
  Row,
  Col,
  Table
} from 'reactstrap';
import Moment from 'react-moment'
import { getOwner, updateOwner } from '../../api/OwnerApi';
import auth from '../../api/auth';
import './styles/editOwner.scss'
import ResponsiveModal from 'react-responsive-modal';
import UpdateModal from './UpdateModal'
import ConfirmModal from '../../views/pages/ConfirmModal'
import SweetAlert from 'react-bootstrap-sweetalert';
import { ToastContainer, toast } from 'react-toastify'
import LoadingModal from './../../components/Navbars/LogoutModal'

class EditOwner extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      newOwner: {},
      owner: {},
      payments: [],
      devices: [],
      total_revenues: [],
      uuid: null,

      updateModal: false,
      confirmModal: false,
      alert: null,

      sort_direction_order_id: false,
      sort_direction_amount: true,
      sort_direction_currency: true,
      sort_direction_token: true,
      sort_direction_date: true,
      sort_direction_email: true,
      sort_direction_payment_method: true,
      sort_direction_service: true,
      
      is_loading: true,
    }
  }

  async fetchData() {
    const res = await getOwner(this.state.uuid)
    this.setData(res)
  }

  componentDidMount() {
    this.fetchData()
  }

  componentWillMount() {
    const { match: { params } } = this.props;
    this.setState({uuid: params.uuid})
  }

  setData = (res) => {
    var payments = res.payments ? res.payments : []
    payments.sort((a,b)=>(a.order_id > b.order_id) ? 1 : ((b.order_id > a.order_id) ? -1 : 0))

    this.setState({
      is_loading: false,
      devices: res.devices ? res.devices : [],
      total_revenues: res.total_revenues ? res.total_revenues : [],
      payments: payments,
      owner: res.owner ? res.owner : {}
    })
  }

  renderPayments = () => {
    const { payments } = this.state
    return payments.map((payment, index) => (
      <tr key={index} onClick={()=>{this.props.history.push('/admin/payment/' + payment.id_payment)}} style={{cursor: 'pointer'}}>
        <td className="text-left">{payment.order_id}</td>
        <td>{new Intl.NumberFormat('us-US', {style: 'currency', currency: 'USD'}).format(payment.total_cost)}</td>
        <td className="upper-case">{payment.currency}</td>
        <td className="upper-case">{payment.brand + ' ' + payment.last_four}</td>
        <td className="text-left"><Moment format="MMM DD, YYYY h:mm A">{payment.date}</Moment></td>
        <td className="text-left">{payment.email}</td>        
        <td className="text-left">{payment.device}</td>
        <td className="text-left">
          <div className="status">
            <span>{payment.status}</span>
            {payment.status === 'Succeded' ? <img alt="" src={require("assets/img/icon_check.png")}/> : 
            payment.status === 'Failed' ? <img alt="" src={require("assets/img/icon_fail.png")}/> : 
            payment.status === 'Pending' ? <img alt="" src={require("assets/img/icon_pending.png")}/> : 
            payment.status === 'Refunded' ? <img alt="" src={require("assets/img/icon_reply.png")}/> : null}
          </div>
        </td>
      </tr>
    ));
  }

  sortData = (type) => {
    let payments = Object.assign([], this.state.payments)
    switch(type) {
      case "order_id":
        if (this.state.sort_direction_order_id) {
          payments.sort((a,b)=>(a.order_id > b.order_id) ? 1 : ((b.order_id > a.order_id) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.order_id < b.order_id) ? 1 : ((b.order_id < a.order_id) ? -1 : 0))
        }
        this.setState({sort_direction_order_id: !this.state.sort_direction_order_id, payments: payments})
        break;
      case "amount":
        if (this.state.sort_direction_amount) {
          payments.sort((a,b)=>(parseFloat(a.total_cost) > parseFloat(b.total_cost)) ? 1 : ((parseFloat(b.total_cost) > parseFloat(a.total_cost)) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(parseFloat(a.total_cost) < parseFloat(b.total_cost)) ? 1 : ((parseFloat(b.total_cost) < parseFloat(a.total_cost)) ? -1 : 0))
        }
        this.setState({sort_direction_amount: !this.state.sort_direction_amount, payments: payments})
        break;
      case "card":
        if (this.state.sort_direction_phone) {
          payments.sort((a,b)=>(a.brand + a.last_four > b.brand + b.last_four) ? 1 : ((b.brand + b.last_four > a.brand + a.last_four) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.brand + a.last_four < b.brand + b.last_four) ? 1 : ((b.brand + b.last_four < a.brand + a.last_four) ? -1 : 0))
        }
        this.setState({sort_direction_phone: !this.state.sort_direction_phone, payments: payments})
        break;
      case "currency":
        if (this.state.sort_direction_currency) {
          payments.sort((a,b)=>(a.currency > b.currency) ? 1 : ((b.currency > a.currency) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.currency < b.currency) ? 1 : ((b.currency < a.currency) ? -1 : 0))
        }
        this.setState({sort_direction_currency: !this.state.sort_direction_currency, payments: payments})
        break;
      case "payment_method":
        if (this.state.sort_direction_payment_method) {
          payments.sort((a,b)=>(a.payment_method > b.payment_method) ? 1 : ((b.payment_method > a.payment_method) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.payment_method < b.payment_method) ? 1 : ((b.payment_method < a.payment_method) ? -1 : 0))
        }
        this.setState({sort_direction_payment_method: !this.state.sort_direction_payment_method, payments: payments})
        break;
      case "service":
        if (this.state.sort_direction_service) {
          payments.sort((a,b)=>(a.service > b.service) ? 1 : ((b.service > a.service) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.service < b.service) ? 1 : ((b.service < a.service) ? -1 : 0))
        }
        this.setState({sort_direction_service: !this.state.sort_direction_service, payments: payments})
        break;
      case "date":
        if (this.state.sort_direction_date) {
          payments.sort((a,b)=>(Date.parse(a.date) > Date.parse(b.date)) ? 1 : ((Date.parse(b.date) > Date.parse(a.date)) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(Date.parse(a.date) < Date.parse(b.date)) ? 1 : ((Date.parse(b.date) < Date.parse(a.date)) ? -1 : 0))
        }
        this.setState({sort_direction_date: !this.state.sort_direction_date, payments: payments})
        break;
      default:
        break;
    }
  }

  closeAlert = () =>{
    this.setState({alert:null})
  }

  updateUser = () => {
    this.setState({updateModal: true})
  }
  onCloseUpdateModal = () => {
    this.setState({updateModal: false})
  }

  updateData = (newOwner) => {
    this.setState({newOwner: newOwner, confirmModal: true, updateModal: false})
  }
  
  onCloseConfirmModal = () => {
    this.setState({confirmModal: false})
  }

  authenticate = async (res) => {
    if (res.response.status !== 200)
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    else {
      this.setState({is_loading: true}, async()=>{
        const result = await updateOwner(this.state.newOwner)
        if (result.response.status !== 200) {
          toast.error(result.response.message, {position: toast.POSITION_TOP_RIGHT})
          this.setState({is_loading: false})
        }
        else {
          this.setState({
            alert: (
              <SweetAlert success title="The owner has been updated!" onConfirm={this.closeAlert}>        
              </SweetAlert>
            ),
            confirmModal: false,
            owner: result.response.owner,
            is_loading: false,
          })
        }
      })
    }
  }

  render() {
    const { owner, devices, total_revenues} = this.state
    return (
      <>
        {this.state.alert}
        <ResponsiveModal open={this.state.is_loading} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LoadingModal title="Loading..."/>
        </ResponsiveModal>
        <div className="content">
          <div className="page-header">
            <span className="arrow-back" onClick={()=>{this.props.history.push('/admin/admin-owner')}}><img alt="" src={require("assets/img/icon_arrow.svg")}/></span>
            <h4 className="title pl-3 icon_owner_white">{owner.first_name + ' ' + owner.last_name}</h4>
          </div>
          <Row>
            <Col md="6">
              <Card>
                <CardHeader>
                  <Row>
                    <Col md="4">
                      <CardTitle tag="h4" className="text-white">Account Information</CardTitle>
                    </Col>
                    <Col md="4">
                      {<div onClick={this.updateUser}><img alt="" src={require("assets/img/icon_edit.svg")}/></div>}
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody className="edit-owner-body">
                  <Row>
                    <Label md="3" className="text-white">UUID</Label>
                    <Label md="9">{owner && owner.uuid ? owner.uuid : ''}</Label>
                  </Row>
                  <Row>
                    <Label md="3" className="text-white">Created</Label>
                    <Label md="9">{owner && owner.created_at ? new Date(owner.created_at).toLocaleString() : ''}</Label>
                  </Row>
                  <Row>
                    <Label md="3" className="text-white">Name</Label>
                    <Label md="9">{owner && owner.first_name ? owner.first_name + ' ' + owner.last_name : ''}</Label>
                  </Row>
                  <Row>
                    <Label md="3" className="text-white">Phone</Label>
                    <Label md="9">{owner && owner.phone ? owner.indicative + owner.phone : ''}</Label>
                  </Row>
                  <Row>
                    <Label md="3" className="text-white">Email</Label>
                    <Label md="9">{owner && owner.email ? owner.email : ''}</Label>
                  </Row>
                </CardBody>
              </Card>
            </Col>
            <Col md="6">
              <Card>
                <CardBody className="edit-owner-body">
                  <Row>
                    <Label md="3" className="text-white">Country</Label>
                    <Label md="9">{owner && owner.country ? owner.country : ''}</Label>
                  </Row>
                  <Row>
                    <Label md="3" className="text-white">State</Label>
                    <Label md="9">{owner && owner.state ? owner.state : ''}</Label>
                  </Row>
                  <Row>
                    <Label md="3" className="text-white">City</Label>
                    <Label md="9">{owner && owner.city ? owner.city : ''}</Label>
                  </Row>
                  <Row>
                    <Label md="3" className="text-white">Street</Label>
                    <Label md="9">{owner && owner.address ? owner.address : ''}</Label>
                  </Row>
                  <Row>
                    <Label md="3" className="text-white">ZIP Code</Label>
                    <Label md="9">{owner && owner.zip_code ? owner.zip_code : ''}</Label>
                  </Row>
                </CardBody>
              </Card>
            </Col>
            <Col md="12">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Associated Services</CardTitle>
                </CardHeader>
                <CardBody className="edit-owner-body">
                  <Row>
                    {devices.map((device, key) => {
                      return (
                        <Label md="3" key={key}>
                          <img alt="" src={
                            device.id_device_type === 0 ? require("assets/img/icon_scooter_rover_gray.svg") :
                            device.id_device_type === 1 ? require("assets/img/icon_e_bike.svg") :
                            require("assets/img/icon_battery.svg")
                            }/>
                          {device.count+'      |     '+device.device}
                        </Label>)
                    })}
                  </Row>
                </CardBody>
              </Card>
            </Col>
            <Col md="12">
              <Card>
                <CardHeader>
                  <Row>
                    <Col md="3" className="text-white">
                      <CardTitle tag="h4">Total Revenues</CardTitle>
                    </Col>
                    {devices.map((device, key) => {
                      return (
                        <Col md="3" key={key} className="text-white">
                          <CardTitle tag="h4">{device.device}</CardTitle>
                        </Col>)
                    })}
                  </Row>
                </CardHeader>
                <CardBody className="edit-owner-body">
                  <Row>
                    <Label md="3">
                      {new Intl.NumberFormat('us-US', {style: 'currency', currency: 'USD'}).format(total_revenues.reduce(function(a, b) {
                        return a + parseFloat(b.revenues);
                      }, 0))} USD
                    </Label>
                    {total_revenues.map((revenues, key) => {
                      return (
                        <Label md="3" key={key}>
                          {new Intl.NumberFormat('us-US', {style: 'currency', currency: 'USD'}).format(revenues.revenues)} USD
                        </Label>)
                    })}
                  </Row>
                </CardBody>
              </Card>
            </Col>
            <Col md="12">
              <Card>
                <CardHeader className="show-more-payment">
                  <CardTitle tag="h4">Payments</CardTitle>
                  <div onClick={()=>{this.props.history.push({pathname:'/admin/payment', state: {email: owner.email}})}}>Show more...</div>
                </CardHeader>
                <CardBody>
                  <Table>
                    <thead className="text-primary">
                      <tr>
                        <th className="text-left" onClick={()=>{this.sortData('order_id')}}>Order ID <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('amount')}}>Amount <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('currency')}}>Currency <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th className="text-left" onClick={()=>{this.sortData('card')}}>Card <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th className="text-left" onClick={()=>{this.sortData('date')}}>Date <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th className="text-left" onClick={()=>{this.sortData('email')}}>User <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th className="text-left" onClick={()=>{this.sortData('service')}}>Service <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th className="text-left">STATUS</th>
                      </tr>
                    </thead>
                    <tbody className="edit-owner-payment">{this.renderPayments()}</tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
        <ResponsiveModal open={this.state.updateModal} onClose={this.onCloseUpdateModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <UpdateModal onClose={this.onCloseUpdateModal} 
              owner={owner}
              updateData={this.updateData}
              />
        </ResponsiveModal>
        <ResponsiveModal open={this.state.confirmModal} onClose={this.onCloseConfirmModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <ConfirmModal onClose={this.onCloseConfirmModal} onAuthenticate={this.authenticate}/>
        </ResponsiveModal>
        <ToastContainer />
      </>
    );
  }
}

export default EditOwner;
