import React from 'react';
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  DropdownToggle,
  DropdownMenu,
  UncontrolledDropdown,
  Table,
  Row,
  Col,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import SweetAlert from 'react-bootstrap-sweetalert';
import { getOwnersAdmin, getNavAdmin, getFilterAdmin, deleteOwnerAdmin } from '../../api/OwnerApi';
import './styles/adminOwner.scss'
import ResponsiveModal from 'react-responsive-modal';
import Datetime from 'react-datetime';
import ConfirmModal from '../../views/pages/ConfirmModal'
import LoadingModal from './../../components/Navbars/LogoutModal'

class AdminOwner extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      is_loading: true,
      owners: [],
      first: null,
      previous: null,
      next: null,
      last: null,
      total: 0,
      index: 0,
      create_date: false,
      filter_start: '',
      filter_end: '',
      sort_direction_name: false,
      sort_direction_email: true,
      sort_direction_phone: true,
      sort_direction_country: true,
      sort_direction_city: true,

      confirmModal: false,
      curOwner: null,
      alert: null,
    }
    this.setData = this.setData.bind(this)
    this.fetchData = this.fetchData.bind(this)
    this.filterCheck = this.filterCheck.bind(this)
    this.clearFilter = this.clearFilter.bind(this)
    this.doneFilter = this.doneFilter.bind(this)
    this.first = this.first.bind(this)
    this.previous = this.previous.bind(this)
    this.next = this.next.bind(this)
    this.last = this.last.bind(this)
    this.setParam = this.setParam.bind(this)
  }
  filterCheck(e) {
    this.setState({create_date: e.target.value})
  }

  clearFilter() {
    this.setState({
      create_date: false,
      filter_start: '',
      filter_end: '',
    })
  }

  async doneFilter() {
    if (!this.state.create_date)
      return;
    if (this.state.filter_start === '' && this.state.filter_end === '')
      return;
    let result;
    var filter_start = 'filter_start_date='
    var filter_end = 'filter_end_date='

    if (this.state.filter_start !== '') {
      filter_start += this.state.filter_start
    }
    if (this.state.filter_end !== '') {
      filter_end += this.state.filter_end
    }
    if (filter_start === 'filter_start_date=') {
      if (filter_end === 'filter_end_date=') return;
      // result = await getFilterAdmin(filter_end)
    } else if (filter_end === 'filter_end_date=') {
      if (filter_start === 'filter_start_date=') return;
      // result = await getFilterAdmin(filter_start)
    } else {
      // result = await getFilterAdmin(filter_start+'&' +filter_end)
    }
    // this.setData(result.response)
  }

  setParam() {
    if (!this.state.create_date)
      return '';
    if (this.state.filter_start === '' && this.state.filter_end === '')
      return '';
    var filter_start = 'filter_start_date='
    var filter_end = 'filter_end_date='

    if (this.state.filter_start !== '') {
      filter_start += this.state.filter_start
    }
    if (this.state.filter_end !== '') {
      filter_end += this.state.filter_end
    }
    if (filter_start === 'filter_start_date=') {
      if (filter_end === 'filter_end_date=') return;
      return filter_end
    } else if (filter_end === 'filter_end_date=') {
      if (filter_start === 'filter_start_date=') return;
      return filter_start
    } else {
      return filter_start+'&' +filter_end
    }
  }

  async fetchData() {
    const result = await getOwnersAdmin();
    this.setData(result.response);
  }

  async first() {
    if (this.state.first === null)
      return

    const result = await getNavAdmin(this.state.first + this.setParam())
    this.setData(result.response)
  }

  async previous() {
    if (this.state.previous === null)
      return
    const result = await getNavAdmin(this.state.previous + this.setParam())
    this.setData(result.response)
  }

  async next() {
    if (this.state.next === null)
      return
    const result = await getNavAdmin(this.state.next + this.setParam())
    this.setData(result.response)
  }

  async last() {
    if (this.state.last === null)
      return
    const result = await getNavAdmin(this.state.last + this.setParam())
    this.setData(result.response)
  }

  componentDidMount() {
    this.fetchData()
  }

  editOwner = (uuid) => {
    this.props.history.push('/admin/edit-owner/'+uuid);
  }

  setData(data) {
    var owners = data.Owners ? data.Owners : []
    owners.sort((a,b)=>(a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
    this.setState({
      owners: owners,
      first: data.first,
      previous: data.previous,
      next: data.next,
      last: data.last,
      total: data.total,
      index: data.index,
      is_loading: false,
    })
  }
  renderOwners() {
    const { owners } = this.state
    return owners.map((owner, index) => {
      return (
        <tr key={index}>
          <td>{owner.name}</td>
          <td>{owner.email}</td>
          <td>{owner.phone}</td>
          <td>{owner.country}</td>
          <td>{owner.city}</td>
          <td>
            <div className="action">
              <div className="edit" onClick={()=>{this.editOwner(owner.uuid)}}>
              <img alt="" src={require("assets/img/icon_edit.svg")}/>
              </div>
              <div className="delete" onClick={()=>{this.deleteOwner(owner.uuid)}}>
              <img alt="" src={require("assets/img/icon_delete.svg")}/>
              </div>
            </div>
          </td>
        </tr>
      );
    });
  }

  startDateChanged = (e) => {
    this.setState({filter_start: e.format('YYYY-MM-DD')});
  }

  endDateChanged = (e) => {
    this.setState({filter_end: e.format('YYYY-MM-DD')});
  }

  onCloseConfirmModal = () => {
    this.setState({confirmModal: false})
  }

  deleteOwner = (ownerId) => {
    this.setState({
      alert: (
        <SweetAlert
          warning
          showCancel
          confirmBtnText="Yes, delete it!"
          title="Are you sure to delete this owner?"
          onConfirm={this.deleteOwnerAlert.bind(this, ownerId)}
          onCancel={this.closeAlert}
        >
        This action can not be undone
        </SweetAlert>
      )
    });
  }

  closeAlert = () =>{
    this.setState({alert:null})
  }

  deleteOwnerAlert = (ownerId) => {
    this.setState({curOwner: ownerId, confirmModal: true, alert: null})
  }

  authenticate = async (res) => {
    let newOwners = this.state.owners
    if (res.response.status !== 200)
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    else {
      const result = await deleteOwnerAdmin({uuid: this.state.curOwner})
      if (result.response.status !== 200)
        toast.error(result.response.message, {position: toast.POSITION_TOP_RIGHT})
      else {
        const index = newOwners.findIndex(owner => owner.uuid === this.state.curOwner)
        newOwners.splice(index, 1)
        this.setState({
          alert: (
            <SweetAlert success title="Owner has been deleted!" onConfirm={this.closeAlert}>        
            </SweetAlert>
          ),
          confirmModal: false, owners: newOwners, curOwner: null, total: this.state.total-1
        })
      }
    }
  }

  sortData = (type) => {
    let owners = Object.assign([], this.state.owners)
    switch(type) {
      case "name":
        if (this.state.sort_direction_name) {
          owners.sort((a,b)=>(a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
        } else {
          owners.sort((a,b)=>(a.name < b.name) ? 1 : ((b.name < a.name) ? -1 : 0))
        }
        this.setState({sort_direction_name: !this.state.sort_direction_name, owners: owners})
        break;
      case "email":
        if (this.state.sort_direction_email) {
          owners.sort((a,b)=>(a.email > b.email) ? 1 : ((b.email > a.email) ? -1 : 0))
        } else {
          owners.sort((a,b)=>(a.email < b.email) ? 1 : ((b.email < a.email) ? -1 : 0))
        }
        this.setState({sort_direction_email: !this.state.sort_direction_email, owners: owners})
        break;
      case "phone":
        if (this.state.sort_direction_phone) {
          owners.sort((a,b)=>(a.phone > b.phone) ? 1 : ((b.phone > a.phone) ? -1 : 0))
        } else {
          owners.sort((a,b)=>(a.phone < b.phone) ? 1 : ((b.phone < a.phone) ? -1 : 0))
        }
        this.setState({sort_direction_phone: !this.state.sort_direction_phone, owners: owners})
        break;
      case "country":
        if (this.state.sort_direction_country) {
          owners.sort((a,b)=>(a.country > b.country) ? 1 : ((b.country > a.country) ? -1 : 0))
        } else {
          owners.sort((a,b)=>(a.country < b.country) ? 1 : ((b.country < a.country) ? -1 : 0))
        }
        this.setState({sort_direction_country: !this.state.sort_direction_country, owners: owners})
        break;
      case "city":
        if (this.state.sort_direction_city) {
          owners.sort((a,b)=>(a.city > b.city) ? 1 : ((b.city > a.city) ? -1 : 0))
        } else {
          owners.sort((a,b)=>(a.city < b.city) ? 1 : ((b.city < a.city) ? -1 : 0))
        }
        this.setState({sort_direction_city: !this.state.sort_direction_city, owners: owners})
        break;
      default:
        break;
    }
  }
  render() {
    const disableStyle = {
      color: '#9a9a9a'
    }
    const { create_date, filter_start, filter_end,
      first, next, previous, last, index, total, owners } = this.state
    const {childrens} = this.props 
    return (
      <React.Fragment>
        <ResponsiveModal open={this.state.is_loading} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LoadingModal title="Loading..."/>
        </ResponsiveModal>
        {this.state.alert}
        <div className="content" id="admin-owner">
          <Row>
            <Col md="12">
              <CardHeader className="management-header">
                <Col md='4'>
                  <CardTitle tag="h4">Admin Owner</CardTitle>
                </Col>
                <Col md='4' className='funtionSpace'>
                  {childrens.indexOf(7)>=0?
                  <Link to='/admin/add-owner'>
                    <button className='btn-add btn-cosmic'>
                      <i className="tim-icons icon_add" />
                      <span>Add Owner</span>
                    </button> 
                  </Link>:null}
                </Col>                                
                <Col md='4'className='filterSpace'> 
                {/* <div className="tools tools-group">
                    <UncontrolledDropdown>
                      <DropdownToggle
                        caret
                        className="btn-filter"
                        data-toggle="dropdown"
                      >
                        Filter
                      </DropdownToggle>
                      <DropdownMenu right>
                        <div className="filter-header">
                          <div className="filter-header-group">
                            <div className="filter-text">Filters</div>
                            <div className="filter-btn-group">
                              <div className="filter-clear" onClick={this.clearFilter}>Clear</div>
                              <div className="filter-done" onClick={this.doneFilter}>Done</div>
                            </div>
                          </div>
                        </div>

                        <div className="filter-check">
                          <input id="create_date" type="checkbox" name="create_date" checked={create_date} onChange={this.filterCheck}/>
                          <label htmlFor="create_date">Created Date</label>
                        </div>
                        <div className="filter-check">
                          <Datetime 
                            className="date_time"
                            inputProps={{placeholder:"Start Date"}}                          
                            timeFormat={false}
                            closeOnSelect={true}
                            value={filter_start}
                            onChange={this.startDateChanged}
                          />
                          <div>-</div>
                          <Datetime 
                            className="date_time"
                            inputProps={{placeholder:"End Date"}}                          
                            timeFormat={false}
                            closeOnSelect={true}
                            value={filter_end}
                            onChange={this.endDateChanged}
                          />
                        </div>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </div> */}               
                </Col>
              </CardHeader>
            </Col>
            <Col md="12">
              <Card>
                <CardBody>
                  <Table>
                    <thead className="text-primary">
                      <tr>
                        <th onClick={()=>{this.sortData('name')}}>Name <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('email')}}>Email <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('phone')}}>Phone <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('country')}}>Country <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('city')}}>City <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody className="management-body">{this.renderOwners()}</tbody>
                  </Table>
                  {owners.length === 0 && <div className="no-result">No results found</div>}
                  {owners.length > 0 &&
                  <div className="nav-group">
                    <div><span>Showing</span></div>
                    <div className="first" style={first===null?disableStyle:{}} onClick={this.first}><i className="fa fa-angle-double-left"></i></div>
                    <div className="previous" style={previous===null?disableStyle:{}} onClick={this.previous}><i className="fa fa-angle-left"></i></div>
                    <div className="index"><span>{index}</span></div>
                    <div><span>{'to'}</span></div>
                    <div className="count"><span>{owners.length === 0 ? 0 : index + owners.length - 1}</span></div>
                    <div><span>{'of'}</span></div>
                    <div className="total"><span>{new Intl.NumberFormat().format(total)}</span></div>
                    <div className="next" style={next===null?disableStyle:{}} onClick={this.next}><i className="fa fa-angle-right"></i></div>
                    <div className="last" style={last===null?disableStyle:{}} onClick={this.last}><i className="fa fa-angle-double-right"></i></div>
                  </div>
                  }
                </CardBody>
              </Card>
            </Col>
          </Row>
          <ResponsiveModal open={this.state.confirmModal} onClose={this.onCloseConfirmModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
            <ConfirmModal onClose={this.onCloseConfirmModal} onAuthenticate={this.authenticate}/>
          </ResponsiveModal>
          <ToastContainer />
        </div>
      </React.Fragment>
    );
  }
}

export default AdminOwner;
