import React from 'react';
import {
  Button,
  Label,
  FormGroup,
  Input,
  Row,
  Col,
} from 'reactstrap';

import { getAllCountries, getAllStates, getAllCities } from '../../api/member'

import './styles/updateModal.scss'

class UpdateModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      first_name: "",
      last_name: "",
      email: "",
      indicative: "",
      phone: "",
      street: "",
      zip_code: "",
      id_country: "-1",
      id_state: "-1",
      id_city: "-1",
      pre_country: "No",

      countries: [],
      states: [],
      cities: [],

      indicatvieActive: false,
    }

    this.indicativeBtn = null
  }

  componentWillMount() {
    const {owner} = this.props
    this.setState({
      first_name: owner.first_name,
      last_name: owner.last_name,
      email: owner.email,
      indicative: owner.indicative,
      phone: owner.phone,
      id_city: owner.id_city,
      id_country: owner.id_country,
      id_state: owner.id_state,
      zip_code: owner.zip_code,
      street: owner.address?owner.address:'',
    }, ()=>{
      this.getAllCountries()
    })
    if (this.props.owner.id_country) this.getAllStates(this.props.owner.id_country)
    if (this.props.owner.id_state) this.getAllCities(this.props.owner.id_state)
  }

  componentDidMount() {
    document.addEventListener('click', this.handleOutsideIndicative, false);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleOutsideIndicative, false);
  }

  async getAllStates(id_country) {
    const res = await getAllStates({id_country: id_country})
    if (res.status === 200 && res.st) {
      var states = res.States      
      this.setState({states: states ? states : []})
    }
  }

  async getAllCities(id_state) {
    const res = await getAllCities({id_state: id_state})
    if (res.status === 200 && res.st) {
      var cities = res.Cities
      this.setState({cities: cities ? cities : []})
    }
  }

  async getAllCountries() {
    const res = await getAllCountries()
    if (res.response.status === 200) {
      var countries = res.response.Countries
      const index = countries.findIndex(country => country.indicative === this.state.indicative)
      if (index !== -1)
        this.setState({pre_country: countries[index].iso, countries: countries ? countries : []})
      else
        this.setState({countries: countries ? countries : []})
    }
  }

  changeStatus = () => {
    var owner = {}
    owner['uuid'] = this.props.owner.uuid
    owner['first_name'] = this.state.first_name
    owner['last_name'] = this.state.last_name
    owner['email'] = this.state.email
    owner['indicative'] = this.state.indicative
    owner['phone'] = this.state.phone
    owner['id_country'] = this.state.id_country
    owner['id_state'] = this.state.id_state
    owner['id_city'] = this.state.id_city
    owner['address'] = this.state.address
    owner['zip_code'] = this.state.zip_code
    this.props.updateData(owner)
  }

  handleOutsideIndicative = (e) => {
    if (this.indicativeList) {
			if (this.indicativeBtn.contains(e.target) || this.indicativeList.contains(e.target)) {return;}
		}
		if(this.state.indicatvieActive){
			this.setState({indicatvieActive: false})
		}
  }

  activeIndicative = () => {
    this.setState({indicatvieActive: !this.state.indicatvieActive})
  }

  changeIndicative = (iso, indicative) => {
    this.setState({indicative: indicative, pre_country: iso, indicatvieActive: false})
  }

  changeValue = (e) => {
    var obj = {}
    var name = e.target.name, value = e.target.value
    obj[name] = value

    this.setState(obj, ()=>{
      if(name==='id_country' && value !== '-1') {
        this.getAllStates(value)
      }
      if(name==='id_state' && value !== '-1') {
        this.getAllCities(value)
      }
    })
  }

  render() {
    const {email, first_name, last_name, indicative, phone, 
        id_city, id_country, id_state, pre_country, countries, states, cities,
        zip_code, street} = this.state
    const { onClose } = this.props
    return (
      <div id="update_owner">
        <div className="update_owner">
          <div className="change-title">
            <span className="title">{'Update user details'}</span>
          </div>
          <div className="line-px"></div>
          <div className="user-detail">
            <Row>
              <Label sm={3} className="id-label">Email</Label>
              <Col sm={9}>
                <Input name="email" type="email" value={email} onChange={this.changeValue}/>              
              </Col>
            </Row>
            <Row>
              <Label sm={3} className="id-label">First Name</Label>
              <Col sm={9}>
                <Input name="first_name" type="text" value={first_name} onChange={this.changeValue}/>              
              </Col>
            </Row>
            <Row>
              <Label sm={3} className="id-label">Last Name</Label>
              <Col sm={9}>
                <Input name="last_name" type="text" value={last_name} onChange={this.changeValue}/>              
              </Col>
            </Row>
            <Row>
              <Label sm={3} className="id-label">Mobile Phone</Label>
              <Col sm={9}>
                <FormGroup className={'phone_class '}>
                  <div ref={(ref) => {this.indicativeBtn = ref;}} className="pre-country non-border" onClick={this.activeIndicative}>
                    {pre_country}
                    <img alt="" src={require("assets/img/asc.gif")}/>
                  </div>
                  <span>{indicative}</span>
                  <Input
                    name="phone"
                    type="text"
                    className="non-border"
                    placeholder="(943) 572-2116"
                    value={phone}
                    onChange={this.changeValue}
                    onBlur={this.changeValue}
                  />
                </FormGroup>
                <div ref={(ref) => {this.indicativeList = ref;}}>
                  {this.state.indicatvieActive && 
                  <div className="indicative_list">
                    {countries.map((country, index) => <div key={index} className="indicative" onClick={()=>this.changeIndicative(country.iso, country.indicative)}>{country.country}</div>)}
                  </div>}
                </div>
              </Col>
            </Row>
          </div>
        </div>

        <div className="update_owner">
          <div className="change-title">
            <span className="title">{'Address'}</span>
          </div>
          <div className="line-px"></div>
          <div className="user-detail">
            <Row>
              <Label sm={3} className="id-label">Country</Label>
              <Col sm={9}>
                <Input
                  name="id_country"
                  type="select"
                  value={id_country}
                  onChange={this.changeValue}
                  onBlur={this.changeValue}
                >
                  <option key={0} value={''} className="dark-back">--Select Country--</option>
                  {countries.map((country, index) => <option key={index} value={country.id_country} className="dark-back">{country.country}</option>)}
                </Input>
              </Col>
            </Row>
            <Row>
              <Label sm={3} className="id-label">State</Label>
              <Col sm={9}>
                <Input
                  name="id_state"
                  type="select"
                  value={id_state}
                  onChange={this.changeValue}
                  onBlur={this.changeValue}
                >
                  <option key={0} value={''} className="dark-back">--Select State--</option>
                  {states.map((state, index) => <option key={index} value={state.id_state} className="dark-back">{state.state}</option>)}
                </Input>
              </Col>
            </Row>
            <Row>
              <Label sm={3} className="id-label">City</Label>
              <Col sm={9}>
                <Input
                  name="id_city"
                  type="select"
                  value={id_city}
                  onChange={this.changeValue}
                  onBlur={this.changeValue}
                >
                  <option key={0} value={''} className="dark-back">--Select City--</option>
                  {cities.map((city, index) => <option key={index} value={city.id_city} className="dark-back">{city.city}</option>)}
                </Input>
              </Col>
            </Row>
            <Row>
              <Label sm={3} className="id-label">Street</Label>
              <Col sm={9}>
                <Input name="street" type="text" value={street} onChange={this.changeValue}/>              
              </Col>
            </Row>
            <Row>
              <Label sm={3} className="id-label">Zip code</Label>
              <Col sm={9}>
                <Input name="zip_code" type="text" value={zip_code} onChange={this.changeValue}/>              
              </Col>
            </Row>
          </div>
        </div>

        <Row className='cosmic-butons butonsUpdateOpe'>
          <Button className="cosmic-cancel" onClick={onClose}>Cancel</Button>
          <Button className="cosmic-confirm" onClick={this.changeStatus}>Update Owner</Button>
        </Row>
      </div>
    )
  }
}

export default UpdateModal;