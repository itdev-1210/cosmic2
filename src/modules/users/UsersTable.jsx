import React from 'react';

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
  DropdownToggle,
  DropdownMenu,
  UncontrolledDropdown,
} from 'reactstrap';

import { Link } from 'react-router-dom';
import { getUsers, getNav, getFilter } from '../../api/UserApi';
import {emitter, subscribe,unSubscribe } from '../../utils/EventComponents';
import './styles.scss'
import LoadingModal from './../../components/Navbars/LogoutModal'
import ResponsiveModal from 'react-responsive-modal';

class UsersTable extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      search:'',
      is_loading: true,
      users: [],
      status: [],
      filters: {},

      first: null,
      previous: null,
      next: null,
      last: null,
      total: 0,
      index: 0,

      sort_direction_name: false,
      sort_direction_email: true,
      sort_direction_phone: true,
      sort_direction_country: true,
      sort_direction_status: true,
    }
  }

  sortData = (type) => {
    let users = Object.assign([], this.state.users)
    switch(type) {
      case "name":
        if (this.state.sort_direction_name) {
          users.sort((a,b)=>(a.first_name+a.last_anme > b.first_name+b.last_name) ? 1 : ((b.first_name+b.last_name > a.first_name+a.last_anme) ? -1 : 0))
        } else {
          users.sort((a,b)=>(a.first_name+a.last_anme < b.first_name+b.last_name) ? 1 : ((b.first_name+b.last_name < a.first_name+a.last_anme) ? -1 : 0))
        }
        this.setState({sort_direction_name: !this.state.sort_direction_name, users: users})
        break;
      case "email":
        if (this.state.sort_direction_email) {
          users.sort((a,b)=>(a.email > b.email) ? 1 : ((b.email > a.email) ? -1 : 0))
        } else {
          users.sort((a,b)=>(a.email < b.email) ? 1 : ((b.email < a.email) ? -1 : 0))
        }
        this.setState({sort_direction_email: !this.state.sort_direction_email, users: users})
        break;
      case "phone":
        if (this.state.sort_direction_phone) {
          users.sort((a,b)=>(a.phone > b.phone) ? 1 : ((b.phone > a.phone) ? -1 : 0))
        } else {
          users.sort((a,b)=>(a.phone < b.phone) ? 1 : ((b.phone < a.phone) ? -1 : 0))
        }
        this.setState({sort_direction_phone: !this.state.sort_direction_phone, users: users})
        break;
      case "country":
        if (this.state.sort_direction_country) {
          users.sort((a,b)=>(a.country > b.country) ? 1 : ((b.country > a.country) ? -1 : 0))
        } else {
          users.sort((a,b)=>(a.country < b.country) ? 1 : ((b.country < a.country) ? -1 : 0))
        }
        this.setState({sort_direction_country: !this.state.sort_direction_country, users: users})
        break;
      case "status":
        if (this.state.sort_direction_status) {
          users.sort((a,b)=>(a.status > b.status) ? 1 : ((b.status > a.status) ? -1 : 0))
        } else {
          users.sort((a,b)=>(a.status < b.status) ? 1 : ((b.status < a.status) ? -1 : 0))
        }
        this.setState({sort_direction_status: !this.state.sort_direction_status, users: users})
        break;
      default:
        break;
    }
  }

  setData = (data) => {
    let status = data.Status ? data.Status : []
    let users = data.Users ? data.Users : []
    if (!this.state.sort_direction_name) {
      users.sort((a,b)=>(a.first_name+a.last_anme > b.first_name+b.last_name) ? 1 : ((b.first_name+b.last_name > a.first_name+a.last_anme) ? -1 : 0))
    } else {
      users.sort((a,b)=>(a.first_name+a.last_anme < b.first_name+b.last_name) ? 1 : ((b.first_name+b.last_name < a.first_name+a.last_anme) ? -1 : 0))
    }
    this.setState({
      users: data.Users ? data.Users : [],
      first: data.first,
      previous: data.previous,
      next: data.next,
      last: data.last,
      total: data.total,
      index: data.index,
      status: status,
    })
  }

  fetchData = async () => {
    const result = await getUsers();
    let data = result.response;
    let status = data.Status ? data.Status : []
    let filters = {}
    status.map((status) => {
      filters['status_'+status.id_status] = false
      return status
    })

    let users = data.Users ? data.Users : []
    users.sort((a,b)=>(a.first_name+a.last_anme > b.first_name+b.last_name) ? 1 : ((b.first_name+b.last_name > a.first_name+a.last_anme) ? -1 : 0))
    
    this.setState({
      is_loading: false,
      users: users,
      first: data.first,
      previous: data.previous,
      next: data.next,
      last: data.last,
      total: data.total,
      index: data.index,
      status: status,
      filters: filters,
    })
  }

  filterCheck = (e) => {
    const name = e.target.name
    let filters = Object.assign({}, this.state.filters)
    filters[name] = !filters[name]
    this.setState({filters: filters})
  }

  clearFilter = () => {
    let filters = {}
    this.state.status.map((status) => filters['status_'+status.id_status_user] = false)
    this.setState({filters: filters,search:''}, () => {
      emitter('searchBar',true)
      this.fetchData()
    })
  }

  doneFilter = async () => {
    const { status, filters } = this.state
    let result;
    var filter_status = 'filter='

    status.map((status) => {
      if (filters['status_'+status.id_status_user]) filter_status += status.id_status_user + ','
      return status;
    })

    if (filter_status === 'filter=') {
      return;
    } else {
      result = await getFilter(filter_status.substr(0, filter_status.length-1))
    }
    this.setData(result.response)
    this.setState({search:''},emitter('searchBar',true))
  }

  setParam = () => {
    const { status, filters,search } = this.state
    var filter_status = 'filter='
    status.map((status) => {
      if (filters['status_'+status.id_status_user]) filter_status += status.id_status_user + ','
      return status;
    })
    if (filter_status === 'filter=') {
      return '';
    } else if(search!==''){
      return ('&query='+this.state.search)
    }else {
      return filter_status.substr(0, filter_status.length-1)
    }
  }

  first = async () => {
    if (this.state.first === null)
      return

    const result = await getNav(this.state.first + this.setParam())
    this.setData(result.response)
  }

  previous = async () => {
    if (this.state.previous === null)
      return
    const result = await getNav(this.state.previous + this.setParam())
    this.setData(result.response)
  }

  next = async () => {
    if (this.state.next === null)
      return
    const result = await getNav(this.state.next + this.setParam())
    this.setData(result.response)
  }

  last = async () => {
    if (this.state.last === null)
      return
    const result = await getNav(this.state.last + this.setParam())
    this.setData(result.response)
  }

  componentDidMount() {
    subscribe('search',(value)=> this.dinamycSearch(value))
    emitter('searchBar',true)
    this.fetchData()
  }
  componentWillUnmount =() => {
    unSubscribe('search')
    emitter('searchBar',false)
  }
  dinamycSearch = async (value) =>{
    const result = await getFilter('query='+value)
    this.setState({
      filters: {},
      search:value
    })
    this.setData(result.response)
  }

  handleUserDetails = (cuid) => {
    this.props.history.push('/admin/user-detail/'+cuid)
  }

  renderUsers = () => {
    const { users } = this.state
    return users.map((user, index) => (
      <tr key={index} onClick={() => this.handleUserDetails(user.cuid)} className="focus_on">
        {/* <td className="text-center">
          <div className="photo">
            <img alt="..." src={require('assets/img/tania.jpg')} />
          </div>
        </td> */}
        <td>{`${user.first_name} ${user.last_name}`}</td>
        <td>{user.email}</td>
        <td>{user.phone}</td>
        <td>{user.country}</td>
        <td>
          <div className="payment-status">
            <span>{user.status}</span>
            {user.status === 'Available' ? <img alt="" src={require("assets/img/icon_check.png")}/> : 
            user.status === 'Banned' ? <img alt="" src={require("assets/img/icon_ban.svg")}/> : 
            user.status === 'Debt' ? <img alt="" src={require("assets/img/icon_pending.svg")}/> : 
            user.status === 'Inactive' ? <img alt="" src={require("assets/img/icon_fail.png")}/> : null}
          </div>
        </td>
      </tr>
    ));
  }

  render() {
    const disableStyle = {
      color: '#9a9a9a'
    }
    const { users, first, last, next, previous, index, total, filters, status } = this.state

    return (
      <React.Fragment>
        <ResponsiveModal open={this.state.is_loading} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LoadingModal title="Loading..."/>
        </ResponsiveModal>
        <div className="content" id="user-table">
          <Row>
            <Col md="12">
              <CardHeader className="management-header">
                <Col md='4'>
                  <CardTitle tag="h4">Admin Riders</CardTitle>
                </Col>
                <Col md='4' className='funtionSpace'>
                </Col>                                
                <Col md='4'className='filterSpace'>
                  <div className="tools tools-group">
                    <UncontrolledDropdown>
                      <DropdownToggle
                        caret
                        className="btn-filter"
                        data-toggle="dropdown"
                      >
                        Filter
                      </DropdownToggle>
                      <DropdownMenu right>
                        <div className="filter-header">
                          <div className="filter-header-group">
                            <div className="filter-text">Filters</div>
                            <div className="filter-btn-group">
                              <div className="filter-clear" onClick={this.clearFilter}>Clear</div>
                              <div className="filter-done" onClick={this.doneFilter}>Done</div>
                            </div>
                          </div>
                        </div>

                        {status.map((status, index) =>  <div className="filter-check" key={index}>
                          <input id={status.status} type="checkbox" name={'status_'+status.id_status_user} checked={filters['status_'+status.id_status_user]} onChange={this.filterCheck}/>
                          <label htmlFor={status.status}>{status.status}</label>
                        </div>)}
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </div>                
                </Col>
              </CardHeader>
            </Col>
          </Row>
          <Row>
            <Col md="12">
              <Card>
                <CardBody>
                  <Table>
                    <thead className="text-primary">
                      <tr>
                        {/* <th className="text-center">#</th> */}
                        <th onClick={()=>{this.sortData('name')}}>Name <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('email')}}>Email <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('phone')}}>Phone <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('country')}}>Country <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('status')}}>Status <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                      </tr>
                    </thead>
                    <tbody>{this.renderUsers()}</tbody>
                  </Table>
                  {this.state.users.length === 0 && <div className="no-result">No results found</div>}
                  {users.length > 0 && 
                  <div className="nav-group">
                    <div><span>Showing</span></div>
                    <div className="first" style={first===null?disableStyle:{}} onClick={this.first}><i className="fa fa-angle-double-left"></i></div>
                    <div className="previous" style={previous===null?disableStyle:{}} onClick={this.previous}><i className="fa fa-angle-left"></i></div>
                    <div className="index"><span>{index}</span></div>
                    <div><span>{'to'}</span></div>
                    <div className="count"><span>{users.length === 0 ? 0 : index + users.length - 1}</span></div>
                    <div><span>{'of'}</span></div>
                    <div className="total"><span>{new Intl.NumberFormat().format(total)}</span></div>
                    <div className="next" style={next===null?disableStyle:{}} onClick={this.next}><i className="fa fa-angle-right"></i></div>
                    <div className="last" style={last===null?disableStyle:{}} onClick={this.last}><i className="fa fa-angle-double-right"></i></div>
                  </div>
                  }
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </React.Fragment>
    );
  }
}

export default UsersTable;
