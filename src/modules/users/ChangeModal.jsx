import React, { Component } from 'react';
import {
  Button,
  Input,
} from 'reactstrap';

import './modal.scss'
class ChangeModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user_status: this.props.user.id_status
    }
    this.change = this.change.bind(this)
  }

  change(e) {
    this.setState({user_status: e.target.value})
  }

  changeStatus = () => {
    let newUser = Object.assign({}, this.props.user);
    newUser.id_status = this.state.user_status
    this.props.onChange(newUser)
  }
  render() {
    const { user, status, onClose } = this.props
    const { user_status } = this.state
    return (
      <div className="user-detail-change">
        <div className="change-title">
          <span className="title">{'Change status'}</span>
        </div>
        <div className="status-group">
          <div className="input_group">
            <div>User</div>
            <div className="text-center">{user.email}</div>

            <div>Status</div>
            <div>
              <Input
                name="status"
                type="select"
                value={user_status}
                className={user_status.toString() === '1' ? 'available' :
                    user_status.toString() === '2' ? 'banned' :
                    user_status.toString() === '3' ? 'debt' :
                    user_status.toString() === '4' ? 'inactive' : ''}
                    onChange={this.change}
              >
              {status.map((status, index) => 
              <option key={index} value={status.id_status_user} 
                className={status.id_status_user === 1 ? 'available' :
                  status.id_status_user === 2 ? 'banned' :
                  status.id_status_user === 3 ? 'debt' :
                  status.id_status_user === 4 ? 'inactive' : ''}>{status.status}</option>)}
              </Input>
            </div>
          </div>
        </div>
        <div className="btn-change-group">
          <Button className="change-btn" onClick={onClose}>Cancel</Button>
          <Button className="change-btn primary" onClick={this.changeStatus}>Change status</Button>
        </div>
      </div>
    )
  }
}

export default ChangeModal