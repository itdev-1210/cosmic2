import React from 'react';

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Form,
  Row,
  Col,
  Table
} from 'reactstrap';
import Moment from 'react-moment'
import { getUser, changeAccount } from '../../api/UserApi';
import CurrencyFormat from 'react-currency-format';
import ResponsiveModal from 'react-responsive-modal';
import ConfirmModal from '../../components/Cosmic/InAuthModal/InAuthModal'
import ChangeModal from './ChangeModal'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import LoadingModal from './../../components/Navbars/LogoutModal'

class UserDetail extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      is_loading: true,
      user: {},
      balance: {},
      payments: [],
      cards: [],
      status: [],
      cuid: null,
      userRole: true,
      showModal: false,
      authModal: false,

      activeUser: null,

      sort_direction_order_id: false,
      sort_direction_amount: true,
      sort_direction_currency: true,
      sort_direction_token: true,
      sort_direction_date: true,
      sort_direction_email: true,
      sort_direction_payment_method: true,
      sort_direction_service: true,
      sort_direction_status: true,
    }
  }

  sortData = (type) => {
    let payments = Object.assign([], this.state.payments)
    switch(type) {
      case "order_id":
        if (this.state.sort_direction_order_id) {
          payments.sort((a,b)=>(a.order_id > b.order_id) ? 1 : ((b.order_id > a.order_id) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.order_id < b.order_id) ? 1 : ((b.order_id < a.order_id) ? -1 : 0))
        }
        this.setState({sort_direction_order_id: !this.state.sort_direction_order_id, payments: payments})
        break;
      case "amount":
        if (this.state.sort_direction_amount) {
          payments.sort((a,b)=>(parseFloat(a.amount) > parseFloat(b.amount)) ? 1 : ((parseFloat(b.amount) > parseFloat(a.amount)) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(parseFloat(a.amount) < parseFloat(b.amount)) ? 1 : ((parseFloat(b.amount) < parseFloat(a.amount)) ? -1 : 0))
        }
        this.setState({sort_direction_amount: !this.state.sort_direction_amount, payments: payments})
        break;
      case "currency":
        if (this.state.sort_direction_currency) {
          payments.sort((a,b)=>(a.currency > b.currency) ? 1 : ((b.currency > a.currency) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.currency < b.currency) ? 1 : ((b.currency < a.currency) ? -1 : 0))
        }
        this.setState({sort_direction_currency: !this.state.sort_direction_currency, payments: payments})
        break;
      case "card":
        if (this.state.sort_direction_phone) {
          payments.sort((a,b)=>(a.brand + a.last_four > b.brand + b.last_four) ? 1 : ((b.brand + b.last_four > a.brand + a.last_four) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.brand + a.last_four < b.brand + b.last_four) ? 1 : ((b.brand + b.last_four < a.brand + a.last_four) ? -1 : 0))
        }
        this.setState({sort_direction_token: !this.state.sort_direction_token, payments: payments})
        break;
      case "date":
        if (this.state.sort_direction_date) {
          payments.sort((a,b)=>(a.date > b.date) ? 1 : ((b.date > a.date) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.date < b.date) ? 1 : ((b.date < a.date) ? -1 : 0))
        }
        this.setState({sort_direction_date: !this.state.sort_direction_date, payments: payments})
        break;
      case "email":
        if (this.state.sort_direction_email) {
          payments.sort((a,b)=>(a.email > b.email) ? 1 : ((b.email > a.email) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.email < b.email) ? 1 : ((b.email < a.email) ? -1 : 0))
        }
        this.setState({sort_direction_email: !this.state.sort_direction_email, payments: payments})
        break;
      case "payment_method":
        if (this.state.sort_direction_payment_method) {
          payments.sort((a,b)=>(a.payment_method > b.payment_method) ? 1 : ((b.payment_method > a.payment_method) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.payment_method < b.payment_method) ? 1 : ((b.payment_method < a.payment_method) ? -1 : 0))
        }
        this.setState({sort_direction_payment_method: !this.state.sort_direction_payment_method, payments: payments})
        break;
      case "service":
        if (this.state.sort_direction_service) {
          payments.sort((a,b)=>(a.service > b.service) ? 1 : ((b.service > a.service) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.service < b.service) ? 1 : ((b.service < a.service) ? -1 : 0))
        }
        this.setState({sort_direction_service: !this.state.sort_direction_service, payments: payments})
        break;
      case "status":
        if (this.state.sort_direction_status) {
          payments.sort((a,b)=>(a.status > b.status) ? 1 : ((b.status > a.status) ? -1 : 0))
        } else {
          payments.sort((a,b)=>(a.status < b.status) ? 1 : ((b.status < a.status) ? -1 : 0))
        }
        this.setState({sort_direction_status: !this.state.sort_direction_status, payments: payments})
        break;
      default:
        break;
    }
  }

  async fetchData() {
    const res = await getUser(this.state.cuid)
    this.setData(res.response)
  }
  componentDidMount() {
    this.fetchData()
  }

  componentWillMount() {
    const { match: { params } } = this.props;
    // let userRole = auth.checkRole('USERS', 'Admin Users')
    // this.setState({cuid: params.cuid, userRole: userRole})
    this.setState({cuid: params.cuid})
  }

  setData = (res) => {
    this.setState({
      is_loading: false,
      balance: res.balance ? res.balance : {},
      cards: res.cards ? res.cards : [],
      payments: res.payments ? res.payments : [],
      user: res.user ? res.user : {},
      status: res.Status ? res.Status : []
    })
  }

  renderPayments = () => {
    const { payments } = this.state
    return payments.map((payment, index) => (
      <tr key={index} onClick={()=>{this.props.history.push('/admin/payment/' + payment.id_payment)}} style={{cursor: 'pointer'}}>
        <td className="text-left">{payment.order_id}</td>
        <td>
          <CurrencyFormat 
            value={payment.amount}
            displayType={'text'}
            thousandSeparator={true}
            decimalScale={2}
            fixedDecimalScale={true}
            renderText={value => <div className="upper-case">{`${value}`}</div>}/>
        </td>
        <td>{typeof payment.currency==='string'?payment.currency.toUpperCase():payment.currency}</td>
        <td>{(typeof payment.brand==='string'?payment.brand.toUpperCase():payment.brand) + ' ' + payment.last_four}</td>
        <td className="text-left"><Moment format="MMM DD, YYYY h:mm A">{payment.date}</Moment></td>
        <td className="text-left">{payment.email}</td>
        <td className="text-left">{payment.device}</td>
        <td className="text-left">
          <div className="payment-status">
            <span>{payment.status}</span>
            {payment.status === 'Succeded' ? <img alt="" src={require("assets/img/icon_check.png")}/> : 
            payment.status === 'Failed' ? <img alt="" src={require("assets/img/icon_fail.png")}/> : 
            payment.status === 'Pending' ? <img alt="" src={require("assets/img/icon_pending.png")}/> : 
            payment.status === 'Refunded' ? <img alt="" src={require("assets/img/icon_reply.png")}/> : null}
          </div>
        </td>
      </tr>
    ));
  }

  reload = () => {
    this.fetchData()
  }

  changeUserStatus = () => {
    this.setState({activeUser: this.state.user, showModal: true})
  }

  onCloseShowModal = () => {
    this.setState({showModal: false})
  }

  changeStatus = (changeUser) => {
    this.setState({activeUser: changeUser, showModal: false, authModal: true})
  }

  onCloseAuthModal = () => {
    this.setState({authModal: false})
  }

  authenticae = async (res) => {
    if (res.response.status !== 200)
      toast.error(res.response.message, {position: toast.POSITION_TOP_RIGHT})
    else {
      const { activeUser } = this.state
      const result = await changeAccount({cuid: activeUser.cuid, id_status_user: activeUser.id_status})
      if(result.st){
        if(result.status===200){
          toast.success(result.message, {position: toast.POSITION_TOP_RIGHT})
          this.setState({authModal: false},this.reload)
        }else{
          toast.erro(result.message, {position: toast.POSITION_TOP_RIGHT})
          this.setState({authModal: false})
        }
      }else{
        toast.erro(result.message, {position: toast.POSITION_TOP_RIGHT})
        this.setState({authModal: false})
      }
    }
  }

  render() {
    const { user, balance, cards } = this.state
    return (
      <>
        <ResponsiveModal open={this.state.is_loading} 
          onClose={()=>{}} center 
          classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
          <LoadingModal title="Loading..."/>
        </ResponsiveModal>
        <div className="content" id="uesr-detail">
          <div className="page-header">
            <span className="arrow-back" onClick={()=>{this.props.history.goBack()}}><img alt="" src={require("assets/img/icon_arrow.svg")}/></span>
            <h4 className="title pl-3 icon-user">{user && user.first_name && user.last_name ? user.first_name + ' ' + user.last_name : ''}</h4>
          </div>
          <div className="page-header">
            <div className="payment-status" onClick={this.changeUserStatus}>
            <span>{user.status}</span>
            {user.id_status ===  1 ? <img alt="" src={require("assets/img/icon_check.png")}/> : 
            user.id_status ===   2 ? <img alt="" src={require("assets/img/icon_ban.svg")}/> : 
            user.id_status ===   3 ? <img alt="" src={require("assets/img/icon_pending.svg")}/> : 
            user.id_status ===   4 ? <img alt="" src={require("assets/img/icon_fail.png")}/> : null}
            </div>
            <div className="icon-reload" onClick={this.reload}>
              <img alt="" src={require("assets/img/icon_reload.svg")}/>
            </div>
          </div>
          <Row>
            <Col md="6">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Account Information</CardTitle>
                </CardHeader>
                <CardBody>
                  <Form className="form-horizontal account-info" action="#">
                    <Row className="one-content">
                      <Col md="3" className="sub-header">CUID</Col>
                      <Col md="9" className="sub-data">{user && user.cuid ? user.cuid : ''}</Col>
                    </Row>
                    <Row className="one-content">
                      <Col md="3" className="sub-header">Created</Col>
                      <Col md="9" className="sub-data">{user && user.created ? new Date(user.created).toLocaleString() : ''}</Col>
                    </Row>
                    <Row className="one-content">
                      <Col md="3" className="sub-header">Name</Col>
                      <Col md="9" className="sub-data">{user && user.first_name && user.last_name ? user.first_name + ' ' + user.last_name : ''}</Col>
                    </Row>
                    <Row className="one-content">
                      <Col md="3" className="sub-header">Email</Col>
                      <Col md="9" className="sub-data">{user && user.email ? user.email : ''}</Col>
                    </Row>
                    <Row className="one-content">
                      <Col md="3" className="sub-header">Phone</Col>
                      <Col md="9" className="sub-data">{user && user.indicative && user.phone ? user.indicative + user.phone : ''}</Col>
                    </Row>
                    <Row className="one-content">
                      <Col md="3" className="sub-header">Birthday</Col>
                      <Col md="9" className="sub-data">{user && user.birthday ? user.birthday : ''}</Col>
                    </Row>
                    <Row className="one-content">
                      <Col md="3" className="sub-header">Country</Col>
                      <Col md="9" className="sub-data">{user && user.country ? user.country : ''}</Col>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </Col>
            <Col md="6">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Cards</CardTitle>
                </CardHeader>
                <CardBody>
                  <Form className="form-horizontal">
                    <Row>
                      <Col md="9">
                      {cards.length ? cards.map((card, index) => <div className="payment-card" key={index}>
                        <div className="brand-card">
                          <img alt="" src={require("assets/img/icon_credit_card_visa.png")}/>
                          {card.brand.toUpperCase() + ' ' + card.last_four}
                        </div>
                        {/* <img alt="" src={require("assets/img/icon_delete.svg")}/> */}
                      </div>) : <div className="non-payment-card">{'No payment method added'}</div>}
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
                {/* <CardHeader>
                  <CardTitle tag="h4">Currency</CardTitle>
                </CardHeader>
                <CardBody>
                  <Form className="form-horizontal">
                    <Row>
                      <Col md="4">
                        <FormGroup>
                          <Input type="text" defaultValue={balance.currency? balance.currency : ''} style={{textTransform:"uppercase"}}/>
                        </FormGroup>
                      </Col>
                    </Row>
                  </Form>
                </CardBody> */}
              </Card>
            </Col>
            <Col md="12">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">User balance</CardTitle>
                </CardHeader>
                <CardBody>
                  <Form action="/" className="form-horizontal" method="get">
                    <Row>
                      <Col md="12">
                        <h6>{new Intl.NumberFormat('us-US', {style: 'currency', currency: 'USD'}).format(balance && balance.balance ? balance.balance : '0')}</h6>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </Col>
            <Col md="12">
              <Card>
                <CardHeader className="show-more-payment">
                  <CardTitle tag="h4">Payments</CardTitle>
                  <div onClick={()=>{this.props.history.push({pathname:'/admin/payment', state: {email: user.email}})}}>Show more...</div>
                </CardHeader>
                <CardBody>
                  <Table>
                    <thead className="text-primary">
                      <tr>
                        <th className="text-left" onClick={()=>{this.sortData('order_id')}}>Order ID <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('amount')}}>Amount <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th onClick={()=>{this.sortData('currency')}}>Currency <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th className="text-left" onClick={()=>{this.sortData('card')}}>Card <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th className="text-left" onClick={()=>{this.sortData('date')}}>Date <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th className="text-left" onClick={()=>{this.sortData('email')}}>User <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>                        
                        <th className="text-left" onClick={()=>{this.sortData('service')}}>Device<img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                        <th className="text-left" onClick={()=>{this.sortData('status')}}>Status <img alt="" src={require("assets/img/icon_arrows_double.svg")}/></th>
                      </tr>
                    </thead>
                    <tbody>{this.renderPayments()}</tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <ResponsiveModal open={this.state.showModal} onClose={this.onCloseShowModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
            <ChangeModal onClose={this.onCloseShowModal} onChange={this.changeStatus} user={this.state.activeUser} status={this.state.status}/>
          </ResponsiveModal>
          <ResponsiveModal open={this.state.authModal} onClose={this.onCloseAuthModal} center classNames={{'modal':'dark-modal','closeButton':'modal-close-button'}}>
            <ConfirmModal onClose={this.onCloseAuthModal} onAuthenticate={this.authenticae}/>
          </ResponsiveModal>
          <ToastContainer />
        </div>
      </>
    );
  }
}

export default UserDetail;
