const events = {}

export const subscribe = (event, callback)=> {
    events[event]=[callback]
}

export const emitter = (event, data)=> {
    if(!events[event]) events[event]=[]
    events[event].forEach(callback => callback(data))
}

export const unSubscribe = (event)=> {
    events[event]=[]
}